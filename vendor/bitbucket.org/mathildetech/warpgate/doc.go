/*
Package warpgate provides a client library to easily access alauda feature gate API.

A simple usage to query if the feature gate `a` is enabled on cluster `foo`:

	import wg "alauda.io/warpgate"
	...
	wgg := wg.NewWarpGate(wg.Config{
		AuthorizationToken: "xsop12.3xlopijkiop",
		APIEndpoint:        "http://archon"})
	enabled, _ := wgg.IsClusterFeatureGateEnabled("foo", "a")

For more client interfaces, refer to the go doc.
*/
package warpgate
