# WarpGate

Warpgate is a golang client library to access alauda feature gate API easily.

## Quick Start

```golang
import wg "alauda.io/warpgate"

func foo() {
	// Create a warpgate by the cluster authorization bearer token
	// and feature gate api endpoint.
	wwg := wg.NewWarpGate(wg.Config{
		AuthorizationToken: "xsop12.3xlopijkiop",
		APIEndpoint: "http://archon"})
	// Query if the feature gate `a` on cluster `foo` is enabled 
	enabled, _ := wwg.IsClusterFeatureGateEnabled("foo", "a")
}
```

For more useful functions, please refer to the godoc of the library.
