package warpgate

import (
	"fmt"
	"net/http"
	"time"
)

const (
	// DefaultHTTPTimeout is the default HTTP timeout of the http client used in WarpGate.
	DefaultHTTPTimeout = time.Duration(time.Second * 30)
	// DefaultAPIEndpoint is the default feature gate API endpoint.
	DefaultAPIEndpoint = "http://archon"

	// FeatureGateListURLTemplate is the API URL template to access global feature gate list.
	FeatureGateListURLTemplate = "%s/fg/v1/featuregates"
	// FeatureGateURLTemplate is the API URL template to access a global feature gate.
	FeatureGateURLTemplate = "%s/fg/v1/featuregates/%s"
	// ClusterFeatureGateListURLTemplate is the API URL template to access cluster feature gate list.
	ClusterFeatureGateListURLTemplate = "%s/fg/v1/%s/featuregates"
	// ClusterFeatureGateURLTemplate is the API URL template to access a cluster feature gate.
	ClusterFeatureGateURLTemplate = "%s/fg/v1/%s/featuregates/%s"
)

// client is a global reusable http client used in WarpGate.
var client = http.Client{
	Timeout: DefaultHTTPTimeout,
}

// Config defines the configuration of the WarpGate.
type Config struct {
	APIEndpoint        string
	AuthorizationToken string
}

// Default defaults the fields in Config if the field has zero value.
func (f *Config) Default() *Config {
	if f.APIEndpoint == "" {
		f.APIEndpoint = DefaultAPIEndpoint
	}
	return f
}

// WarpGate wraps the feature gate API as functions.
type WarpGate struct {
	Config Config
}

// NewWarpGate returns a WarpGate by Config.
func NewWarpGate(config Config) *WarpGate {
	return &WarpGate{
		Config: *config.Default(),
	}
}

// ListFeatureGates returns the all global feature gate list.
func (f *WarpGate) ListFeatureGates() (fglist *AlaudaFeatureGateList, err error) {
	url := fmt.Sprintf(FeatureGateListURLTemplate, f.Config.APIEndpoint)
	fglist = &AlaudaFeatureGateList{}
	err = QueryURLForObject(url, f.Config.AuthorizationToken, fglist)
	if err != nil {
		fglist = nil
	}
	return
}

// GetFeatureGate returns the global feature gate by the name.
func (f *WarpGate) GetFeatureGate(name string) (fg *AlaudaFeatureGate, err error) {
	url := fmt.Sprintf(FeatureGateURLTemplate, f.Config.APIEndpoint, name)
	fg = &AlaudaFeatureGate{}
	err = QueryURLForObject(url, f.Config.AuthorizationToken, fg)
	if err != nil {
		fg = nil
	}
	return
}

// IsFeatureGateEnabled returns if the specified feature gate is enabled.
func (f *WarpGate) IsFeatureGateEnabled(name string) (enabled bool, err error) {
	fg, err := f.GetFeatureGate(name)
	if err != nil {
		return
	}
	enabled = fg.Status.Enabled
	return
}

// ListClusterFeatureGates returns the feature gate list in a cluster.
func (f *WarpGate) ListClusterFeatureGates(cluster string) (fglist *AlaudaFeatureGateList, err error) {
	url := fmt.Sprintf(ClusterFeatureGateListURLTemplate, f.Config.APIEndpoint, cluster)
	fglist = &AlaudaFeatureGateList{}
	err = QueryURLForObject(url, f.Config.AuthorizationToken, fglist)
	if err != nil {
		fglist = nil
	}
	return
}

// GetClusterFeatureGate returns a feature gate in a cluster by name.
func (f *WarpGate) GetClusterFeatureGate(cluster, name string) (fg *AlaudaFeatureGate, err error) {
	url := fmt.Sprintf(ClusterFeatureGateURLTemplate, f.Config.APIEndpoint, cluster, name)
	fg = &AlaudaFeatureGate{}
	err = QueryURLForObject(url, f.Config.AuthorizationToken, fg)
	if err != nil {
		fg = nil
	}
	return
}

// IsClusterFeatureGateEnabled returns if a feature gate in a cluster is enabled.
func (f *WarpGate) IsClusterFeatureGateEnabled(cluster, name string) (enabled bool, err error) {
	fg, err := f.GetClusterFeatureGate(cluster, name)
	if err != nil {
		return
	}
	enabled = fg.Status.Enabled
	return
}
