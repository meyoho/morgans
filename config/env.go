package config

import (
	"io/ioutil"
	"strings"

	"github.com/vrischmann/envconfig"
)

var (
	GlobalConfig Config
)

type Config struct {
	Alert struct {
		LabelsPrefix string `envconfig:"default=alert_"`
		// If MessageTemplatesEnabled is true, alert will be created with some system labels
		// Also alert will have a message in annotations
		MessageTemplatesEnabled bool `envconfig:"default=false"`
		// Language of alert templates. EN, CN to choice
		MessageTemplatesLang string `envconfig:"default=CN"`
	}

	Daemon struct {
		SyncPeriod int `envconfig:"default=300"`
	}

	Davion struct {
		Endpoint   string `envconfig:"default=http://davion:80"`
		ApiVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=10"`
		Enabled    bool   `envconfig:"default=true"`
	}

	DB struct {
		Engine            string `envconfig:"default=pgsql"`
		Host              string `envconfig:"default=localhost"`
		Port              string `envconfig:"default=5432"`
		Name              string `envconfig:"default=morgansdb"`
		User              string `envconfig:"default=alauda"`
		Password          string `envconfig:"default=123456"`
		ConfigDir         string `envconfig:"default=/etc/pass_db"`
		Timeout           int    `envconfig:"default=10"`
		MaxConnection     int    `envconfig:"default=40"`
		MaxIdleConnection int    `envconfig:"default=5"`
		MaxLifetime       int    `envconfig:"default=120"`
		Wait              int    `envconfig:"default=30"`
		Params            string `envconfig:"optional"`
	}

	Erebus struct {
		Endpoint string `envconfig:"default=https://erebus"`
	}

	Archon struct {
		Endpoint string `envconfig:"default=http://archon"`
	}

	Furion struct {
		Endpoint   string `envconfig:"default=http://furion:8080"`
		ApiVersion string `envconfig:"default=v2"`
		Timeout    int    `envconfig:"default=10"`
		SyncPeriod int    `envconfig:"default=300"`
	}

	InnerJakiro struct {
		Endpoint   string `envconfig:"default=http://jakiro:8080"`
		ApiVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=10"`
	}

	Lucifer struct {
		Endpoint   string `envconfig:"default=http://lucifer:8080"`
		ApiVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=10"`
	}

	Kubernetes struct {
		Endpoint string `envconfig:"default=https://kubernetes.default.svc.cluster.local"`
		Token    string `envconfig:"default=token"`
		Timeout  int    `envconfig:"default=10"`
	}

	Log struct {
		Size     int    `envconfig:"default=100"` // unit: M
		Level    string `envconfig:"default=info"`
		Backup   int    `envconfig:"default=2"`
		ToStdout bool   `envconfig:"default=false"`
	}

	Morgans struct {
		Endpoint     string `envconfig:"default=http://localhost:8080"`
		ApiVersion   string `envconfig:"default=v1"`
		Timeout      int    `envconfig:"default=10"`
		CRDEnabled   bool   `envconfig:"default=false"`
		Debug        bool   `envconfig:"default=true"`
		ExposePort   int    `envconfig:"default=8080"`
		PodName      string `envconfig:"POD_NAME,default=morgans-5dcbff4f6f-mlhgc"`
		Namespace    string `envconfig:"NAMESPACE,default=alauda-system"`
		ScaleEnabled bool   `envconfig:"default=false"`
	}

	TimeZone struct {
		Name   string `envconfig:"default=CST"`
		Offset int    `envconfig:"default=28800"` // unit: s
	}

	Prometheus struct {
		Namespace string `envconfig:"default=alauda-system"`
		Timeout   int    `envconfig:"default=10"`
	}

	Notification struct {
		TemplateType string `envconfig:"default=alarm"`
		ChanCapacity int    `envconfig:"default=1024"`
	}

	CCP struct {
		ServerIp                 string `envconfig:"default=https://app.cloopen.com"`
		ServerPort               string `envconfig:"default=8883"`
		SoftVersion              string `envconfig:"default=2013-12-26"`
		AccountSid               string `envconfig:"default=8aaf07086b54a56a016b59b82cc703fa"`
		AccountToken             string `envconfig:"default=9401f34c96f741699dd1001a3c095c5b"`
		AppId                    string `envconfig:"default=8aaf07086b54a56a016b59b82d280401"`
		TemplateId               string `envconfig:"default=1"`
		BuildTemplateId          string `envconfig:"default=1"`
		PipelineTaskTemplateId   string `envconfig:"default=1"`
		PipelineResultTemplateId string `envconfig:"default=1"`
		LogAlarmTemplateId       string `envconfig:"default=1"`
		JenkinsTemplateId        string `envconfig:"default=1"`
		ResetPasswordTemplateId  string `envconfig:"default=1"`
		SmsServerProvider        string `envconfig:"default=provider"`
	}

	SMTP struct {
		ServerHost         string `envconfig:"default=smtp.example.com"`
		ServerPort         int    `envconfig:"default=465"`
		Username           string `envconfig:"default=user@example.com"`
		Password           string `envconfig:"default=password"`
		SSL                bool   `envconfig:"default=true"`
		InsecureSkipVerify bool   `envconfig:"default=true"`
		From               string `envconfig:"EMAIL_FROM,default=user@example.com"`
		Language           string `envconfig:"USER_DEFAULT_LANGUAGE,default=en"`
	}

	ElasticSearch struct {
		Url         string `envconfig:"default=http://cpaas-elasticsearch:9200"`
		LogPrefix   string `envconfig:"default=log"`
		EventPrefix string `envconfig:"default=event"`
		AuditPrefix string `envconfig:"default=audit"`
		Verbose     bool   `envconfig:"default=false"`
	}

	Kafka struct {
		Hosts string `envconfig:"default=localhost:9092"`
		Log   struct {
			Topic string `envconfig:"default=ALAUDA_LOG_TOPIC"`
		}
		Event struct {
			Topic  string `envconfig:"default=ALAUDA_EVENT_TOPIC"`
			Enable bool   `envconfig:"default=true"`
		}
		Audit struct {
			Topic string `envconfig:"default=ALAUDA_AUDIT_TOPIC"`
		}
		MaxRequestSize int  `envconfig:"default=62914590"`
		Auth           bool `envconfig:"default=true"`
	}

	Audit struct {
		SkipAuth    bool `envconfig:"default=false"`
		ResAggrTop  int  `envconfig:"default=100"`
		UserAggrTop int  `envconfig:"default=200"`
	}

	Label struct {
		BaseDomain string `envconfig:"default=alauda.io"`
	}
}

func init() {
	var tokenFile = "/var/run/secrets/kubernetes.io/serviceaccount/token"

	if err := envconfig.Init(&GlobalConfig); err != nil {
		panic("Load config from env error," + err.Error())
	}

	if token, err := ioutil.ReadFile(tokenFile); err == nil {
		GlobalConfig.Kubernetes.Token = string(token)
	}

	var dbPasswordFile = GlobalConfig.DB.ConfigDir + "/password"
	if password, err := ioutil.ReadFile(dbPasswordFile); err == nil {
		GlobalConfig.DB.Password = string(strings.TrimRight(string(password), "\n"))
	}

	if GlobalConfig.DB.Engine == "postgresql" {
		GlobalConfig.DB.Engine = "pgsql"
	}
}
