package common

import (
	"net/http"
	"strings"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Code string

const (
	// Component source
	Source = 1080

	// CodeUnknown means the server has declined to indicate a specific reason.
	// The details field may contain other information about this error.
	// Status code 500.
	CodeUnknown Code = "Unknown"

	// CodeUnauthorized means the server can be reached and understood the request, but requires
	// the user to present appropriate authorization credentials (identified by the WWW-Authenticate header)
	// in order for the action to be completed. If the user has specified credentials on the request, the
	// server considers them insufficient.
	// Status code 401
	CodeUnauthorized Code = "Unauthorized"

	// CodeForbidden means the server can be reached and understood the request, but refuses
	// to take any further action.  It is the result of the server being configured to deny access for some reason
	// to the requested resource by the client.
	// Status code 403
	CodeForbidden Code = "Forbidden"

	// CodeNotFound means one or more resources required for this operation
	// could not be found.
	// Status code 404
	CodeNotFound Code = "NotFound"

	// CodeAlreadyExists means the resource you are creating already exists.
	// Status code 409
	CodeAlreadyExists Code = "AlreadyExists"

	// CodeBadRequest means that the request itself was invalid, because the request
	// doesn't make any sense, for example deleting a read-only object.  This is different than
	// StatusReasonInvalid above which indicates that the API call could possibly succeed, but the
	CodeBadRequest Code = "BadRequest"

	// CodeInternalError indicates that an internal error occurred, it is unexpected
	// and the outcome of the call is unknown.
	// Status code 500
	CodeInternalError Code = "InternalError"

	// CodeMethodNotAllowed means that the action the client attempted to perform on the
	// resource was not supported by the code - for instance, attempting to delete a resource that
	// can only be created. API calls that return MethodNotAllowed can never succeed.
	// Status code 405
	CodeMethodNotAllowed Code = "MethodNotAllowed"

	// CodeServiceUnavailable indicates that a service is unavailable
	// and the outcome of the call is unknown.
	// Status code 503
	CodeServiceUnavailable Code = "ServiceUnavailable"
)

var (
	CodeToHTTPStatus = map[Code]int{
		CodeUnknown:            http.StatusInternalServerError,
		CodeUnauthorized:       http.StatusUnauthorized,
		CodeForbidden:          http.StatusForbidden,
		CodeNotFound:           http.StatusNotFound,
		CodeAlreadyExists:      http.StatusConflict,
		CodeBadRequest:         http.StatusBadRequest,
		CodeInternalError:      http.StatusInternalServerError,
		CodeMethodNotAllowed:   http.StatusMethodNotAllowed,
		CodeServiceUnavailable: http.StatusServiceUnavailable,
	}

	CodeToAlaudaCode = map[Code]string{
		CodeUnknown:          "unknown_issue",
		CodeUnauthorized:     "unauthorized",
		CodeForbidden:        "permission_denied",
		CodeNotFound:         "resource_not_exist",
		CodeAlreadyExists:    "resource_already_exist",
		CodeBadRequest:       "bad_request",
		CodeInternalError:    "server_error",
		CodeMethodNotAllowed: "method_not_allowed",
	}
)

type APIError struct {
	Source  int                 `json:"source"`
	Code    string              `json:"code"`
	Message string              `json:"message"`
	Detail  []map[string]string `json:"fields,omitempty"`
}

type APIErrors struct {
	Errors []*APIError `json:"errors"`
}

func (e APIErrors) Error() string {
	var messages []string
	for _, err := range e.Errors {
		messages = append(messages, err.Message)
	}
	return strings.Join(messages, "\n")
}

type ErrorWrapper struct {
	// Message is a human-readable explanation specific to this occurrence of the problem.
	Message string `json:"message,omitempty"`

	// Status is the HTTP status code applicable to this problem, expressed as a string value.
	Status int `json:"status,omitempty"`

	// Code is an application-specific error code, expressed as a string value.
	Code Code `json:"code,omitempty"`
}

func (w *ErrorWrapper) Error() string {
	return w.Message
}

func (w *ErrorWrapper) ToAPIErrors() *APIErrors {
	return &APIErrors{
		Errors: []*APIError{
			{
				Source:  Source,
				Code:    ToAlaudaCode(w.Code),
				Message: w.Message,
			},
		},
	}
}

func (w *ErrorWrapper) ToK8SErrors() *metav1.Status {
	statusErr := apiErrors.NewInternalError(w)
	statusErr.ErrStatus.Code = int32(w.Status)
	statusErr.ErrStatus.Reason = metav1.StatusReason(w.Code)
	statusErr.ErrStatus.Message = w.Message
	statusErr.ErrStatus.Kind = "Status"
	statusErr.ErrStatus.APIVersion = "v1"

	return &statusErr.ErrStatus
}

func ToAlaudaCode(code Code) string {
	c, ok := CodeToAlaudaCode[code]
	if !ok {
		c = "server_error"
	}
	return c
}

// CodeForError returns the reason for a wrapped error.
func CodeForError(err error) Code {
	switch t := err.(type) {
	case *ErrorWrapper:
		return t.Code
	}
	return CodeUnknown
}

func GetStatusForCode(code Code) int {
	status, ok := CodeToHTTPStatus[code]
	if !ok {
		status = http.StatusInternalServerError
	}
	return status
}

func BuildAlreadyExistsError(message string) error {
	return &ErrorWrapper{
		Message: message,
		Status:  GetStatusForCode(CodeAlreadyExists),
		Code:    CodeAlreadyExists,
	}
}

func BuildBadRequestError(message string) error {
	return &ErrorWrapper{
		Message: message,
		Status:  GetStatusForCode(CodeBadRequest),
		Code:    CodeBadRequest,
	}
}

func BuildForbiddenError(message string) error {
	return &ErrorWrapper{
		Message: message,
		Status:  GetStatusForCode(CodeForbidden),
		Code:    CodeForbidden,
	}
}

func BuildNotFoundError(message string) error {
	return &ErrorWrapper{
		Message: message,
		Status:  GetStatusForCode(CodeNotFound),
		Code:    CodeNotFound,
	}
}

func BuildMethodNotAllowedError(message string) error {
	return &ErrorWrapper{
		Message: message,
		Status:  GetStatusForCode(CodeMethodNotAllowed),
		Code:    CodeMethodNotAllowed,
	}
}
func BuildUnauthorizedError(message string) error {
	return &ErrorWrapper{
		Message: message,
		Status:  GetStatusForCode(CodeUnauthorized),
		Code:    CodeUnauthorized,
	}
}

func BuildUnknownError(message string) error {
	return &ErrorWrapper{
		Message: message,
		Status:  GetStatusForCode(CodeUnknown),
		Code:    CodeUnknown,
	}
}

func BuildServiceUnavailableError(message string) error {
	return &ErrorWrapper{
		Message: message,
		Status:  GetStatusForCode(CodeServiceUnavailable),
		Code:    CodeServiceUnavailable,
	}
}

func NewError(code Code, message string) error {
	return &ErrorWrapper{
		Message: message,
		Status:  GetStatusForCode(code),
		Code:    code,
	}
}

func AnnotateError(err error, annotation string) error {
	return &ErrorWrapper{
		Message: err.Error() + annotation,
		Status:  GetStatusForCode(CodeForError(err)),
		Code:    CodeForError(err),
	}
}

func (w *ErrorWrapper) IsAlreadyExists(err error) bool {
	return CodeForError(err) == CodeAlreadyExists
}

func (w *ErrorWrapper) IsBadRequest(err error) bool {
	return CodeForError(err) == CodeBadRequest
}

func (w *ErrorWrapper) IsForbidden(err error) bool {
	return CodeForError(err) == CodeForbidden
}

func (w *ErrorWrapper) IsNotFound(err error) bool {
	return CodeForError(err) == CodeNotFound
}

func (w *ErrorWrapper) IsUnauthorized(err error) bool {
	return CodeForError(err) == CodeUnauthorized
}

func (w *ErrorWrapper) IsUnknown(err error) bool {
	return CodeForError(err) == CodeUnknown
}
