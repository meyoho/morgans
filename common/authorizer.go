package common

import (
	"encoding/json"
	"time"

	authorizationapi "k8s.io/api/authorization/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"

	"morgans/config"
)

func CanI(token, verb, resource, group, namespace string) (bool, error) {
	cfg, err := rest.InClusterConfig()
	if err != nil {
		return false, err
	}

	accessObj := &authorizationapi.SelfSubjectAccessReview{
		Spec: authorizationapi.SelfSubjectAccessReviewSpec{
			ResourceAttributes: &authorizationapi.ResourceAttributes{
				Verb:      verb,
				Resource:  resource,
				Group:     group,
				Namespace: namespace,
			},
		},
	}

	cfg.Timeout = time.Duration(config.GlobalConfig.Kubernetes.Timeout) * time.Second
	cfg.NegotiatedSerializer = serializer.DirectCodecFactory{CodecFactory: scheme.Codecs}
	gv, _ := schema.ParseGroupVersion("authorization.k8s.io/v1")
	cfg.GroupVersion = &gv
	switch gv.Group {
	case "":
		cfg.APIPath = "/api"
	default:
		cfg.APIPath = "/apis"
	}

	cfg.BearerToken = token
	client, err := rest.RESTClientFor(cfg)
	if err != nil {
		return false, err
	}

	body, _ := json.Marshal(accessObj)
	result := client.Post().Resource("selfsubjectaccessreviews").Body(body).Do()
	data, err := result.Raw()
	if err != nil {
		return false, err
	}

	if err := json.Unmarshal(data, accessObj); err != nil {
		return false, err
	}

	return accessObj.Status.Allowed, nil
}
