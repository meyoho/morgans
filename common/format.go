package common

import (
	"bytes"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"
)

const (
	defaultTimestampFormat = "2006-01-02T15:04:05.00"
	defaultLogFormat       = "[%level%] - %time% - %msg%"
)

type TextFormatter struct {
	LogFormat       string
	TimestampFormat string
	QuoteCharacter  string
	sync.Once
}

func (f *TextFormatter) init(entry *logrus.Entry) {
	if len(f.QuoteCharacter) == 0 {
		f.QuoteCharacter = "\""
	}
}

func (f *TextFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	f.Do(func() {
		f.init(entry)
	})

	timestampFormat := f.TimestampFormat
	if timestampFormat == "" {
		timestampFormat = defaultTimestampFormat
	}
	output := f.LogFormat
	if output == "" {
		output = defaultLogFormat
	}
	timestamp := entry.Time.Format(timestampFormat)

	willReplacer := f.output(entry, timestamp)
	r := strings.NewReplacer(willReplacer...)
	output = r.Replace(output)
	var b bytes.Buffer
	b.WriteString(output)
	b.WriteByte('\n')
	return b.Bytes(), nil
}

func (f *TextFormatter) output(entry *logrus.Entry, timestamp string) []string {
	level := entry.Level.String()
	if level == "info" {
		level = "infof"
	}
	msg := entry.Message
	traceId := ""
	willReplacer := []string{"%level%", level, "%trace_id%", traceId, "%msg%", msg, "%time%", timestamp}
	for key, value := range entry.Data {
		if s, ok := value.(string); ok {
			willReplacer = append(willReplacer, "%"+key+"%", s)
		}
	}
	return willReplacer
}
