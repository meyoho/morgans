package common

import (
	"io"
	"os"
	"sort"
	"strconv"
	"strings"

	"morgans/config"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
)

const (
	RequestIDHeader = "Alauda-Request-Id"
)

var (
	DefaultWriter      io.Writer
	DefaultLogger      *logrus.Logger
	CacheLogger        *logrus.Logger
	DaemonLogger       *logrus.Logger
	NotificationLogger *logrus.Logger
	MeterLogger        *logrus.Logger
	LogLevel           int
	CustomLogSize      int
	DefaultLogSize     int
)

func initLogger() {
	LogLevel = int(getLogLevel())

	if config.GlobalConfig.Log.Size < 100 {
		CustomLogSize = 5
		DefaultLogSize = config.GlobalConfig.Log.Size
	} else {
		CustomLogSize = config.GlobalConfig.Log.Size * 1 / 10
		DefaultLogSize = config.GlobalConfig.Log.Size * 8 / 10
	}

	// Init default logger
	DefaultLogger = newLogger("morgans", DefaultLogSize)

	// Init cache logger
	CacheLogger = newLogger("cache", CustomLogSize)

	// Init daemon logger
	DaemonLogger = newLogger("daemon", CustomLogSize)

	// Init notification logger
	NotificationLogger = newLogger("notification", CustomLogSize)

	// Init notification logger
	MeterLogger = newLogger("meter", CustomLogSize)

	// Init default writer which panic logs will be written to
	DefaultWriter = os.Stdout
}

func newLogger(name string, size int) *logrus.Logger {
	logger := logrus.New()
	logger.Formatter = &TextFormatter{}
	logger.SetLevel(logrus.Level(LogLevel))
	if config.GlobalConfig.Log.ToStdout {
		logger.SetOutput(os.Stdout)
	} else {
		logger.SetOutput(&lumberjack.Logger{
			Filename:   "/var/log/mathilde/" + name + ".log",
			MaxSize:    size, // megabytes
			MaxBackups: config.GlobalConfig.Log.Backup,
			MaxAge:     28,    //days
			Compress:   false, // disabled by default
		})
	}
	return logger
}

func getLogLevel() logrus.Level {
	level, err := strconv.Atoi(config.GlobalConfig.Log.Level)
	if err == nil {
		return logrus.Level(level)
	}

	switch strings.ToLower(config.GlobalConfig.Log.Level) {
	case "panic":
		return logrus.PanicLevel
	case "fatal":
		return logrus.FatalLevel
	case "error":
		return logrus.ErrorLevel
	case "warn":
		return logrus.WarnLevel
	case "warning":
		return logrus.WarnLevel
	case "info":
		return logrus.InfoLevel
	case "debug":
		return logrus.DebugLevel
	default:
		if config.GlobalConfig.Morgans.Debug {
			return logrus.DebugLevel
		} else {
			return logrus.InfoLevel
		}
	}
	logrus.New()
	return logrus.InfoLevel
}

type Logger struct {
	fields map[string]string
	keys   []string
	logger *logrus.Logger
}

func (l *Logger) prefix() string {
	if len(l.fields) == 0 {
		return ""
	}
	prefix := "["
	for _, key := range l.keys {
		prefix += key + ":" + l.fields[key] + ", "
	}
	slice := []byte(prefix)
	slice = slice[:len(slice)-2]
	prefix = string(slice)
	return prefix + "]"
}

// Panicf logs to the PANIC log.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Panicf(format string, args ...interface{}) {
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	l.logger.Panicf(format, args...)
}

// Infof logs to the INFO log.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Infof(format string, args ...interface{}) {
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	l.logger.Infof(format, args...)
}

// Info logs to the INFO log.
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func (l *Logger) Info(args ...interface{}) {
	if l.prefix() != "" {
		args = append([]interface{}{l.prefix()}, args)
	}
	l.logger.Info(args...)
}

// Errorf logs to the ERROR, WARNING, and INFO logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Errorf(format string, args ...interface{}) {
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	l.logger.Errorf(format, args...)
}

// Error logs to the ERROR, WARNING, and INFO logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Error(format string, args ...interface{}) {
	if l.prefix() != "" {
		args = append([]interface{}{l.prefix()}, args)
	}
	l.logger.Error(args...)
}

// Debug logs to the DEBUG log.
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func (l *Logger) Debug(args ...interface{}) {
	if l.prefix() != "" {
		args = append([]interface{}{l.prefix()}, args)
	}
	l.logger.Debug(args...)
}

// Debugf logs to the Debug logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Debugf(format string, args ...interface{}) {
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	l.logger.Debugf(format, args...)
}

// Debugfl logs to the Debug logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Debugfl(level int, format string, args ...interface{}) {
	if LogLevel < level+5 {
		return
	}
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	l.logger.Debugf(format, args...)
}

func (l *Logger) GetFields() map[string]string {
	fields := map[string]string{}
	for k, v := range l.fields {
		fields[k] = v
	}
	return fields
}

func NewLogger(fields map[string]string) *Logger {
	var keys []string
	for k := range fields {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return &Logger{
		fields: fields,
		keys:   keys,
		logger: DefaultLogger,
	}
}

func NewCacheLogger(fields map[string]string) *Logger {
	var keys []string
	for k := range fields {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return &Logger{
		fields: fields,
		keys:   keys,
		logger: CacheLogger,
	}
}

func NewDaemonLogger(fields map[string]string) *Logger {
	var keys []string
	for k := range fields {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return &Logger{
		fields: fields,
		keys:   keys,
		logger: DaemonLogger,
	}
}

// NewNoticationLogger returns a Logger for notification
func NewNoticationLogger(fields map[string]string) *Logger {
	var keys []string
	for k := range fields {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return &Logger{
		fields: fields,
		keys:   keys,
		logger: NotificationLogger,
	}
}

// NewMeterLogger returns a Logger for notification
func NewMeterLogger(fields map[string]string) *Logger {
	var keys []string
	for k := range fields {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return &Logger{
		fields: fields,
		keys:   keys,
		logger: MeterLogger,
	}
}

func GetGinLogger(c *gin.Context) *Logger {
	return NewLogger(map[string]string{"request_id": c.GetHeader(RequestIDHeader)})
}
