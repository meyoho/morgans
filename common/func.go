package common

import (
	"crypto/md5"
	"fmt"
	"io/ioutil"
	"math/rand"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"

	"morgans/config"

	"github.com/Jeffail/gabs"
	"github.com/goware/urlx"
)

func GetKubernetesTypeFromKind(kind string) string {
	switch strings.ToLower(kind) {
	case "networkpolicy":
		return "networkpolicies"
	case "ingress":
		return "ingresses"
	case "storageclass":
		return "storageclasses"
	case "prometheus":
		return "prometheuses"
	case "endpoints":
		return "endpoints"
	default:
		return strings.ToLower(kind) + "s"
	}
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func MergeMaps(a, b map[string]string) map[string]string {
	if a == nil {
		a = map[string]string{}
	}
	if b == nil {
		b = map[string]string{}
	}
	for k, v := range a {
		b[k] = v
	}
	return b
}

func MD5(base string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(base)))
}

func Compare(left float64, comparison string, right float64) bool {

	switch comparison {
	case "==":
		return left == right
	case ">":
		return left > right
	case ">=":
		return left >= right
	case "<":
		return left < right
	case "<=":
		return left <= right
	case "!=":
		return left != right
	default:
		panic(fmt.Sprintf("unknown comparison %s", comparison))
	}
}

func UCFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToUpper(v)) + str[i+1:]
	}
	return ""
}

func ParseFlag(flag string) bool {
	flagBoolean, err := strconv.ParseBool(flag)
	if err != nil {
		flagBoolean = false
	}
	return flagBoolean
}

func TransformString(str string) string {
	rg, err := regexp.Compile("[^a-zA-Z0-9]+")
	if err != nil {
		panic(fmt.Sprintf("can not compile the regular expression."))
	}
	strCompiled := rg.ReplaceAllString(strings.ToLower(str), "-")
	return strings.Trim(strCompiled, "-")
}

// Parse a url string, return the scheme and host
func GetSchemeHostFromUrl(urlString string) (string, string) {
	u, err := urlx.Parse(urlString)
	if err != nil {
		panic(err)
	}
	return u.Scheme, u.Host
}

func ReadFileToString(file string) string {
	fileBytes, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}
	return string(fileBytes)
}

func FixTime(currentTime time.Time) string {
	loc := time.FixedZone(config.GlobalConfig.TimeZone.Name, config.GlobalConfig.TimeZone.Offset)
	return currentTime.In(loc).Format(TimeLayout)
}

func RandStringRunes(n int) string {
	var letterRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyz")
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func GetStringByPath(source []byte, path string) string {
	gabsContainer, err := gabs.ParseJSON(source)
	if err != nil {
		return ""
	}

	value, ok := gabsContainer.Path(path).Data().(string)
	if !ok {
		return ""
	}
	return value
}

func GetFloatByPath(source []byte, path string) float64 {
	gabsContainer, err := gabs.ParseJSON(source)
	if err != nil {
		return 0.0
	}

	value, ok := gabsContainer.Path(path).Data().(float64)
	if !ok {
		return 0.0
	}
	return value
}
