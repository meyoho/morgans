package common

const (
	// HTTP methods
	HttpGet    = "GET"
	HttpPost   = "POST"
	HttpPut    = "PUT"
	HttpDelete = "DELETE"
	HttpPatch  = "PATCH"

	// Golang time layout
	TimeLayout = "2006-01-02 15:04:05 MST"
)

const (
	// Kubernetes api versions
	KubernetesAPIVersionV1 = "v1"
)
