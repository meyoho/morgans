package log

import (
	"context"
	"encoding/json"
	"errors"
	"math"
	"sync"
	"time"

	elasticV6 "gopkg.in/olivere/elastic.v6"

	"morgans/common"
	"morgans/infra/elasticsearch"
	"morgans/model"
)

type Controller struct {
	Request *model.LogRequest
	Logger  *common.Logger
}

func (s *Controller) Search() (*model.LogResourceList, error) {
	var response *model.LogResourceList

	client, err := elasticsearch.NewClient(s.Logger)
	if err != nil {
		s.Logger.Errorf("Get es client error, %s", err.Error())
		return nil, err
	}

	indexNames := client.GetIndexNames(model.LogIndexPrefix, s.Request.StartTime, s.Request.EndTime)
	s.Logger.Infof("Get logRequest for search, %+v", s.Request)
	s.Logger.Infof("Get es client with index, %+v", indexNames)
	searchResult := &elasticV6.SearchResult{}
	if len(indexNames) > 0 {

		searchResult, err = client.Query(context.Background(), s.Request.BuildSearchSource(), indexNames)

		if err != nil {
			s.Logger.Errorf("errors when query es: %+v", err)
			return nil, err
		}
	}

	response, err = model.SearchResultToLogResources(searchResult)
	if err != nil {
		s.Logger.Errorf("Failed to parse es search result to LogResources: %+v, ", err)
		return nil, err
	}

	response.TotalPage = int64(math.Min(math.Ceil(float64(response.TotalItems)/float64(s.Request.Size)), 200))

	return response, nil
}

func (s *Controller) Aggregation() (*model.LogAggregationsList, error) {
	var response *model.LogAggregationsList

	client, err := elasticsearch.NewClient(s.Logger)
	if err != nil {
		s.Logger.Errorf("Get es client error, %s", err.Error())
		return nil, err
	}

	indexNames := client.GetIndexNames(model.LogIndexPrefix, s.Request.StartTime, s.Request.EndTime)
	s.Logger.Infof("Get logRequest for aggregations, %+v", s.Request)
	s.Logger.Infof("Get es client with index, %+v", indexNames)
	searchResult := &elasticV6.SearchResult{}
	if len(indexNames) > 0 {
		searchResult, err = client.Query(context.Background(), s.Request.BuildAggregationsSource(), indexNames)

		if err != nil {
			s.Logger.Errorf("errors when query es: %+v", err)
			return nil, err
		}
	}

	response, err = model.SearchResultToLogAggregations(searchResult)
	if err != nil {
		s.Logger.Errorf("Failed to parse es search result, ", err.Error())
		return nil, err
	}

	return response, nil
}

func (s *Controller) Types() (*model.LogTypeAggregations, error) {
	var response = &model.LogTypeAggregations{}

	now := time.Now()
	s.Request.EndTime = float64(now.UnixNano()) / 1000000000
	s.Request.StartTime = float64(now.AddDate(0, 0, -1).UnixNano()) / 1000000000

	client, err := elasticsearch.NewClient(s.Logger)
	if err != nil {
		s.Logger.Errorf("Get es client error, %s", err.Error())
		return nil, err
	}

	var wg sync.WaitGroup
	var clusters, nodes, components, apps, paths, sources []string

	if s.Request.ProjectNames == "" || s.Request.K8sNamespaces == "" {
		wg.Add(3)
		go func() {
			clusters = s.FieldAggregations(client, s.Request, "region_name.keyword", &wg)
		}()
		go func() {
			nodes = s.FieldAggregations(client, s.Request, "node.keyword", &wg)
		}()
		go func() {
			components = s.FieldAggregations(client, s.Request, "component.keyword", &wg)
		}()
	}

	wg.Add(3)
	go func() {
		apps = s.FieldAggregations(client, s.Request, "application_name.keyword", &wg)
	}()
	go func() {
		paths = s.FieldAggregations(client, s.Request, "file_name.keyword", &wg)
	}()
	go func() {
		sources = s.FieldAggregations(client, s.Request, "source.keyword", &wg)
	}()

	wg.Wait()
	if s.Request.ProjectNames == "" || s.Request.K8sNamespaces == "" {
		response.Items = append(response.Items, map[string][]string{"clusters": clusters})
		response.Items = append(response.Items, map[string][]string{"nodes": nodes})
		response.Items = append(response.Items, map[string][]string{"components": components})
	}
	response.Items = append(response.Items, map[string][]string{"applications": apps})
	response.Items = append(response.Items, map[string][]string{"paths": paths})
	response.Items = append(response.Items, map[string][]string{"sources": sources})

	return response, nil
}

func (s *Controller) Context() (*model.LogResourceList, error) {
	var response = &model.LogResourceList{TotalPage: 1}

	client, err := elasticsearch.NewClient(s.Logger)
	if err != nil {
		s.Logger.Errorf("Get es client error, %s", err.Error())
		return nil, err
	}

	if s.Request.LogId == "" || s.Request.LogType == "" || s.Request.LogIndex == "" {
		return nil, errors.New("log_id, log_type and log_index all required")
	}

	hit, err := client.RetrieveRecord(s.Request.LogIndex, s.Request.LogType, s.Request.LogId, 3)
	if err != nil {
		return nil, err
	}
	logResource, err := model.SearchHitToLogResource(hit)
	if err != nil {
		return nil, err
	}

	s.Request.UpdateByLogResource(logResource)

	if s.Request.Direction == "later" || s.Request.Direction == "" {
		s.Request.StartTime = logResource.Spec.Data.CreatedAt
		s.Request.EndTime = s.Request.StartTime + model.ONE_DAY_SECONDS
		laterItems, err := s.ContextLogResources(client, s.Request, hit.Sort, true)
		if err != nil {
			s.Logger.Infof("Failed to query the context later logs, %+v", err)
		}
		response.Items = append(response.Items, laterItems...)
	}

	if s.Request.Direction == "" {
		response.Items = append(response.Items, logResource)
	}

	if s.Request.Direction == "before" || s.Request.Direction == "" {
		s.Request.EndTime = logResource.Spec.Data.CreatedAt
		s.Request.StartTime = s.Request.EndTime - model.ONE_DAY_SECONDS
		beforeItems, err := s.ContextLogResources(client, s.Request, hit.Sort, false)
		if err != nil {
			s.Logger.Infof("Failed to query the context before logs, %+v", err)
		}
		response.Items = append(response.Items, beforeItems...)
	}

	response.TotalItems = int64(len(response.Items))

	return response, nil
}

// FieldAggregations query es for specified fieldName and return value list
func (s *Controller) FieldAggregations(client *elasticsearch.Client, req *model.LogRequest, fieldName string, wg *sync.WaitGroup) (result []string) {
	defer wg.Done()
	indexNames := client.GetIndexNames(model.LogIndexPrefix, s.Request.StartTime, s.Request.EndTime)
	if len(indexNames) == 0 {
		return
	}
	s.Logger.Infof("Get logRequest for types aggregations, %+v", s.Request)
	s.Logger.Infof("Get es client with index, %+v", indexNames)
	searchResult, err := client.Query(context.Background(), s.Request.BuildFieldAggregationsSource(fieldName), indexNames)

	if err != nil {
		s.Logger.Errorf("Failed to query es for types with fieldName: %v", fieldName)
		return
	}

	rawJson, ok := searchResult.Aggregations[model.AggsFieldName]
	if !ok {
		s.Logger.Error("Es aggregations result does not contains request aggregation")
		return
	}

	var buckets elasticV6.AggregationBucketKeyItems
	err = json.Unmarshal(*rawJson, &buckets)
	if err != nil {
		s.Logger.Errorf("Failed to parse the es result of types with filedName: %v", fieldName)
		return
	}

	for _, bucket := range buckets.Buckets {
		keyStr, _ := bucket.Key.(string)
		if len(keyStr) > 0 {
			result = append(result, keyStr)
		}
	}

	return
}

// ContextLogResources load the log record before/after specified log record(defined by the sortValues)
func (s *Controller) ContextLogResources(client *elasticsearch.Client, req *model.LogRequest, sortValues []interface{}, ascending bool) (items []*model.LogResource, err error) {
	indexNames := client.GetIndexNames(model.LogIndexPrefix, s.Request.StartTime, s.Request.EndTime)
	if len(indexNames) == 0 {
		return
	}
	s.Logger.Infof("Get logRequest, %+v", s.Request)
	s.Logger.Infof("Get es client with index, %+v", indexNames)
	searchResult, err := client.Query(context.Background(), s.Request.BuildContextSource(sortValues, ascending), indexNames)

	if err != nil {
		s.Logger.Errorf("Errors when query es for context: %v", err)
		return
	}

	logList, err := model.SearchResultToLogResources(searchResult)
	if err != nil {
		s.Logger.Errorf("Failed to parse es search result when query context, %+v", err)
		return
	}
	if ascending {
		for i := len(logList.Items)/2 - 1; i >= 0; i-- {
			opp := len(logList.Items) - 1 - i
			logList.Items[i], logList.Items[opp] = logList.Items[opp], logList.Items[i]
		}
	}

	return logList.Items, nil
}
