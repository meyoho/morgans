package callback

import (
	"errors"

	"morgans/common"
	"morgans/config"
	"morgans/infra/elasticsearch"
	"morgans/infra/kafka"
)

type Controller struct {
	Records []map[string]interface{}
	Topic   string
	Logger  *common.Logger
}

func (s *Controller) SaveToStorage(usingKafka bool) (success int, err error) {
	if usingKafka {
		return s.SaveToKafka()
	} else {
		return s.SaveToEs()
	}
}

// SaveToEs only handle save events to es, will not handle logs
func (s *Controller) SaveToEs() (success int, err error) {
	// TODO(ylzhang): es backend support
	_, err = elasticsearch.NewClient(s.Logger)
	if err != nil {
		return 0, err
	}

	return
}

// SaveToKafka handle not only logs but also events
func (s *Controller) SaveToKafka() (success int, err error) {

	if s.Topic == config.GlobalConfig.Kafka.Log.Topic {
		return s.SaveLogsToKafka()
	} else if s.Topic == config.GlobalConfig.Kafka.Audit.Topic {
		return s.SaveAuditsToKafka()
	}
	return s.SaveK8SEventsToKafka()
}

func (s *Controller) SaveLogsToKafka() (success int, err error) {
	defer func() {
		s.Logger.Infof("Successfully send %d logs to kafka with topic: %s", success, s.Topic)
	}()
	client, err := kafka.NewClient(s.Logger, s.Topic)
	if err != nil {
		s.Logger.Errorf("Get kafka client error, %s", err.Error())
		return
	}
	s.Logger.Infof("%d logs tobe sent to kafka", len(s.Records))
	for _, record := range s.Records {
		for key, data := range record {
			value := data.([]byte)
			err = client.SendMsg([]byte(key), value)
			if err != nil {
				s.Logger.Errorf("Post to storage, key: %s, err: %s", key, err.Error())
				return
			} else {
				success++
			}
		}
	}
	return
}

func (s *Controller) SaveK8SEventsToKafka() (success int, err error) {
	defer func() {
		s.Logger.Infof("Successfully send %d k8s events to kafka with topic: %s", success, s.Topic)
	}()
	client, err := kafka.NewClient(s.Logger, s.Topic)
	if err != nil {
		s.Logger.Errorf("Get kafka client error, %s", err.Error())
		return
	}

	s.Logger.Infof("%d events tobe sent to kafka", len(s.Records))
	for _, record := range s.Records {
		for key, data := range record {
			event, ok := data.([]byte)
			if !ok {
				err := errors.New("event data's type is not []byte")
				s.Logger.Errorf("Parse event data failed, err: %s", err.Error())
				return success, err
			}
			err = client.SendMsg([]byte(key), event)
			if err != nil {
				s.Logger.Errorf("Post event to kafka failed, key: %s, err: %s", key, err.Error())
				return success, err
			} else {
				success++
			}
		}
	}
	return success, nil
}

func (s *Controller) SaveAuditsToKafka() (success int, err error) {
	defer func() {
		s.Logger.Infof("Successfully send %d audits to kafka with topic: %s", success, s.Topic)
	}()
	client, err := kafka.NewClient(s.Logger, s.Topic)
	if err != nil {
		s.Logger.Errorf("Get kafka client error, %s", err.Error())
		return
	}
	s.Logger.Infof("%d audits tobe sent to kafka", len(s.Records))
	for _, record := range s.Records {
		for key, data := range record {
			value := data.([]byte)
			err = client.SendMsg([]byte(key), value)
			if err != nil {
				s.Logger.Errorf("Post to storage, key: %s, err: %s", key, err.Error())
				return
			} else {
				success++
			}
		}
	}
	return
}
