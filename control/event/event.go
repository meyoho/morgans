package event

import (
	"context"
	"math"

	elasticV6 "gopkg.in/olivere/elastic.v6"

	"morgans/common"
	"morgans/infra/elasticsearch"
	"morgans/model"
)

type Controller struct {
	Request *model.EventRequest
	Logger  *common.Logger
}

func (s *Controller) ListEvents() (*model.EventResourceList, error) {
	var response *model.EventResourceList

	client, err := elasticsearch.NewClient(s.Logger)
	if err != nil {
		s.Logger.Errorf("Get es client error when request events, %s", err.Error())
		return nil, err
	}

	indexNames := client.GetIndexNames(model.EventIndexPrefix, s.Request.StartTime, s.Request.EndTime)
	s.Logger.Debugf("Get eventRequest, %+v", s.Request)
	s.Logger.Infof("Get es client with index, %+v", indexNames)
	searchResult := &elasticV6.SearchResult{}
	if len(indexNames) > 0 {
		searchResult, err = client.Query(context.Background(), s.Request.BuildSearchSource(), indexNames)

		if err != nil {
			s.Logger.Infof("errors when query es: %+v", err)
			return nil, err
		}
	}

	response, err = model.SearchResultToEventResources(searchResult)
	if err != nil {
		s.Logger.Infof("Failed to parse es search result, %+v", err.Error())
		return nil, err
	}

	response.TotalPage = int64(math.Min(math.Ceil(float64(response.TotalItems)/float64(s.Request.Size)), 200))

	return response, nil
}
