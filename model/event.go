package model

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/twinj/uuid"
	elasticV6 "gopkg.in/olivere/elastic.v6"

	"morgans/config"
)

const (
	DefaultEventSource   = "kubernetes"
	DefaultEventPageSize = 30
)

var EventIndexPrefix string

func init() {
	EventIndexPrefix = config.GlobalConfig.ElasticSearch.EventPrefix
}

type EventRequest struct {
	StartTime     float64
	EndTime       float64
	Cluster       string
	K8sNamespaces string
	K8sKinds      string
	K8sNames      string
	Filters       string
	Source        string
	PartialFields []string
	Fields        []string
	Fuzzy         bool

	Size int
	Page int
}

func (req *EventRequest) getMatchClause() (queries []elasticV6.Query) {
	// match clause
	if len(req.Source) > 0 {
		detailSourceQuery := elasticV6.NewMatchQuery("detail.source", req.Source).Operator("AND")
		queries = append(queries, detailSourceQuery)
	}
	if len(req.Cluster) > 0 {
		var query elasticV6.Query
		if IsValidUUID(req.Cluster) {
			query = elasticV6.NewMatchQuery("detail.cluster_uuid", req.Cluster).Operator("AND")
		} else {
			query = elasticV6.NewMatchQuery("detail.cluster_name", req.Cluster).Operator("AND")
		}
		queries = append(queries, query)
	}

	return
}

func (req *EventRequest) getTermsClause() (queries []elasticV6.Query) {
	if len(req.K8sKinds) > 0 {
		kindTermsQuery := elasticV6.NewTermsQuery("detail.event.involvedObject.kind", getTerms(req.K8sKinds)...)
		queries = append(queries, kindTermsQuery)
	}
	if len(req.K8sNamespaces) > 0 {
		namespaceTermsQuery := elasticV6.NewTermsQuery("detail.event.involvedObject.namespace", getTerms(req.K8sNamespaces)...)
		queries = append(queries, namespaceTermsQuery)
	}

	if len(req.K8sNames) > 0 && !req.Fuzzy {
		k8sNameTermsQuery := elasticV6.NewTermsQuery("detail.event.involvedObject.name", getTerms(req.K8sNames)...)
		queries = append(queries, k8sNameTermsQuery)
	}

	if req.StartTime > 0 && req.EndTime > 0 && req.StartTime <= req.EndTime {
		rangeQuery := elasticV6.NewRangeQuery("detail.event.lastTimestamp").Gte(req.StartTime * 1000).Lt(req.EndTime * 1000).Format("epoch_millis")
		queries = append(queries, rangeQuery)
	}

	return queries
}

func (req *EventRequest) getRegexClause() (queries []elasticV6.Query) {
	if len(req.K8sNames) > 0 && req.Fuzzy {
		for _, name := range strings.Split(req.K8sNames, ",") {
			// use case: name is morgans, result contains: morgans, morgans-123456, morgans-123456-xxxx
			k8sNameTermsQuery := elasticV6.NewRegexpQuery("detail.event.involvedObject.name", fmt.Sprintf("(%s)|(%s-.*)", name, name))
			queries = append(queries, k8sNameTermsQuery)
		}
	}

	return
}

func (req *EventRequest) getSourceContext() *elasticV6.FetchSourceContext {
	if len(req.PartialFields) > 0 || len(req.Fields) > 0 {
		allFields := []string{}
		allFields = append(allFields, req.PartialFields...)
		allFields = append(allFields, req.Fields...)
		return elasticV6.NewFetchSourceContext(true).Include(allFields...)
	}
	return nil
}

func (req *EventRequest) getSorter() elasticV6.Sorter {
	var sorter elasticV6.Sorter
	if req.Source != "" {
		sorter = elasticV6.NewFieldSort("detail.event.lastTimestamp").Desc() // .UnmappedType("string")
	} else {
		sorter = elasticV6.NewFieldSort("time").Desc()
	}

	return sorter
}

func (req *EventRequest) BuildSearchSource() *elasticV6.SearchSource {
	searchSource := elasticV6.NewSearchSource()

	boolQuery := elasticV6.NewBoolQuery()
	boolQuery.Filter(req.getMatchClause()...)
	boolQuery.Filter(req.getTermsClause()...)

	regexQueries := req.getRegexClause()
	if len(regexQueries) > 0 {
		boolQuery.Should(regexQueries...)
		boolQuery.MinimumNumberShouldMatch(1)
	}

	sourceContext := req.getSourceContext()
	if sourceContext != nil {
		searchSource.FetchSourceContext(sourceContext)
	}

	var from = (req.Page - 1) * req.Size
	if req.Page < 1 {
		from = 0
	}

	searchSource.From(from).
		Query(boolQuery).
		Size(req.Size).
		SortBy(req.getSorter())

	return searchSource
}

// IsValidUUID Validate UUID Function. Returns true if valid
func IsValidUUID(uuidValue string) bool {
	if _, err := uuid.Parse(uuidValue); err != nil {
		return false
	}
	return true
}

type EventResource struct {
	// ResourceMeta `json:",inline"`
	Spec EventSpec `json:"spec,omitempty"`
}

type EventSpec map[string]interface{}

func SearchHitToEventResource(hit *elasticV6.SearchHit) (*EventResource, error) {
	r := &EventResource{}
	var event EventSpec
	err := json.Unmarshal(*hit.Source, &event)
	if err != nil {
		return nil, err
	}

	r.Spec = event

	return r, nil
}

type EventResourceList struct {
	// ResourceMeta `json:",inline"`
	Items      []*EventResource `json:"items,omitempty"`
	TotalItems int64            `json:"total_items"`
	TotalPage  int64            `json:"total_page"`
}

func SearchResultToEventResources(searchResult *elasticV6.SearchResult) (*EventResourceList, error) {
	r := &EventResourceList{TotalItems: 0}
	if searchResult == nil || searchResult.Hits == nil {
		return r, nil
	}

	r.TotalItems = searchResult.Hits.TotalHits

	for _, hit := range searchResult.Hits.Hits {
		eventResource, err := SearchHitToEventResource(hit)
		if err != nil {
			return nil, err
		}
		r.Items = append(r.Items, eventResource)
	}

	return r, nil
}
