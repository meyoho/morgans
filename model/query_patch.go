package model

var (
	IndicatorMapForOCP43 = map[string]*IndicatorItem{}
	IndicatorSetForOCP43 = []*IndicatorItem{
		// Workload indicators query expressions, all the indicators can be divided into three categories:
		// * Workload resource indicators
		// * Pod resource indicators
		// * Container resource indicators
		{
			Name: WorkloadCPUUtilization,
			Query: `{{if .Function }}
				        sum by (deployment_name) ({{.Function}}_over_time(container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}[{{.Range}}s])) / sum by (deployment_name) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}) * 100000
				    {{else}}
				        sum by (deployment_name) (container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}) / sum by (deployment_name) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}) * 100000
				    {{end}}`,
		},
		{
			Name: WorkloadMemoryUtilization,
			Query: `{{if .Function }}
				        sum by (deployment_name) ({{.Function}}_over_time(container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}[{{.Range}}s])) / sum by (deployment_name) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"})
				    {{else}}
				        sum by (deployment_name) (container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}) / sum by (deployment_name) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"})
				    {{end}}`,
		},
		{
			Name: WorkloadNetworkReceiveBytes,
			Query: `{{if .Function }}
				        sum by (deployment_name) ({{.Function}}_over_time(container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
                        sum by (deployment_name) (container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"})
				    {{end}}`,
		},
		{
			Name: WorkloadNetworkTransmitBytes,
			Query: `{{if .Function }}
				        sum by (deployment_name) ({{.Function}}_over_time(container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
                        sum by (deployment_name) (container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod=~"{{.PodNamePattern}}"})
				    {{end}}`,
		},
		{
			Name: PodCPUUtilization,
			Query: `{{if .Function }}
				        sum by (pod) ({{.Function}}_over_time(container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}[{{.Range}}s])) / sum by (pod) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}) * 100000
				    {{else}}
				        sum by (pod) (container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}) / sum by (pod) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}) * 100000
				    {{end}}`,
		},
		{
			Name: PodMemoryUtilization,
			Query: `{{if .Function }}
				        sum by(pod) ({{.Function}}_over_time(container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}[{{.Range}}s])) / sum by (pod) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"})
				    {{else}}
				        sum by(pod) (container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}) / sum by (pod) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"})
				    {{end}}`,
		},
		{
			Name: PodNetworkReceiveBytes,
			Query: `{{if .Function }}
				        sum by (pod) ({{.Function}}_over_time(container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
				        sum by (pod) (container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"})
				    {{end}}`,
		},
		{
			Name: PodNetworkTransmitBytes,
			Query: `{{if .Function }}
				        sum by (pod) ({{.Function}}_over_time(container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
				        sum by (pod) (container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod=~"{{.PodNamePattern}}"})
				    {{end}}`,
		},
		{
			Name: ContainerCPUUtilization,
			Query: `{{if .Function }}
				        sum by (pod, container) ({{.Function}}_over_time(container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}[{{.Range}}s])) / sum by (pod, container) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}) * 100000
				    {{else}}
				        sum by (pod, container) (container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}) / sum by (pod, container) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"}) * 100000
				    {{end}}`,
		},
		{
			Name: ContainerMemoryUtilization,
			Query: `{{if .Function }}
				        sum by (pod, container) ({{.Function}}_over_time(container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}[{{.Range}}s])) / sum by (pod, container) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"})
				    {{else}}
				        sum by (pod, container) (container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}) / sum by (pod, container) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",image!="",container!="POD"})
				    {{end}}`,
		},
		{
			Name: ContainerNetworkReceiveBytes,
			Query: `{{if .Function }}
				        sum by (pod, container) ({{.Function}}_over_time(container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
				        sum by (pod, container) (container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"})
				    {{end}}`,
		},
		{
			Name: ContainerNetworkTransmitBytes,
			Query: `{{if .Function }}
				        sum by (pod, container) ({{.Function}}_over_time(container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
				        sum by (pod, container) (container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod=~"{{.PodNamePattern}}"})
				    {{end}}`,
		},


		// Node indicators query expressions, all the indicators can be divided into three categories:
		// * Node resource indicators
		// * Node disk indicators
		// * Node network indicators
		{
			Name: NodeCPUUtilization,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(node_cpu_utilization{instance=~"{{.AliasName}}"}[{{.Range}}s])
				    {{else}}
				        node_cpu_utilization{instance=~"{{.AliasName}}"}
				    {{end}}`,
		},
		{
			Name: NodeMemoryUtilization,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(node_memory_MemUsed{instance=~"{{.AliasName}}"}[{{.Range}}s]) / node_memory_MemTotal{instance=~"{{.AliasName}}"}
				    {{else}}
				        node_memory_MemUsed{instance=~"{{.AliasName}}"} / node_memory_MemTotal{instance=~"{{.AliasName}}"}
				    {{end}}`,
		},
		{
			Name: NodeNetworkReceiveBytes,
			Query: `{{if .Function }}
				        sum({{.Function}}_over_time(node_network_receive_bytes_irate5m{instance=~"{{.AliasName}}",device!~"docker0|cni0|flannel.1|lo|veth.*"}[{{.Range}}s])) by(instance)
				    {{else}}
				        sum(node_network_receive_bytes_irate5m{instance=~"{{.AliasName}}",device!~"docker0|cni0|flannel.1|lo|veth.*"}) by(instance)
				    {{end}}`,
		},
		{
			Name: NodeNetworkTransmitBytes,
			Query: `{{if .Function }}
				        sum({{.Function}}_over_time(node_network_transmit_bytes_irate5m{instance=~"{{.AliasName}}",device!~"docker0|cni0|flannel.1|lo|veth.*"}[{{.Range}}s])) by(instance)
				    {{else}}
				        sum(node_network_transmit_bytes_irate5m{instance=~"{{.AliasName}}",device!~"docker0|cni0|flannel.1|lo|veth.*"}) by(instance)
				    {{end}}`,
		},
		{
			Name:  NodeUP,
			Query: `up{job="node-exporter",instance=~"{{.AliasName}}"}`,
		},
		{
			Name: NodeLoad1,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(node_load1{instance=~"{{.AliasName}}"}[{{.Range}}s])
				    {{else}}
				        node_load1{instance=~"{{.AliasName}}"}
				    {{end}}`,
		},
		{
			Name: NodeLoad5,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(node_load5{instance=~"{{.AliasName}}"}[{{.Range}}s])
				    {{else}}
				        node_load5{instance=~"{{.AliasName}}"}
                    {{end}}`,
		},
		{
			Name: NodeLoad15,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(node_load15{instance=~"{{.AliasName}}"}[{{.Range}}s])
				    {{else}}
				        node_load15{instance=~"{{.AliasName}}"}
                    {{end}}`,
		},
		{
			Name:  NodeLoad1PerCore,
			Query: `avg by (instance) (node_load1{instance=~"{{.AliasName}}"}) / count by(instance) (node_cpu{mode="idle"})`,
		},
		{
			Name:  NodeLoad5PerCore,
			Query: `avg by (instance) (node_load5{instance=~"{{.AliasName}}"}) / count by(instance) (node_cpu{mode="idle"})`,
		},
		{
			Name:  NodeLoad15PerCore,
			Query: `avg by (instance) (node_load15{instance=~"{{.AliasName}}"}) / count by(instance) (node_cpu{mode="idle"})`,
		},
		{
			Name: NodeDiskUtilization,
			Query: `{{if .Function }}
				        avg by (instance, device) ({{.Function}}_over_time(node_filesystem_used{device=~"/dev/.*", instance=~"{{.AliasName}}"}[{{.Range}}s])) / avg by (instance, device) (node_filesystem_size{device=~"/dev/.*",instance=~"{{.AliasName}}"})
				    {{else}}
				        avg by (instance, device) (node_filesystem_used{device=~"/dev/.*", instance=~"{{.AliasName}}"}) / avg by (instance, device) (node_filesystem_size{device=~"/dev/.*",instance=~"{{.AliasName}}"})
				    {{end}}`,
		},
		{
			Name: NodeDiskSpaceUtilization,
			Query: `{{if .Function }}
				        avg by (instance, device) ({{.Function}}_over_time(node_filesystem_used{device=~"/dev/.*", instance=~"{{.AliasName}}"}[{{.Range}}s])) / avg by (instance, device) (node_filesystem_size{device=~"/dev/.*",instance=~"{{.AliasName}}"})
				    {{else}}
				        avg by (instance, device) (node_filesystem_used{device=~"/dev/.*", instance=~"{{.AliasName}}"}) / avg by (instance, device) (node_filesystem_size{device=~"/dev/.*",instance=~"{{.AliasName}}"})
				    {{end}}`,
		},
		{
			Name:  NodeDiskInodeUtilization,
			Query: `1- avg by (instance, device) (node_filesystem_files_free{device=~"/dev/.*",instance=~"{{.AliasName}}"}/node_filesystem_files{device=~"/dev/.*",instance=~"{{.AliasName}}"})`,
		},
		{
			Name: NodeDiskReadBytes,
			Query: `{{if .Function }}
				        avg by (instance, device) ({{.Function}}_over_time(node_disk_bytes_read_irate5m{instance=~"{{.AliasName}}"}[{{.Range}}s]))
				    {{else}}
                        avg by (instance, device) (node_disk_bytes_read_irate5m{instance=~"{{.AliasName}}"})
				    {{end}}`,
		},
		{
			Name: NodeDiskWrittenBytes,
			Query: `{{if .Function }}
				        avg by (instance, device) ({{.Function}}_over_time(node_disk_bytes_written_irate5m{instance=~"{{.AliasName}}"}[{{.Range}}s]))
				    {{else}}
                        avg by (instance, device) (node_disk_bytes_written_irate5m{instance=~"{{.AliasName}}"})
				    {{end}}`,
		},
		{
			Name: NodeDiskIOTime,
			Query: `{{if .Function }}
				        avg by (instance, device) ({{.Function}}_over_time(node_disk_io_time_ms_irate5m{instance=~"{{.AliasName}}"}[{{.Range}}s])) 
				    {{else}}
                        avg by (instance, device) (node_disk_io_time_ms_irate5m{instance=~"{{.AliasName}}"})
				    {{end}}`,
		},
		{
			Name: NodeResourceRequestCPUUtilization,
			Query: `{{if .Function }}
				        sum({{.Function}}_over_time(node_resource_requests_cpu_cores{node=~"{{.AliasName}}"}[{{.Range}}s])) by(node) / sum(kube_node_status_allocatable_cpu_cores{node=~"{{.AliasName}}"}) by(node)
				    {{else}}
				        sum(node_resource_requests_cpu_cores{node=~"{{.AliasName}}"}) by(node) / sum(kube_node_status_allocatable_cpu_cores{node=~"{{.AliasName}}"}) by(node)
				    {{end}}`,
		},
		{
			Name: NodeResourceRequestMemoryUtilization,
			Query: `{{if .Function }}
				        sum({{.Function}}_over_time(node_resource_requests_memory_bytes{node=~"{{.AliasName}}"}[{{.Range}}s])) by(node) / sum(kube_node_status_allocatable_memory_bytes{node=~"{{.AliasName}}"}) by(node)
				    {{else}}
				        sum(node_resource_requests_memory_bytes{node=~"{{.AliasName}}"}) by(node) / sum(kube_node_status_allocatable_memory_bytes{node=~"{{.AliasName}}"}) by(node)
				    {{end}}`,
		},

		// Cluster indicators query expressions, all the indicators can be divided into three categories:
		// * Kubernetes components indicators
		// * Kubernetes node indicators
		// * Kubernetes cluster indicators
		{
			Name:  ClusterKubeETCDHasLeader,
			Query: `max(etcd_server_has_leader{namespace="openshift-etcd"})`,
		},
		{
			Name:  ClusterKubeETCDLeaderElections,
			Query: `changes(etcd_server_leader_changes_seen_total{namespace="openshift-etcd"}[5m])`,
		},
		{
			Name:  ClusterKubeETCDMembers,
			Query: `sum(etcd_server_has_leader{namespace="openshift-etcd"})`,
		},
		{
			Name:  ClusterKubeETCDDBSIZE,
			Query: `sum(etcd_mvcc_db_total_size_in_bytes{namespace="openshift-etcd"})`,
		},
		{
			Name:  ClusterKubeETCDDBSIZEPerInstance,
			Query: `etcd_mvcc_db_total_size_in_bytes{namespace="openshift-etcd"}`,
		},
		{
			Name:  ClusterKubeETCDRPCRate,
			Query: `sum(rate(grpc_server_started_total{namespace="openshift-etcd",grpc_type="unary"}[5m]))`,
		},
		{
			Name:  ClusterKubeETCDRPCFailedRate,
			Query: `sum(rate(grpc_server_handled_total{namespace="openshift-etcd",grpc_type="unary",grpc_code!="OK"}[5m]))`,
		},
		{
			Name:  ClusterKubeETCDActiveWatchStream,
			Query: `sum(grpc_server_started_total{namespace="openshift-etcd",grpc_service="etcdserverpb.Watch",grpc_type="bidi_stream"}) - sum(grpc_server_handled_total{namespace="openshift-etcd",grpc_service="etcdserverpb.Watch",grpc_type="bidi_stream"})`,
		},
		{
			Name:  ClusterKubeETCDActiveLeaseStream,
			Query: `sum(grpc_server_started_total{namespace="openshift-etcd",grpc_service="etcdserverpb.Lease",grpc_type="bidi_stream"}) - sum(grpc_server_handled_total{namespace="openshift-etcd",grpc_service="etcdserverpb.Lease",grpc_type="bidi_stream"})`,
		},
		{
			Name: ClusterKubeETCDRaftProposalCommittedRate,
			Query: `sum(rate(etcd_server_proposals_committed_total{namespace="openshift-etcd"}[5m]))`,
		},
		{
			Name: ClusterKubeETCDRaftProposalAppliedRate,
			Query: `sum(rate(etcd_server_proposals_applied_total{namespace="openshift-etcd"}[5m]))`,
		},
		{
			Name: ClusterKubeETCDRaftProposalFailedRate,
			Query: `sum(rate(etcd_server_proposals_failed_total{namespace="openshift-etcd"}[5m]))`,
		},
		{
			Name: ClusterKubeETCDRaftProposalPendingRate,
			Query: `sum(rate(etcd_server_proposals_pending{namespace="openshift-etcd"}[5m]))`,
		},
		{
			Name: ClusterKubeETCDWALFsyncDurationPerInstance,
			Query: `histogram_quantile(0.99, sum(rate(etcd_disk_wal_fsync_duration_seconds_bucket{namespace="openshift-etcd"}[5m])) by (instance, le))`,
		},
		{
			Name: ClusterKubeETCDDBFsyncDurationPerInstance,
			Query: `histogram_quantile(0.99, sum(rate(etcd_disk_backend_commit_duration_seconds_bucket{namespace="openshift-etcd"}[5m])) by (instance, le))`,
		},
		{
			Name: ClusterKubeETCDClientTrafficIn,
			Query: `sum(rate(etcd_network_client_grpc_received_bytes_total{namespace="openshift-etcd"}[5m]))`,
		},
		{
			Name: ClusterKubeETCDClientTrafficInPerInstance,
			Query: `rate(etcd_network_client_grpc_received_bytes_total{namespace="openshift-etcd"}[5m])`,
		},
		{
			Name: ClusterKubeETCDClientTrafficOut,
			Query: `sum(rate(etcd_network_client_grpc_sent_bytes_total{namespace="openshift-etcd"}[5m]))`,
		},
		{
			Name: ClusterKubeETCDClientTrafficOutPerInstance,
			Query: `rate(etcd_network_client_grpc_sent_bytes_total{namespace="openshift-etcd"}[5m])`,
		},
		{
			Name: ClusterKubeETCDPeerTrafficIn,
			Query: `sum(rate(etcd_network_peer_grpc_received_bytes_total{namespace="openshift-etcd"}[5m]))`,
		},
		{
			Name: ClusterKubeETCDPeerTrafficInPerInstance,
			Query: `rate(etcd_network_peer_grpc_received_bytes_total{namespace="openshift-etcd"}[5m])`,
		},
		{
			Name: ClusterKubeETCDPeerTrafficOut,
			Query: `sum(rate(etcd_network_peer_grpc_sent_bytes_total{namespace="openshift-etcd"}[5m]))`,
		},
		{
			Name: ClusterKubeETCDPeerTrafficOutPerInstance,
			Query: `rate(etcd_network_peer_grpc_sent_bytes_total{namespace="openshift-etcd"}[5m])`,
		},
	}
)
