package model

import (
	"strings"

	"morgans/config"
)

const (
	// Common api version
	AVKubernetesV1           = "v1"
	AVAiOpsV1beta1           = "aiops.alauda.io/v1beta1"
	AVClusterV1alpha1        = "clusterregistry.k8s.io/v1alpha1"
	AVInfrastructureV1alpha1 = "infrastructure.alauda.io/v1alpha1"
	AVProjectV1              = "auth.alauda.io/v1"

	// Common resource kind for aiops
	RKAlertTemplate       = "AlertTemplate"
	RKClusterRegistry     = "Cluster"
	RKClusterFeature      = "Feature"
	RKNotification        = "Notification"
	RKNotificationMessage = "NotificationMessage"
	RKProject             = "Project"
	RKNamespace           = "Namespace"

	VerbGet = "get"

	// Kubernetes timestamp format
	KTimeFormat = "2006-01-02T15:04:05Z"
)

var (
	LKKind = "kind"
	LKUUID = "uuid"
	LKClusterType = "cluster-type"

	AKDisplayName  = "display-name"
	AKDescription  = "description"
	AKOrganization = "organization"
	AKCreatedAt    = "created-at"
	AKUpdatedAt    = "updated-at"
	AKCreator      = "creator"
)

func init() {
	if config.GlobalConfig.Label.BaseDomain != "" {
		LKKind = strings.Join([]string{config.GlobalConfig.Label.BaseDomain, LKKind}, "/")
		LKUUID = strings.Join([]string{config.GlobalConfig.Label.BaseDomain, LKUUID}, "/")
		AKDisplayName = strings.Join([]string{config.GlobalConfig.Label.BaseDomain, AKDisplayName}, "/")
		AKDescription = strings.Join([]string{config.GlobalConfig.Label.BaseDomain, AKDescription}, "/")
		AKOrganization = strings.Join([]string{config.GlobalConfig.Label.BaseDomain, AKOrganization}, "/")
		AKCreatedAt = strings.Join([]string{config.GlobalConfig.Label.BaseDomain, AKCreatedAt}, "/")
		AKUpdatedAt = strings.Join([]string{config.GlobalConfig.Label.BaseDomain, AKUpdatedAt}, "/")
		AKCreator = strings.Join([]string{config.GlobalConfig.Label.BaseDomain, AKCreator}, "/")
	}
}
