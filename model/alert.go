package model

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"

	"morgans/common"
	"morgans/config"

	"gopkg.in/yaml.v2"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
)

const (
	// Alert api version and kind
	APIVersionMonitoringV1 = "monitoring.coreos.com/v1"
	KindPrometheusRule     = "PrometheusRule"
	KindPrometheus         = "Prometheus"

	// Alert constants
	AlertViewLabelValueOps         = "ops"
	AlertViewLabelValueUser        = "user"
	AlertOwnerLabelValue           = "AlaudaSystem"
	AlertMetaAnnotationKey         = "AlertMeta"
	AlertValueAnnotationKey        = "AlertCurrentValue"
	AlertNotificationAnnotationKey = "alert_notifications"
	AlertMessageKey                = "message"
	AlertMessageFrom               = "message_from"
	AlertNormalStatus              = "ok"
	AlertPendingStatus             = "pending"
	AlertFiringStatus              = "firing"
	AlertNameField                 = "alertname"
	AlertStateField                = "alertstate"

	// Alert label names
	AlertIndicatorLabel   = "__name__"
	AlertNameLabel        = "name"
	AlertKindLabel        = "kind"
	AlertNamespaceLabel   = "namespace"
	AlertExprLabel        = "expr"
	AlertApplicationLabel = "application"
	AlertQueryLabel       = "query"

	// Alert severity
	AlertSeverityCritical = "Critical"
	AlertSeverityHigh     = "High"
	AlertSeverityMedium   = "Medium"
	AlertSeverityLow      = "Low"
)

var (
	AlertProjectLabelKey           = "alert.%s/project"
	AlertClusterLabelKey           = "alert.%s/cluster"
	AlertObjectKindLabelKey        = "alert.%s/kind"
	AlertObjectNameLabelKey        = "alert.%s/name"
	AlertObjectNamespaceLabelKey   = "alert.%s/namespace"
	AlertObjectApplicationLabelKey = "alert.%s/application"
	AlertViewLabelKey              = "alert.%s/view"
	AlertOwnerLabelKey             = "alert.%s/owner"

	LabelAlertName   = "name"
	LabelObjectName  = "involved_object_name"
	LabelObjectKind  = "involved_object_kind"
	LabelNamespace   = "involved_object_namespace"
	LabelCreator     = "creator"
	LabelProject     = "project"
	LabelCluster     = "cluster"
	LabelIndicator   = "indicator"
	LabelCompare     = "compare"
	LabelThreshold   = "threshold"
	LabelUnit        = "unit"
	LabelSeverity    = "severity"
	LabelApplication = "application"
	// LabelsChoice is a set for keys be added to alert rule
	LabelsChoice []string
	// AlertSeverity is a set for alert severity
	AlertSeverity = []string{AlertSeverityCritical, AlertSeverityHigh, AlertSeverityMedium, AlertSeverityLow}
	// Prometheus functions set
	PrometheusFunctions = []string{
		"abs", "absent", "avg", "avg_over_time", "ceil", "changes", "clamp_max", "clamp_min", "count_over_time", "day_of_month",
		"day_of_week", "days_in_month", "delta", "deriv", "exp", "floor", "histogram_quantile", "holt_winters", "hour", "idelta",
		"increase", "irate", "label_join", "label_replace", "ln", "log10", "log2", "max", "max_over_time", "min", "min_over_time",
		"minute", "month", "predict_linear", "quantile_over_time", "rate", "resets", "round", "scalar", "sort", "sort_desc", "sqrt",
		"stddev_over_time", "stdvar_over_time", "sum", "sum_over_time", "time", "timestamp", "vector", "year",
	}
	// Alert status map
	AlertStatusMap = map[string]int{
		AlertFiringStatus:  1,
		AlertPendingStatus: 2,
		AlertNormalStatus:  3,
	}
)

var (
	AlertLabels = map[string]string{
		"app":              "prometheus",
		"prometheus":       "kube-prometheus",
		"release":          "kube-prometheus",
		"role":             "alert-rules",
		AlertOwnerLabelKey: AlertOwnerLabelValue,
	}
	AlertMessageTemplatesSet = AlertMessageTemplates{}
)

type AlertTemplate struct {
	Name          string            `json:"name"`
	Metric        QueryRangeRequest `json:"metric"`
	Compare       string            `json:"compare"`
	Threshold     float64           `json:"threshold"`
	Unit          string            `json:"unit,omitempty"`
	Wait          int64             `json:"wait"`
	Labels        map[string]string `json:"labels,omitempty"`
	Annotations   map[string]string `json:"annotations,omitempty"`
	Notifications []*Notification   `json:"notifications,omitempty"`
	ScaleUp       []*Service        `json:"scale_up,omitempty"`
	ScaleDown     []*Service        `json:"scale_down,omitempty"`
}

type AlertRequest struct {
	UUID             string    `json:"uuid"`
	Namespace        string    `json:"namespace"`
	ResourceName     string    `json:"resource_name"`
	GroupName        string    `json:"group_name"`
	Project          string    `json:"project"`
	Creator          string    `json:"creator"`
	CreatedAt        time.Time `json:"created_at"`
	UpdatedAt        time.Time `json:"updated_at"`
	AlertTemplate    `json:",inline"`
	AdditionalConfig map[string]string `json:"additionalConfig,omitempty"`
}

type AlertResponse struct {
	AlertRequest `json:",inline"`
	State        string `json:"state"`
}

type Notification struct {
	UUID      string `json:"uuid,omitempty"`
	Name      string `json:"name"`
	Namespace string `json:"namespace,omitempty"`
}

type Service struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
}

type AlertResource struct {
	ResourceMeta `json:",inline"`
	Spec         AlertSpec `json:"spec,omitempty"`
}

type AlertResourceList struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
	Items             []*AlertResource `json:"items,omitempty"`
}

type AlertSpec struct {
	Groups []*AlertGroup `json:"groups"`
}

type AlertGroup struct {
	Name  string       `yaml:"name,omitempty" json:"name,omitempty"`
	Rules []*AlertRule `yaml:"rules" json:"rules"`
}

type AlertRule struct {
	Meta        *QueryMeta        `json:"-"`
	Alert       string            `yaml:"alert,omitempty" json:"alert,omitempty"`
	Expr        string            `yaml:"expr,omitempty" json:"expr,omitempty"`
	For         string            `yaml:"for,omitempty" json:"for,omitempty"`
	Labels      map[string]string `yaml:"labels,omitempty" json:"labels,omitempty"`
	Annotations map[string]string `yaml:"annotations,omitempty" json:"annotations,omitempty"`
}

type AlertConfig struct {
	SystemLabels        []string `json:"system_label_keys"`
	PrometheusFunctions []string `json:"prometheus_functions"`
	AlertResourceLabels []string `json:"alert_resources_labels"`
}

type AlertMessageTemplates map[string]AlertMessageTemplate

type AlertMessageTemplate struct {
	CN string `json:"cn"`
	EN string `json:"en"`
}

// GetAlertMeta will return alert meta string, it's a mirror for this alert
func (r *AlertRequest) GetAlertMeta() string {
	var err error
	var result []byte
	if r.Annotations != nil {
		originalMeta, in := r.Annotations[AlertMetaAnnotationKey]
		delete(r.Annotations, AlertMetaAnnotationKey)
		result, err = json.Marshal(r)
		if in {
			r.Annotations[AlertMetaAnnotationKey] = originalMeta
		}
	} else {
		result, err = json.Marshal(r)
	}
	if err != nil {
		panic(err.Error())
	}
	return string(result)
}

func GetAlertSelector(labelSelector string) map[string]string {
	slector := ""
	if labelSelector != "" {
		slector = "," + labelSelector
	}
	return map[string]string{
		"labelSelector": AlertOwnerLabelKey + "=" + AlertOwnerLabelValue + slector,
	}
}

func GetAlertNameValue(kind, name string) string {
	if kind == string(NodeKind) && name == ".*" {
		return "all"
	}
	return name
}

func GetAlertGroupName() string {
	return "general"
}

func GetAlertResourceName(r *Cluster, m *QueryMeta) string {
	name := m.Kind + "-" + m.Name

	if m.Kind == string(NodeKind) {
		name = "node-" + GetAlertNameValue(m.Kind, m.Name)
	}
	if m.Kind == string(ClusterKind) {
		name = "cluster-" + common.TransformString(r.Name)
	}
	if m.Kind == string(OtherKind) {
		name = "other-" + common.TransformString(r.Name)
	}
	return name
}

func GetAlertMetaLabels(m *QueryMeta) map[string]string {
	toUpdate := map[string]string{
		LabelIndicator:  m.Indicator,
		LabelObjectKind: m.Kind,
		LabelObjectName: GetAlertNameValue(m.Kind, m.Name),
		LabelNamespace:  m.Namespace,
	}
	return MergeLabelsForAlertRule(toUpdate, map[string]string{})
}

func GetAlertCommonLabels(prometheusName string) map[string]string {
	result := make(map[string]string, len(AlertLabels))
	for k, v := range AlertLabels {
		result[k] = v
	}
	if prometheusName != "" {
		result["prometheus"] = prometheusName
	}
	return result
}

func GetPrometheusAlertName(rn, gn, an string) string {
	return an + "-" + common.MD5(rn+"__alauda__"+gn)
}

func FormatThreshold(num float64) string {
	return strconv.FormatFloat(num, 'f', 5, 64)
}

func MergeLabelsForAlertRule(source, destination map[string]string) map[string]string {
	if source == nil {
		source = map[string]string{}
	}
	if destination == nil {
		destination = map[string]string{}
	}
	for k, v := range source {
		added := false
		for _, c := range LabelsChoice {
			if c == k {
				added = true
				destination[k] = v
				break
			}
		}
		if !added {
			if _, ok := destination[k]; ok {
				delete(destination, k)
			}
		}
	}
	return destination
}

func RestResultToAlert(result *rest.Result) (*AlertResource, error) {
	bt, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var res AlertResource
	if err := json.Unmarshal(bt, &res); err != nil {
		return nil, err
	}
	return &res, nil
}

func RestResultToAlerts(result *rest.Result) ([]*AlertResource, error) {
	bt, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var res AlertResourceList
	if err := json.Unmarshal(bt, &res); err != nil {
		return nil, err
	}
	return res.Items, nil
}

func initAlert() {
	if config.GlobalConfig.Label.BaseDomain != "" {
		AlertProjectLabelKey = fmt.Sprintf(AlertProjectLabelKey, config.GlobalConfig.Label.BaseDomain)
		AlertClusterLabelKey = fmt.Sprintf(AlertClusterLabelKey, config.GlobalConfig.Label.BaseDomain)
		AlertObjectKindLabelKey = fmt.Sprintf(AlertObjectKindLabelKey, config.GlobalConfig.Label.BaseDomain)
		AlertObjectNameLabelKey = fmt.Sprintf(AlertObjectNameLabelKey, config.GlobalConfig.Label.BaseDomain)
		AlertObjectNamespaceLabelKey = fmt.Sprintf(AlertObjectNamespaceLabelKey, config.GlobalConfig.Label.BaseDomain)
		AlertObjectApplicationLabelKey = fmt.Sprintf(AlertObjectApplicationLabelKey, config.GlobalConfig.Label.BaseDomain)
		AlertViewLabelKey = fmt.Sprintf(AlertViewLabelKey, config.GlobalConfig.Label.BaseDomain)
		AlertOwnerLabelKey = fmt.Sprintf(AlertOwnerLabelKey, config.GlobalConfig.Label.BaseDomain)
	} else {
		AlertProjectLabelKey = "alert/project"
		AlertClusterLabelKey = "alert/cluster"
		AlertObjectKindLabelKey = "alert/kind"
		AlertObjectNameLabelKey = "alert/name"
		AlertObjectNamespaceLabelKey = "alert/namespace"
		AlertObjectApplicationLabelKey = "alert/application"
		AlertViewLabelKey = "alert/view"
		AlertOwnerLabelKey = "alert/owner"
	}

	LabelAlertName = config.GlobalConfig.Alert.LabelsPrefix + LabelAlertName
	LabelObjectName = config.GlobalConfig.Alert.LabelsPrefix + LabelObjectName
	LabelObjectKind = config.GlobalConfig.Alert.LabelsPrefix + LabelObjectKind
	LabelNamespace = config.GlobalConfig.Alert.LabelsPrefix + LabelNamespace
	LabelCreator = config.GlobalConfig.Alert.LabelsPrefix + LabelCreator
	LabelProject = config.GlobalConfig.Alert.LabelsPrefix + LabelProject
	LabelCluster = config.GlobalConfig.Alert.LabelsPrefix + LabelCluster
	LabelIndicator = config.GlobalConfig.Alert.LabelsPrefix + LabelIndicator
	LabelCompare = config.GlobalConfig.Alert.LabelsPrefix + LabelCompare
	LabelThreshold = config.GlobalConfig.Alert.LabelsPrefix + LabelThreshold
	LabelUnit = config.GlobalConfig.Alert.LabelsPrefix + LabelUnit

	if config.GlobalConfig.Alert.MessageTemplatesEnabled {
		LabelsChoiceWithPrefix := []string{
			LabelAlertName,
			LabelObjectName,
			LabelObjectKind,
			LabelNamespace,
			LabelCreator,
			LabelProject,
			LabelCluster,
			LabelIndicator,
			LabelThreshold,
			LabelCompare,
			LabelUnit,
		}

		// Init alert templates
		if _, err := os.Stat(AlertTemplatesFile); err != nil {
			panic(err.Error())
		}
		templateContent, err := ioutil.ReadFile(AlertTemplatesFile)
		if err != nil {
			panic(err.Error())
		}
		if err = yaml.Unmarshal(templateContent, AlertMessageTemplatesSet); err != nil {
			panic(err.Error())
		}

		for key, template := range AlertMessageTemplatesSet {
			for _, label := range LabelsChoiceWithPrefix {
				template.CN = strings.Replace(template.CN, "{{ $labels."+label[len(config.GlobalConfig.Alert.LabelsPrefix):], "{{ $labels."+label, -1)
				template.EN = strings.Replace(template.EN, "{{ $labels."+label[len(config.GlobalConfig.Alert.LabelsPrefix):], "{{ $labels."+label, -1)
			}
			AlertMessageTemplatesSet[key] = AlertMessageTemplate{
				CN: template.CN,
				EN: template.EN,
			}
		}
		LabelsChoice = append(LabelsChoiceWithPrefix, []string{LabelSeverity, LabelApplication}...)
	}
}
