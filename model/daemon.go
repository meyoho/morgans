package model

import (
	"time"
)

const (
	SucceededTaskState = "succeeded"
	FailedTaskState    = "failed"
)

type DaemonTask struct {
	Name      string    `json:"name"`
	Status    string    `json:"status"`
	Message   string    `json:"message"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Version   int       `json:"version,omitempty"`
}

func (d *DaemonTask) HasDone() bool {
	return d.Status == SucceededTaskState
}
