package model

import (
	"encoding/json"
	"strings"
)

type Kind string
type Type string
type Unit string
type ValueType string

type IndicatorItem struct {
	Name               string            `json:"name"`
	Kind               Kind              `json:"kind"`
	Type               Type              `json:"type"`
	Query              string            `json:"query"`
	Unit               Unit              `json:"unit"`
	ValueType          ValueType         `json:"value_type"`
	AlertEnabled       bool              `json:"alert_enabled"`
	AggregationEnabled bool              `json:"aggregation_enabled"`
	Annotations        map[string]string `json:"annotations"`
}

type WorkloadItem struct {
	Kind           Kind
	PodNamePattern string
}

const (
	// Annotation languages
	CN = "cn"
	EN = "en"

	// Indicator kinds
	PodKind      Kind = "pod"
	WorkloadKind Kind = "workload"
	NodeKind     Kind = "node"
	ClusterKind  Kind = "cluster"
	OtherKind    Kind = "other"

	// Indicator types
	TypeMetric Type = "metric"
	TypeEvent  Type = "event"
	TypeLog    Type = "log"
	TypeCustom Type = "custom"

	// WorkloadKind set
	DeploymentKind  Kind = "deployment"
	DaemonSetKind   Kind = "daemonset"
	StatefulSetKind Kind = "statefulset"

	// Indicator units
	Percent        Unit = "%"
	Second         Unit = "s"
	MicroSecond    Unit = "ms"
	Bytes          Unit = "bytes"
	BytesPerSecond Unit = "bytes/second"

	// Types for indicator values
	Scalar ValueType = "scalar"
	Vector ValueType = "vector"

	// Custom indicators
	Custom = "custom"

	// Indicators for workload
	WorkloadLogKeywordCount          = "workload.log.keyword.count"
	WorkloadEventReasonCount         = "workload.event.reason.count"
	WorkloadCPUUtilization           = "workload.cpu.utilization"
	WorkloadMemoryUtilization        = "workload.memory.utilization"
	WorkloadNetworkReceiveBytes      = "workload.network.receive_bytes"
	WorkloadNetworkTransmitBytes     = "workload.network.transmit_bytes"
	WorkloadReplicasAvailable        = "workload.replicas.available"
	WorkloadReplicasDesired          = "workload.replicas.desired"
	WorkloadReplicasHealth           = "workload.replicas.health"
	WorkloadPodStatusPhaseRunning    = "workload.pod.status.phase.running"
	WorkloadPodStatusPhaseNotRunning = "workload.pod.status.phase.not.running"
	WorkloadPodRestartedCount        = "workload.pod.restarted.count"
	PodCPUUtilization                = "pod.cpu.utilization"
	PodMemoryUtilization             = "pod.memory.utilization"
	PodNetworkReceiveBytes           = "pod.network.receive_bytes"
	PodNetworkTransmitBytes          = "pod.network.transmit_bytes"
	PodStatusPhaseRunning            = "pod.status.phase.running"
	PodRestartedCount                = "pod.restarted.count"
	ContainerCPUUtilization          = "container.cpu.utilization"
	ContainerMemoryUtilization       = "container.memory.utilization"
	ContainerNetworkReceiveBytes     = "container.network.receive_bytes"
	ContainerNetworkTransmitBytes    = "container.network.transmit_bytes"

	// Indicators for node
	NodeCPUUtilization                   = "node.cpu.utilization"
	NodeMemoryUtilization                = "node.memory.utilization"
	NodeGPUUtilization                   = "node.gpu.utilization"
	NodeGPUMemoryUtilization             = "node.gpu.memory.utilization"
	NodeUP                               = "node.up"
	NodeLoad1                            = "node.load.1"
	NodeLoad5                            = "node.load.5"
	NodeLoad15                           = "node.load.15"
	NodeLoad1PerCore                     = "node.load.1.per.core"
	NodeLoad5PerCore                     = "node.load.5.per.core"
	NodeLoad15PerCore                    = "node.load.15.per.core"
	NodeDiskUtilization                  = "node.disk.utilization"
	NodeDiskSpaceUtilization             = "node.disk.space.utilization"
	NodeDiskInodeUtilization             = "node.disk.inode.utilization"
	NodeDiskReadBytes                    = "node.disk.read.bytes"
	NodeDiskWrittenBytes                 = "node.disk.written.bytes"
	NodeDiskIOTime                       = "node.disk.io.time"
	NodeNetworkIPVSActiveConnections     = "node.network.ipvs.active.connections"
	NodeNetworkReceiveBytes              = "node.network.receive_bytes"
	NodeNetworkTransmitBytes             = "node.network.transmit_bytes"
	NodeResourceRequestCPUUtilization    = "node.resource.request.cpu.utilization"
	NodeResourceRequestMemoryUtilization = "node.resource.request.memory.utilization"

	// Indicators for cluster
	ClusterCPUUtilization                           = "cluster.cpu.utilization"
	ClusterMemoryUtilization                        = "cluster.memory.utilization"
	ClusterGPUUtilization                           = "cluster.gpu.utilization"
	ClusterGPUMemoryUtilization                     = "cluster.gpu.memory.utilization"
	ClusterNodeNotReadyCount                        = "cluster.node.not.ready.count"
	ClusterNodeTotalCount                           = "cluster.node.total.count"
	ClusterResourceRequestCPUUtilization            = "cluster.resource.request.cpu.utilization"
	ClusterResourceRequestMemoryUtilization         = "cluster.resource.request.memory.utilization"
	ClusterKubeAPIServerUp                          = "cluster.kube.apiserver.up"
	ClusterKubeAPIServerHealth                      = "cluster.kube.apiserver.health"
	ClusterKubeControllerManagerUp                  = "cluster.kube.controller.manager.up"
	ClusterKubeControllerManagerHealth              = "cluster.kube.controller.manager.health"
	ClusterKubeSchedulerUp                          = "cluster.kube.scheduler.up"
	ClusterKubeSchedulerHealth                      = "cluster.kube.scheduler.health"
	ClusterDockerDaemonUp                           = "cluster.docker.daemon.up"
	ClusterDockerDaemonHealth                       = "cluster.docker.daemon.health"
	ClusterKubeDNSUp                                = "cluster.kube.dns.up"
	ClusterKubeDNSHealth                            = "cluster.kube.dns.health"
	ClusterKubeETCDUp                               = "cluster.kube.etcd.up"
	ClusterKubeETCDHealth                           = "cluster.kube.etcd.health"
	ClusterKubeKubeletUp                            = "cluster.kube.kubelet.up"
	ClusterKubeKubeletHealth                        = "cluster.kube.kubelet.health"
	ClusterKubeProxyUp                              = "cluster.kube.proxy.up"
	ClusterKubeProxyHealth                          = "cluster.kube.proxy.health"
	ClusterNodeReady                                = "cluster.node.ready"
	ClusterNodeHealth                               = "cluster.node.health"
	ClusterNodeNetworkIpvsActiveConnections         = "cluster.node.network.ipvs.active.connections"
	ClusterAlertsFiring                             = "cluster.alerts.firing"
	ClusterKubeAPIServerRequestLatency              = "cluster.kube.apiserver.request.latency"
	ClusterKubeAPIServerRequestLatencyPerVerb       = "cluster.kube.apiserver.request.latency.per.verb"
	ClusterKubeAPIServerRequestLatencyPerInstance   = "cluster.kube.apiserver.request.latency.per.instance"
	ClusterKubeAPIServerRequestCount                = "cluster.kube.apiserver.request.count"
	ClusterKubeAPIServerRequestCountPerInstance     = "cluster.kube.apiserver.request.count.per.instance"
	ClusterKubeAPIServerRequestErrorRate            = "cluster.kube.apiserver.request.error.rate"
	ClusterKubeAPIServerRequestErrorRatePerInstance = "cluster.kube.apiserver.request.error.rate.per.instance"
	ClusterKubeAPIServerRequestCount2XX             = "cluster.kube.apiserver.request.count.2xx"
	ClusterKubeAPIServerRequestCount2XXPerInstance  = "cluster.kube.apiserver.request.count.2xx.per.instance"
	ClusterKubeAPIServerRequestCount4XX             = "cluster.kube.apiserver.request.count.4xx"
	ClusterKubeAPIServerRequestCount4XXPerInstance  = "cluster.kube.apiserver.request.count.4xx.per.instance"
	ClusterKubeAPIServerRequestCount5XX             = "cluster.kube.apiserver.request.count.5xx"
	ClusterKubeAPIServerRequestCount5XXPerInstance  = "cluster.kube.apiserver.request.count.5xx.per.instance"
	ClusterKubeETCDHasLeader                        = "cluster.kube.etcd.has.leader"
	ClusterKubeETCDLeaderElections                  = "cluster.kube.etcd.leader.elections"
	ClusterKubeETCDMembers                          = "cluster.kube.etcd.members"
	ClusterKubeETCDDBSIZE                           = "cluster.kube.etcd.db.size"
	ClusterKubeETCDDBSIZEPerInstance                = "cluster.kube.etcd.db.size.per.instance"
	ClusterKubeETCDRPCRate                          = "cluster.kube.etcd.rpc.rate"
	ClusterKubeETCDRPCFailedRate                    = "cluster.kube.etcd.rpc.failed.rate"
	ClusterKubeETCDActiveWatchStream                = "cluster.kube.etcd.active.watch.stream"
	ClusterKubeETCDActiveLeaseStream                = "cluster.kube.etcd.active.lease.stream"
	ClusterKubeETCDRaftProposalCommittedRate        = "cluster.kube.etcd.raft.proposal.committed.rate"
	ClusterKubeETCDRaftProposalPendingRate          = "cluster.kube.etcd.raft.proposal.pending.rate"
	ClusterKubeETCDRaftProposalFailedRate           = "cluster.kube.etcd.raft.proposal.failed.rate"
	ClusterKubeETCDRaftProposalAppliedRate          = "cluster.kube.etcd.raft.proposal.applied.rate"
	ClusterKubeETCDWALFsyncDurationPerInstance      = "cluster.kube.etcd.wal.fsync.duration.per.instance"
	ClusterKubeETCDDBFsyncDurationPerInstance       = "cluster.kube.etcd.db.fsync.duration.per.instance"
	ClusterKubeETCDClientTrafficIn                  = "cluster.kube.etcd.client.traffic.in"
	ClusterKubeETCDClientTrafficInPerInstance       = "cluster.kube.etcd.client.traffic.in.per.instance"
	ClusterKubeETCDClientTrafficOut                 = "cluster.kube.etcd.client.traffic.out"
	ClusterKubeETCDClientTrafficOutPerInstance      = "cluster.kube.etcd.client.traffic.out.per.instance"
	ClusterKubeETCDPeerTrafficIn                    = "cluster.kube.etcd.peer.traffic.in"
	ClusterKubeETCDPeerTrafficInPerInstance         = "cluster.kube.etcd.peer.traffic.in.per.instance"
	ClusterKubeETCDPeerTrafficOut                   = "cluster.kube.etcd.peer.traffic.out"
	ClusterKubeETCDPeerTrafficOutPerInstance        = "cluster.kube.etcd.peer.traffic.out.per.instance"
	ClusterPodRestartedTotal                        = "cluster.pod.restarted.total"
	ClusterPodRestartedCount                        = "cluster.pod.restarted.count"
	ClusterPodStatusPhaseRunning                    = "cluster.pod.status.phase.running"
	ClusterPodStatusPhaseSucceeded                  = "cluster.pod.status.phase.succeeded"
	ClusterPodStatusPhaseFailed                     = "cluster.pod.status.phase.failed"
	ClusterPodStatusPhasePending                    = "cluster.pod.status.phase.pending"
	ClusterPodStatusPhaseUnknown                    = "cluster.pod.status.phase.unknown"
	ClusterPodStatusPhaseNotRunning                 = "cluster.pod.status.phase.not.running"
)

var (
	WorkloadSet = map[Kind]*WorkloadItem{
		DeploymentKind: {
			Kind: DeploymentKind,
			// Deployment pod example: kube-dns-5f8c75dccb-rxpbq
			PodNamePattern: "{{.Name}}-[a-z0-9]{7,10}-[a-z0-9]{5}",
		},
		DaemonSetKind: {
			Kind: DaemonSetKind,
			// DaemonSet pod example: kube-flannel-cn7hq
			PodNamePattern: "{{.Name}}-[a-z0-9]{5}",
		},
		StatefulSetKind: {
			Kind: StatefulSetKind,
			// StatefulSet pod example: prometheus-kube-prometheus-0
			PodNamePattern: "{{.Name}}-[0-9]{1,3}",
		},
	}
)

func initQuery() {
	// Init base indicator map
	for _, i := range IndicatorSetBase {
		var querySlice []string
		query := strings.Replace(i.Query, "\n", " ", -1)
		query = strings.Replace(query, "\t", " ", -1)
		for _, str := range strings.Split(query, " ") {
			if str != "" {
				querySlice = append(querySlice, str)
			}
		}
		i.Query = strings.Join(querySlice, " ")
		IndicatorMapBase[i.Name] = i
	}

	// Init indicator map for ocp 4.3 and more
	for k, v := range IndicatorMapBase {
		value := IndicatorItem{}
		bytesArray, err := json.Marshal(*v)
		if err != nil {
			panic("Marshal indicator item error: "+ err.Error())
		}
		if err := json.Unmarshal(bytesArray, &value); err !=  nil {
			panic("Unmarshal indicator item error: "+ err.Error())
		}
		IndicatorMapForOCP43[k] = &value
	}
	for _, i := range IndicatorSetForOCP43 {
		var querySlice []string
		query := strings.Replace(i.Query, "\n", " ", -1)
		query = strings.Replace(query, "\t", " ", -1)
		for _, str := range strings.Split(query, " ") {
			if str != "" {
				querySlice = append(querySlice, str)
			}
		}
		i.Query = strings.Join(querySlice, " ")
		if v, ok := IndicatorMapForOCP43[i.Name]; ok {
			v.Query = i.Query
		}
	}
}

func GetIndicatorMap(c *Cluster) map[string]*IndicatorItem {
	if c.IsOCP && c.Version >= "1.16" {
		return IndicatorMapForOCP43
	} else {
		return IndicatorMapBase
	}
}
