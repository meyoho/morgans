package model

import (
	"time"

	"morgans/config"

	wg "bitbucket.org/mathildetech/warpgate"
)

const (
	MeterFeature    = "meter"
	AVMeteringGroup = "metering.alauda.io"
	RKMeterReport   = "reports"

	MeterTypePodUsage      = "podUsage"
	MeterTypePodRequests   = "podRequests"
	MeterTypeNamespceQuota = "namespaceQuota"
	MeterTypeProjectQuota  = "projectQuota"

	GroupByProject   = "project"
	GroupByNamespace = "namespace"

	OrderByName   = "name"
	OrderByCPU    = "cpu"
	OrderByMemory = "memory"

	DefaultMeterTopN     = 5
	DefaultMeterPage     = 1
	DefaultMeterPageSize = 20

	ReportPath = "/morgans/meter/reports/"
)

type MeterRequest struct {
	Type       string    `json:"type"`
	GroupBy    string    `json:"groupBy"`
	Top        int       `json:"top"`
	StartTime  time.Time `json:"startTime"`
	EndTime    time.Time `json:"endTime"`
	Project    string    `json:"project"`
	Cluster    string    `json:"cluster"`
	Namespace  string    `json:"namespace"`
	Page       int       `json:"page,1"`
	PageSize   int       `json:"pageSize,20"`
	OrderBy    string    `json:"orderBy,name"`
	Sorter     Sorter    `json:"sorter"`
	ClassifyBy string    `json:"classifyBy"`
}

type MeterTopnResponse struct {
	TopN   []*MeterItem `json:"topN"`
	Remain MeterInfo    `json:"remain"`
}

type MeterTotalResponse struct {
	Results []*MeterInfo `json:"results"`
}

type MeterTotalBuckets struct {
	Buckets []*MeterTotalBucket `json:"buckets"`
}

type MeterTotalBucket struct {
	Key    string      `json:"key"`
	CPU    BucketValue `json:"cpu"`
	Memory BucketValue `json:"memory"`
}

type BucketValue struct {
	Value float64 `json:"value"`
}

type MeterItem struct {
	Project   MeterObject `json:"project,omitempty"`
	Cluster   MeterObject `json:"cluster,omitempty"`
	Namespace MeterObject `json:"namespace,omitempty"`
	Pod       string      `json:"pod,omitempty"`
	MeterInfo `json:",inline"`
}

type MeterObject struct {
	Name        string `json:"name,omitempty"`
	DisplayName string `json:"displayName,omitempty"`
}

type MeterInfo struct {
	Meter     MeterKind `json:"meter"`
	Date      string    `json:"date,omitempty"`
	Month     string    `json:"month,omitempty"`
	Day       string    `json:"day,omitempty"`
	StartTime time.Time `json:"startTime"`
	EndTime   time.Time `json:"endTime"`
}

type MeterKind struct {
	CPU    float64 `json:"cpu"`
	Memory float64 `json:"memory"`
}

type Sorter struct {
	Field     string `json:"field"`
	Path      string `json:"path"`
	Ascending bool   `json:"ascending"`
}

func MeterEnabled(token string) (bool, error) {
	featureGate := wg.NewWarpGate(wg.Config{
		APIEndpoint:        config.GlobalConfig.Archon.Endpoint,
		AuthorizationToken: token,
	})
	return featureGate.IsFeatureGateEnabled(MeterFeature)
}
