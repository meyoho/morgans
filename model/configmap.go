package model

import (
	"encoding/json"

	"morgans/common"

	"k8s.io/client-go/rest"
)

const (
	AlertConfigmapName     = "prometheus-core-config"
	AlertPrometheusVersion = "version"
	AlertTemplatesFile     = "/etc/morgans/alert-templates.yml"
	KindConfigMap          = "ConfigMap"
)

type ConfigMapResource struct {
	ResourceMeta `json:",inline"`
	Data         map[string]string `json:"data,omitempty"`
	BinaryData   map[string][]byte `json:"binaryData,omitempty"`
}

func RestResultToConfigMap(result *rest.Result) (*ConfigMapResource, error) {
	data, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var resource ConfigMapResource
	if err := json.Unmarshal(data, &resource); err != nil {
		return nil, err
	}
	return &resource, nil
}
