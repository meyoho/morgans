package model

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strings"

	"morgans/common"

	"k8s.io/apiserver/pkg/authentication/user"
)

type JWTToken struct {
	Issuer        string      `json:"iss"`
	Subject       string      `json:"sub"`
	Audience      string      `json:"aud"`
	Expiry        int         `json:"exp"`
	IssuedAt      int         `json:"iat"`
	Nonce         string      `json:"nonce"`
	Email         string      `json:"email"`
	EmailVerified bool        `json:"email_verified"`
	Name          string      `json:"name"`
	Groups        []string    `json:"groups"`
	Ext           JWTTokenExt `json:"ext"`
	MetadataName  string
}

type JWTTokenExt struct {
	IsAdmin bool   `json:"is_admin"`
	ConnID  string `json:"conn_id"`
}

func (t *JWTToken) toUserInfo() user.Info {
	var info user.DefaultInfo
	info.Name = strings.Split(t.Email, "@")[0]
	info.Groups = t.Groups
	return &info
}

func parseJWT(rawToken string) (*JWTToken, error) {
	var token JWTToken

	if rawToken == "" {
		return nil, common.NewError(common.CodeBadRequest, "authentication head is invalid")
	}
	parts := strings.Split(rawToken, ".")
	if len(parts) < 2 {
		return nil, fmt.Errorf("oidc: malformed jwt, expected 3 parts got %d", len(parts))
	}
	payload, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, fmt.Errorf("oidc: malformed jwt payload: %v", err)
	}
	if err := json.Unmarshal(payload, &token); err != nil {
		fmt.Println(err)
		return nil, err
	}
	token.MetadataName = getUserMetadataName(token.Email)
	return &token, nil
}

func getUserMetadataName(userID string) string {
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(strings.TrimSpace(userID)))
	cipherStr := md5Ctx.Sum(nil)
	return hex.EncodeToString(cipherStr)
}

func ParseUser(token string) (user.Info, error) {
	jwtToken, err := parseJWT(token)
	if err != nil {
		return nil, err
	}
	userInfo := jwtToken.toUserInfo()

	return userInfo, nil
}
