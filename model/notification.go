package model

import (
	"encoding/json"
	"time"

	"morgans/common"
	"morgans/config"

	"k8s.io/client-go/rest"
)

var (
	NotificationChannel = make(chan NotificationTask, config.GlobalConfig.Notification.ChanCapacity)

	NotificationSupported = []string{
		"email", "sms", "webhook", "dingtalk",
	}
)

type NotificationTask struct {
	Meta     map[string]string `json:"meta"`
	Receiver []*Subscription   `json:"receiver"`
	Body     *NotificationBody `json:"body"`
}

type NotificationBody struct {
	TemplateType string            `json:"template_type"`
	Data         *NotificationData `json:"data"`
}

type NotificationData struct {
	Subject string            `json:"subject"`
	Content string            `json:"content"`
	Time    time.Time         `json:"time,omitempty"`
	Payload map[string]string `json:"payload"`
}

type NotificationRequest struct {
	UUID          string          `json:"uuid"`
	Name          string          `json:"name"`
	Namespace     string          `json:"namespace,omitempty"`
	Description   string          `json:"description,omitempty"`
	Subscriptions []*Subscription `json:"subscriptions"`
	Project       string          `json:"project"`
	Creator       string          `json:"created_by"`
	CreatedAt     time.Time       `json:"created_at"`
	UpdatedAt     time.Time       `json:"updated_at,omitempty"`
}

type NotificationResponse struct {
	NotificationRequest `json:",inline"`
}

type NotificationResource struct {
	ResourceMeta `json:",inline"`
	Spec         NotificationSpec `json:"spec,omitempty"`
}

type NotificationResourceList struct {
	ResourceMeta `json:",inline"`
	Items        []*NotificationResource `json:"items,omitempty"`
}

type NotificationSpec struct {
	Subscriptions []*Subscription `json:"subscriptions"`
}

type Subscription struct {
	Method    string `yaml:"method" json:"method"`
	Recipient string `yaml:"recipient" json:"recipient"`
	Secret    string `yaml:"secret" json:"secret"`
	Remark    string `yaml:"remark" json:"remark"`
}

func RestResultToNotification(result *rest.Result) (*NotificationResource, error) {
	bt, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var res NotificationResource
	if err := json.Unmarshal(bt, &res); err != nil {
		return nil, err
	}
	return &res, nil
}

func RestResultToNotifications(result *rest.Result) ([]*NotificationResource, error) {
	bt, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var res NotificationResourceList
	if err := json.Unmarshal(bt, &res); err != nil {
		return nil, err
	}
	return res.Items, nil
}
