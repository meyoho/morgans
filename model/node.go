package model

import (
	"encoding/json"

	"morgans/common"

	"k8s.io/api/core/v1"
	"k8s.io/client-go/rest"
)

type NodeResource struct {
	ResourceMeta `json:",inline"`
	Spec         v1.NodeSpec   `json:"spec,omitempty"`
	Status       v1.NodeStatus `json:"status,omitempty"`
}

func RestResultToNodes(result *rest.Result) ([]v1.Node, error) {
	data, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var nodes v1.NodeList
	if err := json.Unmarshal(data, &nodes); err != nil {
		return nil, err
	}
	return nodes.Items, nil
}
