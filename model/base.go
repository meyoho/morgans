package model

import (
	"encoding/json"

	"morgans/common"

	"github.com/Jeffail/gabs"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
)

type ResourceBase struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

type ResourceMeta struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
}

func (r *ResourceMeta) GetGroup() string {
	return r.APIVersion
}

func (r *ResourceMeta) GetType() string {
	return common.GetKubernetesTypeFromKind(r.Kind)
}

func (r *ResourceMeta) GetName() string {
	return r.Name
}

func (r *ResourceMeta) GetNamespace() string {
	return r.Namespace
}

func (r *ResourceMeta) GetSubType() string {
	return ""
}

func (r *ResourceMeta) GetPatchType() string {
	return ""
}

func (r *ResourceMeta) GetResourceVersion() string {
	return r.ResourceVersion
}

type ResourceList struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
	Items             []*ResourceItem `json:"items,omitempty"`
}

type ResourceItem struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
	Data              map[string]interface{} `json:"data,omitempty"`
	Spec              map[string]interface{} `json:"spec,omitempty"`
	Status            map[string]interface{} `json:"status,omitempty"`
	GabsContainer     *gabs.Container        `json:"-"`
}

func (r *ResourceItem) GetStringByPath(path string) (string, error) {
	if r.GabsContainer == nil {
		body, err := json.Marshal(*r)
		if err != nil {
			return "", common.AnnotateError(err, "marshal resource item error")
		}
		r.GabsContainer, err = gabs.ParseJSON([]byte(body))
		if err != nil {
			return "", common.AnnotateError(err, "parse json error")
		}
	}
	value, ok := r.GabsContainer.Path(path).Data().(string)
	if !ok {
		return value, common.NewError(common.CodeInternalError, "get path error")
	}
	return value, nil
}

func RestToResourceItem(result *rest.Result) (*ResourceItem, error) {
	body, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var object ResourceItem
	if err := json.Unmarshal(body, &object); err != nil {
		return nil, err
	}
	return &object, nil
}

func RestToResourceList(result *rest.Result) ([]*ResourceItem, error) {
	body, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var object ResourceList
	if err := json.Unmarshal(body, &object); err != nil {
		return nil, err
	}
	return object.Items, nil
}
