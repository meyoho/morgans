package model

import (
	"encoding/json"
	"fmt"
	"strings"

	"morgans/common"

	"k8s.io/client-go/rest"
)

const (
	DefaultNamespace = "default"

	// Resource kind
	KindServiceMonitor = "ServiceMonitor"
	KindService        = "Service"
	KindEndpoints      = "Endpoints"
)

var (
	// Resources belong to apps
	ApplicationResourcesSet = []string{
		"deployment",
		"statefulset",
		"daemonset",
	}
	// Reource apiVersion map
	ResourceApiversionMap = map[string]string{
		"deployment":     "apps/v1",
		"daemonset":      "apps/v1",
		"statefulset":    "apps/v1",
		"prometheusrule": "monitoring.coreos.com/v1",
		"prometheus":     "monitoring.coreos.com/v1",
		"configmap":      "v1",
		"namespace":      "v1",
		"node":           "v1",
		"notification":   "aiops.alauda.io/v1beta1",
		"alerttemplate":  "aiops.alauda.io/v1beta1",
	}
	// Resource namespeaced or not
	ResourceNamespaced = map[string]bool{
		"deployment":               true,
		"statefulset":              true,
		"daemonset":                true,
		"replicaset":               true,
		"prometheusrule":           true,
		"prometheus":               true,
		"configmap":                true,
		"namespace":                false,
		"node":                     false,
		"customresourcedefinition": false,
		"notification":             true,
		"alerttemplate":            false,
	}
)

type KubernetesResource map[string]interface{}

func RestResultToResource(result *rest.Result) (*KubernetesResource, error) {
	bt, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var res KubernetesResource
	if err := json.Unmarshal(bt, &res); err != nil {
		return nil, err
	}
	return &res, nil
}

func GetResourceApiVersion(resourceType string) string {
	return ResourceApiversionMap[resourceType]
}

func GetResourceNamespace(resourceType, namespace string) string {
	if ResourceNamespaced[resourceType] {
		return namespace
	}
	return ""
}

func (kr KubernetesResource) GetGroup() string {
	if version, ok := kr["apiVersion"]; ok {
		return version.(string)
	}
	panic(fmt.Sprintf("apiVersion not found in resource."))
}

func (kr KubernetesResource) GetType() string {
	if kind, ok := kr["kind"]; ok {
		return common.GetKubernetesTypeFromKind(kind.(string))
	}
	panic(fmt.Sprintf("kind not found in resource."))
}

func (kr KubernetesResource) GetName() string {
	if meta, ok := kr["metadata"]; ok {
		if name, ok := meta.(map[string]interface{})["name"]; ok {
			return name.(string)
		}
	}
	panic(fmt.Sprintf("name not found in resource."))
}

func (kr KubernetesResource) GetSubType() string {
	return ""
}

func (kr KubernetesResource) GetNamespace() string {
	if meta, ok := kr["metadata"]; ok {
		if namespace, ok := meta.(map[string]interface{})["namespace"]; ok {
			return namespace.(string)
		}
		if ResourceNamespaced[strings.ToLower(kr["kind"].(string))] {
			return DefaultNamespace
		} else {
			return ""
		}
	}
	panic(fmt.Sprintf("namespace not found in resource."))
}

func (kr KubernetesResource) GetPatchType() string {
	return ""
}

func (kr KubernetesResource) GetResourceVersion() string {
	if meta, ok := kr["metadata"]; ok {
		if rv, ok := meta.(map[string]interface{})["resourceVersion"]; ok {
			return rv.(string)
		}
	}
	panic(fmt.Sprintf("resourceVersion not found in resource."))
}
