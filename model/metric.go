package model

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	"morgans/common"
)

const (
	// Metric function names
	FunctionMax = "max"
	FunctionMin = "min"

	// Query id key for label in metric
	QueryIDLabelKey = "__query_id__"
)

var (
	// Aggregator options and filters options
	Aggregators = []string{"avg", "min", "max", "sum", ""}
	Filters     = []string{"EQUAL", "NOT_EQUAL", "IN", "NOT_IN"}

	// Inverse functions for aggregator functions
	InverseFunctions = map[string]string{
		FunctionMax: FunctionMin,
		FunctionMin: FunctionMax,
	}
	// Indicators which need transition before
	IndicatorsNeedTranslate = []string{
		NodeResourceRequestCPUUtilization,
		NodeResourceRequestMemoryUtilization,
	}
)

type QueryMeta struct {
	ID              string
	Indicator       string
	Kind            string
	Name            string
	Range           int64
	Expr            string
	Function        string
	InverseFunction string
	Query           string
	ClusterName     string
	ClusterUUID     string
	Namespace       string
	AliasName       string
	Application     string
	IndicatorMap    map[string]*IndicatorItem
}

type QueryRequest struct {
	Time    int64             `json:"time,omitempty"`
	Queries []QueryExpression `json:"queries"`
}

type QueryRangeRequest struct {
	Start   int64             `json:"start,omitempty"`
	End     int64             `json:"end,omitempty"`
	Step    int64             `json:"step,omitempty"`
	Queries []QueryExpression `json:"queries"`
}

type QueryExpression struct {
	ID         string            `json:"id,omitempty"`
	Aggregator string            `json:"aggregator"`
	Range      int64             `json:"range"`
	Labels     []LabelExpression `json:"labels"`
}

type LabelExpression struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

func (m *QueryMeta) GetQuery() (string, error) {
	if m.Indicator == Custom {
		if m.Expr != "" {
			return m.Expr, nil
		} else {
			return "", common.NewError(common.CodeBadRequest, "prometheus query expr not found for custom indicator")
		}
	}

	var query string
	kind := strings.ToLower(m.Kind)
	item, ok := m.IndicatorMap[m.Indicator]
	if !ok {
		return "", fmt.Errorf("indicator %s not found", m.Indicator)
	}

	switch Kind(kind) {
	case PodKind:
		query = strings.Replace(item.Query, "{{.PodNamePattern}}", m.Name, -1)
	case DeploymentKind, DaemonSetKind, StatefulSetKind:
		query = strings.Replace(item.Query, "{{.PodNamePattern}}", WorkloadSet[Kind(kind)].PodNamePattern, -1)
	case NodeKind, ClusterKind:
		query = item.Query
	default:
		return "", fmt.Errorf("kind %s not found", kind)
	}

	t, err := template.New("query").Parse(query)
	if err != nil {
		return "", err
	}

	result := bytes.NewBufferString("")
	if err = t.Execute(result, m); err != nil {
		return "", err
	}
	return strings.TrimSpace(result.String()), nil
}

func (m *QueryMeta) GetUnit() Unit {
	kind := strings.ToLower(m.Kind)
	item, ok := m.IndicatorMap[m.Indicator]
	if !ok {
		return ""
	}
	if _, ok := WorkloadSet[Kind(kind)]; ok {
		return item.Unit
	}
	if string(item.Kind) != kind {
		return ""
	}
	return item.Unit
}

func (qe *QueryExpression) GetMeta(r *Cluster) *QueryMeta {
	meta := QueryMeta{}
	query := qe
	meta.ID = qe.ID
	if query.Aggregator != "" {
		meta.Function = query.Aggregator
	}
	if query.Range == 0 {
		meta.Range = 60
	} else {
		meta.Range = query.Range
	}
	inverse, ok := InverseFunctions[meta.Function]
	if ok {
		meta.InverseFunction = inverse
	} else {
		meta.InverseFunction = meta.Function
	}
	meta.ClusterName = r.Name
	meta.ClusterUUID = r.UUID
	for _, label := range query.Labels {
		switch label.Name {
		case AlertIndicatorLabel:
			meta.Indicator = label.Value
		case AlertNameLabel:
			meta.Name = label.Value
		case AlertKindLabel:
			meta.Kind = strings.ToLower(label.Value)
		case AlertNamespaceLabel:
			meta.Namespace = label.Value
		case AlertExprLabel:
			meta.Expr = label.Value
		case AlertApplicationLabel:
			meta.Application = label.Value
		case AlertQueryLabel:
			meta.Query = label.Value
		default:
		}
	}
	return &meta
}
