package model

type Integration struct {
	ID          string             `json:"id"`
	Description string             `json:"description"`
	Type        string             `json:"type"`
	Fields      *IntegrationFields `json:"fields"`
	Enabled     bool               `json:"enabled"`
	//CreatedAt   time.Time          `json:"created_at"`
	//UpdatedAt   time.Time          `json:"updated_at"`
}

type IntegrationFields struct {
	Name                 string `json:"name"`
	Namespace            string `json:"namespace"`
	PrometheusURL        string `json:"prometheus_url"`
	PrometheusTimeout    int    `json:"prometheus_timeout"`
	GrafanaURL           string `json:"grafana_url"`
	GrafanaAdminUser     string `json:"grafana_admin_user"`
	GrafanaAdminPassword string `json:"grafana_admin_password"`
}
