package model

type NotificationMessageResource struct {
	ResourceMeta `json:",inline"`
	Spec         NotificationMessageSpec `json:"spec,omitempty"`
}

type NotificationMessageSpec struct {
	Notifications []NotificationItem `json:"notifications"`
	Body          interface{}        `json:"body"`
}

type NotificationItem struct {
	Name string `json:"name"`
}
