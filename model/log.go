package model

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	elasticV6 "gopkg.in/olivere/elastic.v6"

	"morgans/config"
)

const (
	DefaultLogPageSize = 30

	AggsTimeName  = "aggs_by_time"
	AggsFieldName = "field_dict"

	ONE_DAY_SECONDS float64 = 60 * 60 * 24
)

var LogIndexPrefix string

func init() {
	LogIndexPrefix = config.GlobalConfig.ElasticSearch.LogPrefix
}

type LogRequest struct {
	StartTime     float64
	EndTime       float64
	RootAccount   string
	Nodes         string
	PodNames      string
	RegionNames   string
	K8sNamespaces string
	AppNames      string
	ContainerID8s string
	ProjectNames  string
	Paths         string
	Sources       string
	Components    string
	QueryString   string

	Fields []string

	Size int
	Page int

	LogId     string
	LogType   string
	LogIndex  string
	Direction string
}

func (req *LogRequest) UpdateByLogResource(r *LogResource) {
	req.RegionNames = r.Spec.Data.RegionName
	req.ProjectNames = r.Spec.Data.ProjectName
	req.AppNames = r.Spec.Data.AppName
	req.K8sNamespaces = r.Spec.Data.K8sNamespace
	req.Nodes = r.Spec.Data.Node
	req.PodNames = r.Spec.Data.PodName
	req.Paths = r.Spec.Data.FileName
	req.Sources = r.Spec.Data.Source
}

func (req *LogRequest) buildQueries(includeBoundes ...bool) []elasticV6.Query {
	queries := make([]elasticV6.Query, 0)

	if len(req.RootAccount) > 0 {
		orgTermsQuery := elasticV6.NewTermsQuery("root_account.keyword", req.RootAccount)
		queries = append(queries, orgTermsQuery)
	}
	if len(req.ProjectNames) > 0 {
		projectTermsQuery := elasticV6.NewTermsQuery("project_name.keyword", getTerms(req.ProjectNames)...)
		queries = append(queries, projectTermsQuery)
	}
	if len(req.RegionNames) > 0 {
		regionTermsQuery := elasticV6.NewTermsQuery("region_name.keyword", getTerms(req.RegionNames)...)
		queries = append(queries, regionTermsQuery)
	}
	if len(req.Nodes) > 0 {
		nodeTermsQuery := elasticV6.NewTermsQuery("node.keyword", getTerms(req.Nodes)...)
		queries = append(queries, nodeTermsQuery)
	}
	if len(req.K8sNamespaces) > 0 {
		k8sNsTermsQuery := elasticV6.NewTermsQuery("kubernetes_namespace.keyword", getTerms(req.K8sNamespaces)...)
		queries = append(queries, k8sNsTermsQuery)
	}
	if len(req.AppNames) > 0 {
		appNameTermsQuery := elasticV6.NewTermsQuery("application_name.keyword", getTerms(req.AppNames)...)
		queries = append(queries, appNameTermsQuery)
	}
	if len(req.PodNames) > 0 {
		podNameTermsQuery := elasticV6.NewTermsQuery("pod_name.keyword", getTerms(req.PodNames)...)
		queries = append(queries, podNameTermsQuery)
	}
	if len(req.ContainerID8s) > 0 {
		containerIdTermsQuery := elasticV6.NewTermsQuery("container_id8.keyword", getTerms(req.ContainerID8s)...)
		queries = append(queries, containerIdTermsQuery)

	}
	if len(req.Paths) > 0 {
		pathTermsQuery := elasticV6.NewTermsQuery("file_name.keyword", getTerms(req.Paths)...)
		queries = append(queries, pathTermsQuery)
	}
	if len(req.Sources) > 0 {
		sourceTermsQuery := elasticV6.NewTermsQuery("source.keyword", getTerms(req.Sources)...)
		queries = append(queries, sourceTermsQuery)
	}
	if len(req.Components) > 0 {
		componentTermsQuery := elasticV6.NewTermsQuery("component.keyword", getTerms(req.Components)...)
		queries = append(queries, componentTermsQuery)
	}

	if len(req.QueryString) > 0 {
		matchQuery := elasticV6.NewMatchQuery("log_data", req.QueryString).Operator("AND")
		queries = append(queries, matchQuery)
	}

	if req.StartTime > 0 && req.EndTime > 0 && req.StartTime <= req.EndTime {
		rangeQuery := elasticV6.NewRangeQuery("time").Gte(req.StartTime * 1000000).Lt(req.EndTime * 1000000)
		if len(includeBoundes) == 1 {
			rangeQuery.IncludeLower(includeBoundes[0]).IncludeUpper(includeBoundes[0])
		} else if len(includeBoundes) == 2 {
			rangeQuery.IncludeLower(includeBoundes[0]).IncludeUpper(includeBoundes[1])
		}
		queries = append(queries, rangeQuery)
	}

	return queries
}

func (req *LogRequest) buildBaseSource() *elasticV6.SearchSource {
	searchSource := elasticV6.NewSearchSource()

	boolQuery := elasticV6.NewBoolQuery()
	boolQuery.Filter(req.buildQueries()...)

	return searchSource.Query(boolQuery)
}

func (req *LogRequest) BuildSearchSource() *elasticV6.SearchSource {
	searchSource := req.buildBaseSource()

	var from = (req.Page - 1) * req.Size
	if req.Page < 1 {
		from = 0
	}

	searchSource.From(from).
		Size(req.Size).
		Sort("time", false)

	if len(req.QueryString) > 0 {
		highlightField := elasticV6.NewHighlighterField("log_data").NumOfFragments(0)
		highlight := elasticV6.NewHighlight().Fields(highlightField).BoundaryChars("200")
		highlight.Encoder("html")
		searchSource.Highlight(highlight)
	}

	return searchSource
}

func (req *LogRequest) BuildAggregationsSource() *elasticV6.SearchSource {
	searchSource := req.buildBaseSource()

	aggs := elasticV6.NewDateHistogramAggregation()
	aggs.Field("time")
	aggs.MinDocCount(0)
	interval := fmt.Sprintf("%ds", int((req.EndTime-req.StartTime)*1000/30))
	aggs.Interval(interval)
	aggs.ExtendedBoundsMin(req.StartTime * 1000000)
	aggs.ExtendedBoundsMax(req.EndTime * 1000000)

	searchSource.Aggregation(AggsTimeName, aggs)

	searchSource.Size(0)

	return searchSource
}

func (req *LogRequest) BuildFieldAggregationsSource(filedName string) *elasticV6.SearchSource {
	searchSource := req.buildBaseSource()

	aggs := elasticV6.NewTermsAggregation()
	aggs.Field(filedName)
	aggs.Size(65535)

	searchSource.Aggregation(AggsFieldName, aggs)

	searchSource.Size(0)

	return searchSource
}

func (req *LogRequest) BuildContextSource(sortValues []interface{}, ascending bool) *elasticV6.SearchSource {
	searchSource := elasticV6.NewSearchSource()

	boolQuery := elasticV6.NewBoolQuery()
	boolQuery.Must(req.buildQueries(false)...)

	timeSorter := elasticV6.NewFieldSort("time").Order(ascending)
	docSorter := elasticV6.NewFieldSort("_doc").Order(!ascending)

	searchSource.Query(boolQuery).
		Size(req.Size).
		SortBy(timeSorter, docSorter).
		SearchAfter(sortValues...)

	return searchSource
}

func getTerms(source string) []interface{} {
	words := strings.Split(source, ",")
	terms := make([]interface{}, len(words))
	for index, value := range words {
		terms[index] = value
	}

	return terms
}

type LogRecord struct {
	*LogResource
}

type LogResource struct {
	// ResourceMeta `json:",inline"`
	Spec LogSpec `json:"spec,omitempty"`
}

type LogSpec struct {
	Data LogData `json:"data"`
}
type LogData struct {
	CreatedAt    float64           `json:"time,omitemtpy"`
	Node         string            `json:"node,omitemtpy"`
	Nodes        string            `json:"nodes,omitemtpy"`
	PodName      string            `json:"pod_name,omitemtpy"`
	PodID        string            `json:"pod_id,omitemtpy"`
	ContainerID  string            `json:"container_id,omitemtpy"`
	LogData      string            `json:"log_data,omitemtpy"`
	LogLevel     int8              `json:"log_level,omitemtpy"`
	FileName     string            `json:"file_name,omitemtpy"`
	DockerName   string            `json:"docker_container_name,omitemtpy"`
	K8sName      string            `json:"kubernetes_container_name,omitemtpy"`
	RegionName   string            `json:"region_name,omitemtpy"`
	RegionID     string            `json:"region_id,omitemtpy"`
	ContainerID8 string            `json:"container_id8,omitemtpy"`
	Timestamp    string            `json:"@timestamp,omitemtpy"`
	K8sNamespace string            `json:"kubernetes_namespace,omitemtpy"`
	RootAccount  string            `json:"root_account,omitemtpy"`
	K8sLabels    map[string]string `json:"kubernetes_labels,omitemtpy"`
	AppName      string            `json:"application_name,omitemtpy"`
	ProjectName  string            `json:"project_name,omitemtpy"`
	Path         string            `json:"paths,omitemtpy"`
	LogId        string            `json:"log_id,omitempty"`
	LogType      string            `json:"log_type,omitempty"`
	LogIndex     string            `json:"log_index,omitempty"`
	Source       string            `json:"source,omitempty"`
	Component    string            `json:"component,omitempty"`
}

func SearchHitToLogResource(hit *elasticV6.SearchHit) (*LogResource, error) {
	var log LogData
	err := json.Unmarshal(*hit.Source, &log)
	if err != nil {
		return nil, err
	}
	log.LogIndex = hit.Index
	log.LogType = hit.Type
	log.LogId = hit.Id
	log.CreatedAt = log.CreatedAt / 1000000 // convert to seconds

	// replace back from & to dot in labels
	if len(log.K8sLabels) > 0 {
		labels := make(map[string]string)
		for k, v := range log.K8sLabels {
			labels[strings.Replace(k, "&", ".", -1)] = v
		}
		log.K8sLabels = labels
	}

	logResource := &LogResource{
		Spec: LogSpec{
			Data: log,
		},
	}

	return logResource, nil
}

type LogResourceList struct {
	// ResourceMeta `json:",inline"`
	Items      []*LogResource `json:"items,omitempty"`
	TotalItems int64          `json:"total_items"`
	TotalPage  int64          `json:"total_page"`
}

func SearchResultToLogResources(searchResult *elasticV6.SearchResult) (*LogResourceList, error) {
	result := &LogResourceList{TotalItems: 0}
	if searchResult == nil || searchResult.Hits == nil {
		return result, nil
	}

	result.TotalItems = searchResult.Hits.TotalHits

	for _, hit := range searchResult.Hits.Hits {
		logResource, err := SearchHitToLogResource(hit)
		if err != nil {
			return nil, err
		}
		result.Items = append(result.Items, logResource)
	}

	return result, nil
}

type LogTimeAggregations struct {
	Time  float64 `json:"time,omitempty"`
	Count *int64  `json:"count,omitempty"`
}

type LogAggregationsList struct {
	Buckets []*LogTimeAggregations `json:"items,omitempty"`
}

func SearchResultToLogAggregations(searchResult *elasticV6.SearchResult) (*LogAggregationsList, error) {
	var result = &LogAggregationsList{}
	if searchResult == nil || searchResult.Hits == nil {
		return result, nil
	}

	rawJson, ok := searchResult.Aggregations[AggsTimeName]
	if !ok {
		return nil, errors.New("Es aggregations result does not contains request aggregation")
	}

	var buckets elasticV6.AggregationBucketHistogramItems
	err := json.Unmarshal(*rawJson, &buckets)
	if err != nil {
		return nil, err
	}

	for _, bucket := range buckets.Buckets {
		result.Buckets = append(result.Buckets, &LogTimeAggregations{
			Count: &bucket.DocCount,
			Time:  bucket.Key / 1000000, // convert microsecond to second
		})
	}

	return result, nil
}

type LogTypeAggregations struct {
	Items []map[string][]string `json:"items,omitempty"`
}
