package model

import (
	"time"
)

type AlertRouter struct {
	Alerts []*AlertItem `json:"alerts"`
}

type AlertItem struct {
	Status      string            `json:"status"`
	Labels      map[string]string `json:"labels"`
	Annotations map[string]string `json:"annotations"`
	StartsAt    time.Time         `json:"startsAt"`
	EndsAt      time.Time         `json:"endsAt"`
}
