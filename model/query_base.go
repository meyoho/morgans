package model

var (
	IndicatorMapBase = map[string]*IndicatorItem{}
	IndicatorSetBase = []*IndicatorItem{
		// Workload indicators query expressions, all the indicators can be divided into three categories:
		// * Workload resource indicators
		// * Pod resource indicators
		// * Container resource indicators
		{
			Name:               WorkloadLogKeywordCount,
			Kind:               WorkloadKind,
			Type:               TypeLog,
			Query:              `avg(increase(workload_log_keyword_count{cluster_name="{{.ClusterName}}",kind=~"(?i:({{.Kind}}))",name="{{.Name}}",namespace="{{.Namespace}}",application="{{.Application}}",query="{{.Query}}"}[{{.Range}}s]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Workload的日志条数，通过关键字进行检索",
				EN: "Workload log metrics retrieved by key word",
			},
		},
		{
			Name:               WorkloadEventReasonCount,
			Kind:               WorkloadKind,
			Type:               TypeEvent,
			Query:              `round(avg(increase(workload_event_reason_count{cluster_name="{{.ClusterName}}",kind="{{.Kind}}",name="{{.Name}}",namespace="{{.Namespace}}",application="{{.Application}}",query="{{.Query}}"}[{{.Range}}s])))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Workload的事件条数，通过Reason字段进行检索",
				EN: "Workload event metrics retrieved by reason field",
			},
		},
		{
			Name: WorkloadCPUUtilization,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (deployment_name) ({{.Function}}_over_time(container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}[{{.Range}}s])) / sum by (deployment_name) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}) * 100000
				    {{else}}
				        sum by (deployment_name) (container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}) / sum by (deployment_name) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}) * 100000
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload的CPU使用率",
				EN: "Workload cpu utilization",
			},
		},
		{
			Name: WorkloadMemoryUtilization,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (deployment_name) ({{.Function}}_over_time(container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"}[{{.Range}}s])) / sum by (deployment_name) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"})
				    {{else}}
				        sum by (deployment_name) (container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"}) / sum by (deployment_name) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"})
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload的内存使用率",
				EN: "Workload memory utilization",
			},
		},
		{
			Name: WorkloadNetworkReceiveBytes,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (deployment_name) ({{.Function}}_over_time(container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
                        sum by (deployment_name) (container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"})
				    {{end}}`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload过去5分钟内的平均网络接收字节速率",
				EN: "Workload received bytes rate in the last 5 minutes",
			},
		},
		{
			Name: WorkloadNetworkTransmitBytes,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (deployment_name) ({{.Function}}_over_time(container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod_name=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
                        sum by (deployment_name) (container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod_name=~"{{.PodNamePattern}}"})
				    {{end}}`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload过去5分钟内的平均网络发送字节速率",
				EN: "Workload transmit bytes rate in the last 5 minutes",
			},
		},
		{
			Name: WorkloadReplicasAvailable,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if eq .Kind "DeploymentKind"}}
						min(kube_deployment_status_replicas_available{deployment="{{.Name}}",namespace="{{.Namespace}}"})
				    {{else if eq .Kind "DaemonSetKind" }}
                        min(kube_daemonset_status_number_available{daemonset="{{.Name}}",namespace="{{.Namespace}}"})
				    {{else}}
						min(kube_statefulset_status_replicas_ready{statefulset="{{.Name}}",namespace="{{.Namespace}}"})
                    {{end}}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload可用的副本数",
				EN: "Available replicas for workload",
			},
		},
		{
			Name: WorkloadReplicasDesired,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if eq .Kind "DeploymentKind"}}
						min(kube_deployment_spec_replicas{deployment="{{.Name}}",namespace="{{.Namespace}}"}) without (instance, pod)
				    {{else if eq .Kind "DaemonSetKind" }}
                        min(kube_daemonset_status_desired_number_scheduled{daemonset="{{.Name}}",namespace="{{.Namespace}}"})
				    {{else}}
						min(kube_statefulset_status_replicas{statefulset="{{.Name}}",namespace="{{.Namespace}}"})
                    {{end}}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload期望的副本数",
				EN: "Desired replicas for workload",
			},
		},
		{
			Name: WorkloadReplicasHealth,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if eq .Kind "DeploymentKind"}}
						min(kube_deployment_status_replicas_available{deployment="{{.Name}}",namespace="{{.Namespace}}"}) / min(kube_deployment_spec_replicas{deployment="{{.Name}}",namespace="{{.Namespace}}"})
				    {{else if eq .Kind "DaemonSetKind" }}
                        min(kube_daemonset_status_number_available{daemonset="{{.Name}}",namespace="{{.Namespace}}"}) / min(kube_daemonset_status_desired_number_scheduled{daemonset="{{.Name}}",namespace="{{.Namespace}}"})
				    {{else}}
						min(kube_statefulset_status_replicas_ready{statefulset="{{.Name}}",namespace="{{.Namespace}}"}) / min(kube_statefulset_status_replicas{statefulset="{{.Name}}",namespace="{{.Namespace}}"})
                    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload的健康程度，当前可用的副本数跟期望副本数的百分比",
				EN: "Health replicas for workload",
			},
		},
		{
			Name:               WorkloadPodStatusPhaseRunning,
			Kind:               WorkloadKind,
			Type:               TypeMetric,
			Query:              `sum(kube_pod_status_phase{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",phase="Running"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload处于运行状态的Pod数",
				EN: "Running pod count for workload",
			},
		},
		{
			Name:               WorkloadPodStatusPhaseNotRunning,
			Kind:               WorkloadKind,
			Type:               TypeMetric,
			Query:              `sum(kube_pod_status_phase{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",phase!="Running"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload处于非运行状态的Pod数",
				EN: "Not running pod count for workload",
			},
		},
		{
			Name:               WorkloadPodRestartedCount,
			Kind:               WorkloadKind,
			Type:               TypeMetric,
			Query:              `sum (delta(kube_pod_container_status_restarts_total{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload过去5分钟Pod重启的次数",
				EN: "Pod restarted count for workload in the last 5 minutes",
			},
		},
		{
			Name: PodCPUUtilization,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (pod_name) ({{.Function}}_over_time(container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}[{{.Range}}s])) / sum by (pod_name) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}) * 100000
				    {{else}}
				        sum by (pod_name) (container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}) / sum by (pod_name) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}) * 100000
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Vector,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload的CPU使用率，按照Pod进行分组",
				EN: "Pod cpu utilization",
			},
		},
		{
			Name: PodMemoryUtilization,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by(pod_name) ({{.Function}}_over_time(container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"}[{{.Range}}s])) / sum by (pod_name) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"})
				    {{else}}
				        sum by(pod_name) (container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"}) / sum by (pod_name) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"})
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Vector,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload的内存使用率，按照Pod进行分组",
				EN: "Pod memory utilization",
			},
		},
		{
			Name: PodNetworkReceiveBytes,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (pod_name) ({{.Function}}_over_time(container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
				        sum by (pod_name) (container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"})
				    {{end}}`,
			Unit:               BytesPerSecond,
			ValueType:          Vector,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload过去5分钟内的平均网络接收字节速率，按照Pod进行分组",
				EN: "Workload received bytes rate group by pod in the last 5 minutes",
			},
		},
		{
			Name: PodNetworkTransmitBytes,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (pod_name) ({{.Function}}_over_time(container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod_name=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
				        sum by (pod_name) (container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod_name=~"{{.PodNamePattern}}"})
				    {{end}}`,
			Unit:               BytesPerSecond,
			ValueType:          Vector,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload过去5分钟内的平均网络发送字节速率",
				EN: "Workload received bytes rate group by pod in the last 5 minutes",
			},
		},
		{
			Name:               PodStatusPhaseRunning,
			Kind:               WorkloadKind,
			Type:               TypeMetric,
			Query:              `kube_pod_status_phase{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}",phase!="Running"}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload处于非运行状态的Pod",
				EN: "Not running pod for workload",
			},
		},
		{
			Name:               PodRestartedCount,
			Kind:               WorkloadKind,
			Type:               TypeMetric,
			Query:              `sum by (pod) (delta(kube_pod_container_status_restarts_total{namespace="{{.Namespace}}",pod=~"{{.PodNamePattern}}"}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Workload过去5分钟Pod重启的次数，按照Pod进行分组",
				EN: "Pod restarted count for workload group by pod in the last 5 minutes",
			},
		},
		{
			Name: ContainerCPUUtilization,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (pod_name, container_name) ({{.Function}}_over_time(container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}[{{.Range}}s])) / sum by (pod_name, container_name) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}) * 100000
				    {{else}}
				        sum by (pod_name, container_name) (container_cpu_usage_seconds_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}) / sum by (pod_name, container_name) (container_spec_cpu_quota{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"}) * 100000
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Vector,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "容器的CPU使用率，按照容器所属的Workload进行过滤",
				EN: "Workload cpu utilization group by container",
			},
		},
		{
			Name: ContainerMemoryUtilization,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (pod_name, container_name) ({{.Function}}_over_time(container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"}[{{.Range}}s])) / sum by (pod_name, container_name) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"})
				    {{else}}
				        sum by (pod_name, container_name) (container_memory_usage_bytes_without_cache{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"}) / sum by (pod_name, container_name) (container_spec_memory_limit_bytes{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}",image!="",container_name!="POD"})
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Vector,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "容器的内存使用率，按照容器所属的Workload进行过滤",
				EN: "Workload memory utilization group by container",
			},
		},
		{
			Name: ContainerNetworkReceiveBytes,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (pod_name, container_name) ({{.Function}}_over_time(container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
				        sum by (pod_name, container_name) (container_network_receive_bytes_total_irate5m{namespace="{{.Namespace}}",pod_name=~"{{.PodNamePattern}}"})
				    {{end}}`,
			Unit:               BytesPerSecond,
			ValueType:          Vector,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Container过去5分钟内的平均网络接收字节速率，按照容器所属的Workload进行过滤",
				EN: "Workload received bytes rate group by container in the last 5 minutes",
			},
		},
		{
			Name: ContainerNetworkTransmitBytes,
			Kind: WorkloadKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum by (pod_name, container_name) ({{.Function}}_over_time(container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod_name=~"{{.PodNamePattern}}"}[{{.Range}}s]))
				    {{else}}
				        sum by (pod_name, container_name) (container_network_transmit_bytes_total_irate5m{namespace="{{.Namespace}}", pod_name=~"{{.PodNamePattern}}"})
				    {{end}}`,
			Unit:               BytesPerSecond,
			ValueType:          Vector,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "Container过去5分钟内的平均网络发送字节速率，按照容器所属的Workload进行过滤",
				EN: "Workload received bytes rate group by container in the last 5 minutes",
			},
		},

		// Node indicators query expressions, all the indicators can be divided into three categories:
		// * Node resource indicators
		// * Node disk indicators
		// * Node network indicators
		{
			Name: NodeCPUUtilization,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(node_cpu_utilization{instance=~"{{.Name}}"}[{{.Range}}s])
				    {{else}}
				        node_cpu_utilization{instance=~"{{.Name}}"}
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机实时的CPU使用率",
				EN: "Node real-time cpu utilization",
			},
		},
		{
			Name: NodeMemoryUtilization,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(node_memory_MemUsed{instance=~"{{.Name}}"}[{{.Range}}s]) / node_memory_MemTotal{instance=~"{{.Name}}"}
				    {{else}}
				        node_memory_MemUsed{instance=~"{{.Name}}"} / node_memory_MemTotal{instance=~"{{.Name}}"}
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机实时的内存使用率",
				EN: "Node real-time memory utilization",
			},
		},
		{
			Name: NodeGPUUtilization,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
						{{.Function}}_over_time(node_gpu_utilization{node_name=~"{{.Name}}"}[{{.Range}}s])
					{{else}}
						node_gpu_utilization{node_name=~"{{.Name}}"}
					{{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机实时的物理GPU使用率",
				EN: "Node real-time gpu utilization",
			},
		},
		{
			Name: NodeGPUMemoryUtilization,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
						{{.Function}}_over_time(node_gpu_memory_utilization{node_name=~"{{.Name}}"}[{{.Range}}s])
					{{else}}
						node_gpu_memory_utilization{node_name=~"{{.Name}}"}
					{{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机实时的物理GPU内存使用率",
				EN: "Node real-time physical GPU memory utilization",
			},
		},
		{
			Name: NodeNetworkReceiveBytes,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum({{.Function}}_over_time(node_network_receive_bytes_irate5m{instance=~"{{.Name}}",device!~"docker0|cni0|flannel.1|lo|veth.*"}[{{.Range}}s])) by(instance)
				    {{else}}
				        sum(node_network_receive_bytes_irate5m{instance=~"{{.Name}}",device!~"docker0|cni0|flannel.1|lo|veth.*"}) by(instance)
				    {{end}}`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机过去5分钟网络接收字节的平均速率",
				EN: "Node network received bytes rate in the past 5 minutes",
			},
		},
		{
			Name: NodeNetworkTransmitBytes,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum({{.Function}}_over_time(node_network_transmit_bytes_irate5m{instance=~"{{.Name}}",device!~"docker0|cni0|flannel.1|lo|veth.*"}[{{.Range}}s])) by(instance)
				    {{else}}
				        sum(node_network_transmit_bytes_irate5m{instance=~"{{.Name}}",device!~"docker0|cni0|flannel.1|lo|veth.*"}) by(instance)
				    {{end}}`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机过去5分钟网络发送字节的平均速率",
				EN: "Node network transmit bytes rate in the past 5 minutes",
			},
		},
		{
			Name:               NodeUP,
			Kind:               NodeKind,
			Type:               TypeMetric,
			Query:              `up{job="node-exporter",instance=~"{{.Name}}"}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "主机的状态，1/0分别表示up/down",
				EN: "The host is up or down, by the value of up metrics in node-exporter",
			},
		},
		{
			Name: NodeLoad1,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(node_load1{instance=~"{{.Name}}"}[{{.Range}}s])
				    {{else}}
				        node_load1{instance=~"{{.Name}}"}
				    {{end}}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机过去1分钟的平均负载",
				EN: "Node load in the past 1 minutes",
			},
		},
		{
			Name: NodeLoad5,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(node_load5{instance=~"{{.Name}}"}[{{.Range}}s])
				    {{else}}
				        node_load5{instance=~"{{.Name}}"}
                    {{end}}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机过去5分钟的平均负载",
				EN: "Node load in the past 5 minutes",
			},
		},
		{
			Name: NodeLoad15,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(node_load15{instance=~"{{.Name}}"}[{{.Range}}s])
				    {{else}}
				        node_load15{instance=~"{{.Name}}"}
                    {{end}}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机过去15分钟的负载",
				EN: "Node load in the past 15 minutes",
			},
		},
		{
			Name:               NodeLoad1PerCore,
			Kind:               NodeKind,
			Type:               TypeMetric,
			Query:              `avg by (instance) (node_load1{instance=~"{{.Name}}"}) / count by(instance) (node_cpu{mode="idle"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "主机过去1分钟的归一化负载，归一化的方法用主机负载除以CPU核心数",
				EN: "Node per core load in the past 1 minutes",
			},
		},
		{
			Name:               NodeLoad5PerCore,
			Kind:               NodeKind,
			Type:               TypeMetric,
			Query:              `avg by (instance) (node_load5{instance=~"{{.Name}}"}) / count by(instance) (node_cpu{mode="idle"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "主机过去5分钟的归一化负载，归一化的方法用主机负载除以CPU核心数",
				EN: "Node per core load in the past 5 minutes",
			},
		},
		{
			Name:               NodeLoad15PerCore,
			Kind:               NodeKind,
			Type:               TypeMetric,
			Query:              `avg by (instance) (node_load15{instance=~"{{.Name}}"}) / count by(instance) (node_cpu{mode="idle"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "主机过去15分钟的归一化负载，归一化的方法用主机负载除以CPU核心数",
				EN: "Node per core load in the past 15 minutes",
			},
		},
		{
			Name: NodeDiskUtilization,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        avg by (instance, device) ({{.Function}}_over_time(node_filesystem_used{device=~"/dev/.*", instance=~"{{.Name}}"}[{{.Range}}s])) / avg by (instance, device) (node_filesystem_size{device=~"/dev/.*",instance=~"{{.Name}}"})
				    {{else}}
				        avg by (instance, device) (node_filesystem_used{device=~"/dev/.*", instance=~"{{.Name}}"}) / avg by (instance, device) (node_filesystem_size{device=~"/dev/.*",instance=~"{{.Name}}"})
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机磁盘空间利用率，按照分区名称进行分组",
				EN: "Node disk space utilization group by partition name",
			},
		},
		{
			Name: NodeDiskSpaceUtilization,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        avg by (instance, device) ({{.Function}}_over_time(node_filesystem_used{device=~"/dev/.*", instance=~"{{.Name}}"}[{{.Range}}s])) / avg by (instance, device) (node_filesystem_size{device=~"/dev/.*",instance=~"{{.Name}}"})
				    {{else}}
				        avg by (instance, device) (node_filesystem_used{device=~"/dev/.*", instance=~"{{.Name}}"}) / avg by (instance, device) (node_filesystem_size{device=~"/dev/.*",instance=~"{{.Name}}"})
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机磁盘空间利用率，按照分区名称进行分组",
				EN: "Node disk space utilization group by partition name",
			},
		},
		{
			Name:               NodeDiskInodeUtilization,
			Kind:               NodeKind,
			Type:               TypeMetric,
			Query:              `1- avg by (instance, device) (node_filesystem_files_free{device=~"/dev/.*",instance=~"{{.Name}}"}/node_filesystem_files{device=~"/dev/.*",instance=~"{{.Name}}"})`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "主机磁盘inode利用率，按照分区名称进行分组",
				EN: "Node disk inode utilization group by partition name",
			},
		},
		{
			Name: NodeDiskReadBytes,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        avg by (instance, device) ({{.Function}}_over_time(node_disk_bytes_read_irate5m{instance=~"{{.Name}}"}[{{.Range}}s]))
				    {{else}}
                        avg by (instance, device) (node_disk_bytes_read_irate5m{instance=~"{{.Name}}"})
				    {{end}}`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机读取磁盘数据的速度，按照设备名称进行分组",
				EN: "Node disk read rate group by disk name",
			},
		},
		{
			Name: NodeDiskWrittenBytes,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        avg by (instance, device) ({{.Function}}_over_time(node_disk_bytes_written_irate5m{instance=~"{{.Name}}"}[{{.Range}}s]))
				    {{else}}
                        avg by (instance, device) (node_disk_bytes_written_irate5m{instance=~"{{.Name}}"})
				    {{end}}`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机写磁盘数据的速度，按照设备名称进行分组",
				EN: "Node disk write rate group by disk name",
			},
		},
		{
			Name: NodeDiskIOTime,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        avg by (instance, device) ({{.Function}}_over_time(node_disk_io_time_ms_irate5m{instance=~"{{.Name}}"}[{{.Range}}s])) 
				    {{else}}
                        avg by (instance, device) (node_disk_io_time_ms_irate5m{instance=~"{{.Name}}"})
				    {{end}}`,
			Unit:               MicroSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机每秒中花在磁盘读写上的时间，按照设备名称进行分组",
				EN: "Node disk io time group by disk name",
			},
		},
		{
			Name: NodeResourceRequestCPUUtilization,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum({{.Function}}_over_time(node_resource_requests_cpu_cores{node=~"{{.AliasName}}"}[{{.Range}}s])) by(node) / sum(kube_node_status_allocatable_cpu_cores{node=~"{{.AliasName}}"}) by(node)
				    {{else}}
				        sum(node_resource_requests_cpu_cores{node=~"{{.AliasName}}"}) by(node) / sum(kube_node_status_allocatable_cpu_cores{node=~"{{.AliasName}}"}) by(node)
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机CPU Request使用率",
				EN: "Node CPU request utilization",
			},
		},
		{
			Name: NodeResourceRequestMemoryUtilization,
			Kind: NodeKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        sum({{.Function}}_over_time(node_resource_requests_memory_bytes{node=~"{{.AliasName}}"}[{{.Range}}s])) by(node) / sum(kube_node_status_allocatable_memory_bytes{node=~"{{.AliasName}}"}) by(node)
				    {{else}}
				        sum(node_resource_requests_memory_bytes{node=~"{{.AliasName}}"}) by(node) / sum(kube_node_status_allocatable_memory_bytes{node=~"{{.AliasName}}"}) by(node)
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "主机内存Request使用率",
				EN: "Node memory request utilization",
			},
		},
		{
			Name:               NodeNetworkIPVSActiveConnections,
			Kind:               NodeKind,
			Type:               TypeMetric,
			Query:              `sum by(instance) (node_ipvs_backend_connections_active{instance=~"{{.Name}}"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       false,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "主机当前活跃的网络连接数，包括本地连接和远程连接",
				EN: "Node ipvs active connections by local and remote address",
			},
		},

		// Cluster indicators query expressions, all the indicators can be divided into three categories:
		// * Kubernetes components indicators
		// * Kubernetes node indicators
		// * Kubernetes cluster indicators
		{
			Name: ClusterCPUUtilization,
			Kind: ClusterKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(cluster_cpu_utilization[{{.Range}}s]) / count(avg by(instance,cpu)(node_cpu{job="node-exporter",mode="idle"}))
				    {{else}}
				        cluster_cpu_utilization / count(avg by(instance,cpu)(node_cpu{job="node-exporter",mode="idle"}))
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "集群的实际CPU使用率",
				EN: "Real-time CPU utilization for cluster, cluster is treated as a set of nodes",
			},
		},
		{
			Name: ClusterMemoryUtilization,
			Kind: ClusterKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(cluster_memory_MemUsed[{{.Range}}s]) / sum(node_memory_MemTotal)
                    {{else}}
				        cluster_memory_MemUsed / sum(node_memory_MemTotal)
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "集群的实际内存使用率",
				EN: "Real-time memory utilization for cluster, cluster is treated as a set of nodes",
			},
		},
		{
			Name: ClusterGPUUtilization,
			Kind: ClusterKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
						{{.Function}}_over_time(cluster_gpu_utilization[{{.Range}}s])
					{{else}}
						cluster_gpu_utilization
					{{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "集群的物理GPU使用率",
				EN: "Real-time physical GPU utilization for cluster, cluster is treated as a set of nodes",
			},
		},
		{
			Name: ClusterGPUMemoryUtilization,
			Kind: ClusterKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
						{{.Function}}_over_time(cluster_gpu_memory_utilization[{{.Range}}s])
					{{else}}
						cluster_gpu_memory_utilization
					{{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "集群的物理GPU显存使用率",
				EN: "Real-time physical GPU memory utilization for cluster, cluster is treated as a set of nodes",
			},
		},
		{
			Name:               ClusterNodeNotReadyCount,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(kube_node_status_condition{condition="Ready",status!="true"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群中处于非Ready状态的主机总数",
				EN: "Node count which condition status is not ready in cluster",
			},
		},
		{
			Name:               ClusterNodeTotalCount,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(kube_node_status_condition{condition="Ready"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       false,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群中的主机总数",
				EN: "Node count in cluster",
			},
		},
		{
			Name: ClusterResourceRequestCPUUtilization,
			Kind: ClusterKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(cluster_resource_requests_cpu_cores[{{.Range}}s]) / sum(kube_node_status_allocatable_cpu_cores)
				    {{else}}
				        cluster_resource_requests_cpu_cores / sum(kube_node_status_allocatable_cpu_cores)
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "集群的CPU Requests使用率",
				EN: "CPU requests utilization for cluster, cluster is treated as a set of nodes",
			},
		},
		{
			Name: ClusterResourceRequestMemoryUtilization,
			Kind: ClusterKind,
			Type: TypeMetric,
			Query: `{{if .Function }}
				        {{.Function}}_over_time(cluster_resource_requests_memory_bytes[{{.Range}}s]) / sum(kube_node_status_allocatable_memory_bytes)
				    {{else}}
				        cluster_resource_requests_memory_bytes / sum(kube_node_status_allocatable_memory_bytes)
				    {{end}}`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: true,
			Annotations: map[string]string{
				CN: "集群的内存Requests使用率",
				EN: "Memory requests utilization for cluster, cluster is treated as a set of nodes",
			},
		},
		{
			Name:               ClusterKubeAPIServerUp,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `up{job="apiserver"}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的apiserver是否处于启动状态，判断的标准是apiserver是否可以接受http请求，每个apiserver都会产生一条Metric",
				EN: "The kubernetes apiserver is up or not, an instance will be treated as up if a http request can be handled by it",
			},
		},
		{
			Name:               ClusterKubeAPIServerHealth,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(up{job="apiserver"} == 1) / count(up{job="apiserver"})`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的apiserver的健康状态，即是处于启动状态的apiserver跟总的apiserver实例数的百分比",
				EN: "The kubernetes apiserver health status, that is to say the percentage of normal apiserver count and total apiserver count",
			},
		},
		{
			Name:               ClusterKubeControllerManagerUp,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `up{job="kube-controller-manager"}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的controller-manager是否处于启动状态，判断的标准是controller-manager是否可以接受http请求，每个controller-manager都会产生一条metric",
				EN: "The kubernetes controller-manager is up or not, an instance will be treated as up if a http request can be handled by it",
			},
		},
		{
			Name:               ClusterKubeControllerManagerHealth,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(up{job="kube-controller-manager"} == 1) / count(up{job="kube-controller-manager"})`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的controller-manager的健康状态，即是处于启动状态的controller-manager跟总的controller-manager实例数的百分比",
				EN: "The kubernetes controller-manager health status, that is to say the percentage of normal controller-manager count and total controller-manager count",
			},
		},
		{
			Name:               ClusterKubeSchedulerUp,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `up{job="kube-scheduler"}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的kube-scheduler是否处于启动状态，判断的标准是kube-scheduler是否可以接受http请求，每个kube-scheduler都会产生一条Metric",
				EN: "The kubernetes kube-scheduler is up or not, an instance will be treated as up if a http request can be handled by it",
			},
		},
		{
			Name:               ClusterKubeSchedulerHealth,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(up{job="kube-scheduler"} == 1) / count(up{job="kube-scheduler"})`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的kube-scheduler的健康状态，即是处于启动状态的kube-scheduler跟总的kube-scheduler实例数的百分比",
				EN: "The kubernetes kube-scheduler health status, that is to say the percentage of normal kube-scheduler count and total kube-scheduler count",
			},
		},
		{
			Name:               ClusterDockerDaemonUp,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `increase(engine_daemon_health_checks_failed_total[5m]) > 5`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       false,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群主机的docker daemon是否处于启动状态，判断的标准是daemon过去5分钟健康健康失败的次数是否大于5次",
				EN: "The docker daemon is up or not, an instance will be treated as down if 5 health check",
			},
		},
		{
			Name:               ClusterDockerDaemonHealth,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `(count(kube_node_info) - (count(increase(engine_daemon_health_checks_failed_total[5m])) > 5)) / count(kube_node_info)`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       false,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的apiserver的健康状态，即是处于启动状态的apiserver跟总的apiserver实例数的百分比",
				EN: "The kubernetes apiserver health status, that is to say the percentage of normal apiserver count and total apiserver count",
			},
		},
		{
			Name:               ClusterKubeDNSUp,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `up{job="kube-dns"}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的kube-dns是否处于启动状态，判断的标准是kube-dns是否可以接受http请求，每个kube-dns都会产生一条Metric",
				EN: "The kubernetes dns is up or not, an instance will be treated as up if a http request can be handled by it",
			},
		},
		{
			Name:               ClusterKubeDNSHealth,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(up{job="kube-dns"} == 1) / count(up{job="kube-dns"})`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的kube-dns的健康状态，即是处于启动状态的kube-dns跟总的kube-dns实例数的百分比",
				EN: "The kubernetes dns health status, that is to say the percentage of normal dns count and total dns count",
			},
		},
		{
			Name:               ClusterKubeETCDUp,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `up{job="kube-etcd"}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的kube-etcd是否处于启动状态，判断的标准是kube-etcd是否可以接受http请求，每个kube-etcd都会产生一条Metric",
				EN: "The kubernetes etcd is up or not, an instance will be treated as up if a http request can be handled by it",
			},
		},
		{
			Name:               ClusterKubeETCDHealth,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(up{job="kube-etcd"} == 1) / count(up{job="kube-etcd"})`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群的kube-etcd的健康状态，即是处于启动状态的etcd跟总的etcd实例数的百分比",
				EN: "The kubernetes etcd health status, that is to say the percentage of normal etcd count and total etcd count",
			},
		},
		{
			Name:               ClusterKubeKubeletUp,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `up{job="kubelet"}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群主机的kubelet是否处于启动状态，判断的标准是kubelet是否可以接受http请求，每个kubelet都会产生一条Metric",
				EN: "The kubernetes kubelet is up or not, an instance will be treated as up if a http request can be handled by it",
			},
		},
		{
			Name:               ClusterKubeKubeletHealth,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(up{job="kubelet"} == 1) / count(up{job="kubelet"})`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群主机的kubelet的健康状态，即是处于启动状态的kubelet跟总的kubelet实例数的百分比",
				EN: "The kubernetes kubelet health status, that is to say the percentage of normal kubelet count and total kubelet count",
			},
		},
		{
			Name:               ClusterKubeProxyUp,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `up{job="kube-proxy"}`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群主机的kube-proxy是否处于启动状态，判断的标准是kube-proxy是否可以接受http请求，每个kube-proxy都会产生一条Metric",
				EN: "The kubernetes kube-proxy is up or not, an instance will be treated as up if a http request can be handled by it",
			},
		},
		{
			Name:               ClusterKubeProxyHealth,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(up{job="kube-proxy"} == 1) / count(up{job="kube-proxy"})`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群主机的kube-proxy的健康状态，即是处于启动状态的kube-proxy跟总的kube-proxy实例数的百分比",
				EN: "The kubernetes kube-proxy health status, that is to say the percentage of normal kube-proxy count and total kube-proxy count",
			},
		},
		{
			Name:               ClusterNodeReady,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `avg by (node) (kube_node_status_condition{condition="Ready",status="true"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群主机的Status是否处于Ready状态",
				EN: "The kubernetes node condition status is ready or not",
			},
		},
		{
			Name:               ClusterNodeHealth,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(kube_node_status_condition{condition="Ready",status="true"})/sum(kube_node_status_condition{condition="Ready"})`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群主机的健康状态，即是处于Ready状态的Node跟总的Node数的百分比",
				EN: "The kubernetes nodes health status, that is to say the percentage of ready node count and total node count",
			},
		},
		{
			Name:               ClusterNodeNetworkIpvsActiveConnections,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(node_ipvs_backend_connections_active)`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       false,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群中所有主机活动的网络连接数总和，包括本地连接和远程连接",
				EN: "The sum of ipvs active connections by local and remote address for all node",
			},
		},
		{
			Name:               ClusterAlertsFiring,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `(sum(ALERTS{alertstate="firing",alertname!="DeadMansSwitch"}) or vector(0))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群中所有处于告警状态的告警数",
				EN: "The total number of firing alerts",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestLatency,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `avg(rate(apiserver_request_latencies_count{}[5m]))`,
			Unit:               Second,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理请求的平均延迟",
				EN: "Average apiserver request latency in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestLatencyPerVerb,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `avg by (verb) (rate(apiserver_request_latencies_count{}[5m]))`,
			Unit:               Second,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理请求的平均延迟，按照请求方法进行分组，每种方法会生成一条metric",
				EN: "Average apiserver request latency in the last 5 minutes group by verb",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestLatencyPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `avg by (instance) (rate(apiserver_request_latencies_count{}[5m]))`,
			Unit:               Second,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理请求的平均延迟，按照apiserver实例进行分组，每个实例会生成一条metric",
				EN: "Average apiserver request latency in the last 5 minutes group by instance",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestCount,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(apiserver_request_count{}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理的请求总数",
				EN: "Total apiserver request count in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestCountPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum by (instance) (rate(apiserver_request_count{}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理的请求总数，按照apiserver实例进行分组，每个实例会生成一条metric",
				EN: "Total apiserver request count in the last 5 minutes group by instance",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestErrorRate,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `1-sum(rate(apiserver_request_count{code=~"2.."}[5m]))/sum (rate(apiserver_request_count[5m]))`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理的请求出错率，返回码大于300的请求会被视为错误请求",
				EN: "Apiserver request error rate in the last 5 minutes, request code more than 300 will be treated as error",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestErrorRatePerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `1-sum by (instance) (rate(apiserver_request_count{code=~"2.."}[5m]))/sum by (instance) (rate(apiserver_request_count[5m]))`,
			Unit:               Percent,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理的请求出错率，返回码大于300的请求会被视为错误请求，按照apiserver实例进行分组，每个实例会生成一条metric",
				EN: "Apiserver request error rate group by instance in the last 5 minutes, request code more than 300 will be treated as error",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestCount2XX,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(apiserver_request_count{code=~"2.."}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理中返回码为2XX的请求个数",
				EN: "Http requests count with response code 2xx for apiserver",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestCount2XXPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum by (instance) (rate(apiserver_request_count{code=~"2.."}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理中返回码为2XX的请求个数，按照apiserver实例进行分组，每个实例会生成一条metric",
				EN: "Http requests count with response code 2xx for apiserver by instance",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestCount4XX,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(apiserver_request_count{code=~"4.."}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理中返回码为4XX的请求个数",
				EN: "Http requests count with response code 4xx for apiserver",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestCount4XXPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum by (instance) (rate(apiserver_request_count{code=~"4.."}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理中返回码为4XX的请求个数，按照apiserver实例进行分组，每个实例会生成一条metric",
				EN: "Http requests count with response code 4xx for apiserver by instance",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestCount5XX,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(apiserver_request_count{code=~"5.."}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理中返回码为5XX的请求个数",
				EN: "Http requests count with response code 5xx for apiserver",
			},
		},
		{
			Name:               ClusterKubeAPIServerRequestCount5XXPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum by (instance) (rate(apiserver_request_count{code=~"5.."}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "集群过去5分钟内所有apiserver处理中返回码为5XX的请求个数，按照apiserver实例进行分组，每个实例会生成一条metric",
				EN: "Http requests count with response code 5xx for apiserver by instance",
			},
		},
		{
			Name:               ClusterKubeETCDHasLeader,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `max(etcd_server_has_leader{namespace="kube-system"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD集群是不是有leader",
				EN: "The kubernetes etcd cluster has leader or not",
			},
		},
		{
			Name:               ClusterKubeETCDLeaderElections,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `changes(etcd_server_leader_changes_seen_total{namespace="kube-system"}[5m])`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD集群过去5分钟发生leader选举的次数",
				EN: "The kubernetes etcd cluster leader elections in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDMembers,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(etcd_server_has_leader{namespace="kube-system"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD集群的成员数",
				EN: "The kubernetes etcd cluster members",
			},
		},
		{
			Name:               ClusterKubeETCDDBSIZE,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(etcd_mvcc_db_total_size_in_bytes{namespace="kube-system"})`,
			Unit:               Bytes,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD所有实例的DB的大小之和",
				EN: "Sum of DB size for all kubernetes etcd cluster members",
			},
		},
		{
			Name:               ClusterKubeETCDDBSIZEPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `etcd_mvcc_db_total_size_in_bytes{namespace="kube-system"}`,
			Unit:               Bytes,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD实例的DB大小",
				EN: "DB size for all kubernetes etcd cluster members",
			},
		},
		{
			Name:               ClusterKubeETCDRPCRate,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(grpc_server_started_total{namespace="kube-system",grpc_type="unary"}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟的RPC次数",
				EN: "RPC rate for all kubernetes etcd cluster members in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDRPCFailedRate,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(grpc_server_handled_total{namespace="kube-system",grpc_type="unary",grpc_code!="OK"}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟的RPC失败的次数",
				EN: "RPC failed rate for all kubernetes etcd cluster members in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDActiveWatchStream,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(grpc_server_started_total{namespace="kube-system",grpc_service="etcdserverpb.Watch",grpc_type="bidi_stream"}) - sum(grpc_server_handled_total{namespace="kube-system",grpc_service="etcdserverpb.Watch",grpc_type="bidi_stream"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD当前还在活跃的Watcher数",
				EN: "Watchers count for all kubernetes etcd cluster members",
			},
		},
		{
			Name:               ClusterKubeETCDActiveLeaseStream,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(grpc_server_started_total{namespace="kube-system",grpc_service="etcdserverpb.Lease",grpc_type="bidi_stream"}) - sum(grpc_server_handled_total{namespace="kube-system",grpc_service="etcdserverpb.Lease",grpc_type="bidi_stream"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD当前还在活跃的Lease数",
				EN: "Watchers count for all kubernetes etcd cluster members",
			},
		},
		{
			Name:               ClusterKubeETCDRaftProposalCommittedRate,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(etcd_server_proposals_committed_total{namespace="kube-system"}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟Raft提议提交的速率",
				EN: "Raft proposal committed rate for all kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDRaftProposalCommittedRate,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(etcd_server_proposals_committed_total{namespace="kube-system"}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟Raft提议提交的速率",
				EN: "Raft proposal committed rate for all kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDRaftProposalAppliedRate,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(etcd_server_proposals_applied_total{namespace="kube-system"}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟Raft提议应用的速率",
				EN: "Raft proposal applied rate for all kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDRaftProposalFailedRate,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(etcd_server_proposals_failed_total{namespace="kube-system"}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟Raft提议失败的次数",
				EN: "Raft proposal failed rate for all kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDRaftProposalPendingRate,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(etcd_server_proposals_pending{namespace="kube-system"}[5m]))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟Raft提议排队的速率",
				EN: "Raft proposal pending rate for all kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDWALFsyncDurationPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `histogram_quantile(0.99, sum(rate(etcd_disk_wal_fsync_duration_seconds_bucket{namespace="kube-system"}[5m])) by (instance, le))`,
			Unit:               Second,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟WAL同步的平均时间，按照实例进行分组，每个实例会生成一条metric",
				EN: "WAL average fsync duration for kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDDBFsyncDurationPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `histogram_quantile(0.99, sum(rate(etcd_disk_backend_commit_duration_seconds_bucket{namespace="kube-system"}[5m])) by (instance, le))`,
			Unit:               Second,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟DB同步的平均时间，按照实例进行分组，每个实例会生成一条metric",
				EN: "DB average fsync duration for kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDClientTrafficIn,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(etcd_network_client_grpc_received_bytes_total{namespace="kube-system"}[5m]))`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟client网络流量的接收速率",
				EN: "Client traffic in rate for kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDClientTrafficInPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `rate(etcd_network_client_grpc_received_bytes_total{namespace="kube-system"}[5m])`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟client网络流量的接收速率，按照实例进行分组，每个实例会生成一条metric",
				EN: "Client traffic in rate for kubernetes etcd cluster group by instance in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDClientTrafficOut,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(etcd_network_client_grpc_sent_bytes_total{namespace="kube-system"}[5m]))`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟client网络流量的发送速率",
				EN: "Client traffic out rate for kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDClientTrafficOutPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `rate(etcd_network_client_grpc_sent_bytes_total{namespace="kube-system"}[5m])`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟client网络流量的发送速率，按照实例进行分组，每个实例会生成一条metric",
				EN: "Client traffic out rate for kubernetes etcd cluster group by instance in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDPeerTrafficIn,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(etcd_network_peer_grpc_received_bytes_total{namespace="kube-system"}[5m]))`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟peer之间网络流量的接收速率",
				EN: "Peer traffic in rate for kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDPeerTrafficInPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `rate(etcd_network_peer_grpc_received_bytes_total{namespace="kube-system"}[5m])`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟peer之间网络流量的接收速率，按照实例进行分组，每个实例会生成一条metric",
				EN: "Peer traffic in rate for kubernetes etcd cluster group by instance in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDPeerTrafficOut,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(rate(etcd_network_peer_grpc_sent_bytes_total{namespace="kube-system"}[5m]))`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟peer之间网络流量的发送速率",
				EN: "Peer traffic out rate for kubernetes etcd cluster in the last 5 minutes",
			},
		},
		{
			Name:               ClusterKubeETCDPeerTrafficOutPerInstance,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `rate(etcd_network_peer_grpc_sent_bytes_total{namespace="kube-system"}[5m])`,
			Unit:               BytesPerSecond,
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes的ETCD过去5分钟peer之间网络流量的发送速率，按照实例进行分组，每个实例会生成一条metric",
				EN: "Peer traffic out rate for kubernetes etcd cluster group by instance in the last 5 minutes",
			},
		},
		{
			Name:               ClusterPodRestartedTotal,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `count(sum by (pod)(delta(kube_pod_container_status_restarts_total[5m]) > 0))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes集群过去5分钟发生重启的pod的个数",
				EN: "Restarted pod count in the last 5 minutes",
			},
		},
		{
			Name:               ClusterPodRestartedCount,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `round(sum by (pod)(delta(kube_pod_container_status_restarts_total[5m]) > 0))`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes集群过去5分钟发生重启的pod的重启次数，每个重启过的pod会生成一条metric",
				EN: "Pod restarted count in the last 5 minutes",
			},
		},
		{
			Name:               ClusterPodStatusPhaseRunning,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(kube_pod_status_phase{phase="Running"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes集群当前处于Running状态的Pod数",
				EN: "Running pod count in the cluster",
			},
		},
		{
			Name:               ClusterPodStatusPhaseSucceeded,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(kube_pod_status_phase{phase="Succeeded"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes集群当前处于Succeeded状态的Pod数",
				EN: "Succeeded pod count in the cluster",
			},
		},
		{
			Name:               ClusterPodStatusPhaseFailed,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(kube_pod_status_phase{phase="Failed"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes集群当前处于Failed状态的Pod数",
				EN: "Failed pod count in the cluster",
			},
		},
		{
			Name:               ClusterPodStatusPhasePending,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(kube_pod_status_phase{phase="Pending"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes集群当前处于Pending状态的Pod数",
				EN: "Pending pod count in the cluster",
			},
		},
		{
			Name:               ClusterPodStatusPhaseUnknown,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(kube_pod_status_phase{phase="Unknown"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes集群当前处于Unknown状态的Pod数",
				EN: "Unknown pod count in the cluster",
			},
		},
		{
			Name:               ClusterPodStatusPhaseNotRunning,
			Kind:               ClusterKind,
			Type:               TypeMetric,
			Query:              `sum(kube_pod_status_phase{phase!="Running",phase!="Succeeded"})`,
			Unit:               "",
			ValueType:          Scalar,
			AlertEnabled:       true,
			AggregationEnabled: false,
			Annotations: map[string]string{
				CN: "Kubernetes集群当前处于非正常状态的Pod数，排除Running和Succeeded状态",
				EN: "Not running pod count in the cluster",
			},
		},
	}
)
