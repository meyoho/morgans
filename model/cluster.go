package model

import (
	"encoding/json"
	"reflect"
	"time"
)

type PrometheusConfig struct {
	Username        string `json:"username"`
	Password        string `json:"password"`
	Endpoint        string `json:"endpoint"`
	Namespace       string `json:"namespace"`
	Name            string `json:"name"`
	Timeout         int    `json:"timeout"`
	IntegrationUUID string `json:"integration_uuid"`
}

type KubernetesConfig struct {
	Endpoint string `json:"endpoint"`
	Token    string `json:"token"`
	Timeout  int    `json:"timeout"`
}

type Cluster struct {
	UUID             string            `json:"uuid"`
	Name             string            `json:"name"`
	DisplayName      string            `json:"display_name"`
	IsGlobal         bool              `json:"is_global"`
	IsOCP            bool              `json:"is_ocp"`
	HasMetricFeature bool              `json:"has_metric_feature"`
	KubernetesConfig *KubernetesConfig `json:"kubernetes_config"`
	PrometheusConfig *PrometheusConfig `json:"prometheus_config"`
	CreatedAt        time.Time         `json:"created_at"`
	UpdatedAt        time.Time         `json:"updated_at"`
	Version          string            `json:"version"`
}

func (r *Cluster) Equal(c *Cluster) bool {
	if r.UUID != c.UUID || r.Name != c.Name {
		return false
	}
	if r.IsGlobal != c.IsGlobal || r.HasMetricFeature != c.HasMetricFeature {
		return false
	}
	if !reflect.DeepEqual(r.KubernetesConfig, c.KubernetesConfig) {
		return false
	}
	return reflect.DeepEqual(r.PrometheusConfig, c.PrometheusConfig)
}

func (r *Cluster) Clone() *Cluster {
	result := &Cluster{}
	data, _ := json.Marshal(r)
	json.Unmarshal(data, result)
	return result
}
