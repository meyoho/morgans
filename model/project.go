package model

import (
	"encoding/json"
	"morgans/common"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
)

type ProjectResource struct {
	ResourceMeta `json:",inline"`
	Spec         ProjectSpec            `json:"spec,omitempty"`
	Status       map[string]interface{} `json:"status,omitempty"`
}

type ProjectSpec struct {
	Clusters []*ProjectCluster `json:"clusters,omitempty"`
}

type ProjectCluster struct {
	Name  string       `json:"name"`
	Quota ClusterQuota `json:"quota"`
	Type  string       `json:"string"`
}
type ClusterQuota struct {
	LimitsCPU              string `json:"limits.cpu"`
	LimitsMemory           string `json:"limits.memory"`
	PersistentVolumeClaims string `json:"persistentvolumeclaims"`
	Pods                   string `json:"pods"`
	RequestsCPU            string `json:"requests.cpu"`
	RequestsMemory         string `json:"requests.memory"`
	RequestsStorage        string `json:"requests.storage"`
}

type ProjectList struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
	Items             []*ProjectResource `json:"items,omitempty"`
}

func (r *ProjectResource) Clone() *ProjectResource {
	result := &ProjectResource{}
	data, _ := json.Marshal(r)
	json.Unmarshal(data, result)
	return result
}

func RestResultToProjects(result *rest.Result) ([]*ProjectResource, error) {
	data, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var projects ProjectList
	if err := json.Unmarshal(data, &projects); err != nil {
		return nil, err
	}
	return projects.Items, nil
}
