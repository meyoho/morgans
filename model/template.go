package model

import (
	"encoding/json"
	"time"

	"morgans/common"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
)

type AlertTemplateResponse AlertTemplateRequest

type AlertTemplateRequest struct {
	UUID         string           `json:"uuid"`
	Name         string           `json:"name"`
	Organization string           `json:"organization"`
	Kind         string           `json:"kind"`
	Description  string           `json:"description"`
	Creator      string           `json:"creator"`
	Template     []*AlertTemplate `json:"template"`
	CreatedAt    time.Time        `json:"created_at"`
	UpdatedAt    time.Time        `json:"updated_at"`
}

type AlertTemplateRecord struct {
	UUID         string    `db:"uuid"`
	Name         string    `db:"name"`
	Organization string    `db:"organization"`
	Kind         string    `db:"kind"`
	Description  string    `db:"description"`
	Creator      string    `db:"creator"`
	Template     string    `db:"template"`
	CreatedAt    time.Time `db:"created_at"`
	UpdatedAt    time.Time `db:"updated_at"`
}

type AlertTemplateParser struct {
	Cluster   ResourceBase      `json:"cluster"`
	Metric    QueryRangeRequest `json:"metric"`
	Project   string            `json:"project"`
	Creator   string            `json:"creator"`
	CreatedAt time.Time         `json:"created_at"`
	UpdatedAt time.Time         `json:"updated_at"`
}

type AlertTemplateResource struct {
	ResourceMeta `json:",inline"`
	Spec         AlertTemplateSpec `json:"spec,omitempty"`
}

type AlertTemplateSpec struct {
	Templates []*AlertTemplate `json:"templates"`
}

type AlertTemplateResourceList struct {
	ResourceMeta `json:",inline"`
	Items        []*AlertTemplateResource `json:"items,omitempty"`
}

func (t *AlertTemplateRequest) ToRecord() *AlertTemplateRecord {
	bytes, err := json.Marshal(t.Template)
	if err != nil {
		panic("marshal template to bytes error, " + err.Error())
	}
	return &AlertTemplateRecord{
		UUID:         t.UUID,
		Name:         t.Name,
		Organization: t.Organization,
		Kind:         t.Kind,
		Description:  t.Description,
		Creator:      t.Creator,
		Template:     string(bytes),
	}
}

func (t *AlertTemplateRequest) ToResource() *AlertTemplateResource {
	resource := &AlertTemplateResource{
		ResourceMeta: ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: AVAiOpsV1beta1,
				Kind:       RKAlertTemplate,
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name: t.Name,
				Labels: map[string]string{
					LKKind: t.Kind,
				},
				Annotations: map[string]string{
					AKDescription:  t.Description,
					AKOrganization: t.Organization,
					AKUpdatedAt:    t.UpdatedAt.Format(KTimeFormat),
					AKCreator:      t.Creator,
				},
			},
		},
		Spec: AlertTemplateSpec{
			Templates: t.Template,
		},
	}
	return resource
}

func (t *AlertTemplateResource) ToResponse() *AlertTemplateResponse {
	updatedAt, _ := time.Parse(KTimeFormat, t.Annotations[AKUpdatedAt])
	response := &AlertTemplateResponse{
		UUID:         string(t.UID),
		Name:         t.Name,
		Organization: t.Annotations[AKOrganization],
		Kind:         t.Labels[LKKind],
		Description:  t.Annotations[AKDescription],
		Creator:      t.Annotations[AKCreator],
		Template:     t.Spec.Templates,
		CreatedAt:    t.CreationTimestamp.Time,
		UpdatedAt:    updatedAt,
	}
	return response
}

func (t *AlertTemplateRecord) ToResponse() *AlertTemplateResponse {
	var template []*AlertTemplate
	if err := json.Unmarshal([]byte(t.Template), &template); err != nil {
		panic("unmarshal bytes to template to error, " + err.Error())
	}
	return &AlertTemplateResponse{
		UUID:         t.UUID,
		Name:         t.Name,
		Organization: t.Organization,
		Kind:         t.Kind,
		Description:  t.Description,
		Creator:      t.Creator,
		Template:     template,
		CreatedAt:    t.CreatedAt,
		UpdatedAt:    t.UpdatedAt,
	}
}

func RestResultToAlertTemplateResource(result *rest.Result) (*AlertTemplateResource, error) {
	bt, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var res AlertTemplateResource
	if err := json.Unmarshal(bt, &res); err != nil {
		return nil, err
	}
	return &res, nil
}

func RestResultToAlertTemplateResources(result *rest.Result) ([]*AlertTemplateResource, error) {
	bt, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var res AlertTemplateResourceList
	if err := json.Unmarshal(bt, &res); err != nil {
		return nil, err
	}
	return res.Items, nil
}
