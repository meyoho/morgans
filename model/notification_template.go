package model

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"text/template"

	"morgans/common"
)

const (
	NotificationSubjectTemplate = `{{ if eq .AlertStatus "FIRING" }} 警报 -- {{ .AlertName }}
                                   {{ else if eq .AlertStatus "INSUFFICIENT_DATA" }} 数据不足 -- {{ .AlertName }}
                                   {{ else }} {{ .AlertStatus }} -- {{ .AlertName }}
                                   {{ end }}`
	NotificationContentTemplate = `{{ if eq .AlertStatus "FIRING" }}
                                   当前值 {{ .CurrentValue }} {{ .Comparison }} 阈值 {{ .Threshold }}
                                   指标名称  {{ .MetricName }}
                                   资源名称  {{ .ResourceName }}
                                   {{ else }}
                                   当前状态  {{ .AlertStatus }}
                                   指标名称  {{ .MetricName }}
                                   资源名称  {{ .ResourceName }}
                                   {{ end }}`
)

var (
	ComparisonMap = map[string]map[string]string{
		"en": {
			"<":  "LessThan",
			"<=": "LessThanOrEqualTo",
			"==": "EqualTo",
			"!=": "UnequalTo",
			">":  "GreaterThan",
			">=": "GreaterThanOrEqualTo",
		},
		"cn": {
			"<":  "小于",
			"<=": "小于等于",
			"==": "等于",
			"!=": "不等于",
			">":  "大于",
			">=": "大于等于",
		},
	}
)

type NotificationPayload struct {
	AlertName            string `json:"alarm_name"`
	CurrentValue         string `json:"current_value"`
	Threshold            string `json:"threshold"`
	Statistic            string `json:"statistic"`
	StatisticDuration    string `json:"statistic_duration"`
	MetricName           string `json:"metric_name"`
	MetricDisplayName    string `json:"metric_display_name"`
	MetricUnit           string `json:"metric_unit"`
	ResourceName         string `json:"resource_name"`
	StartTime            string `json:"start_time"`
	CurrentTime          string `json:"current_time"`
	ClusterName          string `json:"cluster_name"`
	AlertStatus          string `json:"alarm_status"`
	Comparison           string `json:"comparison"`
	NotificationInterval string `json:"notification_interval"`
}

func (b *NotificationBody) String() string {
	data, err := json.Marshal(*b)
	if err != nil {
		panic(err.Error())
	}
	return string(data)
}

func (p *NotificationPayload) GetSubject() (string, error) {
	s := strings.Replace(NotificationSubjectTemplate, "\n", " ", -1)
	s = strings.Replace(s, "\t", " ", -1)

	t, err := template.New("Subject").Parse(s)
	if err != nil {
		return "", err
	}

	result := bytes.NewBufferString("")
	if err = t.Execute(result, p); err != nil {
		return "", err
	}
	return result.String(), nil
}

func (p *NotificationPayload) GetContent() (string, error) {
	s := strings.Replace(NotificationContentTemplate, "\n", " ", -1)
	s = strings.Replace(s, "\t", " ", -1)

	t, err := template.New("Content").Parse(s)
	if err != nil {
		return "", err
	}

	result := bytes.NewBufferString("")
	if err = t.Execute(result, p); err != nil {
		return "", err
	}
	return result.String(), nil
}

func (p *NotificationPayload) Translate() {
	// TODO support cn
	m := ComparisonMap["en"]
	p.Comparison = m[p.Comparison]
	p.AlertStatus = common.UCFirst(p.AlertStatus)
}

func (p *NotificationPayload) TransferPayload() (map[string]string, error) {
	p.Translate()
	var pl = map[string]string{}
	pBytes, err := json.Marshal(p)
	if err != nil {
		return nil, fmt.Errorf("translate notification payload error: %s", err.Error())
	}

	if err = json.Unmarshal(pBytes, &pl); err != nil {
		return nil, fmt.Errorf("translate notification payload error: %s", err.Error())
	}
	return pl, nil
}
