package model

import (
	"morgans/common"
)

type LoggerInstance interface {
	GetLogger() *common.Logger
}
