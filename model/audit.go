package model

import (
	"encoding/json"
	"errors"
	"time"

	elasticV6 "gopkg.in/olivere/elastic.v6"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/apis/audit"

	"morgans/config"
)

const (
	DefaultAuditPageSize = 30
)

var AuditIndexPrefix string

func init() {
	AuditIndexPrefix = config.GlobalConfig.ElasticSearch.AuditPrefix
}

type AuditRequest struct {
	// common
	StartTime float64
	EndTime   float64

	// aggregate audit
	Field string

	// list audit
	Username      string
	OperationType string
	ResourceName  string
	ResourceType  string
	Size          int
	Page          int
}

func (req *AuditRequest) BuildListSource() *elasticV6.SearchSource {
	searchSource := elasticV6.NewSearchSource()

	boolQuery := elasticV6.NewBoolQuery()

	rangeQuery := elasticV6.NewRangeQuery("StageTimestamp").
		Gt(time.Unix(int64(req.StartTime), 0).UTC().Format(time.RFC3339)).
		Lt(time.Unix(int64(req.EndTime), 0).UTC().Format(time.RFC3339))
	boolQuery.Filter(rangeQuery)

	if req.OperationType != "" {
		operateTermQuery := elasticV6.NewTermQuery("Verb", req.OperationType)
		boolQuery.Must(operateTermQuery)
	}
	if req.ResourceType != "" {
		resourceTypeTermQuery := elasticV6.NewTermQuery("ObjectRef.Resource", req.ResourceType)
		boolQuery.Must(resourceTypeTermQuery)
	}
	if req.ResourceName != "" {
		resourceNameTermQuery := elasticV6.NewMatchQuery("ReferObject", req.ResourceName)
		boolQuery.Must(resourceNameTermQuery)
	}
	if req.Username != "" {
		userTermQuery := elasticV6.NewTermQuery("User.Username", req.Username)
		boolQuery.Must(userTermQuery)
	}

	var from = (req.Page - 1) * req.Size
	if req.Page < 1 {
		from = 0
	}

	searchSource.From(from).
		Query(boolQuery).
		Size(req.Size).
		Sort("StageTimestamp", false)

	return searchSource
}

func (req *AuditRequest) BuildAggregateSource() *elasticV6.SearchSource {
	searchSource := elasticV6.NewSearchSource()

	boolQuery := elasticV6.NewBoolQuery()
	rangeQuery := elasticV6.NewRangeQuery("StageTimestamp").
		Gt(time.Unix(int64(req.StartTime), 0).UTC().Format(time.RFC3339)).
		Lt(time.Unix(int64(req.EndTime), 0).UTC().Format(time.RFC3339))
	boolQuery.Filter(rangeQuery)

	var topSize int
	if req.Field == "User.Username" {
		topSize = config.GlobalConfig.Audit.UserAggrTop
	} else if req.Field == "ObjectRef.Resource" {
		topSize = config.GlobalConfig.Audit.ResAggrTop
	} else {
		topSize = 10
	}
	termAggregation := elasticV6.NewTermsAggregation()
	termAggregation = termAggregation.Field(req.Field)
	termAggregation.Size(topSize)

	searchSource.
		Query(boolQuery).
		Size(0).
		Aggregation(AggsFieldName, termAggregation)

	return searchSource
}

// Audit struct is used to receive audit.Event stored in es
type Audit struct {
	audit.Event
	Cluster     string
	ReferObject string
}

type AuditList struct {
	metav1.TypeMeta
	// +optional
	metav1.ListMeta

	TotalItems int64 `json:"total_items"`
	TotalPage  int64 `json:"total_page"`

	Items []*Audit
}

type AuditAggregation struct {
	Items []string
}

func SearchResultToAuditAggregation(searchResult *elasticV6.SearchResult) (*AuditAggregation, error) {
	var result = &AuditAggregation{}
	if searchResult == nil || searchResult.Hits == nil {
		return result, nil
	}

	rawJson, ok := searchResult.Aggregations[AggsFieldName]
	if !ok {
		return nil, errors.New("Es aggregations result does not contains request aggregation")
	}

	var buckets elasticV6.AggregationBucketKeyItems
	err := json.Unmarshal(*rawJson, &buckets)
	if err != nil {
		return nil, err
	}

	for _, bucket := range buckets.Buckets {
		result.Items = append(result.Items, bucket.Key.(string))
	}

	return result, nil
}

func SearchHitToAudit(hit *elasticV6.SearchHit) (*Audit, error) {
	var audit Audit
	err := json.Unmarshal(*hit.Source, &audit)
	if err != nil {
		return nil, err
	}

	return &audit, nil
}

func SearchResultToAuditList(searchResult *elasticV6.SearchResult) (*AuditList, error) {
	result := &AuditList{
		TypeMeta: metav1.TypeMeta{
			Kind:       "EventList",
			APIVersion: "audit.k8s.io/v1",
		},
		ListMeta: metav1.ListMeta{},
	}

	if searchResult == nil || searchResult.Hits == nil {
		return result, nil
	}

	result.TotalItems = searchResult.Hits.TotalHits

	for _, hit := range searchResult.Hits.Hits {
		audit, err := SearchHitToAudit(hit)
		if err != nil {
			return nil, err
		}

		if audit != nil {
			result.Items = append(result.Items, audit)
		}
	}

	return result, nil
}
