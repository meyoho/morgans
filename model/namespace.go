package model

import (
	"encoding/json"

	"morgans/common"

	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/rest"
)

func RestResultToNamespaces(result *rest.Result) ([]v1.Namespace, error) {
	data, err := result.Raw()
	if err != nil {
		return nil, common.AnnotateError(err, "get raw bytes error for rest client result")
	}

	var ns v1.NamespaceList
	if err := json.Unmarshal(data, &ns); err != nil {
		return nil, err
	}
	return ns.Items, nil
}
