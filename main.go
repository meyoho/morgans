package main

import (
	"fmt"

	"morgans/api/handler"
	"morgans/api/middleware"
	"morgans/cache"
	"morgans/common"
	"morgans/config"
	"morgans/cron"
	"morgans/daemon"

	"github.com/DeanThompson/ginpprof"
	"github.com/gin-gonic/gin"
	ginprometheus "github.com/zsais/go-gin-prometheus"
	statusApi "gopkg.in/appleboy/gin-status-api.v1"
)

func main() {
	log := common.NewLogger(map[string]string{"r": "m"})
	log.Infof("Current env settings: %+v", config.GlobalConfig)

	// Set routers for gin
	gin.DefaultWriter = common.DefaultWriter
	gin.DefaultErrorWriter = common.DefaultWriter
	gin.SetMode(gin.ReleaseMode)
	engine := gin.New()
	engine.Use(middleware.Logger(), gin.Recovery())
	p := ginprometheus.NewPrometheus("http")
	p.Use(engine)
	handler.AddRoutes(engine)

	// Add debug apis
	ginpprof.Wrap(engine)
	engine.GET("/debug/api/status", statusApi.StatusHandler)

	// Start cache and daemon
	go cache.Run()
	go daemon.Run()
	go cron.Run()

	log.Info("Morgans starting...")
	// Listen and Server in 0.0.0.0:8080
	addr := fmt.Sprintf(":%d", config.GlobalConfig.Morgans.ExposePort)
	engine.Run(addr)
}
