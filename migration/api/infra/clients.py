#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
from .exceptions import APIException
from .util import get_logger

__author__ = 'Jianhua Shi'

logger = get_logger()


class MorgansRequest(object):
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'morgans/v1.0'
    }

    @classmethod
    def _parse_response(cls, response):
        code = response.status_code
        logger.debug('Response status={}, content={}'.format(code, response.content))
        if 200 <= code < 300:
            return response.json() if code != 204 else None
        raise APIException(response)

    @classmethod
    def _request(cls, url, method='GET', params=None, data=None, headers=None, **kwargs):
        logger.debug('Request url={}, method={}, data={}'.format(url, method, data))

        if method == 'POST':
            response = requests.post(url, params=params, json=data, headers=headers, **kwargs)
        elif method == 'PUT':
            response = requests.put(url, params=params, json=data, headers=headers, **kwargs)
        elif method == 'DELETE':
            response = requests.delete(url, params=params, headers=headers, **kwargs)
        elif method == 'GET':
            response = requests.get(url, params=params, headers=headers, **kwargs)
        else:
            raise Exception('method_not_implemented')
        return cls._parse_response(response)


class MorgansClient(MorgansRequest):
    endpoint = 'http://127.0.0.1:8080/v1/'
    alerts = endpoint + 'alerts/'
    alert_templates = endpoint + 'alert_templates/'
    inner = endpoint + 'inner/'
    migration = inner + 'migration/'
    kubernetes = inner + 'kubernetes/'
    notifications = endpoint + 'notifications/'
    forceParam = {'force': 'true'}
    paginatedParam = {'paginated': 'false'}

    @classmethod
    def get_headers(cls, cluster):
        headers = {'Authorization': 'Bearer ' + cluster['kubernetes_config']['token']}
        headers.update(cls.headers)
        return headers

    # Inner api
    @classmethod
    def list_clusters(cls):
        return cls._request(url=cls.inner + 'clusters')

    @classmethod
    def get_version(cls, cluster):
        return cls._request(url=cls.migration + cluster['name'] + '/version',
                            headers=cls.get_headers(cluster))

    @classmethod
    def update_version(cls, cluster, version):
        return cls._request(url=cls.migration + cluster['name'] + '/version',
                            method='PUT',
                            params={'version': version},
                            headers=cls.get_headers(cluster))

    # Alert api
    @classmethod
    def list_alerts(cls, cluster):
        return cls._request(url=cls.alerts + cluster['name'],
                            params=cls.paginatedParam,
                            headers=cls.get_headers(cluster))

    @classmethod
    def create_alert(cls, cluster, data):
        return cls._request(url=cls.alerts + cluster['name'],
                            method='POST',
                            params=cls.forceParam,
                            data=data,
                            headers=cls.get_headers(cluster))

    @classmethod
    def update_alert(cls, cluster, namespace, resource, group, name, data):
        return cls._request(url=cls.alerts + '/'.join([cluster['name'], namespace, resource, group, name]),
                            method='PUT',
                            params=cls.forceParam,
                            data=data,
                            headers=cls.get_headers(cluster))

    @classmethod
    def delete_alert(cls, cluster, namespace, resource, group, name):
        return cls._request(url=cls.alerts + '/'.join([cluster['name'], namespace, resource, group, name]),
                            method='DELETE',
                            headers=cls.get_headers(cluster))

    # Alert template api
    @classmethod
    def list_alert_templates(cls, organization, cluster, params):
        params = params.update(cls.paginatedParam)
        return cls._request(url=cls.alert_templates + organization,
                            params=params,
                            headers=cls.get_headers(cluster))

    @classmethod
    def create_alert_template(cls, organization, cluster, data, params):
        return cls._request(url=cls.alert_templates + organization,
                            method='POST',
                            data=data,
                            params=params,
                            headers=cls.get_headers(cluster))

    @classmethod
    def update_alert_template(cls, organization, cluster, name, data, params):
        return cls._request(url=cls.alert_templates + organization + '/' + name,
                            method='PUT',
                            data=data,
                            params=params,
                            headers=cls.get_headers(cluster))

    # Kubernetes resource api
    @classmethod
    def create_resource(cls, cluster, kind, data, apply_flag=False):
        return cls._request(url=cls.kubernetes + cluster['name'] + '/' + kind,
                            method='POST',
                            params={'apply': apply_flag},
                            data=data,
                            headers=cls.get_headers(cluster))

    @classmethod
    def get_resource(cls, cluster, namespace, kind, name):
        return cls._request(url=cls.kubernetes + '/'.join([cluster['name'], namespace, kind, name]),
                            method="GET",
                            headers=cls.get_headers(cluster))

    @classmethod
    def list_resources(cls, cluster, namespace, kind):
        return cls._request(url=cls.kubernetes + '/'.join([cluster['name'], namespace, kind]),
                            method="GET",
                            headers=cls.get_headers(cluster))

    @classmethod
    def get_notification(cls, cluster, namespace, name):
        return cls._request(url=cls.notifications + '/'.join([namespace, name]),
                            method="GET",
                            headers=cls.get_headers(cluster))

    @classmethod
    def create_notification(cls, cluster, namespace, data):
        return cls._request(url=cls.notifications + namespace,
                            method="POST",
                            data=data,
                            headers=cls.get_headers(cluster))

    @classmethod
    def update_notification(cls, cluster, namespace, name, data):
        return cls._request(url=cls.notifications + '/'.join([namespace, name]),
                            method="PUT",
                            data=data,
                            headers=cls.get_headers(cluster))

