#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
import traceback
import uuid

__author__ = 'Jianhua Shi'


def get_logger(log_name='', log_path='', to_console=False):
    if not log_path:
        log_path = "/var/log/mathilde/migration.log"
    if not log_name:
        log_name = str(uuid.uuid4())
    logger = logging.getLogger(log_name)
    # create a handler to print log to file
    fh = logging.FileHandler(log_path)
    ff = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(ff)
    # create a handler to print log to console
    ch = logging.StreamHandler()
    cf = logging.Formatter('%(message)s')
    ch.setFormatter(cf)
    # add handler if needed
    logger.addHandler(fh)
    if to_console:
        logger.addHandler(ch)
    logger.setLevel(logging.DEBUG)
    return logger


def handle_exception(l):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as ex:
                l.info(traceback.format_exc(ex))
                return False

        return wrapper

    return decorator


def read_db_config():
    username = os.getenv("DB_USER")
    password = os.getenv("DB_PASSWORD")
    try:
        with open("/etc/pass_db/username", 'r') as f:
            username = f.read().strip('\n')
        with open("/etc/pass_db/password", 'r') as f:
            password = f.read().strip('\n')
    except IOError:
        pass
    finally:
        return username, password
