#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

__author__ = 'Jianyong Lian'

base_domain = 'alauda.io'

label_creator = 'creator'

label_app = 'app.{}/name'
label_project = 'project.{}/name'

label_alert_kind = 'alert.{}/kind'
label_alert_name = 'alert.{}/name'
label_alert_namespace = 'alert.{}/namespace'
label_alert_owner = 'alert.{}/owner'
label_alert_notifications = 'alert.{}/notifications'
label_alert_cluster = 'alert.{}/cluster'
label_alert_project = 'alert.{}/project'
label_alert_application = 'alert.{}/application'


def init_variables():
    global base_domain
    global label_creator, label_app, label_project, label_alert_kind
    global label_alert_name, label_alert_namespace, label_alert_owner
    global label_alert_notifications, label_alert_cluster, label_alert_project, label_alert_application
    base_domain = os.getenv('LABEL_BASEDOMAIN', 'alauda.io')
    if base_domain != '':
        label_creator = '/'.join([base_domain, label_creator])

        label_app = label_app.format(base_domain)
        label_project = label_project.format(base_domain)

        label_alert_kind = label_alert_kind.format(base_domain)
        label_alert_name = label_alert_name.format(base_domain)
        label_alert_namespace = label_alert_namespace.format(base_domain)
        label_alert_owner = label_alert_owner.format(base_domain)
        label_alert_notifications = label_alert_notifications.format(base_domain)
        label_alert_cluster = label_alert_cluster.format(base_domain)
        label_alert_project = label_alert_project.format(base_domain)
        label_alert_application = label_alert_application.format(base_domain)
    else:
        label_app = 'app/name'
        label_project = 'project/name'

        label_alert_kind = 'alert/kind'
        label_alert_name = 'alert/name'
        label_alert_namespace = 'alert/namespace'
        label_alert_owner = 'alert/owner'
        label_alert_notifications = 'alert/notifications'
        label_alert_cluster = 'alert/cluster'
        label_alert_project = 'alert/project'
        label_alert_application = 'alert/application'
