#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from infra.clients import MorgansClient
from infra.util import get_logger, handle_exception
from infra.variables import label_creator

__author__ = 'Jianyong Lian'

logger = get_logger()
version = 8
MIGRATION_CONFIG_DIR = os.path.dirname(__file__) + '/migration.d/'


@handle_exception(logger)
def migrate(cluster):
    if not cluster['is_global']:
        return True

    update_alert_templates(cluster)
    update_notifications(cluster)
    return True


def update_alert_templates(cluster):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    templates = MorgansClient.list_resources(
        cluster, '', 'alerttemplate')
    for t in templates['items']:
        logger.info('{} update {} {}'.format(
            prefix, t['kind'], t['metadata']['name']))

        creator = t['metadata'].get('labels', {}).get(label_creator, '')
        if creator:
            t['metadata']['annotations'][label_creator] = creator
        MorgansClient.create_resource(
            cluster, t['kind'], t, apply_flag=True)


def update_notifications(cluster):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    notifications = MorgansClient.list_resources(
        cluster, os.getenv('NAMESPACE', 'alauda-system'), 'notification')
    for n in notifications['items']:
        logger.info('{} update {} {}'.format(
            prefix, n['kind'], n['metadata']['name']))

        creator = n['metadata'].get('labels', {}).get(label_creator, '')
        if creator:
            n['metadata']['annotations'][label_creator] = creator
        MorgansClient.create_resource(
            cluster, n['kind'], n, apply_flag=True)
