#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import importlib
from infra.clients import MorgansClient
from infra.util import get_logger
from prettytable import PrettyTable

__author__ = 'Jianhua Shi'

f_logger = get_logger(to_console=False)
c_logger = get_logger(to_console=True)

versions = [
    {
        'version': 1,
        'module': 'migration_0001'
    },
    {
        'version': 2,
        'module': 'migration_0002'
    },
    {
        'version': 3,
        'module': 'migration_0003'
    },
    {
        'version': 4,
        'module': 'migration_0004'
    },
    {
        'version': 5,
        'module': 'migration_0005'
    },
    {
        'version': 6,
        'module': 'migration_0006'
    },
    {
        'version': 7,
        'module': 'migration_0007'
    },
    {
        'version': 8,
        'module': 'migration_0008'
    },
    {
        'version': 9,
        'module': 'migration_0009'
    },
    {
        'version': 10,
        'module': 'migration_0010'
    },
    {
        'version': 11,
        'module': 'migration_0011'
    },
    {
        'version': 12,
        'module': 'migration_0012'
    }
]


@click.command()
@click.option('--clusters', default='', help='Clusters to migrate, separated by comma')
@click.option('--set-version', type=int, help='Set a specified version for clusters')
@click.option('--show-versions', is_flag=True, help='Show versions info for clusters')
def migrate(clusters, set_version, show_versions):
    clusters = filter_clusters(clusters)

    if set_version > 0:
        c_logger.info('Set version for clusters...')
        for c in clusters:
            update_version(c, set_version)
        c_logger.info('Current versions info ...')
        list_versions(clusters)
        return

    if show_versions:
        c_logger.info('Current versions info ...')
        list_versions(clusters)
        return

    c_logger.info('Before migration ...')
    list_versions(clusters)
    c_logger.info('Migrate start ...')

    for c in clusters:
        for v in versions:
            result = check_version(c, v['version'])
            if result == 1:
                continue
            if result == -1:
                break
            m = importlib.import_module(v['module'])
            if not m.migrate(c):
                c_logger.info('[{}/{}] execute migration {} failed, abort ...'
                              .format(c['name'], c['uuid'], v['version']))
                break
            if not update_version(c, v['version']):
                break

    c_logger.info('After migration ...')
    list_versions(clusters)
    c_logger.info('Migrate end ...')


# Filter clusters by clusters option (like <cluster_uuid,cluster_uuid2>)
def filter_clusters(clusters):
    cluster_set = MorgansClient.list_clusters()
    cluster_list = clusters.split(',')
    if clusters and len(cluster_list) > 0:
        result = []
        for s in cluster_set:
            for l in cluster_list:
                if s['uuid'] == l:
                    result.append(s)
        return result
    else:
        return cluster_set


# Check the conditions to perform the migration for a specified version
def check_version(cluster, version):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)
    try:
        current = MorgansClient.get_version(cluster)
        if current['version'] >= version:
            c_logger.info('{} current {} >= {}, skip migration {} ...'
                          .format(prefix, current['version'], version, version))
            return 1
        if current['version'] == version - 1:
            c_logger.info('{} current {}, start migration to {} ...'
                          .format(prefix, current['version'], version))
            return 0
        c_logger.info('{} current {} < {}, abort migration {} ...'
                      .format(prefix, current['version'], version - 1, version))
        return -1
    except Exception as ex:
        f_logger.exception(ex)
        c_logger.info('{} check version failed, abort migration {} ...'.format(prefix, version))
        return -1


# Update to a specified version
def update_version(cluster, version):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)
    try:
        c_logger.info('{} update version to {} ...'.format(prefix, version))
        response = MorgansClient.update_version(cluster, version)
        if response:
            f_logger.info('{} update version failed with response {}'.format(prefix, response))
            c_logger.info('{} update version failed, abort migration {} ...'.format(prefix, version))
            return False
        return True
    except Exception as ex:
        f_logger.exception(ex)
        c_logger.info('{} update version failed, abort migration {} ...'.format(prefix, version))
        return False


# List versions for all clusters
def list_versions(clusters):
    desired = 0
    for v in versions:
        if v['version'] > desired:
            desired = v['version']
    x = PrettyTable(['name', 'uuid', 'current', 'desired'])
    for c in clusters:
        try:
            record = {
                'uuid': c['uuid'],
                'name': c['name'],
                'current': 0,
                'desired': desired,
            }
            v = MorgansClient.get_version(c)
            record['current'] = v['version']
        except Exception as ex:
            f_logger.exception(ex)
            record['current'] = '?'
        finally:
            x.add_row([record['name'], record['uuid'], record['current'], record['desired']])
    c_logger.info(x)


if __name__ == '__main__':
    migrate()
