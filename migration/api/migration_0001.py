#!/usr/bin/env python
# -*- coding: utf-8 -*-
from infra.clients import MorgansClient
from infra.util import get_logger, handle_exception

__author__ = 'Jianhua Shi'

logger = get_logger()
version = 1


@handle_exception(logger)
def migrate(cluster):
    if not cluster['has_metric_feature']:
        return True
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    alerts = MorgansClient.list_alerts(cluster)
    for alert in alerts:
        namespace = alert['namespace']
        resource_name = alert['resource_name']
        group_name = alert['group_name']
        alert_name = alert['name']
        logger.info('{} update alert {}/{}/{}/{}'.format(prefix, namespace, resource_name, group_name, alert_name))
        translate(alert)
        MorgansClient.update_alert(cluster, namespace, resource_name, group_name, alert_name, alert)
    return True


def translate(alert):
    translate_map = {
        'application.cpu.utilization': 'workload.cpu.utilization',
        'application.memory.utilization': 'workload.memory.utilization',
        'application.network.receive_bytes': 'workload.network.receive_bytes',
        'application.network.transmit_bytes': 'workload.network.transmit_bytes',
    }
    alert['wait'] = alert.get('wait', 60)
    queries = alert.get('metric', {}).get('queries', [])
    for q in queries:
        for l in q.get('labels', []):
            if l.get('value', '') in translate_map.keys():
                l['value'] = translate_map[l.get('value', '')]
