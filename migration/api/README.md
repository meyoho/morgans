### How to add migration scripts for morgans
1. Before start, you must promise: 
	* Your migration script will be applied via kubernetes api, prometheus api and morgans api.
	* Your migration script must be repeatable.
	* Your must ensure that it's safe to exectute your script in all case (that is to say no exception).
	* Your must check the conditions to perform the migration, do the migration and update version if succeeded in your script.
2. Add you migration script to directory /morgans/migration.
3. Add version to /morgans/migrate.py.

