#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import yaml

from infra.clients import MorgansClient
from infra.util import get_logger, handle_exception

__author__ = 'Jianyong Lian'

logger = get_logger()
version = 10
MIGRATION_CONFIG_DIR = os.path.dirname(__file__) + '/migration.d/'


@handle_exception(logger)
def migrate(cluster):
    ## this migration no need to do, skip it
    return True

    if not cluster['has_metric_feature']:
        return True
    update_recording_rules(cluster)
    return True


def update_recording_rules(cluster):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    with open(MIGRATION_CONFIG_DIR + '0010_recording.rules.yml') as stream:
        resource = yaml.safe_load(stream)

    resource['metadata']['namespace'] = cluster["prometheus_config"]['namespace']
    resource['metadata']['labels']['prometheus'] = cluster["prometheus_config"]['name']

    rule_selector = get_prometheus_rule_selector(cluster)
    resource['metadata']['labels'].update(rule_selector)

    logger.info('{} Create resource {} {} in {}'.format(
        prefix, resource['kind'], resource['metadata']['name'],
        resource['metadata'].get('namespace')))
    MorgansClient.create_resource(
        cluster, resource['kind'], resource, apply_flag=True)


def get_prometheus_rule_selector(cluster):
    resource = MorgansClient.get_resource(
        cluster, cluster["prometheus_config"]["namespace"], "prometheus", cluster["prometheus_config"]["name"])
    return resource.get("spec", {}).get("ruleSelector", {}).get("matchLabels", {})
