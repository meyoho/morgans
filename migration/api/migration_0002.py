#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import uuid as uuid_pkg

import yaml

from infra.clients import MorgansClient
from infra.util import get_logger, handle_exception

__author__ = 'Jianyong Lian'

logger = get_logger()
version = 2
MIGRATION_CONFIG_DIR = os.path.dirname(__file__) + '/migration.d/'


@handle_exception(logger)
def migrate(cluster):
    if not cluster['has_metric_feature']:
        return True
    updateAlerts(cluster)
    return True


def updateAlerts(cluster):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    alerts = MorgansClient.list_alerts(cluster)
    for alert in alerts:
        namespace = alert['namespace']
        resource_name = alert['resource_name']
        group_name = alert['group_name']
        alert_name = alert['name']
        logger.info('{} update alert {}, {}, {}'.format(prefix, resource_name, group_name, alert_name))
        if not alert['uuid']:
            alert['uuid'] = str(uuid_pkg.uuid4())
        MorgansClient.update_alert(cluster, namespace, resource_name, group_name, alert_name, alert)
