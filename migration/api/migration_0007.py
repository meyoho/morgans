#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import yaml

from infra.clients import MorgansClient
from infra.exceptions import APIException
from infra.util import get_logger, handle_exception, read_db_config

__author__ = 'Jianyong Lian'

logger = get_logger()
version = 7
MIGRATION_CONFIG_DIR = os.path.dirname(__file__) + '/migration.d/'

db_engine = os.getenv('DB_ENGINE')
db_host = os.getenv('DB_HOST')
db_port = int(os.getenv('DB_PORT', 3306))
db_user, db_password = read_db_config()


@handle_exception(logger)
def migrate(cluster):
    if not cluster['is_global']:
        return True

    migrate_notification(cluster)
    return True


def migrate_notification(cluster):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    if not db_host and not db_password:
        logger.info(
            '{} it seems that morgans has no db engine, skip'.format(prefix))
        return

    notifications_dict = {}
    # search jakirodb for notification info
    db_conn, db_cursor = get_db_cursor(db_engine, "jakirodb")
    try:
        db_cursor.execute(
            "select name,namespace,uuid,created_by,created_at from resources_resource where type = 'NOTIFICATION'")
        for c in db_cursor:
            notification = {}
            notification["name"] = c['name'].replace('_', '-')
            notification["namespace"] = os.getenv("NAMESPACE", "cpaas-system")
            notification["created_at"] = c['created_at'].strftime("%Y-%m-%dT%H:%M:%SZ")
            notification["created_by"] = c["created_by"]
            notification["subscriptions"] = []

            notifications_dict[c['uuid']] = notification
    except:
        import traceback
        traceback.print_exc()
    finally:
        db_cursor.close()
        db_conn.close()

    # search luciferdb for subscriptions info
    db_conn, db_cursor = get_db_cursor(db_engine, "luciferdb")
    try:
        db_cursor.execute("select * from notification_subscription")
        for c in db_cursor:
            notifications_dict[c['notification_id']]['subscriptions'].append({
                'method': c['method'],
                'recipient': c['recipient'],
                'secret': c['secret'],
                'remark': c['remark']
            })
    except:
        import traceback
        traceback.print_exc()
    finally:
        db_cursor.close()
        db_conn.close()

    # migrate notification to morgans
    for uuid in notifications_dict:
        notification = notifications_dict[uuid]
        try:
            logger.info('{} try to migrate notification {}'.format(
                prefix, notification['name']))
            MorgansClient.create_notification(
                cluster, notification['namespace'], notification)
        except APIException as e:
            if e.status_code == 409:
                logger.info('{} try to update notification {}'.format(
                    prefix, notification['name']))
                MorgansClient.update_notification(
                    cluster, notification['namespace'], notification['name'], notification)
                continue
            else:
                logger.info('{} create notification exception {} {}'.format(
                    prefix, e.status_code, e.error_type))
                raise e


def get_db_cursor(db_engine, db_name):
    if db_engine == 'mysql':
        import pymysql
        conn = pymysql.connect(host=db_host, port=db_port,
                               user=db_user, password=db_password, database=db_name,
                               charset='utf8', cursorclass=pymysql.cursors.DictCursor)
        cur = conn.cursor()
        return conn, cur
    elif db_engine == 'pgsql':
        import psycopg2
        from psycopg2 import extras
        conn = psycopg2.connect(host=db_host, port=db_port,
                                user=db_user, password=db_password, database=db_name)
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        return conn, cur
