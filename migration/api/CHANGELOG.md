## Migration 0001 / 2018-12-05
* Add labels for alert resource(such as namespace, project, view, kind)
* Add labels for alert rule(such as severity)
* Rename application indicators to workload(like application.cpu.utilization to workload.cpu.utilization)
* Set wait(`for` in prometheus rule) as step, due to they are the same for old alerts


Here is the Old format:

```
{
    "apiVersion": "monitoring.coreos.com/v1",
    "kind": "PrometheusRule",
    "metadata": {
        "clusterName": "",
        "creationTimestamp": "2018-11-30T04:09:35Z",
        "generation": 0,
        "labels": {
            "alert.alauda.io/owner": "AlaudaSystem",
            "app": "prometheus",
            "prometheus": "ddddd",
            "release": "kube-prometheus",
            "role": "alert-rules"
        },
        "name": "ns-test.nginx.rules",
        "namespace": "monitoring",
        "resourceVersion": "18024949",
        "selfLink": "/apis/monitoring.coreos.com/v1/namespaces/monitoring/prometheusrules/ns-test.nginx.rules",
        "uid": "bf1fc112-f455-11e8-9f79-525400170e45"
    },
    "spec": {
        "groups": [
            {
                "name": "daemonset.nginx",
                "rules": [
                    {
                        "alert": "jhshi-testyyy-c587b37bb78e473c447d6bb441c44d1b",
                        "annotations": {
                            "AlertMeta": "{\"name\":\"jhshi-testyyy\",\"metric\":{\"step\":60,\"queries\":[{\"aggregator\":\"avg\",\"downsample\":\"avg\",\"labels\":[{\"type\":\"IN\",\"name\":\"name\",\"value\":\"nginx\"},{\"type\":\"IN\",\"name\":\"namespace\",\"value\":\"test\"},{\"type\":\"IN\",\"name\":\"kind\",\"value\":\"DaemonSet\"},{\"type\":\"EQUAL\",\"name\":\"__name__\",\"value\":\"workload.memory.utilization\"}]}]},\"compare\":\"\\u003e\",\"threshold\":0.9,\"wait\":0,\"labels\":{\"key\":\"value\"},\"annotations\":{\"description\":\"fsdfs\"},\"project\":\"system\",\"creator\":\"jianhua\",\"created_at\":\"2018-11-30T04:09:38.1304965Z\",\"updated_at\":\"2018-12-05T08:07:51.9098478Z\"}",
                            "description": "Just for test"
                        },
                        "expr": "sum by (deployment_name) (avg_over_time (container_memory_usage_bytes{namespace=\"test\",pod_name=~\"nginx-[a-z0-9]{5}\",image!=\"\",container_name!=\"POD\"}[60s])) / sum by (deployment_name) (container_spec_memory_limit_bytes{namespace=\"test\",pod_name=~\"nginx-[a-z0-9]{5}\",image!=\"\",container_name!=\"POD\"})\u003e0.90000",
                        "for": "60s",
                        "labels": {
                            "key": "value"
                        }
                    }
                ]
            }
        ]
    }
}
```
Here is the New format:
```
{
    "apiVersion": "monitoring.coreos.com/v1",
    "kind": "PrometheusRule",
    "metadata": {
        "clusterName": "",
        "creationTimestamp": "2018-11-30T04:09:35Z",
        "generation": 0,
        "labels": {
            "alert.alauda.io/kind": "workload",
            "alert.alauda.io/namespace": "test",
            "alert.alauda.io/owner": "AlaudaSystem",
            "alert.alauda.io/project": "system",
            "alert.alauda.io/view": "user",
            "app": "prometheus",
            "prometheus": "ddddd",
            "release": "kube-prometheus",
            "role": "alert-rules"
        },
        "name": "ns-test.nginx.rules",
        "namespace": "monitoring",
        "resourceVersion": "18024949",
        "selfLink": "/apis/monitoring.coreos.com/v1/namespaces/monitoring/prometheusrules/ns-test.nginx.rules",
        "uid": "bf1fc112-f455-11e8-9f79-525400170e45"
    },
    "spec": {
        "groups": [
            {
                "name": "daemonset.nginx",
                "rules": [
                    {
                        "alert": "jhshi-testyyy-c587b37bb78e473c447d6bb441c44d1b",
                        "annotations": {
                            "AlertMeta": "{\"name\":\"jhshi-testyyy\",\"metric\":{\"step\":60,\"queries\":[{\"aggregator\":\"avg\",\"downsample\":\"avg\",\"labels\":[{\"type\":\"IN\",\"name\":\"name\",\"value\":\"nginx\"},{\"type\":\"IN\",\"name\":\"namespace\",\"value\":\"test\"},{\"type\":\"IN\",\"name\":\"kind\",\"value\":\"DaemonSet\"},{\"type\":\"EQUAL\",\"name\":\"__name__\",\"value\":\"workload.memory.utilization\"}]}]},\"compare\":\"\\u003e\",\"threshold\":0.9,\"wait\":60,\"labels\":{\"key\":\"value\",\"severity\":\"High\"},\"annotations\":{\"description\":\"fsdfs\"},\"project\":\"system\",\"creator\":\"jianhua\",\"created_at\":\"2018-11-30T04:09:38.1304965Z\",\"updated_at\":\"2018-12-05T08:07:51.9098478Z\"}",
                            "description": "Just for test"
                        },
                        "expr": "sum by (deployment_name) (avg_over_time (container_memory_usage_bytes{namespace=\"test\",pod_name=~\"nginx-[a-z0-9]{5}\",image!=\"\",container_name!=\"POD\"}[60s])) / sum by (deployment_name) (container_spec_memory_limit_bytes{namespace=\"test\",pod_name=~\"nginx-[a-z0-9]{5}\",image!=\"\",container_name!=\"POD\"})\u003e0.90000",
                        "for": "60s",
                        "labels": {
                            "key": "value",
                            "severity": "High"
                        }
                    }
                ]
            }
        ]
    }
}
```

## Migration 0002 / 2019-01-02
* Create recording rules in prometheus
* Update alerts to add uuid and replace expr

## Migration 003 / 2019-02-25
* Create recording rules in prometheus
* Update alerts to replace expr

## Migration 0004 / 2019-03-12
* Update alerts to apply new naming method for alerts resource

## Migration 0005 / 2019-06-11
* Update alerts to support prometheus cross namespace monitoring

## Migration 0006 / 2019-06-11
* Update alert template to use etcd storage

## Migration 0007 / 2019-06-18
* Migrate notifications to use etcd storage

## Migration 0008 / 2019-07-28
* Add creator in annotations for alerttemplate and notification

## Migration 0009 / 2019-08-06
* Remove meta in annotations and add labels to reference object for alert rule


Here is the New format:

``` 
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  creationTimestamp: "2019-07-03T07:00:30Z"
  generation: 1
  labels:
    # 设置该告警关联的告警对象的信息
    alert.alauda.io/application: monitoring            # 告警所属的应用名称【可选，workload相关的告警则是必选】
    alert.alauda.io/cluster: global                    # 告警所在的集群名称【必选】
    alert.alauda.io/kind: Deployment                   # 告警所属的对象类型【必选，Deployment/StatefulSet/DaemonSet/Node/Cluster之一】
    alert.alauda.io/name: morgans                      # 告警所属的对象名称【必选】
    alert.alauda.io/namespace: default                 # 告警所属的对象的命名空间【必选，但值可能为空】
    alert.alauda.io/project: dev1                      # 告警所属的对象的项目名称【必选，但值可能为空】
    alert.alauda.io/owner: System                      # 指示告警是否是通过我们的平台创建的【必选，值必须为是System，可以根据这个label过滤】
    prometheus: kube-prometheus                        # 指示告警的Prometheus的名称【必选，值必须是所在集群的Prometheus的名称】
  annotations:
    # 设置该告警关联的通知的列表，存放告警要触发的通知列表的json串
    alert.alauda.io/notifications: '[{"namespace": "default", "name": "test_notification1"}]' 
  # 设置该告警所关联的告警对象的信息
  ownerReferences:
  - apiVersion: apps/v1
    controller: true
    blockOwnerDeletion: true
    kind: Deployment
    name: morgans
    uid: d9607e19-f88f-11e6-a518-42010a800195
  name: deployment-morgans
  namespace: alauda-system
  resourceVersion: "7305470"
  selfLink: /apis/monitoring.coreos.com/v1/namespaces/alauda-system/prometheusrules/cluster-global
  uid: 3eb9acbc-9d60-11e9-bf02-52540006c2c3
spec:
  groups:
  - name: general                                            # 告警规则所属的group的名称，统一为general
    rules:
      # 告警规则的名称，主要分为两部分：<规则名称>-<一个32位MD5值>
      # 第一部分的规则名称生成方法：指标名称+'-'+<5位随机字符串(小写的字母+数字的组合)>
      # 第二部分的MD5值生成方法：MD5<resourceName__groupName__ruleName>，这部分是为规则生成一个唯一的名字
    - alert: workload.cpu.utilization-xf1bc-805ce846817e07bb7e02e4f92eb6e37e                    
      annotations:
        # 带回告警规则触发时的当前值
        alert_current_value: "{{ $value }}"
        # 设置该告警关联的通知的列表，存放告警要触发的通知列表的json串
        alert_notifications: '[{"namespace": "default", "name": "test_notification1"}]'
      expr: workload_cpu_utilization / count(avg by(instance)(node_cpu{job="node-exporter",mode="idle"})) > 0.20000
      for: 60s                                     # 告警规则的持续时间
      labels:
        severity: Medium                           # 告警规则等级【必选，Critical/High/Medium/Low四种等级之一】
        application: monitoring                    # 告警规则所属的应用名称【可选，workload相关的告警则是必选】
        alert_name: cpu-utilization                # 告警规则的名称【必选】
        alert_involved_object_kind: Deployment     # 告警规则所属的对象类型【必选，Deployment/StatefulSet/DaemonSet/Node/Cluster之一】
        alert_involved_object_name: morgans        # 告警规则所属的对象名称【必选】
        alert_involved_object_namespace: default   # 告警规则所属的对象的命名空间【必选，但值可能为空】
        alert_cluster: global                      # 告警规则所在的集群名称【必选】
        alert_project: dev1                        # 告警规则所属的对象的项目名称【必选，但值可能为空】
        alert_creator: jhshi                       # 告警规则的创建者【可选】
        alert_indicator: workload_cpu_utilization  # 告警规则的指标名称【必选，自定义告警(custom)/其它告警(指标名称)】
        alert_indicator_aggregate_range: 5m        # 告警规则的聚合时间【可选】
        alert_indicator_aggregate_function: max    # 告警规则的聚合方法【可选，max/min/avg三种】
        alert_indicator_comparison: >=             # 告警规则的比较方法【必选】
        alert_indicator_threshold: 0.8             # 告警规则的阈值【必选】
        alert_indicator_log_query: test            # 告警规则的日志告警的查询字段【可选，日志告警的查询语句】
        alert_indicator_unit: KB/S                 # 告警规则的指标单位【可选，值可能为空】
```

## Migration 010 / 2019-08-16
* Create recording rules in prometheus

## Migration 011 / 2019-10-09
* Update recording rules in prometheus
