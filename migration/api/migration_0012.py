#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import re

from infra.clients import MorgansClient
from infra.util import get_logger, handle_exception
from infra.variables import label_alert_owner

__author__ = 'Jianhua Shi'

logger = get_logger()
version = 12


@handle_exception(logger)
def migrate(cluster):
    if not cluster['has_metric_feature']:
        return True
    update_prometheus_rules(cluster)
    return True


def update_prometheus_rules(cluster):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    namespaces = MorgansClient.list_resources(cluster, '', 'namespace')
    for namespace in namespaces['items']:
        namespace = namespace['metadata']['name']
        resources = MorgansClient.list_resources(cluster, namespace, 'prometheusrule')
        for resource in resources['items']:
            update_prometheus_rule(resource)
            MorgansClient.create_resource(cluster, 'prometheusrule', resource, True)

def update_prometheus_rule(resource):
    owner = resource.get('metadata', {}).get('labels', {}).get(label_alert_owner, '')
    if owner != 'AlaudaSystem' and owner != 'System':
        return
    if len(resource['spec']['groups']) < 1:
        return
    for group in resource['spec']['groups']:
        for rule in group['rules']:
            if rule.get('labels', {}).get('alert_indicator', '') != 'workload.log.keyword.count':
                rule['labels'].pop('alert_indicator_log_query', None)
                continue
            rule['labels']['alert_indicator_query'] = rule['labels'].get('alert_indicator_log_query', '')
            rule['labels'].pop('alert_indicator_log_query', None)
            rule['expr'] = re.sub(r'kind="deployment"', 'kind=~"(?i:(Deployment))"', rule['expr'], flags=re.IGNORECASE)
            rule['expr'] = re.sub(r'kind="daemonset"', 'kind=~"(?i:(DaemonSet))"', rule['expr'], flags=re.IGNORECASE)
            rule['expr'] = re.sub(r'kind="statefulset"', 'kind=~"(?i:(StatefulSet))"', rule['expr'], flags=re.IGNORECASE)
