#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import yaml
from infra.clients import MorgansClient
from infra.exceptions import APIException
from infra.util import get_logger, handle_exception, read_db_config

__author__ = 'Jianhua Shi'

logger = get_logger()
version = 6
MIGRATION_CONFIG_DIR = os.path.dirname(__file__) + '/migration.d/'


@handle_exception(logger)
def migrate(cluster):
    if not cluster['is_global']:
        return True
    update_alert_templates(cluster)
    return True

def update_alert_templates(cluster):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    _, db_password = read_db_config()
    if not os.getenv('DB_HOST') and not db_password:
        logger.info('{} it seems that morgans has no template db, skip'.format(prefix))
        return

    organization = raw_input("请输入该环境的org名字(默认是alauda):")
    organization = 'alauda' if organization == "" else organization
    templates = MorgansClient.list_alert_templates(organization, cluster, {'backend': "DB", 'page_size': 10000})

    for template in templates.get('results', []):
        name = template['name']
        organization = template['organization']

        for alert in template.get('template', []):
            for q in alert.get('metric', {}).get('queries', []):
                if q.get('range', 0) > 0:
                    continue
                q['range'] = alert.get('metric', {}).get('step', 60)

        try:
            logger.info('{} try to migrate alert template {}/{}'.format(
                prefix, organization, name))
            MorgansClient.create_alert_template(organization, cluster, template, {'backend': "CRD"})
        except APIException as e:
            if e.status_code == 409:
                logger.info('{} try to update alert template {}/{}'.format(
                    prefix, organization, name))
                MorgansClient.update_alert_template(organization, cluster, name, template, {'backend': "CRD"})
                continue
            else:
                logger.info('{} create alert template exception {} {}'.format(prefix, e.status_code, e.error_type))
                raise e
