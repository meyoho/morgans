#!/usr/bin/env python
# -*- coding: utf-8 -*-

from infra.clients import MorgansClient
from infra.util import get_logger, handle_exception
from infra.variables import label_app, label_project

__author__ = 'Jianyong Lian'

logger = get_logger()
version = 4


@handle_exception(logger)
def migrate(cluster):
    if not cluster['has_metric_feature']:
        return True
    updateAlerts(cluster)
    return True


def updateAlerts(cluster):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    # Get the namespaces resources of cluster
    ns_resources = MorgansClient.list_resources(cluster, '', 'namespace')

    alerts = MorgansClient.list_alerts(cluster)
    for alert in alerts:
        namespace = alert['namespace']
        resource_name = alert['resource_name']
        group_name = alert['group_name']
        alert_name = alert['name']

        # if group_name is 'general', it means that the migration job has been done. skip
        if group_name == 'general':
            continue

        logger.info('{} update alert {}, {}, {}'.format(prefix, resource_name, group_name, alert_name))

        alert_object_kind, alert_object_name, alert_object_namespace = getQueryLabels(alert)

        # Add project and application for workload alerts
        if alert_object_kind in ['deployment', 'daemonset', 'statefuleset']:
            resource = MorgansClient.get_resource(cluster, alert_object_namespace, alert_object_kind,
                                                  alert_object_name)
            alert_object_application = resource.get('metadata', {}).get('labels', {}).get(label_app, '')
            for query in alert['metric']['queries']:
                query['labels'].append(
                    {
                        'name': 'application',
                        'type': 'EQUAL',
                        'value': alert_object_application,
                    },
                )

            for ns in ns_resources['items']:
                if ns['metadata']['name'] == alert_object_namespace:
                    alert['project'] = ns.get('metadata', {}).get('labels', {}).get(label_project, '')
                    break
        # Turn Other alerts to Cluster
        elif alert_object_kind == 'Other':
            for query in alert['metric']['queries']:
                for label in query['labels']:
                    if label['name'] == 'kind':
                        label['value'] = "Cluster"
                    if label['name'] == 'name':
                        label['value'] = cluster['name']

        MorgansClient.create_alert(cluster, alert)
        MorgansClient.delete_alert(cluster, namespace, resource_name, group_name, alert_name)


def getQueryLabels(alert):
    alert_object_kind = ''
    alert_object_name = ''
    alert_object_namespace = ''
    for query in alert['metric']['queries']:
        for label in query['labels']:
            if label['name'] == 'kind':
                alert_object_kind = label['value']
            if label['name'] == 'name':
                alert_object_name = label['value']
            if label['name'] == 'namespace':
                alert_object_namespace = label['value']
    return alert_object_kind, alert_object_name, alert_object_namespace
