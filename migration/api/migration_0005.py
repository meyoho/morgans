#!/usr/bin/env python
# -*- coding: utf-8 -*-

from infra.clients import MorgansClient
from infra.exceptions import APIException
from infra.util import get_logger, handle_exception

__author__ = 'Jianhua Shi'

logger = get_logger()
version = 5


@handle_exception(logger)
def migrate(cluster):
    if not cluster['has_metric_feature']:
        return True
    update_alerts(cluster)
    return True


def update_alerts(cluster):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    alerts = MorgansClient.list_alerts(cluster)
    for alert in alerts:
        namespace = alert['namespace']
        resource_name = alert['resource_name']
        group_name = alert['group_name']
        alert_name = alert['name']

        queries = alert.get('metric', {}).get('queries', [])
        for q in queries:
            if q.get('range', -1) >= 0:
                continue
            q['range'] = alert.get('metric', {}).get('step', 60)

        try:
            logger.info('{} try to migrate alert {}/{}/{}/{}'.format(
                prefix, namespace, resource_name, group_name, alert_name))
            MorgansClient.create_alert(cluster, alert)
        except APIException as e:
            if e.status_code == 409:
                logger.info('{} try to update alert {}/{}/{}/{}'.format(
                    prefix, namespace, resource_name, group_name, alert_name))
                MorgansClient.update_alert(cluster, namespace, resource_name, group_name, alert_name, alert)
                continue
            else:
                logger.info('{} create alert exception {} {}'.format(prefix, e.status_code, e.error_type))
                raise e
        logger.info('{} try to delete alert {}/{}/{}/{}'.format(
            prefix, namespace, resource_name, group_name, alert_name))
        MorgansClient.delete_alert(cluster, namespace, resource_name, group_name, alert_name)
