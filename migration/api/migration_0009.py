#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

from infra.clients import MorgansClient
from infra.exceptions import APIException
from infra.util import get_logger, handle_exception
from infra.variables import *

__author__ = 'Jianhua Shi'

logger = get_logger()
version = 9


@handle_exception(logger)
def migrate(cluster):
    if not cluster['has_metric_feature']:
        return True
    update_prometheus_rules(cluster)
    return True


def update_prometheus_rules(cluster):
    uuid = cluster['uuid']
    name = cluster['name']
    prefix = '[{}/{}]'.format(name, uuid)

    namespaces = MorgansClient.list_resources(cluster, '', 'namespace')
    for namespace in namespaces['items']:
        namespace = namespace['metadata']['name']
        resources = MorgansClient.list_resources(cluster, namespace, 'prometheusrule')
        for resource in resources['items']:
            resource_prefix = prefix + ' [{}/{}]'.format(resource.get('metadata').get('namespace'),
                                                         resource.get('metadata').get('name'))
            translate_resource_labels(resource)
            logger.info('{} begin to update with json: {}'.format(
                            resource_prefix, json.dumps(resource, indent=4, sort_keys=True)))
            if not can_migrate(resource):
                logger.info('{} skip migrate this prometheus rule'.format(resource_prefix))
                continue
            update_resource_owner_reference(cluster, resource)
            update_resource_labels(resource)
            update_resource_annotations(resource)
            for group in resource['spec']['groups']:
                for rule in group['rules']:
                    update_rule_labels(resource, rule)
                    update_rule_annotations(resource, rule)
            logger.info('{} try to update with json: {}'.format(
                resource_prefix, json.dumps(resource, indent=4, sort_keys=True)))
            MorgansClient.create_resource(cluster, 'prometheusrule', resource, True)

def translate_resource_labels(resource):
    labels = resource['metadata'].get('labels', {})
    for k, v in labels.items():
        n = k.replace('alert.alauda.io', 'alert.'+ base_domain)
        if n != k:
            labels[n] = v
            labels.pop(k, None)
    resource['metadata']['labels'] = labels

def can_migrate(resource):
    owner = resource.get('metadata', {}).get('labels', {}).get(label_alert_owner, '')

    if owner != 'AlaudaSystem' and owner != 'System':
        return False
    if len(resource['spec']['groups']) < 1:
        return False
    if len(resource['spec']['groups'][0]['rules']) < 1:
        return False
    for group in resource['spec']['groups']:
        for rule in group['rules']:
            meta_string = rule.get('annotations', {}).get('AlertMeta', '')
            if meta_string == '':
                return False
            meta_dict = json.loads(meta_string)
            queries = meta_dict.get('metric', {}).get('queries', [])
            if len(queries) < 0:
                return False
    return True


def get_rule_meta(rule):
    meta_string = rule.get('annotations').get('AlertMeta')
    result = json.loads(meta_string)
    query = result.get('metric').get('queries')[0]
    meta = {
        'aggregator': query.get('aggregator', ''),
        'range': query.get('range', 0),
        'alert_name': result.get('name'),
        'creator': result.get('creator', ''),
        'comparison': result.get('compare'),
        'threshold': result.get('threshold'),
        'unit': result.get('unit', ''),
    }
    for item in query.get('labels', []):
        meta[item['name'].lower()] = item['value']
    return meta


def update_resource_owner_reference(cluster, resource):
    def get_involved_node_resource(_namespace, _kind, _name):
        _resource = MorgansClient.list_resources(cluster, _namespace, _kind)
        for i in _resource['items']:
            for j in i.get('status', {}).get('addresses', []):
                if j['type'] == 'InternalIP' and j['address'] == _name:
                    i['apiVersion'] = _resource['apiVersion']
                    i['kind'] = 'Node'
                    return i

        return

    def get_involved_workload_resource(_namespace, _kind, _name):
        try:
            _resource = MorgansClient.get_resource(cluster, _namespace, _kind, _name)
        except APIException as e:
            if e.status_code == 404:
                m = resource.get('metadata', {})
                logger.info('[{}/{}] [{}/{}] can not find owner reference'.format(
                    cluster['name'], cluster['uuid'], m.get('namespace'), m.get('name')))
                return
            else:
                raise e
        return _resource

    rule = resource['spec']['groups'][0]['rules'][0]
    meta = get_rule_meta(rule)
    name = meta.get('name', '')
    kind = meta.get('kind', '').lower()
    namespace = meta.get('namespace', 'default')
    involved_resource = None
    if kind in ['deployment', 'daemonset', 'statefulset']:
        involved_resource = get_involved_workload_resource(namespace, kind, name)
    if kind == 'node':
        involved_resource = get_involved_node_resource(namespace, kind, name)
    if not involved_resource:
        return

    resource['ownerReferences'] = [
        {
            'apiVersion': involved_resource['apiVersion'],
            'controller': True,
            'blockOwnerDeletion': True,
            'kind': involved_resource['kind'],
            'name': involved_resource['metadata']['name'],
            'uid': involved_resource['metadata']['uid'],
        }
    ]


def update_resource_labels(resource):
    rule = resource['spec']['groups'][0]['rules'][0]
    meta = get_rule_meta(rule)
    kind = meta.get('kind', '').lower()
    kind_alias = {
        'deployment': 'Deployment',
        'daemonset': 'DaemonSet',
        'statefulset': 'StatefulSet',
        'node': 'Node',
        'cluster': 'Cluster'
    }
    resource['metadata']['labels'][label_alert_kind] = kind_alias.get(kind, kind)
    resource['metadata']['labels'][label_alert_owner] = 'System'
    if resource['metadata']['labels'][label_alert_kind] == 'Node' \
            and resource['metadata']['labels'][label_alert_name] == 'all':
        resource['metadata']['labels'][label_alert_name] = '0.0.0.0'


def update_resource_annotations(resource):
    notifications_list = []
    for group in resource['spec']['groups']:
        for rule in group['rules']:
            meta_string = rule.get('annotations').get('AlertMeta')
            meta_dict = json.loads(meta_string)
            nfs = meta_dict.get('notifications', [])
            notifications_list += nfs
    notifications = []
    unique_name_set = {}
    for nf in notifications_list:
        unique_name = '___'.join([nf.get('uuid', ''), nf['name'], nf.get('namespace', 'alauda-system')])
        if not unique_name_set.get(unique_name):
            notifications.append(nf)
            unique_name_set[unique_name] = True
    annotations = (resource['metadata'].get('annotations', {}))
    annotations[label_alert_notifications] = json.dumps(notifications)
    resource['metadata']['annotations'] = annotations


def update_rule_labels(resource, rule):
    meta = get_rule_meta(rule)
    resource_labels = resource['metadata']['labels']
    labels = rule.get('labels', {})
    labels['alert_name'] = meta['alert_name']
    labels['alert_creator'] = meta['creator']
    labels['application'] = resource_labels[label_alert_application]
    labels['alert_cluster'] = resource_labels[label_alert_cluster]
    labels['alert_project'] = resource_labels[label_alert_project]
    labels['alert_involved_object_name'] = resource_labels[label_alert_name]
    labels['alert_involved_object_kind'] = resource_labels[label_alert_kind]
    labels['alert_involved_object_namespace'] = resource_labels[label_alert_namespace]
    labels['alert_indicator'] = meta['__name__']
    labels['alert_indicator_aggregate_range'] = str(meta['range'])
    labels['alert_indicator_aggregate_function'] = meta['aggregator']
    labels['alert_indicator_comparison'] = meta['comparison']
    labels['alert_indicator_threshold'] = str(meta['threshold'])
    labels['alert_indicator_log_query'] = meta.get('query', '')
    labels['alert_indicator_unit'] = meta['unit']
    labels.pop("alert_unit", None)
    labels.pop("alert_compare", None)
    labels.pop("alert_threshold", None)
    rule['labels'] = labels


def update_rule_annotations(resource, rule):
    resource_annotations = resource['metadata']['annotations']
    annotations = rule.get('annotations', {})
    annotations['alert_current_value'] = '{{ $value }}'
    annotations['alert_notifications'] = resource_annotations[label_alert_notifications]
    annotations.pop("AlertMeta", None)
    annotations.pop("AlertCurrentValue", None)
    rule['annotations'] = annotations
