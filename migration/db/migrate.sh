#!/usr/bin/env sh
set -a

MYSQL_ENGINE="mysql"
DB_ENGINE=${DB_ENGINE:-pgsql}
DB_PORT=${DB_PORT:-5432}
DB_NAME=${DB_NAME:-morgansdb}

if [ -e "/etc/pass_db/username" ]; then
    DB_USER=$(cat /etc/pass_db/username)
fi

if [ -e "/etc/pass_db/password" ]; then
    DB_PASSWORD=$(cat /etc/pass_db/password)
fi

if [ -z "$DB_HOST" -a -z "$DB_PASSWORD" ]; then
    echo "It seems that morgans has no db engine. Migration canceled."
    exit 1
fi

DB_PASSWORD=$(python -c 'import os, urllib; print urllib.quote(os.getenv("DB_PASSWORD", ""))')
if [ "$DB_ENGINE" = "$MYSQL_ENGINE" ]; then
    url="mysql://$DB_USER:$DB_PASSWORD@tcp($DB_HOST:$DB_PORT)/$DB_NAME"
    echo "Running mysql migration with connection string: "${url}
    migrate-mysql -verbose -database ${url} -path /morgans/migration/db/mysql up
else
    url="postgres://$DB_USER:$DB_PASSWORD@$DB_HOST:$DB_PORT/$DB_NAME?sslmode=disable"
    echo "Running postgresql migration with connection string: "${url}
    migrate-pgsql -verbose -database ${url} -path /morgans/migration/db/pgsql up
fi
