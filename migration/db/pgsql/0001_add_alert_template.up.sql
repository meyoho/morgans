CREATE TABLE IF NOT EXISTS morgans_alert_template (
  uuid         VARCHAR(36)              NOT NULL PRIMARY KEY,
  name         VARCHAR(64)              NOT NULL,
  organization VARCHAR(64)              NOT NULL,
  kind         VARCHAR(64)              NOT NULL,
  creator      VARCHAR(64)              NOT NULL,
  template     TEXT                     NOT NULL,
  created_at   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  description  VARCHAR(512),
  CONSTRAINT organization_name_unique UNIQUE (organization, name)
);

CREATE INDEX idx_alert_template_organization
  ON morgans_alert_template (organization);
