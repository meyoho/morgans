package dingtalk

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"morgans/common"
	"morgans/config"
	"morgans/model"
	nfModel "morgans/notification/model"
)

var (
	urlTemplate = "https://oapi.dingtalk.com/robot/send?access_token=%v"
)

type textMessage struct {
	MsgType string      `json:"msgtype"`
	Text    textContent `json:"text"`
}

type textContent struct {
	Content string `json:"content"`
}

type linkMessage struct {
	MsgType string      `json:"msgtype"`
	Link    linkContent `json:"link"`
}
type linkContent struct {
	Title      string `json:"title"`
	Text       string `json:"text"`
	MessageUrl string `json:"messageUrl"`
	PicUrl     string `json:"picUrl"`
}

type Options struct {
	Token string `json:"token" required:"true"`
}

type Client struct {
	Opt    Options        `json:"opt"`
	Body   interface{}    `json:"body"`
	Logger *common.Logger `json:"logger"`
}

func NewDingtalk(subscription *model.Subscription, tmplType string, tmplData map[string]string, logger *common.Logger) *Client {
	var opt = Options{
		Token: subscription.Recipient,
	}

	client := New(opt)
	client.Logger = logger
	return client.withBody(tmplType, tmplData)
}

func New(opt Options) *Client {
	return &Client{Opt: opt}
}

func (c Client) withBody(tmplType string, tmplData map[string]string) *Client {

	content := fmt.Sprintf(`{{ template "dingtalk.%s.%s.text" . }}`,
		config.GlobalConfig.SMTP.Language,
		tmplType)
	content, err := nfModel.Tmpl.ExecuteTextString(content, tmplData)
	if err != nil {
		panic(err)
	}

	if tmplData["dingtalk_format"] == "link" {
		body := linkMessage{
			MsgType: "link",
			Link:    linkContent{},
		}

		var lc = linkContent{}
		if err := json.Unmarshal([]byte(content), lc); err != nil {
			c.Logger.Errorf("Unmarshal content to dingtalk message err: %v", err.Error())
		}
		body.Link = lc

		c.Body = body
		return &c
	} else {
		body := textMessage{
			MsgType: "text",
			Text:    textContent{},
		}

		var tc = textContent{}
		if err := json.Unmarshal([]byte(content), &tc); err != nil {
			c.Logger.Errorf("Unmarshal content to dingtalk message err: %v", err.Error())
		}
		body.Text = tc

		c.Body = body
		return &c
	}
}

func (c Client) Send() error {
	hc := &http.Client{Timeout: time.Second * 10}

	url := fmt.Sprintf(urlTemplate, c.Opt.Token)
	c.Logger.Infof("Dingtalk url: %+v", url)

	bodyBytes, err := json.Marshal(c.Body)
	if err != nil {
		c.Logger.Infof("Dingtalk marshal body error: %+v", err.Error())
		return err
	}
	bodyBuf := bytes.NewBuffer(bodyBytes)
	c.Logger.Infof("Dingtalk body: %+v", bodyBuf.String())

	req, err := http.NewRequest("POST", url, bodyBuf)
	if err != nil {
		c.Logger.Infof("Dingtalk new request error: %+v", err.Error())
		return err
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json;charset=utf-8")

	resp, err := hc.Do(req)
	if err != nil {
		c.Logger.Infof("Dingtalk send error: %+v", err.Error())
		return err
	}
	c.Logger.Infof("Dingtalk response status: %+v", resp.Status)

	respBuf := new(bytes.Buffer)
	_, err = respBuf.ReadFrom(resp.Body)
	if err != nil {
		c.Logger.Infof("Dingtalk read from resp body err, %s", err.Error())
		return err
	}
	c.Logger.Infof("Dingtalk response body: %+v", respBuf.String())
	c.Logger.Infof("Dingtalk to %+v successfully", c.Opt.Token)
	resp.Body.Close()

	return nil
}
