package notification

import (
	"morgans/common"
	"morgans/model"
	"morgans/notification/dingtalk"
	"morgans/notification/email"
	nfModel "morgans/notification/model"
	"morgans/notification/sms"
	"morgans/notification/webhook"
)

func Run() {
	for {
		nf := <-model.NotificationChannel
		logger := common.NewNoticationLogger(nf.Meta)
		for _, r := range nf.Receiver {
			sender := GetSender(r, nf.Body, logger)
			if sender != nil {
				go sender.Send()
			}
		}
	}
}

func GetSender(subscription *model.Subscription, body *model.NotificationBody, logger *common.Logger) nfModel.NotificationSender {
	tmplType := body.TemplateType
	tmplData := nfModel.GetTemplateData(body)
	switch subscription.Method {
	case nfModel.ByEmail:
		return email.NewEmail(subscription, tmplType, tmplData, logger)
	case nfModel.BySMS:
		return sms.NewSMS(subscription, tmplType, tmplData, logger)
	case nfModel.ByWebhook:
		return webhook.NewWebhook(subscription, tmplType, tmplData, logger)
	case nfModel.ByDingtalk:
		return dingtalk.NewDingtalk(subscription, tmplType, tmplData, logger)
	default:
		return nil
	}
}
