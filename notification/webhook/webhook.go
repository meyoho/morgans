package webhook

import (
	"net/http"

	"morgans/common"
	"morgans/model"

	"github.com/appscode/go/net/httpclient"
)

type Body struct {
	Type    string            `json:"type"`
	Payload map[string]string `json:"payload"`
}
type Options struct {
	URL                string   `json:"url"`
	To                 []string `json:"to"`
	Username           string   `json:"username"`
	Password           string   `json:"password"`
	Token              string   `json:"token"`
	CACertData         string   `json:"ca+cert_data"`
	ClientCertData     string   `json:"client_cert_data"`
	ClientKeyData      string   `json:"client_key_data"`
	InsecureSkipVerify bool     `json:"insecure_skip_verify"`
}

type Client struct {
	Opt    Options        `json:"opt"`
	Body   interface{}    `json:"body"`
	Logger *common.Logger `json:"logger"`
}

func NewWebhook(subscription *model.Subscription, tmplType string, tmplData map[string]string, logger *common.Logger) *Client {
	var opt = Options{
		URL:   subscription.Recipient,
		Token: subscription.Secret,
		To:    []string{},
	}
	client := New(opt)
	client.Logger = logger

	return client.to([]string{subscription.Recipient}).
		withBody(tmplType, tmplData)
}

func New(opt Options) *Client {
	return &Client{Opt: opt}
}

func (c Client) withBody(tmplType string, tmplData map[string]string) *Client {
	var body = Body{
		Type:    tmplType,
		Payload: tmplData,
	}
	c.Body = body
	return &c
}

func (c Client) to(to []string) *Client {
	c.Opt.To = to
	return &c
}

func (c Client) Send() error {
	if len(c.Opt.To) == 0 {
		c.Logger.Infof("Webhook no receiver")
		return common.NewError(common.CodeBadRequest, "Missing to")
	}
	c.Logger.Infof("Webhook to: %+v", c.Opt.To)
	c.Logger.Infof("Webhook data: %+v", c.Body)

	hc := httpclient.Default().
		WithBaseURL(c.Opt.URL).
		WithBasicAuth(c.Opt.Username, c.Opt.Password).
		WithBearerToken(c.Opt.Token)
	if c.Opt.CACertData != "" {
		if c.Opt.ClientCertData != "" && c.Opt.ClientKeyData != "" {
			hc = hc.WithTLSConfig([]byte(c.Opt.CACertData), []byte(c.Opt.ClientKeyData), []byte(c.Opt.ClientKeyData))
		} else {
			hc = hc.WithTLSConfig([]byte(c.Opt.CACertData))
		}
	}
	if c.Opt.InsecureSkipVerify {
		hc = hc.WithInsecureSkipVerify()
	}

	resp, err := hc.Call(http.MethodPost, "", c.Body, nil, true)
	if err != nil {
		c.Logger.Errorf("Webhook call error: %v", err.Error())
		return err
	}
	c.Logger.Infof("Webhook response status: %+v", resp.Status)
	c.Logger.Infof("Webhook to %+v successfully", c.Opt.To)
	resp.Body.Close()

	return nil
}
