package model

import (
	"time"

	"morgans/common"
	"morgans/model"

	amTemplate "github.com/prometheus/alertmanager/template"
)

const (
	// Notification method
	ByWebhook  = "webhook"
	ByEmail    = "email"
	BySMS      = "sms"
	ByDingtalk = "dingtalk"

	TypeDockerBuildCompleted = "docker_build_completed"
	TypeAccountsCreated      = "accounts_created"

	// Template files
	TemplateFile = "/etc/morgans/notification/template/*.tmpl"
)

var Tmpl amTemplate.Template

func init() {
	tmpl, err := amTemplate.FromGlobs(TemplateFile)
	if err != nil {
		panic(err)
	}
	Tmpl = *tmpl
}

type NotificationSender interface {
	Send() error
}

type SMSTemplate struct {
	Type   string   `json:"type"`
	Fields []string `json:"fields"`
}

var (
	SMSAlarm = SMSTemplate{
		Type:   "alarm",
		Fields: []string{"subject", "reason", "time"},
	}
	SMSBuild = SMSTemplate{
		Type:   "build",
		Fields: []string{"subject", "reason", "time"},
	}
	SMSPipelineTask = SMSTemplate{
		Type:   "pipeline_task",
		Fields: []string{"pipeline", "task", "time"},
	}
	SMSPipelineResult = SMSTemplate{
		Type: "pipeline_result",
		Fields: []string{"pipeline", "registry", "repository",
			"image_tag", "started_at", "ended_at", "reason"},
	}
	SMSLogAlarm = SMSTemplate{
		Type: "log_alarm",
		Fields: []string{"subject", "trigger_type", "saved_search",
			"time_range", "threshold", "result", "time"},
	}
	SMSJenkins = SMSTemplate{
		Type: "jenkins",
		Fields: []string{"result", "status", "repository", "branch",
			"version", "started_at", "duration", "timestamp"},
	}
	SMSResetPassword = SMSTemplate{
		Type:   "reset_password",
		Fields: []string{"captcha", "expiration_date"},
	}

	SMSTemplates = []*SMSTemplate{
		&SMSAlarm,
		&SMSBuild,
		&SMSLogAlarm,
		&SMSJenkins,
		&SMSPipelineResult,
		&SMSPipelineTask,
		&SMSResetPassword,
	}

	HtmlTemplate = []string{
		TypeDockerBuildCompleted,
		TypeAccountsCreated,
	}
)

func GetTemplateData(nb *model.NotificationBody) map[string]string {
	var payload = map[string]string{}
	payload = nb.Data.Payload
	payload["subject"] = nb.Data.Subject
	payload["reason"] = nb.Data.Content
	payload["time"] = common.FixTime(time.Now())
	return payload
}
