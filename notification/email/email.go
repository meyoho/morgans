package email

import (
	"crypto/tls"
	"fmt"
	"strings"

	"morgans/common"
	"morgans/config"
	"morgans/model"
	nfModel "morgans/notification/model"

	"gopkg.in/gomail.v2"
)

type Options struct {
	Host               string   `json:"host" required:"true"`
	Port               int      `json:"port" required:"true"`
	InsecureSkipVerify bool     `json:"insecure_skip_verify"`
	Username           string   `json:"username" required:"true"`
	Password           string   `json:"password"  required:"true"`
	From               string   `json:"from" required:"true"`
	To                 []string `json:"to"`
}

type Client struct {
	Opt     Options        `json:"opt"`
	Subject string         `json:"subject"`
	Body    string         `json:"body"`
	Html    bool           `json:"html"`
	Logger  *common.Logger `json:"logger"`
}

func NewEmail(subscription *model.Subscription, tmplType string, tmplData map[string]string, logger *common.Logger) *Client {
	var opt = Options{
		Host:               config.GlobalConfig.SMTP.ServerHost,
		Port:               config.GlobalConfig.SMTP.ServerPort,
		InsecureSkipVerify: config.GlobalConfig.SMTP.InsecureSkipVerify,
		Username:           config.GlobalConfig.SMTP.Username,
		Password:           config.GlobalConfig.SMTP.Password,
		From:               config.GlobalConfig.SMTP.From,
		To:                 []string{},
	}

	client := New(opt)
	client.Logger = logger

	return client.to(strings.Split(subscription.Recipient, ",")).
		withSubject(tmplType, tmplData).
		withBody(tmplType, tmplData)
}

func New(opt Options) *Client {
	return &Client{Opt: opt}
}

func (c Client) from(from string) Client {
	c.Opt.From = from
	return c
}

func (c Client) to(to []string) *Client {
	c.Opt.To = to
	return &c
}

func (c Client) withSubject(tmplType string, tmplData map[string]string) *Client {
	subject, err := nfModel.Tmpl.ExecuteTextString(
		fmt.Sprintf(`{{ template "email.%s.%s.subject" . }}`,
			config.GlobalConfig.SMTP.Language,
			tmplType),
		tmplData)
	if err != nil {
		panic(err)
	}
	c.Subject = subject
	return &c
}

func (c Client) withBody(tmplType string, tmplData map[string]string) *Client {
	body := fmt.Sprintf(`{{ template "email.%s.%s.message.txt" . }}`,
		config.GlobalConfig.SMTP.Language,
		tmplType)
	if common.StringInSlice(tmplType, nfModel.HtmlTemplate) {
		body = fmt.Sprintf(`{{ template "email.%s.%s.message.html" . }}`,
			config.GlobalConfig.SMTP.Language,
			tmplType)
	}
	eBody, err := nfModel.Tmpl.ExecuteTextString(body, tmplData)
	if err != nil {
		panic(err)
	}
	c.Body = eBody
	return &c
}

func (c Client) Send() error {
	if len(c.Opt.To) == 0 {
		c.Logger.Infof("Email no receiver")
		return common.NewError(common.CodeBadRequest, "Missing to")
	}
	c.Logger.Infof("Email to: %+v", c.Opt.To)
	c.Logger.Infof("Email body: %+v", c.Body)

	mail := gomail.NewMessage()
	mail.SetHeader("From", c.Opt.From)
	mail.SetHeader("To", c.Opt.To...)
	mail.SetHeader("Subject", c.Subject)
	if c.Html {
		mail.SetBody("text/html", c.Body)
	} else {
		mail.SetBody("text/plain", c.Body)
	}

	var d *gomail.Dialer
	if c.Opt.Username != "" && c.Opt.Password != "" {
		d = gomail.NewDialer(c.Opt.Host, c.Opt.Port, c.Opt.Username, c.Opt.Password)
	} else {
		d = &gomail.Dialer{Host: c.Opt.Host, Port: c.Opt.Port}
	}
	d.SSL = config.GlobalConfig.SMTP.SSL
	if c.Opt.InsecureSkipVerify {
		d.TLSConfig = &tls.Config{InsecureSkipVerify: config.GlobalConfig.SMTP.InsecureSkipVerify}
	}
	if err := d.DialAndSend(mail); err != nil {
		c.Logger.Errorf("Email to %+v error: %v", c.Opt.To, err.Error())
		return err
	}
	c.Logger.Infof("Email to %+v successfully", c.Opt.To)

	return nil
}

func (c Client) SendHtml() error {
	c.Html = true
	return c.Send()
}
