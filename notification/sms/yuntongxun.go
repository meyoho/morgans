package sms

import (
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"morgans/common"
	"morgans/config"
	"morgans/model"
	nfModel "morgans/notification/model"
)

var (
	smsURL = "%v:%v/%v/Accounts/%v/SMS/TemplateSMS?sig=%v"
)

type Body struct {
	To         string      `json:"to"`
	AppId      string      `json:"appId"`
	TemplateId string      `json:"templateId"`
	Datas      interface{} `json:"datas"`
}

type Options struct {
	AccountSid   string   `json:"account_sid"`
	AccountToken string   `json:"account_token"`
	To           []string `json:"to"`
}

type Client struct {
	Opt    Options        `json:"opt"`
	Body   interface{}    `json:"body"`
	Logger *common.Logger `json:"logger"`
}

func NewSMS(subscription *model.Subscription, tmplType string, tmplData map[string]string, logger *common.Logger) nfModel.NotificationSender {
	if config.GlobalConfig.CCP.SmsServerProvider != "provider" {
		return NewAdapter(subscription, tmplType, tmplData, logger)
	}

	var opt = Options{
		AccountSid:   config.GlobalConfig.CCP.AccountSid,
		AccountToken: config.GlobalConfig.CCP.AccountToken,
		To:           []string{},
	}
	client := New(opt)
	client.Logger = logger

	return client.to(strings.Split(subscription.Recipient, ",")).
		withBody(subscription, tmplType, tmplData)
}

func New(opt Options) *Client {
	return &Client{Opt: opt}
}

func (c Client) to(to []string) *Client {
	c.Opt.To = to
	return &c
}

func (c Client) withBody(subscription *model.Subscription, tmplType string, tmplData map[string]string) *Client {
	var datas []string
	for _, st := range nfModel.SMSTemplates {
		switch tmplType {
		case st.Type:
			for _, field := range st.Fields {
				datas = append(datas, tmplData[field])
			}
		}
	}
	c.Body = Body{
		To:         subscription.Recipient,
		AppId:      config.GlobalConfig.CCP.AppId,
		TemplateId: getTemplateId(tmplType),
		Datas:      datas,
	}
	return &c
}

func (c Client) Send() error {
	if len(c.Opt.To) == 0 {
		c.Logger.Infof("SMS no receiver")
		return common.NewError(common.CodeBadRequest, "Missing to")
	}
	c.Logger.Infof("SMS to: %+v", c.Opt.To)

	timeNow := time.Now().Format("20060102150405")
	hc := &http.Client{Timeout: time.Second * 10}

	sigString := config.GlobalConfig.CCP.AccountSid +
		config.GlobalConfig.CCP.AccountToken + timeNow
	sigParameter := md5.Sum([]byte(sigString))
	url := fmt.Sprintf(smsURL,
		config.GlobalConfig.CCP.ServerIp,
		config.GlobalConfig.CCP.ServerPort,
		config.GlobalConfig.CCP.SoftVersion,
		config.GlobalConfig.CCP.AccountSid,
		strings.ToUpper(fmt.Sprintf("%x", sigParameter)),
	)
	c.Logger.Infof("SMS url: %+v", url)

	bodyBytes, err := json.Marshal(c.Body)
	if err != nil {
		c.Logger.Infof("SMS marshal body error: %+v", err.Error())
		return err
	}
	bodyBuf := bytes.NewBuffer(bodyBytes)
	c.Logger.Infof("SMS body: %+v", bodyBuf.String())
	req, err := http.NewRequest("POST", url, bodyBuf)
	if err != nil {
		c.Logger.Infof("SMS new request error: %+v", err.Error())
		return err
	}

	authData := config.GlobalConfig.CCP.AccountSid + ":" + timeNow
	authString := base64.StdEncoding.EncodeToString([]byte(authData))
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json;charset=utf-8")
	req.Header.Add("Authorization", authString)
	c.Logger.Infof("SMS request header: %+v", req.Header)

	resp, err := hc.Do(req)
	if err != nil {
		c.Logger.Infof("SMS send error: %+v", err.Error())
		return err
	}
	c.Logger.Infof("SMS response status: %+v", resp.Status)

	respBuf := new(bytes.Buffer)
	_, err = respBuf.ReadFrom(resp.Body)
	if err != nil {
		c.Logger.Infof("SMS read from resp body err, %s", err.Error())
		return err
	}
	c.Logger.Infof("SMS response body: %+v", respBuf.String())
	c.Logger.Infof("SMS to %+v successfully", c.Opt.To)
	resp.Body.Close()

	return nil
}

func getTemplateId(tmplType string) string {
	id := ""
	switch tmplType {
	case nfModel.SMSAlarm.Type:
		id = config.GlobalConfig.CCP.TemplateId
	case nfModel.SMSBuild.Type:
		id = config.GlobalConfig.CCP.BuildTemplateId
	case nfModel.SMSPipelineTask.Type:
		id = config.GlobalConfig.CCP.PipelineTaskTemplateId
	case nfModel.SMSPipelineResult.Type:
		id = config.GlobalConfig.CCP.PipelineResultTemplateId
	case nfModel.SMSLogAlarm.Type:
		id = config.GlobalConfig.CCP.LogAlarmTemplateId
	case nfModel.SMSJenkins.Type:
		id = config.GlobalConfig.CCP.JenkinsTemplateId
	case nfModel.SMSResetPassword.Type:
		id = config.GlobalConfig.CCP.ResetPasswordTemplateId
	}
	return id
}
