package sms

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"strings"
	"time"

	"morgans/common"
	"morgans/config"
	"morgans/model"
	nfModel "morgans/notification/model"
)

var (
	adapterURL = config.GlobalConfig.CCP.SmsServerProvider
)

type AdapterBody struct {
	MsgMethod string      `json:"msg_method"`
	MsgType   string      `json:"msg_type"`
	DestIsdns []string    `json:"destIsdns"`
	Datas     interface{} `json:"datas"`
}

type AdapterOptions struct {
	To []string `json:"to"`
}

type AdapterClient struct {
	AdapterOptions Options        `json:"opt"`
	AdapterBody    interface{}    `json:"body"`
	Logger         *common.Logger `json:"logger"`
}

func NewAdapter(subscription *model.Subscription, tmplType string, tmplData map[string]string, logger *common.Logger) *AdapterClient {
	var opt = Options{
		To: []string{},
	}
	client := &AdapterClient{AdapterOptions: opt}
	client.Logger = logger

	return client.to(strings.Split(subscription.Recipient, ",")).
		withBody(subscription, tmplType, tmplData)
}

func (c AdapterClient) to(to []string) *AdapterClient {
	c.AdapterOptions.To = to
	return &c
}

func (c AdapterClient) withBody(subscription *model.Subscription, tmplType string, tmplData map[string]string) *AdapterClient {
	var datas []string
	for _, st := range nfModel.SMSTemplates {
		switch tmplType {
		case st.Type:
			for _, field := range st.Fields {
				datas = append(datas, tmplData[field])
			}
		}
	}
	c.AdapterBody = AdapterBody{
		MsgMethod: "mobile",
		MsgType:   tmplType,
		DestIsdns: strings.Split(subscription.Recipient, ","),
		Datas:     datas,
	}
	return &c
}

func (c AdapterClient) Send() error {
	if len(c.AdapterOptions.To) == 0 {
		c.Logger.Infof("SMS no receiver")
		return errors.New("missing to")
	}
	c.Logger.Infof("SMS to: %+v", c.AdapterOptions.To)

	hc := &http.Client{Timeout: time.Second * 10}

	c.Logger.Infof("SMS url: %+v", adapterURL)

	bodyBytes, err := json.Marshal(c.AdapterBody)
	if err != nil {
		c.Logger.Errorf("SMS marshal body error: %+v", err.Error())
		return err
	}
	bodyBuf := bytes.NewBuffer(bodyBytes)
	c.Logger.Infof("SMS body: %+v", bodyBuf.String())
	req, err := http.NewRequest("POST", adapterURL, bodyBuf)
	if err != nil {
		c.Logger.Errorf("SMS new request error: %+v", err.Error())
		return err
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json;charset=utf-8")
	c.Logger.Infof("SMS request header: %+v", req.Header)

	resp, err := hc.Do(req)
	if err != nil {
		c.Logger.Errorf("SMS send error: %+v", err.Error())
		return err
	}
	c.Logger.Infof("SMS response status: %+v", resp.Status)

	respBuf := new(bytes.Buffer)
	_, err = respBuf.ReadFrom(resp.Body)
	if err != nil {
		c.Logger.Errorf("SMS read from resp body err, %s", err.Error())
		return err
	}
	c.Logger.Infof("SMS response body: %+v", respBuf.String())
	c.Logger.Infof("SMS to %+v successfully", c.AdapterOptions.To)
	resp.Body.Close()

	return nil
}
