OS = Linux
VERSION = 0.0.1

fmt:
	go fmt ./...

build: fmt
	docker-compose -p morgans -f run/docker/morgans-mysql.yaml build

up:
	docker-compose -p morgans -f run/docker/morgans-mysql.yaml up

compose:
	docker-compose -p morgans -f run/docker/morgans-mysql.yaml build
	docker-compose -p morgans -f run/docker/morgans-mysql.yaml up

help:
	@$(ECHO) "Targets:"
	@$(ECHO) "compose           - docker compose to build and start this project"
