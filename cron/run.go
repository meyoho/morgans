package cron

import (
	"flag"

	"morgans/common"

	"github.com/robfig/cron"
)

var (
	Logger *common.Logger
	// GCEnabled enable gc or not, flag "enable-gc", default true
	GCEnabled bool
	// GCInterval set gc interval, flag "gc-interval", default 12h
	GCInterval string
	// ReportTTLDays set ttl for meter report, flag "report-ttl-days", default 7
	ReportTTLDays int
)

func init() {
	Logger := common.NewMeterLogger(map[string]string{"job": "report_gc"})

	flag.BoolVar(&GCEnabled, "enable-gc", true, "Enable gc for manager.")
	flag.StringVar(&GCInterval, "gc-interval", "12h", "GC interval for manager.")
	flag.IntVar(&ReportTTLDays, "report-ttl-days", 7, "TTL days for meter report.")
	flag.Parse()

	Logger.Infof("Get gc config: enable-gc %t, gc-interval %s, report-ttl-days %d", GCEnabled, GCInterval, ReportTTLDays)
}

func Run() error {
	if GCEnabled {
		if err := startReportGC(); err != nil {
			return err
		}
	}
	return nil
}

func startReportGC() error {
	c := cron.New()
	rgc, err := NewReportGC(ReportTTLDays)
	if err != nil {
		return err
	}
	_, err = c.AddJob("@every "+GCInterval, rgc)
	if err != nil {
		return err
	}
	go c.Start()
	return nil
}
