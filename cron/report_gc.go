package cron

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"morgans/common"
	"morgans/config"
	"morgans/model"
)

type reportGC struct {
	Endpoint   string
	ApiVersion string
	Timeout    int
	Logger     *common.Logger
	TTL        int
}

func NewReportGC(ttl int) (*reportGC, error) {
	return &reportGC{
		Endpoint:   config.GlobalConfig.Morgans.Endpoint,
		ApiVersion: config.GlobalConfig.Morgans.ApiVersion,
		Timeout:    config.GlobalConfig.Morgans.Timeout,
		Logger:     Logger,
		TTL:        ttl,
	}, nil
}

func (g *reportGC) Run() {
	// if meter feature gate not enabled, no need to run gc job, skip
	if enabled, err := model.MeterEnabled(config.GlobalConfig.Kubernetes.Token); !enabled {
		if err != nil {
			g.Logger.Errorf("Get meter gate error, %s, skip", err.Error())
		} else {
			g.Logger.Errorf("Meter gate has been closed, skip")
		}
		return
	}

	// Sleep random minutes to avoid multiple instance conflict
	rand.Seed(time.Now().Unix())
	minutes := rand.Intn(30)
	g.Logger.Info(fmt.Sprintf("Step in report gc, sleep %d minutes to avoid conflict", minutes))
	time.Sleep(time.Minute * time.Duration(minutes))
	// Try to delete unused report files
	deleted := int64(0)
	t := (time.Now().Add(-time.Hour * 24 * time.Duration(g.TTL+1)))

	dirInfo, err := os.Open(model.ReportPath)
	if err != nil {
		g.Logger.Errorf("Open directory %s error, %s", model.ReportPath, err.Error())
		return
	}
	files, err := dirInfo.Readdir(0)
	if err != nil {
		g.Logger.Errorf("Read directory %s error, %s", model.ReportPath, err.Error())
		return
	}

	for _, file := range files {
		if file.ModTime().Before(t) {
			filePath := fmt.Sprintf("%s/%s", model.ReportPath, file.Name())
			if err != os.Remove(filePath) {
				g.Logger.Errorf("Delete report file %s err, %s. skip", filePath, err.Error())
				continue
			}
			g.Logger.Infof("Delete report file %s.", filePath)
			deleted++
		}
	}
	g.Logger.Info(fmt.Sprintf("Step out report gc, deleted %d reports", deleted))
}
