package alert

import (
	"fmt"

	"morgans/common"
	"morgans/infra/kubernetes"
	"morgans/model"
	"morgans/store"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

type Store struct {
	store.BaseStore
	Request  *model.AlertRequest
	Response *model.AlertResponse
}

func (s *Store) CreateAlert() (*model.AlertResource, error) {
	resource, err := s.GenerateDesiredAlertResource(nil)
	if err != nil {
		s.Logger.Errorf("Generate desired resource error, %s", err.Error())
		return nil, err
	}
	return s.ApplyAlertResource(resource)
}

func (s *Store) UpdateAlert() (*model.AlertResource, error) {
	resource, err := s.generateResourceForUpdate()
	if err != nil {
		s.Logger.Errorf("Generate resource error, %s", err.Error())
		return nil, err
	}

	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		s.Logger.Errorf("Get "+prompt+" error, %s", err.Error())
		return nil, err
	}

	s.Logger.Infof(prompt + "found, update it")
	previous, err := model.RestResultToAlert(result)
	if err != nil {
		return nil, common.BuildUnknownError("rest result to alert error, " + err.Error())
	}
	current, err := s.mergeForUpdate(previous, resource)
	if err != nil {
		s.Logger.Errorf("Merge resource error, %s", err.Error())
		return nil, err
	}
	result, err = client.Request(common.HttpPut, current, nil)
	if err != nil {
		s.Logger.Errorf("Update "+prompt+" error, %s", err.Error())
		return nil, err
	}
	return model.RestResultToAlert(result)
}

func (s *Store) DeleteAlert(resourceName, groupName, alertName, labelSelector string) error {
	resource := s.generateResourceForRetrieve(resourceName)
	params := map[string]string{}
	if labelSelector != "" {
		params = map[string]string{
			"labelSelector": labelSelector,
		}
	}
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, params)
	if err != nil {
		s.Logger.Errorf(prompt + " get error, " + err.Error())
		return err
	}
	if resourceName != "" {
		previous, err := model.RestResultToAlert(result)
		if err != nil {
			return common.BuildUnknownError("rest result to alert error, " + err.Error())
		}

		previous = s.mergeForDelete(previous, groupName, alertName)
		client.Resource = previous
		if len(previous.Spec.Groups) == 0 {
			result, err = client.Request(common.HttpDelete, previous, nil)
		} else {
			result, err = client.Request(common.HttpPut, previous, nil)
		}
		return err
	} else {
		previous, err := model.RestResultToAlerts(result)
		if err != nil {
			return common.BuildUnknownError("rest result to alert error, " + err.Error())
		}

		for _, item := range previous {
			client.Resource = item
			result, err = client.Request(common.HttpDelete, item, params)
			return err
		}
	}
	return nil
}

func (s *Store) GetAlert(resourceName string) (*model.AlertResource, error) {
	resource := s.generateResourceForRetrieve(resourceName)
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		s.Logger.Errorf("Get "+prompt+" error, %s", err.Error())
		return nil, err
	}
	return model.RestResultToAlert(result)
}

func (s *Store) ListAlerts(labelSelector string) ([]*model.AlertResource, error) {
	resource := s.generateResourceForRetrieve("")
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, model.GetAlertSelector(labelSelector))
	if err != nil {
		return nil, err
	}
	return model.RestResultToAlerts(result)
}

func (s *Store) ApplyAlertResource(resource *model.AlertResource) (*model.AlertResource, error) {
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		if !apiErrors.IsNotFound(err) {
			return nil, err
		}
		s.Logger.Infof(prompt + "not found, create it")
		result, err := client.Request(common.HttpPost, resource, nil)
		if err != nil {
			s.Logger.Errorf("Create "+prompt+" error, %s", err.Error())
			return nil, err
		}
		return model.RestResultToAlert(result)
	}

	s.Logger.Infof(prompt + "found, update it")
	client.Resource = resource
	result, err = client.Request(common.HttpPut, resource, nil)
	if err != nil {
		s.Logger.Errorf("Update "+prompt+" error, %s", err.Error())
		return nil, err
	}
	return model.RestResultToAlert(result)
}

func (s *Store) GenerateDesiredAlertResource(previous *model.AlertResource) (*model.AlertResource, error) {
	var err error
	resource, err := s.generateResourceForUpdate()
	if err != nil {
		s.Logger.Errorf("Generate resource error, %s", err.Error())
		return nil, err
	}

	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	if previous == nil {
		client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
		result, err := client.Request(common.HttpGet, resource, nil)
		if err != nil {
			if !apiErrors.IsNotFound(err) {
				s.Logger.Errorf("Get "+prompt+" error, %s", err.Error())
				return nil, err
			}
			resource, err := s.mergeForCreate(nil, resource)
			if err != nil {
				s.Logger.Errorf("Merge resource error, %s", err.Error())
				return nil, err
			}
			s.Logger.Debugf(prompt + "not found, should create it")
			return resource, nil
		}
		previous, err = model.RestResultToAlert(result)
		if err != nil {
			return nil, common.BuildUnknownError("rest result to alert error, " + err.Error())
		}
	}

	current, err := s.mergeForCreate(previous, resource)
	if err != nil {
		s.Logger.Errorf("Merge resource error, %s", err.Error())
		return nil, err
	}
	s.Logger.Infof(prompt + "found, should update it")
	return current, nil
}
