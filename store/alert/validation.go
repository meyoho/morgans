package alert

import (
	"fmt"

	"morgans/common"
	"morgans/model"

	"github.com/asaskevich/govalidator"
	"github.com/gin-gonic/gin"
	pm "github.com/prometheus/common/model"
)

func (s *Store) Validate(force bool) error {
	// skip validation when set force flag as true
	if force {
		return nil
	}

	// Validate the alert name
	if !govalidator.Matches(s.Request.Name, "^[A-Za-z][A-Za-z0-9._-]*$") {
		return fmt.Errorf("%s is not a valid name for alert", s.Request.Name)
	}

	// Validate the alert label name
	for labelName := range s.Request.Labels {
		if !pm.LabelName(labelName).IsValid() {
			return fmt.Errorf("%s is not a valid label name for alert", labelName)
		}
	}

	// Validate the alert serverity
	if !common.StringInSlice(s.Request.Labels[model.LabelSeverity], model.AlertSeverity) {
		return fmt.Errorf("%s is not a valid serverity for alert", s.Request.Labels[model.LabelSeverity])
	}

	// Validate the application and project for application alerts
	meta := s.GetMeta(&s.Request.Metric.Queries[0])
	if common.StringInSlice(meta.Kind, model.ApplicationResourcesSet) {
		if meta.Application == "" {
			return fmt.Errorf("application is needed in labels for creating a application alert")
		}
		if s.Request.Project == "" {
			return fmt.Errorf("project is needed for creating a application alert")
		}
	}
	return nil
}

func (s *Store) ValidateForDelete(c *gin.Context) error {
	if c.Param("resourceName") == "" && c.Query("labelSelector") == "" {
		return fmt.Errorf("no label selector specified when deleting alerts")
	}
	return nil
}
