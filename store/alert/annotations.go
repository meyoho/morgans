package alert

import (
	"strings"

	"morgans/config"
	"morgans/model"
)

func (s *Store) setRuleAnnotations(current *model.AlertResource, alertRequest *model.AlertRequest) *model.AlertResource {
	meta := current.Spec.Groups[0].Rules[0].Meta
	rule := current.Spec.Groups[0].Rules[0]
	if rule.Annotations[model.AlertMessageKey] == "" && config.GlobalConfig.Alert.MessageTemplatesEnabled {
		s.Logger.Infof("Try to set a message for this alert.")
		annotationsMessage := ""
		if template, in := model.AlertMessageTemplatesSet[meta.Indicator]; !in {
			s.Logger.Errorf("Failed to get message: No this indicator in alert templates file; generate alert without message label.")
		} else {
			if config.GlobalConfig.Alert.MessageTemplatesLang == "EN" {
				annotationsMessage = template.EN
			} else {
				annotationsMessage = template.CN
			}
		}
		for _, choice := range model.LabelsChoice {
			annotationsMessage = strings.Replace(annotationsMessage, "{{ $labels."+choice+" }}", rule.Labels[choice], -1)
		}
		rule.Annotations[model.AlertMessageKey] = annotationsMessage
		s.Logger.Infof("Get message: %s", rule.Annotations[model.AlertMessageKey])
	}
	rule.Annotations[model.AlertValueAnnotationKey] = "{{ $value }}"
	// Set alert meta to annotation, it's a mirror for this alert
	rule.Annotations[model.AlertMetaAnnotationKey] = alertRequest.GetAlertMeta()
	return current
}
