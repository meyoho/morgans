package alert

import (
	"morgans/cache"
	"morgans/common"
	"morgans/model"
)

func getAlertViewValue(kind string) string {
	if _, ok := model.WorkloadSet[model.Kind(kind)]; ok {
		return model.AlertViewLabelValueUser
	} else {
		return model.AlertViewLabelValueOps
	}
}

func getAlertKindValue(kind string) string {
	if _, ok := model.WorkloadSet[model.Kind(kind)]; ok {
		return string(model.WorkloadKind)
	} else {
		return kind
	}
}

func (s *Store) setResourceLabels(current *model.AlertResource) *model.AlertResource {
	meta := current.Spec.Groups[0].Rules[0].Meta
	current.Labels = model.GetAlertCommonLabels(s.Cluster.PrometheusConfig.Name)
	toUpdateCurrent := map[string]string{
		model.AlertProjectLabelKey:           s.Request.Project,
		model.AlertClusterLabelKey:           s.Cluster.Name,
		model.AlertObjectKindLabelKey:        getAlertKindValue(meta.Kind),
		model.AlertObjectNameLabelKey:        model.GetAlertNameValue(meta.Kind, meta.Name),
		model.AlertObjectNamespaceLabelKey:   meta.Namespace,
		model.AlertObjectApplicationLabelKey: meta.Application,
	}
	current.Labels = common.MergeMaps(toUpdateCurrent, current.Labels)
	if _, ok := current.Labels[model.AlertViewLabelKey]; !ok {
		current.Labels[model.AlertViewLabelKey] = getAlertViewValue(meta.Kind)
	}
	current.Labels = cache.MergePrometheusRuleSelectorLabels(s.Cluster, current.Labels)
	return current
}

func (s *Store) setRuleLabels(current *model.AlertResource) *model.AlertResource {
	rule := current.Spec.Groups[0].Rules[0]
	rule.Labels = common.MergeMaps(model.GetAlertMetaLabels(rule.Meta), rule.Labels)
	toUpdateRule := map[string]string{
		model.LabelCluster:   s.Cluster.Name,
		model.LabelCreator:   s.Request.Creator,
		model.LabelProject:   s.Request.Project,
		model.LabelAlertName: s.Request.Name,
		model.LabelCompare:   s.Request.Compare,
		model.LabelThreshold: model.FormatThreshold(s.Request.Threshold),
		model.LabelUnit:      s.Request.Unit,
	}
	rule.Labels = model.MergeLabelsForAlertRule(toUpdateRule, rule.Labels)
	return current
}

func (s *Store) setRuleLabelsForUpdate(current *model.AlertResource, from *model.AlertRequest) *model.AlertResource {
	rule := current.Spec.Groups[0].Rules[0]
	rule.Labels = common.MergeMaps(model.GetAlertMetaLabels(rule.Meta), rule.Labels)
	toUpdateRule := map[string]string{
		model.LabelCluster:   s.Cluster.Name,
		model.LabelCreator:   from.Creator,
		model.LabelProject:   from.Project,
		model.LabelAlertName: from.Name,
		model.LabelCompare:   s.Request.Compare,
		model.LabelThreshold: model.FormatThreshold(s.Request.Threshold),
		model.LabelUnit:      s.Request.Unit,
	}
	rule.Labels = model.MergeLabelsForAlertRule(toUpdateRule, rule.Labels)
	return current
}
