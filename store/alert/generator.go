package alert

import (
	"strconv"

	"morgans/model"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (s *Store) generateResourceForRetrieve(resourceName string) *model.AlertResource {
	return &model.AlertResource{
		ResourceMeta: model.ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: model.APIVersionMonitoringV1,
				Kind:       model.KindPrometheusRule,
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name:      resourceName,
				Namespace: s.Request.Namespace,
			},
		},
	}
}

func (s *Store) generateResourceForUpdate() (*model.AlertResource, error) {
	meta := s.GetMeta(&s.Request.Metric.Queries[0])
	alertName := s.Request.Name
	resourceName := s.Request.ResourceName
	groupName := s.Request.GroupName

	expr, err := meta.GetQuery()
	if err != nil {
		s.Logger.Errorf("Get query error, %s", err.Error())
		return nil, err
	}
	expr += " " + s.Request.Compare + " " + model.FormatThreshold(s.Request.Threshold)

	resource := &model.AlertResource{
		ResourceMeta: model.ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: model.APIVersionMonitoringV1,
				Kind:       model.KindPrometheusRule,
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name:      resourceName,
				Namespace: s.Request.Namespace,
			},
		},
		Spec: model.AlertSpec{
			Groups: []*model.AlertGroup{
				{
					Name: groupName,
					Rules: []*model.AlertRule{{
						Meta:        meta,
						Alert:       model.GetPrometheusAlertName(resourceName, groupName, alertName),
						Expr:        expr,
						Labels:      s.Request.Labels,
						For:         strconv.FormatInt(s.Request.Wait, 10) + "s",
						Annotations: s.Request.Annotations,
					}},
				},
			},
		},
	}
	s.Response.Name = alertName
	s.Response.GroupName = groupName
	return resource, nil
}
