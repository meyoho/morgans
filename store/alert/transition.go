package alert

import (
	"encoding/json"
	"time"

	"morgans/common"
	"morgans/config"
	"morgans/infra/prometheus"
	"morgans/model"

	"github.com/pborman/uuid"
	proModel "github.com/prometheus/common/model"
)

func (s *Store) translate() {
	meta := s.GetMeta(&s.Request.Metric.Queries[0])
	if meta.Indicator == model.WorkloadLogKeywordCount {
		s.Request.Wait = 0
	}

	if s.Request.Labels == nil {
		s.Request.Labels = map[string]string{}
	}
	if _, ok := s.Request.Labels[model.LabelSeverity]; !ok {
		s.Request.Labels[model.LabelSeverity] = model.AlertSeverityHigh
	}

	// TODO handle multiple queries in body for alert create/update
	if s.Request.Unit == "" {
		if len(s.Request.Metric.Queries) > 0 {
			s.Request.Unit = string(meta.GetUnit())
		}
	}
	s.Request.AdditionalConfig = map[string]string{}
	if s.Request.Annotations == nil {
		s.Request.Annotations = map[string]string{}
		s.Request.AdditionalConfig[model.AlertMessageFrom] = "no"
	} else if s.Request.Annotations[model.AlertMessageKey] != "" {
		s.Request.AdditionalConfig[model.AlertMessageFrom] = "custom"
	} else if s.Request.Annotations[model.AlertMessageKey] == "" {
		if config.GlobalConfig.Alert.MessageTemplatesEnabled {
			s.Request.AdditionalConfig[model.AlertMessageFrom] = "default"
		} else {
			s.Request.AdditionalConfig[model.AlertMessageFrom] = "no"
		}
	}
	if s.Request.CreatedAt.IsZero() {
		s.Request.CreatedAt = time.Now().UTC()
	}
	s.Request.UpdatedAt = time.Now().UTC()
}

func (s *Store) TranslateForCreate() {
	s.translate()
	if s.Request.UUID == "" {
		s.Request.UUID = uuid.New()
	}

	meta := s.GetMeta(&s.Request.Metric.Queries[0])
	if meta.Namespace != "" {
		s.Request.Namespace = meta.Namespace
	} else {
		s.Request.Namespace = s.Cluster.PrometheusConfig.Namespace
	}

	s.Request.ResourceName = model.GetAlertResourceName(s.Cluster, meta)
	s.Request.GroupName = model.GetAlertGroupName()
}

func (s *Store) TranslateForUpdate() {
	s.translate()
}

func (s *Store) AlertResourceToAlerts(resource *model.AlertResource, groupName string) ([]*model.AlertResponse, error) {
	return s.addStateForAlerts(s.findAlertItemsInResource(resource, groupName, ""), nil)
}

func (s *Store) AlertResourcesToAlerts(resources []*model.AlertResource) ([]*model.AlertResponse, error) {
	result := make([]*model.AlertResponse, 0)
	for _, resource := range resources {
		result = append(result, s.findAlertItemsInResource(resource, "", "")...)
	}
	return s.addStateForAlerts(result, nil)
}

func (s *Store) AlertResourceToAlert(resource *model.AlertResource, groupName string, alertName string) (*model.AlertResponse, error) {
	items := s.findAlertItemsInResource(resource, groupName, alertName)
	if len(items) < 1 {
		s.Logger.Infof("Can not find alert rule in resource")
		return nil, common.BuildNotFoundError("can not find alert rule in resource")
	}
	return s.addStateForAlert(items[0], nil)
}

func (s *Store) findAlertItemsInResource(resource *model.AlertResource, groupName string, alertName string) []*model.AlertResponse {
	var groups []*model.AlertGroup
	result := make([]*model.AlertResponse, 0)
	for _, g := range resource.Spec.Groups {
		if groupName != "" {
			if g.Name == groupName {
				groups = append(groups, g)
			}
		} else {
			groups = append(groups, g)
		}
	}
	if len(groups) == 0 {
		s.Logger.Errorf("No group found, return")
		return []*model.AlertResponse{}
	}

	for _, group := range groups {
		for _, rule := range group.Rules {
			meta, ok := rule.Annotations[model.AlertMetaAnnotationKey]
			if !ok {
				s.Logger.Errorf("Can not found alert meta, skip")
				continue
			}
			if alertName != "" && rule.Alert != model.GetPrometheusAlertName(resource.Name, groupName, alertName) {
				continue
			}
			item := model.AlertResponse{}
			if err := json.Unmarshal([]byte(meta), &item); err != nil {
				s.Logger.Errorf("Unmarshal alert meta, skip")
				continue
			}
			item.Namespace = resource.Namespace
			item.GroupName = group.Name
			item.ResourceName = resource.Name
			// Remove alert meta and alert current value in annotations, it's confused
			delete(item.Annotations, model.AlertMetaAnnotationKey)
			delete(item.Annotations, model.AlertValueAnnotationKey)
			if item.AdditionalConfig[model.AlertMessageFrom] != "custom" {
				delete(item.Annotations, model.AlertMessageKey)
			}
			item.AdditionalConfig = nil
			result = append(result, &item)
		}
	}
	return result
}

func (s *Store) addStateForAlert(alert *model.AlertResponse, firingAlerts *proModel.Vector) (*model.AlertResponse, error) {
	alert.State = model.AlertNormalStatus
	if firingAlerts == nil {
		temp, err := s.getFiringAlerts()
		if err != nil {
			return alert, err
		}
		firingAlerts = &temp
	}
	for _, item := range *firingAlerts {
		name, ok := item.Metric[model.AlertNameField]
		if !ok {
			continue
		}
		if string(name) != model.GetPrometheusAlertName(alert.ResourceName, alert.GroupName, alert.Name) {
			continue
		}
		state, ok := item.Metric[model.AlertStateField]
		if !ok {
			continue
		}
		if p, ok := model.AlertStatusMap[string(state)]; ok {
			if p < model.AlertStatusMap[alert.State] {
				alert.State = string(state)
			}
		}
	}
	return alert, nil
}

func (s *Store) addStateForAlerts(alerts []*model.AlertResponse, firingAlerts *proModel.Vector) ([]*model.AlertResponse, error) {
	s.Logger.Debugf("Try to add state for alerts")
	if firingAlerts == nil {
		temp, err := s.getFiringAlerts()
		if err != nil {
			return alerts, err
		}
		firingAlerts = &temp
	}
	for _, alert := range alerts {
		s.addStateForAlert(alert, firingAlerts)
	}
	return alerts, nil
}

func (s *Store) getFiringAlerts() (proModel.Vector, error) {
	s.Logger.Debugf("Try to get firing alerts")
	client := prometheus.NewClient(*s.Cluster.PrometheusConfig, s.Logger)
	expr, ts := "ALERTS{}", time.Now()
	value, err := client.Query(expr, ts.Unix())
	if err != nil {
		return nil, err
	}
	s.Logger.Debugf("Param: expr [%s], time [%v/%s]", expr, ts.Unix(), ts.Format(time.RFC3339Nano))
	vector := value.(proModel.Vector)
	s.Logger.Debugfl(3, "Firing alerts: [%d/%s]", len(vector), vector.String())
	return vector, err
}
