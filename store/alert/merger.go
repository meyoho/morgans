package alert

import (
	"encoding/json"
	"fmt"

	"morgans/common"
	"morgans/model"
)

func (s *Store) mergeForCreate(previous, current *model.AlertResource) (*model.AlertResource, error) {
	added := false
	current = s.updateResource(current)
	tg := current.Spec.Groups[0]
	tr := current.Spec.Groups[0].Rules[0]
	if previous == nil {
		current.Spec.Groups[0].Rules[0] = tr
		return current, nil
	}
	var groups []*model.AlertGroup
	for _, group := range previous.Spec.Groups {
		if group.Name == tg.Name {
			var rules []*model.AlertRule
			for _, rule := range group.Rules {
				if rule.Alert == tr.Alert {
					s.Logger.Errorf("Alert rule %s/%s/%s already exists in group", current.Name, tg.Name, tr.Alert)
					return nil, common.BuildAlreadyExistsError(fmt.Sprintf("Alert %s already exists in resource %s", tr.Alert, current.Name))
				}
				rules = append(rules, rule)
			}
			if !added {
				rules = append(rules, tr)
				s.Logger.Infof("Append rule %s/%s/%s to group", current.Name, tg.Name, tr.Alert)
				added = true
			}
			group.Rules = rules
		}
		groups = append(groups, group)
	}
	if !added {
		groups = append(groups, tg)
		s.Logger.Infof("Append group %s/%s/%s to spec", current.Name, tg.Name, tr.Alert)
		added = true
	}
	previous.Spec.Groups = groups
	previous.Labels = current.Labels
	return previous, nil
}

func (s *Store) mergeForUpdate(previous, current *model.AlertResource) (*model.AlertResource, error) {
	added := false
	tg := current.Spec.Groups[0]
	tr := current.Spec.Groups[0].Rules[0]
	var groups []*model.AlertGroup
	for _, group := range previous.Spec.Groups {
		if group.Name == tg.Name {
			var rules []*model.AlertRule
			for _, rule := range group.Rules {
				if rule.Alert == tr.Alert {
					s.updateRequest(rule)
					current = s.updateResource(current)
					tr = current.Spec.Groups[0].Rules[0]
					rules = append(rules, tr)
					s.Logger.Infof("Update rule %s/%s/%s in group", current.Name, tg.Name, tr.Alert)
					added = true
					continue
				}
				rules = append(rules, rule)
			}
			if !added {
				s.Logger.Infof("Alert rule %s/%s/%s not found in group", current.Name, tg.Name, tr.Alert)
				return nil, common.BuildNotFoundError(fmt.Sprintf("Alert %s not found in resource %s", tr.Alert, current.Name))
			}
			group.Rules = rules
		}
		groups = append(groups, group)
	}
	if !added {
		s.Logger.Infof("Alert rule %s/%s/%s not found", current.Name, tg.Name, tr.Alert)
		return nil, common.BuildNotFoundError(fmt.Sprintf("Alert %s not found in resource %s", tr.Alert, current.Name))
	}
	previous.Spec.Groups = groups
	previous.Labels = current.Labels
	return previous, nil
}

func (s *Store) mergeForDelete(previous *model.AlertResource, groupName, alertName string) *model.AlertResource {
	var groups []*model.AlertGroup
	for _, group := range previous.Spec.Groups {
		if group.Name == groupName {
			if alertName == "" {
				continue
			}
			var rules []*model.AlertRule
			for _, rule := range group.Rules {
				if rule.Alert == model.GetPrometheusAlertName(previous.Name, groupName, alertName) {
					s.Logger.Infof("Find rule to delete (%s/%s)", groupName, alertName)
					continue
				}
				rules = append(rules, rule)
			}
			if len(rules) == 0 {
				continue
			}
			group.Rules = rules
		}
		groups = append(groups, group)
	}
	previous.Spec.Groups = groups
	return previous
}

func (s *Store) updateResource(current *model.AlertResource) *model.AlertResource {
	toRule := current.Spec.Groups[0].Rules[0]

	// Set labels for the alert resource and alert rule
	current = s.setResourceLabels(current)
	current = s.setRuleLabels(current)

	// Set annotations for alert rule
	s.Request.Labels = toRule.Labels
	current = s.setRuleAnnotations(current, s.Request)
	return current
}

func (s *Store) updateRequest(rule *model.AlertRule) {
	if rule != nil {
		from := &model.AlertRequest{}
		if err := json.Unmarshal([]byte(rule.Annotations[model.AlertMetaAnnotationKey]), from); err != nil {
			s.Logger.Errorf("Json unmarshal error, %s", err.Error())
			panic(err.Error())
		}

		// Set uuid, creator, project and created_at for the alert rule to update
		if from.UUID != "" {
			s.Request.UUID = from.UUID
		}
		s.Request.Name = from.Name
		s.Request.Creator = from.Creator
		s.Request.Project = from.Project
		s.Request.CreatedAt = from.CreatedAt
	}
}
