package router

import (
	"morgans/model"
	"morgans/store"
)

type Store struct {
	store.BaseStore
	Request *model.AlertRouter
}
