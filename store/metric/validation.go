package metric

import (
	"fmt"

	"morgans/common"
	"morgans/model"

	"github.com/asaskevich/govalidator"
)

func (s *QueryStore) Validate() error {
	r := s.Request
	if r.Time <= 0 {
		return common.BuildBadRequestError("invalid unix timestamp for time")
	}
	if len(r.Queries) < 1 {
		return common.BuildBadRequestError("queries can not be empty")
	}
	for _, q := range s.Request.Queries {
		if !govalidator.IsIn(q.Aggregator, model.Aggregators...) {
			return fmt.Errorf("invalid aggregator value %s", q.Aggregator)
		}
	}
	return nil
}

func (s *QueryRangeStore) Validate() error {
	r := s.Request
	if r.Step <= 0 {
		return common.BuildBadRequestError("invalid unix timestamp for step")
	}
	if r.Start <= 0 || r.End <= 0 || r.End-r.Start < r.Step {
		return common.BuildBadRequestError("invalid unix timestamp for start and end")
	}
	if len(r.Queries) < 1 {
		return common.BuildBadRequestError("queries can not be empty")
	}
	for _, q := range s.Request.Queries {
		if !govalidator.IsIn(q.Aggregator, model.Aggregators...) {
			return fmt.Errorf("invalid aggregator value %s", q.Aggregator)
		}
	}
	return nil
}
