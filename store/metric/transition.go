package metric

import (
	"morgans/cache"
	"morgans/common"
	"morgans/model"
	"morgans/store"

	pm "github.com/prometheus/common/model"
)

func TranslateMetrics(bs *store.BaseStore, meta *model.QueryMeta, result pm.Value) {
	addIP := func(metric pm.Metric) {
		node, ok := metric["node"]
		if !ok {
			bs.Logger.Infof("Can not find node field")
			return
		}
		ip, err := cache.GetHostIP(bs.Cluster, string(node))
		if err != nil {
			bs.Logger.Errorf("Find ip for %s error: %s", string(node), err.Error())
			return
		}
		bs.Logger.Debugf("Find ip for %s: %s", node, ip)
		metric["ip"] = pm.LabelValue(ip)
	}

	// Add query id label for metrics
	switch result.Type() {
	case pm.ValMatrix:
		for _, item := range result.(pm.Matrix) {
			if meta.ID != "" {
				item.Metric[model.QueryIDLabelKey] = pm.LabelValue(meta.ID)
			}
		}
	case pm.ValVector:
		for _, item := range result.(pm.Vector) {
			if meta.ID != "" {
				item.Metric[model.QueryIDLabelKey] = pm.LabelValue(meta.ID)
			}
		}
	}

	// Add ip label for metrics if needed
	if !common.StringInSlice(meta.Indicator, model.IndicatorsNeedTranslate) {
		return
	}
	switch result.Type() {
	case pm.ValMatrix:
		for _, item := range result.(pm.Matrix) {
			addIP(item.Metric)
		}
	case pm.ValVector:
		for _, item := range result.(pm.Vector) {
			addIP(item.Metric)
		}
	}
}
