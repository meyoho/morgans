package metric

import (
	"sync"

	"morgans/infra/prometheus"
	"morgans/model"
	"morgans/store"

	pm "github.com/prometheus/common/model"
)

type QueryStore struct {
	store.BaseStore
	Request *model.QueryRequest
}

type QueryRangeStore struct {
	store.BaseStore
	Request *model.QueryRangeRequest
}

func (s *QueryStore) Query() (pm.Vector, error) {
	result := make(pm.Vector, 0)
	client := prometheus.NewClient(*s.Cluster.PrometheusConfig, s.Logger)
	lock := &sync.Mutex{}

	err := Parallel(s.Request.Queries, func(query model.QueryExpression) error {
		meta := s.GetMeta(&query)
		expr, err := meta.GetQuery()
		if err != nil {
			return err
		}
		s.Logger.Infof("Query string: %s", expr)
		m, err := client.Query(expr, s.Request.Time)
		if err != nil {
			return err
		}
		TranslateMetrics(&s.BaseStore, meta, m)
		lock.Lock()
		defer lock.Unlock()
		s.Logger.Debugfl(2, "Query result: %s", m)
		result = append(result, m.(pm.Vector)...)
		return nil
	})
	return result, err
}

func (s *QueryRangeStore) Query() (pm.Matrix, error) {
	result := make(pm.Matrix, 0)
	client := prometheus.NewClient(*s.Cluster.PrometheusConfig, s.Logger)
	lock := &sync.Mutex{}
	err := Parallel(s.Request.Queries, func(query model.QueryExpression) error {
		meta := s.GetMeta(&query)
		expr, err := meta.GetQuery()
		if err != nil {
			return err
		}
		s.Logger.Infof("Query string: %s", expr)
		m, err := client.GetRange(expr, s.Request.Start, s.Request.End, s.Request.Step)
		if err != nil {
			return err
		}
		TranslateMetrics(&s.BaseStore, meta, m)
		lock.Lock()
		defer lock.Unlock()
		s.Logger.Debugfl(2, "Query result: %s", m)
		result = append(result, m.(pm.Matrix)...)
		return nil
	})
	return result, err
}

func Parallel(queries []model.QueryExpression, fn func(query model.QueryExpression) error) error {
	var wg sync.WaitGroup
	errCh := make(chan error, 1)
	for _, q := range queries {
		wg.Add(1)
		// Concurrently calls prometheus query range api to reduce api latency
		go func(q model.QueryExpression) {
			defer wg.Done()
			if err := fn(q); err != nil {
				select {
				case errCh <- err:
				default:
				}
			}
		}(q)
	}
	wg.Wait()

	// All queries will be executed at the same time, the first error will be returned if any
	select {
	case err := <-errCh:
		return err
	default:
		return nil
	}
}
