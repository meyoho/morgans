package inner

import (
	"strconv"

	"morgans/infra/kubernetes"
	"morgans/model"
	"morgans/store"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

type Store struct {
	store.BaseStore
}

func (s *Store) GetVersion() (int64, error) {
	client := kubernetes.NewClient(s.Cluster, kubernetes.GenerateCoreConfigmapResource(s.Cluster), s.Logger)
	c, err := client.GetCoreConfigMap()
	if err != nil {
		if apiErrors.IsNotFound(err) {
			return 0, nil
		}
		return 0, err
	}
	v, ok := c.Data[model.AlertPrometheusVersion]
	if !ok {
		return 0, nil
	}
	i, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return 0, err
	}
	return i, nil
}

func (s *Store) UpdateVersion(version int64) error {
	client := kubernetes.NewClient(s.Cluster, kubernetes.GenerateCoreConfigmapResource(s.Cluster), s.Logger)
	c, err := client.GetCoreConfigMap()
	if err != nil {
		if !apiErrors.IsNotFound(err) {
			return err
		}
		c = kubernetes.GenerateCoreConfigmapResource(s.Cluster)
		c.Data = map[string]string{model.AlertPrometheusVersion: strconv.FormatInt(version, 10)}
		return client.CreateCoreConfigmap(c)
	}
	c.Data[model.AlertPrometheusVersion] = strconv.FormatInt(version, 10)
	return client.UpdateCoreConfigmap(c)
}
