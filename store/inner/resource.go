package inner

import (
	"encoding/json"
	"fmt"

	"morgans/common"
	"morgans/infra/kubernetes"
	"morgans/model"
	"morgans/store"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type KubernetesResourceStore struct {
	store.BaseStore
	Request *model.KubernetesResource
}

func (s *KubernetesResourceStore) CreateResource(apply bool) (*model.KubernetesResource, error) {
	client := kubernetes.NewClient(s.Cluster, s.Request, s.Logger)
	getResponse, err := client.Request(common.HttpGet, s.Request, nil)
	if err != nil {
		if !apiErrors.IsNotFound(err) {
			s.Logger.Errorf("Get resource error, %s", err.Error())
			return nil, err
		}
		s.Logger.Infof("Resource not found, create it")
		client = kubernetes.NewClient(s.Cluster, s.Request, s.Logger)
		result, err := client.Request(common.HttpPost, s.Request, nil)
		if err != nil {
			s.Logger.Errorf("Create resource error, %s", err.Error())
			return nil, err
		}
		return model.RestResultToResource(result)
	}

	if !apply {
		return nil, common.BuildAlreadyExistsError("resource already exists in kubernetes")
	}

	// Resource exists, then update it
	var resourceData = &model.KubernetesResource{}
	getData, err := getResponse.Raw()
	if err = json.Unmarshal(getData, resourceData); err != nil {
		s.Logger.Infof("Unmarshal error, %s", err.Error())
		return nil, err
	}
	// Get the ResourceVersion that the resource has
	resource, err := s.addResourceMeta(*s.Request, *resourceData)
	if err != nil {
		s.Logger.Infof("Add resource meta error, %s", err.Error())
		return nil, err
	}

	// Update kubernetes resource
	client = kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpPut, resource, nil)
	if err != nil {
		s.Logger.Infof("Update resource error, %s", err)
		return nil, err
	}
	return model.RestResultToResource(result)
}

func (s *KubernetesResourceStore) GetResource(namespace, resourceType, resourceName string) (*model.KubernetesResource, error) {
	resource := &model.ResourceMeta{
		TypeMeta: metaV1.TypeMeta{
			APIVersion: model.GetResourceApiVersion(resourceType),
			Kind:       resourceType,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      resourceName,
			Namespace: model.GetResourceNamespace(resourceType, namespace),
		},
	}

	prompt := fmt.Sprintf("%v %v in %v", resource.Kind, resource.Name, namespace)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		s.Logger.Errorf(prompt+" get error, %s", err.Error())
		return nil, err
	}
	return model.RestResultToResource(result)
}

func (s *KubernetesResourceStore) addResourceMeta(to, from model.KubernetesResource) (model.KubernetesResource, error) {
	fromMeta, ok := from["metadata"].(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("failed to get resourceVersion when apply resource")
	}
	toMeta, ok := to["metadata"].(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("failed to get resourceVersion when apply resource")
	}
	toMeta["resourceVersion"] = fromMeta["resourceVersion"]
	return to, nil
}
