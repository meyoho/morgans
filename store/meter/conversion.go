package meter

import (
	"encoding/json"
	"time"

	"morgans/cache/cluster"
	"morgans/cache/namespace"
	"morgans/cache/project"
	"morgans/common"
	"morgans/model"

	elasticV6 "gopkg.in/olivere/elastic.v6"
)

func (m *MeterStore) RestResultToMeterTopn(topn, total *elasticV6.SearchResult) (*model.MeterTopnResponse, error) {
	response := m.initTopNResponse()
	if topn == nil || topn.Hits == nil {
		return response, nil
	}

	topnCPU := 0.0
	topnMemory := 0.0
	if hits, ok := topn.Aggregations.TopHits("topN"); ok {
		for _, hit := range hits.Hits.Hits {
			item, err := m.restHitToMeterItem(hit)
			if err != nil {
				m.Logger.Error("rest hit to meter item error, %s", err.Error())
				continue
			}
			response.TopN = append(response.TopN, item)
			topnCPU += item.Meter.CPU
			topnMemory += item.Meter.Memory
		}
	}

	if totalCPU, ok := total.Aggregations.Sum("cpu"); ok {
		response.Remain.Meter.CPU = *(totalCPU.Value) - topnCPU
	}
	if totalMemory, ok := total.Aggregations.Sum("memory"); ok {
		response.Remain.Meter.Memory = *(totalMemory.Value) - topnMemory
	}

	return response, nil
}

func (m *MeterStore) RestResultToMeterTotal(total *elasticV6.SearchResult) (*model.MeterTotalResponse, error) {
	response := m.initTotalResponse()
	if total == nil || total.Aggregations == nil {
		return response, nil
	}

	if totalMessage, ok := total.Aggregations["total"]; ok {
		var buckets model.MeterTotalBuckets
		err := json.Unmarshal([]byte(*totalMessage), &buckets)
		if err != nil {
			m.Logger.Error("unmarshal message to buckets error, %s", err.Error())
			return response, err
		}
		for _, bucket := range buckets.Buckets {
			for _, item := range response.Results {
				if item.Date == bucket.Key {
					item.Meter.CPU = bucket.CPU.Value
					item.Meter.Memory = bucket.Memory.Value
				}
			}
		}
	}
	return response, nil
}

func (m *MeterStore) RestResultToMeterSummary(result *elasticV6.SearchResult) ([]*model.MeterItem, int, error) {
	if result == nil || result.Hits == nil {
		return []*model.MeterItem{}, 0, nil
	}

	items := []*model.MeterItem{}
	for _, hit := range result.Hits.Hits {
		item, err := m.restHitToMeterItem(hit)
		if err != nil {
			m.Logger.Error("rest hit to meter item error, %s", err.Error())
			continue
		}
		items = append(items, item)
	}

	return items, int(result.TotalHits()), nil
}

func (m *MeterStore) RestResultToMeterQuery(result *elasticV6.SearchResult) ([]*model.MeterItem, int, error) {
	if result == nil || result.Hits == nil {
		return []*model.MeterItem{}, 0, nil
	}

	items := []*model.MeterItem{}
	for _, hit := range result.Hits.Hits {
		item, err := m.restHitToMeterItem(hit)
		if err != nil {
			m.Logger.Error("rest hit to meter item error, %s", err.Error())
			continue
		}
		items = append(items, item)
	}

	return items, int(result.TotalHits()), nil
}

func (m *MeterStore) restHitToMeterItem(hit *elasticV6.SearchHit) (*model.MeterItem, error) {
	source, err := hit.Source.MarshalJSON()
	if err != nil {
		m.Logger.Error("marshal source of hit error, %s", err.Error())
		return nil, err
	}

	item := model.MeterItem{}
	item.MeterInfo.StartTime, _ = time.Parse(model.KTimeFormat, common.GetStringByPath(source, "startTime"))
	item.MeterInfo.EndTime, _ = time.Parse(model.KTimeFormat, common.GetStringByPath(source, "endTime"))
	item.MeterInfo.Meter.CPU = common.GetFloatByPath(source, "meter.cpuRequests")
	item.MeterInfo.Meter.Memory = common.GetFloatByPath(source, "meter.memoryRequests")
	item.Project.Name = common.GetStringByPath(source, "project")
	item.Cluster.Name = common.GetStringByPath(source, "cluster")
	item.Namespace.Name = common.GetStringByPath(source, "namespace")
	item.Pod = common.GetStringByPath(source, "pod")
	if m.Request.Type == model.MeterTypePodUsage {
		item.MeterInfo.Meter.CPU = common.GetFloatByPath(source, "meter.cpuUsage")
		item.MeterInfo.Meter.Memory = common.GetFloatByPath(source, "meter.memoryUsage")
	}

	// Fetch displayName for project/cluster/namespace
	itemCluster := common.GetStringByPath(source, "cluster")
	itemNamespace := common.GetStringByPath(source, "namespace")
	if itemCluster != "" && itemNamespace != "" {
		c, err := cluster.GetCluster(itemCluster)
		if err != nil {
			m.Logger.Errorf("get display name for cluster %s error, %s", itemCluster, err.Error())
		} else {
			item.Cluster.DisplayName = c.DisplayName
			item.Namespace.DisplayName = namespace.GetDisplayName(c, itemNamespace)
		}
	}
	itemProject := common.GetStringByPath(source, "project")
	if itemProject != "" {
		item.Project.DisplayName = project.GetDisplayName(itemProject)
	}

	return &item, nil
}

func (m *MeterStore) initTopNResponse() *model.MeterTopnResponse {
	return &model.MeterTopnResponse{
		TopN: []*model.MeterItem{},
		Remain: model.MeterInfo{
			StartTime: m.Request.StartTime,
			EndTime:   m.Request.EndTime,
		},
	}
}

func (m *MeterStore) initTotalResponse() *model.MeterTotalResponse {
	response := &model.MeterTotalResponse{}

	itemDate := m.GetMonthList()
	if m.Request.ClassifyBy == "day" {
		itemDate = m.GetDayList()
	}

	items := make([]*model.MeterInfo, 0)
	for _, date := range itemDate {
		start := time.Time{}
		end := time.Time{}
		if m.Request.ClassifyBy == "day" {
			start, _ = time.Parse("20060102", date)
			end = start.AddDate(0, 0, 1).Add(-1 * time.Second)
		} else {
			start, _ = time.Parse("200601", date)
			end = start.AddDate(0, 1, 0).Add(-1 * time.Second)
		}

		if start.Before(m.Request.StartTime) {
			start = m.Request.StartTime
		}
		if end.After(m.Request.EndTime) {
			end = m.Request.EndTime
		}

		item := &model.MeterInfo{
			Date:      date,
			StartTime: start,
			EndTime:   end,
		}
		items = append(items, item)
	}
	response.Results = items
	return response
}
