package meter

import (
	"fmt"
)

func (m *MeterStore) GetIndexNames(prefix string) []string {
	startTime := m.Request.StartTime
	endTime := m.Request.EndTime
	if endTime.Before(startTime) {
		m.Logger.Errorf("getIndexNames: endTime before startTime, return an empty index")
		return []string{}
	}

	indexes := []string{}
	months := m.GetMonthList()
	m.Logger.Debugf("Get indexes name for meter bewteen %s and %s", startTime, endTime)
	for _, month := range months {
		indexName := fmt.Sprintf("%s-%s", prefix, month)
		if exists := m.Client.Exists(indexName); exists {
			indexes = append(indexes, indexName)
		} else {
			m.Logger.Debugf("Get indexes name %s, not found. Skip", indexName)
		}
	}
	m.Logger.Debugf("Get indexes name for meter: %#v", indexes)

	return indexes
}
