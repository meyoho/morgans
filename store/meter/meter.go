package meter

import (
	"context"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"morgans/common"
	"morgans/infra/elasticsearch"
	"morgans/model"
	"morgans/store"
)

type MeterStore struct {
	store.BaseStore
	Request *model.MeterRequest
	Client  *elasticsearch.Client
}

func (m *MeterStore) MeterTopN() (*model.MeterTopnResponse, error) {
	response := m.initTopNResponse()
	// generate index and source to search
	searchIndex := m.GetIndexNames("meter-summary")
	if len(searchIndex) < 1 {
		m.Logger.Errorf("no index found for meter topn between %s and %s", m.Request.StartTime.String(), m.Request.EndTime.String())
		return response, nil
	}
	m.Logger.Infof("Get search index for topn: %+v", searchIndex)

	topnSource := m.BuildTopnSource()
	totalSource := m.BuildTotalSource()
	m.Logger.Infof("Get search index for topn: %+v, %+v", topnSource, totalSource)

	// get topn hits
	topnResult, err := m.Client.Query(context.Background(), topnSource, searchIndex)
	if err != nil {
		m.Logger.Infof("errors when query es: %#v", err.Error())
		return response, err
	}
	m.Logger.Infof("Get search result with topn source: %#v", topnResult)

	// get total data to calculate remain data
	totalResult, err := m.Client.Query(context.Background(), totalSource, searchIndex)
	if err != nil {
		m.Logger.Infof("errors when query es: %#v", err.Error())
		return response, err
	}
	m.Logger.Infof("Get search result with total source: %#v", totalResult)
	return m.RestResultToMeterTopn(topnResult, totalResult)
}

func (m *MeterStore) MeterTotal() (*model.MeterTotalResponse, error) {
	response := m.initTotalResponse()

	// generate index to search
	searchIndex := []string{}
	if m.Request.ClassifyBy == "day" {
		searchIndex = m.GetIndexNames("meter-detail")
	} else {
		searchIndex = m.GetIndexNames("meter-summary")
	}
	if len(searchIndex) < 1 {
		m.Logger.Errorf("no index found for meter total between %s and %s", m.Request.StartTime.String(), m.Request.EndTime.String())
		return response, nil
	}
	m.Logger.Infof("Get search index for total: %+v", searchIndex)

	// generate source to search
	searchSource := m.BuildTotalByDateSource()
	m.Logger.Infof("Get search source for total: %+v", searchSource)

	totalResult, err := m.Client.Query(context.Background(), searchSource, searchIndex)
	if err != nil {
		m.Logger.Infof("errors when query es: %#v", err.Error())
		return response, err
	}
	m.Logger.Infof("Get search result with total source: %#v", totalResult)

	return m.RestResultToMeterTotal(totalResult)
}

func (m *MeterStore) MeterSummary() ([]*model.MeterItem, int, error) {
	// generate index and source to search
	searchIndex := m.GetIndexNames("meter-summary")
	if len(searchIndex) < 1 {
		m.Logger.Errorf("no index found for meter summary between %s and %s", m.Request.StartTime.String(), m.Request.EndTime.String())
		return nil, 0, nil
	}
	m.Logger.Infof("Get search index for summary: %+v", searchIndex)

	searchSource := m.BuildSummarySource()
	m.Logger.Infof("Get search source for summary: %+v", searchSource)

	searchResult, err := m.Client.Query(context.Background(), searchSource, searchIndex)
	if err != nil {
		m.Logger.Infof("errors when query es: %+v", err.Error())
		return nil, 0, err
	}
	m.Logger.Infof("Get search result with summary source: %#v", searchResult)
	return m.RestResultToMeterSummary(searchResult)
}

func (m *MeterStore) MeterQuery() ([]*model.MeterItem, int, error) {
	// generate index and source to search
	searchIndex := m.GetIndexNames("meter-detail")
	if len(searchIndex) < 1 {
		m.Logger.Errorf("no index found for meter query between %s and %s", m.Request.StartTime.String(), m.Request.EndTime.String())
		return nil, 0, nil
	}
	m.Logger.Infof("Get search index for query: %+v", searchIndex)

	searchSource := m.BuildQuerySource()
	m.Logger.Infof("Get search source for query: %+v", searchSource)

	searchResult, err := m.Client.Query(context.Background(), searchSource, searchIndex)
	if err != nil {
		m.Logger.Infof("errors when query es: %+v", err.Error())
		return nil, 0, err
	}
	m.Logger.Infof("Get search result with query source: %#v", searchResult)
	return m.RestResultToMeterQuery(searchResult)
}

func (m *MeterStore) MeterReportUpload(r *http.Request) error {
	m.Logger.Debugf("Get request headers of meter report: %v", r.Header)
	m.Logger.Debugf("Get request body of meter report: %s", r.Body)

	mr, err := r.MultipartReader()
	if err != nil {
		m.Logger.Errorf("Read multi part from request error, %s", err.Error())
		return err
	}

	for {
		p, err := mr.NextPart()
		if err == io.EOF {
			break
		}
		if err != nil {
			m.Logger.Errorf("Read meter reports from request error, %s", err.Error())
			return err
		}
		m.Logger.Debugf("Get subpart header of meter report: %v", p.Header)

		// Get disposition content
		formName := p.FormName()
		fileName := p.FileName()
		if formName == "" || fileName == "" {
			return common.BuildBadRequestError("Invalid parameters, just accept file to upload")
		}
		m.Logger.Infof("Read meter report from request. Form name %s, file name %s", formName, fileName)
		fileContent, err := ioutil.ReadAll(p)
		if err != nil {
			m.Logger.Errorf("Read report content from request error, %s", err.Error())
			return err
		}
		// Write content to report file
		filePath := filepath.Join(model.ReportPath, fileName)
		m.Logger.Infof("Create report file %s in %s", fileName, model.ReportPath)
		reportFile, err := os.Create(filePath)
		if err != nil {
			m.Logger.Errorf("Create report file in path %s error, %s", filePath, err.Error())
			return err
		}
		defer reportFile.Close()
		m.Logger.Infof("Write content to report file %s", filePath)
		if _, err := reportFile.Write(fileContent); err != nil || reportFile.Close() != nil {
			m.Logger.Errorf("Write content to report file error, %s", err.Error())
			return err
		}
		m.Logger.Infof("Upload report file %s to path %s successfully", fileName, filePath)
	}
	return nil
}
