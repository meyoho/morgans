package meter

import (
	"fmt"
	"strings"
	"time"

	"morgans/model"

	elasticV6 "gopkg.in/olivere/elastic.v6"
)

func (m *MeterStore) BuildTopnSource() *elasticV6.SearchSource {
	source := elasticV6.NewSearchSource()

	filterQuery := elasticV6.NewBoolQuery().
		Filter(m.buildFilterQueries("month")...).
		MustNot(m.buildMustNotQueries()...)

	topnAggs := elasticV6.NewTopHitsAggregation().
		From(0).
		Size(m.Request.Top).
		Sort(m.Request.Sorter.Path, m.Request.Sorter.Ascending)

	return source.Query(filterQuery).Size(0).Aggregation("topN", topnAggs)
}

func (m *MeterStore) BuildTotalSource() *elasticV6.SearchSource {
	source := elasticV6.NewSearchSource()

	filterQuery := elasticV6.NewBoolQuery().
		Filter(m.buildFilterQueries("month")...).
		MustNot(m.buildMustNotQueries()...)

	cpuField := "meter.cpuRequests"
	memoryField := "meter.memoryRequests"
	if m.Request.Type == model.MeterTypePodUsage {
		cpuField = "meter.cpuUsage"
		memoryField = "meter.memoryUsage"
	}
	cpuSumAggs := elasticV6.NewSumAggregation().Field(cpuField)
	memorySumAggs := elasticV6.NewSumAggregation().Field(memoryField)

	return source.Query(filterQuery).Size(0).
		Aggregation("cpu", cpuSumAggs).Aggregation("memory", memorySumAggs)
}

func (m *MeterStore) BuildTotalByDateSource() *elasticV6.SearchSource {
	source := elasticV6.NewSearchSource()

	filterQuery := elasticV6.NewBoolQuery().
		Filter(m.buildFilterQueries(m.Request.ClassifyBy)...).
		MustNot(m.buildMustNotQueries()...)

	cpuField := "meter.cpuRequests"
	memoryField := "meter.memoryRequests"
	if m.Request.Type == model.MeterTypePodUsage {
		cpuField = "meter.cpuUsage"
		memoryField = "meter.memoryUsage"
	}
	cpuSumAggs := elasticV6.NewSumAggregation().Field(cpuField)
	memorySumAggs := elasticV6.NewSumAggregation().Field(memoryField)
	sumAggs := elasticV6.NewTermsAggregation().
		Field(fmt.Sprintf("%s.keyword", m.Request.ClassifyBy)).
		SubAggregation("cpu", cpuSumAggs).SubAggregation("memory", memorySumAggs)

	return source.Query(filterQuery).Size(0).Aggregation("total", sumAggs)
}

func (m *MeterStore) BuildSummarySource() *elasticV6.SearchSource {
	source := elasticV6.NewSearchSource()

	filterQuery := elasticV6.NewBoolQuery().
		Filter(m.buildFilterQueries("month")...).
		MustNot(m.buildMustNotQueries()...)

	from := (m.Request.Page - 1) * m.Request.PageSize

	return source.Query(filterQuery).
		From(from).Size(m.Request.PageSize).
		Sort(m.Request.Sorter.Path, m.Request.Sorter.Ascending)
}

func (m *MeterStore) BuildQuerySource() *elasticV6.SearchSource {
	source := elasticV6.NewSearchSource()

	filterQuery := elasticV6.NewBoolQuery().
		Filter(m.buildFilterQueries("day")...).
		MustNot(m.buildMustNotQueries()...)

	from := (m.Request.Page - 1) * m.Request.PageSize
	source.Query(filterQuery).
		From(from).Size(m.Request.PageSize).
		Sort(m.Request.Sorter.Path, m.Request.Sorter.Ascending)

	return source
}

func (m *MeterStore) buildFilterQueries(date string) []elasticV6.Query {
	queries := make([]elasticV6.Query, 0)

	regexpQuery := elasticV6.NewRegexpQuery(m.Request.GroupBy, ".+")
	queries = append(queries, regexpQuery)

	if m.Request.Type != "" {
		meterType := m.Request.Type
		if m.Request.Type == model.MeterTypePodRequests {
			meterType = model.MeterTypePodUsage
		}
		m.Logger.Debugf("build terms query for keyword type: %s", meterType)
		typeQuery := elasticV6.NewTermsQuery("type.keyword", m.GetQueryValues(strings.Split(meterType, ","))...)
		queries = append(queries, typeQuery)
	}
	if m.Request.Project != "" {
		m.Logger.Debugf("build terms query for keyword project: %s", m.Request.Project)
		projectQuery := elasticV6.NewTermsQuery("project.keyword", m.GetQueryValues(strings.Split(m.Request.Project, ","))...)
		queries = append(queries, projectQuery)
	}
	if m.Request.Cluster != "" {
		m.Logger.Debugf("build terms query for keyword cluster: %s", m.Request.Cluster)
		clusterQuery := elasticV6.NewTermsQuery("cluster.keyword", m.GetQueryValues(strings.Split(m.Request.Cluster, ","))...)
		queries = append(queries, clusterQuery)
	}
	if m.Request.Namespace != "" {
		m.Logger.Debugf("build terms query for keyword namespace: %s", m.Request.Namespace)
		namespaceQuery := elasticV6.NewTermsQuery("namespace.keyword", m.GetQueryValues(strings.Split(m.Request.Namespace, ","))...)
		queries = append(queries, namespaceQuery)
	}
	if date != "" {
		dateValue := m.GetMonthList()
		if date == "day" {
			dateValue = m.GetDayList()
		}
		m.Logger.Debugf("build terms query for keyword %s: %s", date, dateValue)
		dateQuery := elasticV6.NewTermsQuery(fmt.Sprintf("%s.keyword", date), m.GetQueryValues(dateValue)...)
		queries = append(queries, dateQuery)
	}
	m.Logger.Debugf("Get search queries for meter: %#v", queries)
	return queries
}

func (m *MeterStore) buildMustNotQueries() []elasticV6.Query {
	queries := make([]elasticV6.Query, 0)
	if m.Request.GroupBy == model.GroupByProject {
		emptyValueQuery := elasticV6.NewRegexpQuery("namespace.keyword", ".+")
		queries = append(queries, emptyValueQuery)
	}
	return queries
}

func (m *MeterStore) GetMonthList() []string {
	months := []string{}
	start := time.Date(m.Request.StartTime.Year(), m.Request.StartTime.Month(), 1, 0, 0, 0, 0, m.Request.StartTime.Location())
	for !start.After(m.Request.EndTime) {
		month := fmt.Sprintf("%4d%02d", start.Year(), int(start.Month()))
		months = append(months, month)
		start = start.AddDate(0, 1, 0)
	}
	return months
}

func (m *MeterStore) GetDayList() []string {
	days := []string{}
	start := time.Date(m.Request.StartTime.Year(), m.Request.StartTime.Month(), m.Request.StartTime.Day(), 0, 0, 0, 0, m.Request.StartTime.Location())
	for !start.After(m.Request.EndTime) {
		day := fmt.Sprintf("%4d%02d%02d", start.Year(), int(start.Month()), start.Day())
		days = append(days, day)
		start = start.AddDate(0, 0, 1)
	}
	return days
}

func (m *MeterStore) GetQueryValues(src []string) []interface{} {
	des := make([]interface{}, len(src))
	for i, v := range src {
		des[i] = v
	}
	return des
}
