package meter

import (
	"strings"

	"morgans/common"
	"morgans/model"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/schema"
)

func (m *MeterStore) Translate(c *gin.Context) error {
	// Decode http query to request body
	var decoder = schema.NewDecoder()
	err := decoder.Decode(m.Request, c.Request.URL.Query())
	if err != nil {
		m.Logger.Errorf("Decode request query error, %s", err.Error())
		return common.BuildBadRequestError(err.Error())
	}
	m.Logger.Debugf("Get original request body for meter: %+v", m.Request)

	// Set default value for option
	if m.Request.Top == 0 {
		m.Request.Top = 5
	}
	if m.Request.OrderBy == "" {
		m.Request.OrderBy = "name"
	}
	if m.Request.ClassifyBy == "" {
		m.Request.ClassifyBy = "month"
	}
	if m.Request.Page == 0 {
		m.Request.Page = 1
	}
	if m.Request.PageSize == 0 {
		m.Request.PageSize = 20
	}

	// Generate sorter for meter request
	m.generateSorter()

	m.Logger.Infof("Get request body for meter: %+v", m.Request)
	return nil
}

func (m *MeterStore) generateSorter() {
	orderBy := m.Request.OrderBy
	sorter := model.Sorter{
		Field:     orderBy,
		Path:      "namespace.keyword",
		Ascending: true,
	}

	if strings.HasPrefix(orderBy, "-") {
		sorter.Ascending = false
		orderBy = orderBy[1:]
	}
	switch orderBy {
	case model.OrderByCPU:
		sorter.Path = "meter.cpuRequests"
		if m.Request.Type == model.MeterTypePodUsage {
			sorter.Path = "meter.cpuUsage"
		}
	case model.OrderByMemory:
		sorter.Path = "meter.memoryRequests"
		if m.Request.Type == model.MeterTypePodUsage {
			sorter.Path = "meter.memoryUsage"
		}
	case model.OrderByName:
		if m.Request.GroupBy == model.GroupByProject {
			sorter.Path = "project.keyword"
		}
	}

	m.Request.Sorter = sorter
}
