package meter

import (
	"morgans/common"
	"morgans/model"
)

func (m *MeterStore) Validate(skip bool) error {
	if m.Cluster.KubernetesConfig.Token == "" {
		return common.BuildUnauthorizedError("Token required")
	}
	// check the featuregate of meter if enabled
	if enabled, err := model.MeterEnabled(m.Cluster.KubernetesConfig.Token); !enabled {
		if err != nil {
			m.Logger.Errorf("Validate meter featuregate err, %s", err.Error())
			return err
		}
		return common.BuildMethodNotAllowedError("feature gate of meter not enabled")
	}

	// check user can execute the action
	if valid, err := common.CanI(m.Cluster.KubernetesConfig.Token, model.VerbGet, model.RKMeterReport, model.AVMeteringGroup, ""); !valid {
		if err != nil {
			m.Logger.Errorf("Validate permission err: %s", err.Error())
			return err
		}
		return common.BuildForbiddenError("user has no permission to execute")
	}

	// if skip is true, skip request validation
	if skip {
		return nil
	}

	// Validate the meter request
	if m.Request.Type == "" || m.Request.GroupBy == "" || m.Request.StartTime.IsZero() || m.Request.EndTime.IsZero() {
		m.Logger.Errorf("Validate request err, type, groupBy, startTime, endTime are required for meter")
		return common.BuildBadRequestError("type, groupBy, startTime, endTime are required for meter")
	}
	if m.Request.StartTime.After(m.Request.EndTime) {
		m.Logger.Errorf("Validate request err, startTime should not after endTime for meter")
		return common.BuildBadRequestError("startTime should not after endTime for meter")
	}

	return nil
}
