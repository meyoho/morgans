package template

import (
	"fmt"
	"time"

	"morgans/common"
	"morgans/infra/database"
	"morgans/infra/kubernetes"
	"morgans/model"
)

type StorageManager interface {
	// Create a template
	Create(request *model.AlertTemplateRequest) (*model.AlertTemplateResponse, error)
	// Get a template by organization and template name
	Get(organization, name string) (*model.AlertTemplateResponse, error)
	// List templates under an organization
	List(organization string) ([]*model.AlertTemplateResponse, error)
	// Update a template
	Update(r *model.AlertTemplateRequest) (*model.AlertTemplateResponse, error)
	// Delete a template
	Delete(organization, name string) error
}

type DBStorage struct {
	DB     *database.DBHandler
	Logger *common.Logger
}

func NewDBStorageManager(logger *common.Logger) StorageManager {
	return &DBStorage{
		DB:     database.NewDBHandler(logger),
		Logger: logger,
	}
}

func (s *DBStorage) Create(request *model.AlertTemplateRequest) (*model.AlertTemplateResponse, error) {
	if err := s.DB.InsertTemplate(request.ToRecord()); err != nil {
		return nil, err
	}
	return s.Get(request.Organization, request.Name)
}

func (s *DBStorage) Get(organization, name string) (*model.AlertTemplateResponse, error) {
	record, err := s.DB.RetrieveTemplate(organization, name)
	if err != nil {
		return nil, err
	}
	return record.ToResponse(), nil
}

func (s *DBStorage) List(organization string) ([]*model.AlertTemplateResponse, error) {
	records, err := s.DB.ListTemplate(organization)
	if err != nil {
		return nil, err
	}
	items := make([]*model.AlertTemplateResponse, len(records))
	for i, r := range records {
		items[i] = r.ToResponse()
	}
	return items, nil
}

func (s *DBStorage) Update(request *model.AlertTemplateRequest) (*model.AlertTemplateResponse, error) {
	if err := s.DB.UpdateTemplate(request.ToRecord()); err != nil {
		return nil, err
	}
	return s.Get(request.Organization, request.Name)
}

func (s *DBStorage) Delete(organization, name string) error {
	_, err := s.DB.DeleteTemplate(organization, name)
	return err
}

type ETCDStorage struct {
	Cluster *model.Cluster
	Logger  *common.Logger
}

func NewETCDStorageManager(cluster *model.Cluster, logger *common.Logger) StorageManager {
	return &ETCDStorage{
		Cluster: cluster,
		Logger:  logger,
	}
}

func (s *ETCDStorage) Create(request *model.AlertTemplateRequest) (*model.AlertTemplateResponse, error) {
	resource := request.ToResource()
	prompt := fmt.Sprintf("%v %v", resource.Kind, resource.Name)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpPost, resource, nil)
	if err != nil {
		s.Logger.Errorf("Create " + prompt + " error, " + err.Error())
		return nil, err
	}
	response, err := model.RestResultToAlertTemplateResource(result)
	if err != nil {
		return nil, err
	}
	return response.ToResponse(), nil
}

func (s *ETCDStorage) Get(organization, name string) (*model.AlertTemplateResponse, error) {
	request := model.AlertTemplateRequest{Name: name, Organization: organization}
	resource := request.ToResource()
	prompt := fmt.Sprintf("%v %v", resource.Kind, resource.Name)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		s.Logger.Errorf("Get " + prompt + " error, " + err.Error())
		return nil, err
	}
	response, err := model.RestResultToAlertTemplateResource(result)
	if err != nil {
		return nil, err
	}
	return response.ToResponse(), nil
}

func (s *ETCDStorage) List(organization string) ([]*model.AlertTemplateResponse, error) {
	request := model.AlertTemplateRequest{Organization: organization}
	resource := request.ToResource()
	prompt := fmt.Sprintf("%v with organization %v ", resource.Kind, organization)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		s.Logger.Errorf("List " + prompt + " error, " + err.Error())
		return nil, err
	}
	items, err := model.RestResultToAlertTemplateResources(result)
	if err != nil {
		return nil, err
	}
	templates := make([]*model.AlertTemplateResponse, len(items))
	for index, item := range items {
		templates[index] = item.ToResponse()
	}
	return templates, nil
}

func (s *ETCDStorage) Update(request *model.AlertTemplateRequest) (*model.AlertTemplateResponse, error) {
	resource := request.ToResource()
	prompt := fmt.Sprintf("%v %v", resource.Kind, resource.Name)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)

	// Get alert template resource and merge
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		s.Logger.Errorf("Get " + prompt + " error, " + err.Error())
		return nil, err
	}
	response, err := model.RestResultToAlertTemplateResource(result)
	if err != nil {
		return nil, common.AnnotateError(err, prompt+"rest to resource error")
	}
	resource.ResourceVersion = response.ResourceVersion
	resource.Labels[model.LKKind] = response.Labels[model.LKKind]
	resource.Annotations[model.AKCreator] = response.Annotations[model.AKCreator]
	resource.Annotations[model.AKUpdatedAt] = time.Now().UTC().Format(model.KTimeFormat)

	// Update alert template resource
	client.Resource = resource
	result, err = client.Request(common.HttpPut, resource, nil)
	if err != nil {
		s.Logger.Errorf("Update " + prompt + " error, " + err.Error())
		return nil, err
	}
	responseResource, err := model.RestResultToAlertTemplateResource(result)
	if err != nil {
		return nil, err
	}
	return responseResource.ToResponse(), nil
}

func (s *ETCDStorage) Delete(organization, name string) error {
	request := model.AlertTemplateRequest{Name: name}
	resource := request.ToResource()
	prompt := fmt.Sprintf("%v %v", resource.Kind, resource.Name)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	_, err := client.Request(common.HttpDelete, resource, nil)
	if err != nil {
		s.Logger.Errorf("Delete " + prompt + " error, " + err.Error())
		return err
	}
	return nil
}
