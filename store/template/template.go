package template

import (
	"morgans/common"
	"morgans/model"
)

type Store struct {
	Request  *model.AlertTemplateRequest
	Response *model.AlertTemplateResponse
	Storage  StorageManager
	Logger   *common.Logger
}

func (s *Store) Create() error {
	response, err := s.Storage.Create(s.Request)
	if err != nil {
		return err
	}
	s.Response = response
	return nil
}

func (s *Store) Retrieve() error {
	response, err := s.Storage.Get(s.Request.Organization, s.Request.Name)
	if err != nil {
		return err
	}
	s.Response = response
	return nil
}

func (s *Store) List() ([]*model.AlertTemplateResponse, error) {
	responses, err := s.Storage.List(s.Request.Organization)
	if err != nil {
		return nil, err
	}
	return responses, nil
}

func (s *Store) Update() error {
	response, err := s.Storage.Update(s.Request)
	if err != nil {
		return err
	}
	s.Response = response
	return nil
}

func (s *Store) Delete() error {
	return s.Storage.Delete(s.Request.Organization, s.Request.Name)
}

func (s *Store) GetParser() (*Parser, error) {
	if err := s.Retrieve(); err != nil {
		return nil, err
	}
	return &Parser{
		Logger:   s.Logger,
		Request:  &model.AlertTemplateParser{},
		Template: s.Response,
	}, nil
}
