package template

import (
	"morgans/common"
	"morgans/model"
)

type Parser struct {
	Request  *model.AlertTemplateParser
	Template *model.AlertTemplateResponse
	Logger   *common.Logger
}

func (p *Parser) Parse() []*model.AlertRequest {
	result := make([]*model.AlertRequest, len(p.Template.Template))
	for i, t := range p.Template.Template {
		result[i] = &model.AlertRequest{
			AlertTemplate: *t,
			Creator:       p.Request.Creator,
			Project:       p.Request.Project,
		}
		result[i].Metric.Queries[0].Labels = append(t.Metric.Queries[0].Labels, p.Request.Metric.Queries[0].Labels...)
	}
	return result
}

func (p *Parser) Filter(alerts []*model.AlertResponse) []*model.AlertResponse {
	result := make([]*model.AlertResponse, 0)
	for _, alert := range alerts {
		for _, template := range p.Template.Template {
			if template.Name == alert.Name {
				result = append(result, alert)
				break
			}
		}
	}
	return result
}
