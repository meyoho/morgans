package template

import (
	"fmt"

	"morgans/common"
	"morgans/model"

	"github.com/deckarep/golang-set"
)

func (s *Store) Validate() error {
	if s.Request.Name == "" {
		return fmt.Errorf("name can not be empty")
	}
	if s.Request.Organization == "" {
		return fmt.Errorf("organization can not be empty")
	}
	if s.Request.Creator == "" {
		return fmt.Errorf("creator can not be empty")
	}

	// Validate the alert serverity
	for _, t := range s.Request.Template {
		if !common.StringInSlice(t.Labels[model.LabelSeverity], model.AlertSeverity) {
			return fmt.Errorf("%s is not a valid serverity for template", t.Labels[model.LabelSeverity])
		}
	}

	// Validate duplicated alert names
	alertNames := mapset.NewSet()
	for _, t := range s.Request.Template {
		if alertNames.Contains(t.Name) {
			return fmt.Errorf("alert name %s already exists in this template", t.Name)
		} else {
			alertNames.Add(t.Name)
		}
	}
	return nil
}
