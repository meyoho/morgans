package template

import (
	"time"

	"morgans/model"

	"github.com/pborman/uuid"
)

func (s *Store) translate() {
	for _, t := range s.Request.Template {
		if t.Labels == nil {
			t.Labels = map[string]string{}
		}
		if _, ok := t.Labels[model.LabelSeverity]; !ok {
			t.Labels[model.LabelSeverity] = model.AlertSeverityMedium
		}
	}
}

func (s *Store) TranslateForCreate() {
	s.translate()
	if s.Request.UUID == "" {
		s.Request.UUID = uuid.New()
	}

	if s.Request.CreatedAt.IsZero() {
		s.Request.CreatedAt = time.Now().UTC()
	}
	if s.Request.UpdatedAt.IsZero() {
		s.Request.UpdatedAt = s.Request.CreatedAt
	}
}

func (s *Store) TranslateForUpdate() {
	s.translate()
	s.Request.UpdatedAt = time.Now().UTC()
}
