package notification

import (
	"strings"

	"morgans/model"
)

func (s *Store) setResourceAnnotations(current *model.NotificationResource) *model.NotificationResource {
	var annotations = map[string]string{}
	annotations[model.AKDisplayName] = s.Request.Name
	annotations[model.AKCreatedAt] = s.Request.CreatedAt.Format(model.KTimeFormat)
	annotations[model.AKUpdatedAt] = s.Request.UpdatedAt.Format(model.KTimeFormat)
	annotations[model.AKDescription] = s.Request.Description
	annotations[model.AKCreator] = s.Request.Creator

	if s.Request.Description == "" {
		var remarkSlice = make([]string, 0)
		for _, val := range s.Request.Subscriptions {
			if val.Remark != "" {
				remarkSlice = append(remarkSlice, val.Remark)
			}
		}
		annotations[model.AKDescription] = strings.Join(remarkSlice, ";")
	}

	current.Annotations = annotations
	return current
}
