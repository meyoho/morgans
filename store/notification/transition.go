package notification

import (
	"time"

	"morgans/config"
	"morgans/model"

	"github.com/pborman/uuid"
)

func (s *Store) translate() {
	s.Request.Namespace = config.GlobalConfig.Morgans.Namespace
}

// TranslateForCreate translate request body
func (s *Store) TranslateForCreate() {
	s.translate()
	if s.Request.UUID == "" {
		s.Request.UUID = uuid.New()
	}
	if s.Request.Creator == "" {
		userInfo, err := model.ParseUser(s.Cluster.KubernetesConfig.Token)
		if err == nil {
			s.Request.Creator = userInfo.GetName()
		}
	}
	if s.Request.CreatedAt.IsZero() {
		s.Request.CreatedAt = time.Now().UTC()
	}
	s.Request.UpdatedAt = time.Now().UTC()
}

func (s *Store) TranslateForUpdate() {
	s.translate()
	s.Request.UpdatedAt = time.Now().UTC()
}

func (s *Store) Translate() {
	s.translate()
}

func (s *Store) ResourceToResponse(nr *model.NotificationResource) (*model.NotificationResponse, error) {
	response := model.NotificationResponse{}
	response.Name = nr.Name
	response.UUID = nr.Labels[model.LKUUID]
	response.Creator = nr.Annotations[model.AKCreator]
	response.Namespace = nr.Namespace
	response.Description = nr.Annotations[model.AKDescription]
	response.Project = nr.Namespace

	createdAt, _ := time.Parse(model.KTimeFormat, nr.Annotations[model.AKCreatedAt])
	response.CreatedAt = createdAt

	updatedAt, _ := time.Parse(model.KTimeFormat, nr.Annotations[model.AKUpdatedAt])
	response.UpdatedAt = updatedAt

	response.Subscriptions = nr.Spec.Subscriptions
	return &response, nil
}

func (s *Store) ResourcesToResponses(nr []*model.NotificationResource) ([]*model.NotificationResponse, error) {
	response := make([]*model.NotificationResponse, 0)
	for _, r := range nr {
		res, _ := s.ResourceToResponse(r)
		response = append(response, res)
	}
	return response, nil
}
