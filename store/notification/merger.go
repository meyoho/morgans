package notification

import (
	"morgans/model"
)

// mergeForCreate generate labels/annotations for notification
func (s *Store) mergeForCreate(current *model.NotificationResource) (*model.NotificationResource, error) {
	// Set labels for the notification resource
	current = s.setResourceLabels(current)
	// Set annotations for notification resource
	current = s.setResourceAnnotations(current)
	return current, nil
}

// mergeForUpdate generate labels/annotations for notification
func (s *Store) mergeForUpdate(current *model.NotificationResource) (*model.NotificationResource, error) {
	// Set labels for the notification resource
	current = s.setResourceLabels(current)
	// Set annotations for notification resource
	current = s.setResourceAnnotations(current)
	current.Spec.Subscriptions = s.Request.Subscriptions
	return current, nil
}
