package notification

import (
	"fmt"
	"time"

	"morgans/common"
	"morgans/config"
	"morgans/infra/kubernetes"
	"morgans/model"
	"morgans/store"

	"github.com/pborman/uuid"
)

type Store struct {
	store.BaseStore
	Request  *model.NotificationRequest
	Response *model.NotificationResponse
	Message  map[string]interface{}
}

func (s *Store) CreateNotification() (*model.NotificationResponse, error) {
	resource, err := s.generateNotificationResource(s.Request.Namespace, s.Request.Name)
	if err != nil {
		s.Logger.Errorf("Generate resource error, %s", err.Error())
		return nil, err
	}
	resource, err = s.mergeForCreate(resource)
	if err != nil {
		s.Logger.Errorf("Merge resource error, %s", err.Error())
		return nil, err
	}
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpPost, resource, nil)
	if err != nil {
		s.Logger.Errorf(prompt+"create error: %s", err.Error())
		return nil, err
	}

	nf, err := model.RestResultToNotification(result)
	if err != nil {
		s.Logger.Errorf("rest result to notification resource error: %s", err.Error())
		return nil, err
	}

	return s.ResourceToResponse(nf)
}

func (s *Store) UpdateNotification() (*model.NotificationResponse, error) {
	resource, err := s.getNotificationResource(s.Request.Namespace, s.Request.Name)
	if err != nil {
		s.Logger.Errorf("Get notification resource error: %s", err.Error())
		return nil, err
	}
	s.UpdateRequest(resource)

	current, err := s.mergeForUpdate(resource)
	if err != nil {
		s.Logger.Errorf("Merge resource error, %s", err.Error())
		return nil, err
	}

	client := kubernetes.NewClient(s.Cluster, current, s.Logger)
	client.Resource = current
	result, err := client.Request(common.HttpPut, current, nil)
	if err != nil {
		s.Logger.Errorf("Update notification resource error: %s", err.Error())
		return nil, err
	}

	nf, err := model.RestResultToNotification(result)
	if err != nil {
		s.Logger.Errorf("rest result to notification resource error: %s", err.Error())
		return nil, err
	}

	return s.ResourceToResponse(nf)
}

func (s *Store) DeleteNotification(namespace, name string) error {
	resource, err := s.generateNotificationResource(namespace, name)
	if err != nil {
		s.Logger.Errorf("Generate resource error, %s", err.Error())
		return err
	}

	if uuid.Parse(name) != nil {
		resource, err = s.GetNotificationByLabels(namespace, name)
	}

	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	client.Resource = resource
	_, err = client.Request(common.HttpDelete, resource, nil)
	return err
}

func (s *Store) ListNotifications() ([]*model.NotificationResponse, error) {
	resource, err := s.generateNotificationResource(s.Request.Namespace, "")
	if err != nil {
		s.Logger.Errorf("Generate resource error, %s", err.Error())
		return nil, err
	}
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		return nil, err
	}

	r, err := model.RestResultToNotifications(result)
	if err != nil {
		s.Logger.Errorf("List resource error, %s", err.Error())
		return nil, err
	}
	return s.ResourcesToResponses(r)
}

func (s *Store) GetNotification(namespace, name string) (*model.NotificationResponse, error) {
	resource, err := s.getNotificationResource(namespace, name)
	if err != nil {
		s.Logger.Errorf("Get notification resource error, %s", err.Error())
		return nil, err
	}
	response, err := s.ResourceToResponse(resource)
	if err != nil {
		s.Logger.Errorf("Transition error, %s", err.Error())
		return nil, err
	}
	return response, nil
}

func (s *Store) getNotificationResource(namespace, name string) (*model.NotificationResource, error) {
	if uuid.Parse(name) != nil {
		return s.GetNotificationByLabels(namespace, name)
	}

	resource, err := s.generateNotificationResource(namespace, name)
	if err != nil {
		s.Logger.Errorf("Generate resource error, %s", err.Error())
		return nil, err
	}
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	s.Logger.Infof("Try to get " + prompt)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		s.Logger.Errorf(prompt+" get error, %s", err.Error())
		return nil, err
	}
	return model.RestResultToNotification(result)
}

func (s *Store) GetNotificationByLabels(namespace, name string) (*model.NotificationResource, error) {
	resource, err := s.generateNotificationResource(namespace, "")
	if err != nil {
		s.Logger.Errorf("Generate resource error, %s", err.Error())
		return nil, err
	}
	params := map[string]string{
		"labelSelector": model.LKUUID + "=" + name,
	}
	prompt := fmt.Sprintf("%v with label %v ", resource.Kind, model.LKUUID+"="+name)
	s.Logger.Infof("Try to get " + prompt)
	client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
	result, err := client.Request(common.HttpGet, resource, params)
	if err != nil {
		s.Logger.Errorf(prompt+"get error: %s", err.Error())
		return nil, err
	}
	ns, err := model.RestResultToNotifications(result)
	if len(ns) < 1 {
		return nil, common.BuildNotFoundError("can not find any notifications")
	}
	return ns[0], nil
}

func (s *Store) SendNotification(notifications []model.Notification) error {
	if len(notifications) < 1 {
		s.Logger.Infof("Has no associated notifications, skip")
		return nil
	}
	notificationsMap := map[string][]model.Notification{}
	for _, i := range notifications {
		if i.Namespace == "" {
			i.Namespace = config.GlobalConfig.Morgans.Namespace
		}
		if _, ok := notificationsMap[i.Namespace]; !ok {
			notificationsMap[i.Namespace] = []model.Notification{i}
		} else {
			notificationsMap[i.Namespace] = append(notificationsMap[i.Namespace], i)
		}
	}

	for _, v := range notificationsMap {
		resource := s.generateNotificationMessageResource(v)
		prompt := fmt.Sprintf(" %v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
		s.Logger.Infof("Try to create" + prompt)
		client := kubernetes.NewClient(s.Cluster, resource, s.Logger)
		_, err := client.Request(common.HttpPost, resource, nil)
		if err != nil {
			s.Logger.Errorf(prompt+"post error, %s", err.Error())
			continue
		}
	}
	return nil
}

func (s *Store) UpdateRequest(nr *model.NotificationResource) {
	s.Request.Name = nr.Name
	s.Request.UUID = nr.Labels[model.LKUUID]
	s.Request.Creator = nr.Annotations[model.AKCreator]
	s.Request.CreatedAt, _ = time.Parse(model.KTimeFormat, nr.Annotations[model.AKCreatedAt])
}
