package notification

import (
	"morgans/common"
	"morgans/model"
)

func (s *Store) setResourceLabels(current *model.NotificationResource) *model.NotificationResource {
	toSetLabels := map[string]string{
		model.LKUUID: s.Request.UUID,
	}
	current.Labels = common.MergeMaps(toSetLabels, current.Labels)
	return current
}
