package notification

import (
	"fmt"

	"morgans/common"
	"morgans/model"
)

func (s *Store) Validate() error {
	// TODO add some validation
	for _, subscription := range s.Request.Subscriptions {
		if !common.StringInSlice(subscription.Method, model.NotificationSupported) {
			return fmt.Errorf("notification method %s not supported yet", subscription.Method)
		}
	}
	return nil
}
