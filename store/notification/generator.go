package notification

import (
	"morgans/common"
	"morgans/model"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (s *Store) generateNotificationResource(namespace, name string) (*model.NotificationResource, error) {
	resource := &model.NotificationResource{
		ResourceMeta: model.ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: model.AVAiOpsV1beta1,
				Kind:       model.RKNotification,
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name:      name,
				Namespace: namespace,
			},
		},
		Spec: model.NotificationSpec{
			Subscriptions: s.Request.Subscriptions,
		},
	}
	return resource, nil
}

func (s *Store) generateNotificationMessageResource(notifications []model.Notification) *model.NotificationMessageResource {
	var notificationItems = make([]model.NotificationItem, len(notifications))
	for i, n := range notifications {
		notificationItems[i] = model.NotificationItem{Name: n.Name}
	}

	resource := &model.NotificationMessageResource{
		ResourceMeta: model.ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: model.AVAiOpsV1beta1,
				Kind:       model.RKNotificationMessage,
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name:      notifications[0].Name + "-message-" + common.RandStringRunes(7),
				Namespace: notifications[0].Namespace,
			},
		},
		Spec: model.NotificationMessageSpec{
			Notifications: notificationItems,
			Body:          s.Message,
		},
	}
	return resource
}
