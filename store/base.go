package store

import (
	"net"
	"strings"

	"morgans/cache"
	"morgans/common"
	"morgans/model"
)

type BaseStore struct {
	Cluster *model.Cluster
	Logger  *common.Logger
}

func (bs *BaseStore) GetLogger() *common.Logger {
	return bs.Logger
}

func (bs *BaseStore) GetMeta(qe *model.QueryExpression) *model.QueryMeta {
	meta := qe.GetMeta(bs.Cluster)
	meta.IndicatorMap = model.GetIndicatorMap(bs.Cluster)
	if meta.Kind != string(model.NodeKind) {
		return meta
	}
	// Do not translate wildcard for node name
	if meta.Name == ".*" {
		meta.AliasName = ".*"
		return meta
	}
	// Ignore wildcard if node name likes ip:.*
	ip := meta.Name
	colon := strings.Index(meta.Name, ":")
	if colon != -1 {
		ip = meta.Name[:colon]
	}
	// Ignore invalid ip address
	address := net.ParseIP(ip)
	if address == nil {
		bs.Logger.Errorf("Ip address %s is invalid, can not get hostname", ip)
		meta.AliasName = meta.Name
		return meta
	}
	// Treat host name as alias name for node
	hostname, err := cache.GetHostName(bs.Cluster, ip)
	if err != nil {
		bs.Logger.Errorf("Find hostname for %s error: %s", ip, err.Error())
		meta.AliasName = meta.Name
		return meta
	}
	bs.Logger.Infof("Find hostname for %s: %s", meta.Name, hostname)
	meta.AliasName = hostname
	return meta
}
