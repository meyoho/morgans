package meter

import (
	"os"

	"morgans/common"
	"morgans/config"
	"morgans/model"

	"github.com/alauda/bergamot/diagnose"
)

type Client struct {
	Endpoint   string
	ApiVersion string
	Timeout    int
	Logger     *common.Logger
}

func NewClient(logger *common.Logger) *Client {
	return &Client{
		Endpoint:   config.GlobalConfig.Morgans.Endpoint,
		ApiVersion: config.GlobalConfig.Morgans.ApiVersion,
		Timeout:    config.GlobalConfig.Morgans.Timeout,
		Logger:     logger,
	}
}

func (c *Client) Diagnose() diagnose.ComponentReport {
	reporter := diagnose.ComponentReport{
		Name:       "meterReport",
		Status:     "ok",
		Message:    "report function enabled",
		Suggestion: "",
		Latency:    0,
	}

	if _, err := os.Stat(model.ReportPath); err != nil {
		reporter.Status = "error"
		reporter.Suggestion = "We need a shared volume if you want to enable meter report"
		if os.IsNotExist(err) == true {
			reporter.Message = "Has no shared volume mounted"
		} else {
			reporter.Message = err.Error()
		}
	}
	return reporter
}
