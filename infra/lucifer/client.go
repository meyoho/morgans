package lucifer

import (
	"fmt"
	"time"

	"morgans/common"
	"morgans/config"
	"morgans/model"

	"github.com/alauda/bergamot/diagnose"
	"github.com/levigross/grequests"
)

type Client struct {
	Endpoint   string
	ApiVersion string
	Timeout    int
	Logger     *common.Logger
}

func (c *Client) Diagnose() diagnose.ComponentReport {
	reporter := diagnose.ComponentReport{
		Name:       "lucifer",
		Status:     "ok",
		Message:    "",
		Suggestion: "",
		Latency:    0,
	}
	url := fmt.Sprintf("%s/_ping", c.Endpoint)
	begin := time.Now()
	c.Logger.Infof("Request lucifer: GET %s", url)
	response, err := grequests.Get(url, &grequests.RequestOptions{RequestTimeout: time.Duration(c.Timeout) * time.Second})
	c.Logger.Infof("Request lucifer: Code %d, Latency %.6fs", response.StatusCode, time.Since(begin).Seconds())
	reporter.Latency = time.Since(begin)
	if err != nil {
		reporter.Message = err.Error()
		reporter.Status = "error"
		c.Logger.Errorf("Request lucifer error, %s", err.Error())
		return reporter
	}
	if response.StatusCode >= 300 {
		reporter.Message = fmt.Sprintf("Lucifer response code (%d) is more than 300", response.StatusCode)
		reporter.Status = "error"
		c.Logger.Infof("Code %d, Response %s", response.StatusCode, response.String())
		return reporter
	}
	reporter.Message = response.String()
	return reporter
}

func (c *Client) SendNotification(uuid string, body *model.NotificationBody) error {
	url := fmt.Sprintf("%s/%s/notifications/%s/messages", c.Endpoint, c.ApiVersion, uuid)
	begin := time.Now()
	c.Logger.Infof("Request lucifer: POST %s, Body %s", url, body.String())
	response, err := grequests.Post(url, &grequests.RequestOptions{RequestTimeout: time.Duration(c.Timeout) * time.Second, JSON: body})
	c.Logger.Infof("Request lucifer: Code %d, Latency %.6fs", response.StatusCode, time.Since(begin).Seconds())
	if err != nil {
		c.Logger.Errorf("Request lucifer error, %s", err.Error())
		return err
	}
	if response.StatusCode >= 300 {
		c.Logger.Infof("Code %d, Response %s", response.StatusCode, response.String())
		return common.NewError(common.CodeInternalError, fmt.Sprintf("Lucifer response code (%d) is more than 300", response.StatusCode))
	}
	return nil
}

func NewClient(logger *common.Logger) *Client {
	return &Client{
		Endpoint:   config.GlobalConfig.Lucifer.Endpoint,
		ApiVersion: config.GlobalConfig.Lucifer.ApiVersion,
		Timeout:    config.GlobalConfig.Lucifer.Timeout,
		Logger:     logger,
	}
}
