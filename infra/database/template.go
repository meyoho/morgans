package database

import (
	"regexp"

	"morgans/common"
	"morgans/model"

	"gopkg.in/doug-martin/goqu.v5"
)

const (
	UUIDField         = "uuid"
	NameField         = "name"
	OrganizationField = "organization"
	CreatorField      = "creator"
	KindField         = "kind"
	DescriptionField  = "description"
	TemplateField     = "template"
	UpdatedAtField    = "updated_at"
	TemplateTable     = "morgans_alert_template"
)

func (h *DBHandler) getEx(m map[string]string) goqu.Ex {
	ex := goqu.Ex{}
	for k, v := range m {
		ex[k] = v
	}
	return ex
}

func (h *DBHandler) InsertTemplate(record *model.AlertTemplateRecord) error {
	_, err := h.DB.From(TemplateTable).Insert(goqu.Record{
		UUIDField:         record.UUID,
		NameField:         record.Name,
		OrganizationField: record.Organization,
		KindField:         record.Kind,
		DescriptionField:  record.Description,
		CreatorField:      record.Creator,
		TemplateField:     record.Template,
	}).Exec()
	if err != nil && h.IsAlreadyExists(err) {
		return common.BuildAlreadyExistsError("template already exists in db")
	}
	return err
}

func (h *DBHandler) RetrieveTemplate(organization, name string) (*model.AlertTemplateRecord, error) {
	var rd model.AlertTemplateRecord
	found, err := h.DB.From(TemplateTable).Where(goqu.Ex{OrganizationField: organization, NameField: name}).ScanStruct(&rd)
	if err != nil {
		return &rd, err
	}
	if !found {
		return &rd, common.BuildNotFoundError("can not find template in db")
	}
	return &rd, nil
}

func (h *DBHandler) ListTemplate(organization string) ([]*model.AlertTemplateRecord, error) {
	var rds []*model.AlertTemplateRecord
	query := h.DB.From(TemplateTable).Where(goqu.Ex{OrganizationField: organization})
	sql, args, err := query.ToSql()
	h.Logger.Debugf("SQL %v, Args %v, Error %v", sql, args, err)
	err = query.ScanStructs(&rds)
	return rds, err
}

func (h *DBHandler) UpdateTemplate(record *model.AlertTemplateRecord) error {
	result, err := h.DB.From(TemplateTable).Where(goqu.Ex{
		OrganizationField: record.Organization,
		NameField:         record.Name,
	}).Update(goqu.Record{
		TemplateField:    record.Template,
		DescriptionField: record.Description,
		UpdatedAtField:   goqu.L(`now()`),
	}).Exec()
	if err != nil {
		return err
	}
	count, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if count < 1 {
		return common.BuildNotFoundError("can not find template in db")
	}
	return nil
}

func (h *DBHandler) DeleteTemplate(organization, name string) (int64, error) {
	result, err := h.DB.From(TemplateTable).Where(goqu.Ex{OrganizationField: organization, NameField: name}).Delete().Exec()
	if err != nil {
		return 0, err
	}
	return result.RowsAffected()
}

func (h *DBHandler) IsAlreadyExists(err error) bool {
	m, _ := regexp.MatchString(`^Error 1062: Duplicate entry .* for key 'organization_name_unique'$`, err.Error())
	if m {
		return true
	}
	m, _ = regexp.MatchString(`^pq: duplicate .* "organization_name_unique"'$`, err.Error())
	return m
}
