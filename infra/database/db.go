package database

import (
	"os"
	"time"

	"morgans/common"
	"morgans/config"
	"morgans/pkg/db"

	"github.com/Rican7/retry"
	"github.com/Rican7/retry/strategy"
	"github.com/sirupsen/logrus"
	"gopkg.in/doug-martin/goqu.v5"
)

var (
	GoQuDB *goqu.Database
)

type DBHandler struct {
	DB     *goqu.Database
	Logger *common.Logger
}

func init() {
	action := func(attempt uint) error {
		logrus.Debugf("Trying to init database: %d", attempt)
		database, err := GetDB()
		if err != nil {
			return err
		}
		logrus.Infof("Init database done: %+v", database)
		GoQuDB = database
		return nil
	}
	err := retry.Retry(
		action,
		strategy.Limit(10),
		strategy.Wait(time.Duration(config.GlobalConfig.DB.Wait)*time.Second),
	)
	if err != nil {
		logrus.WithError(err).Info("Connect db error, exit program")
		os.Exit(int(2))
	}
}

// GetDB return a database connection or error
func GetDB() (*goqu.Database, error) {
	var DBEngine db.Engine
	var DBConfig db.DatabaseConnectionOpts

	switch config.GlobalConfig.DB.Engine {
	case "mysql":
		DBEngine = db.MySQL
	default:
		DBEngine = db.Postgres
	}
	DBConfig = *db.NewDatabaseConnectionOpts(
		config.GlobalConfig.DB.Host,
		config.GlobalConfig.DB.Name,
		config.GlobalConfig.DB.User,
		config.GlobalConfig.DB.Password,
		config.GlobalConfig.DB.Port,
		config.GlobalConfig.DB.Timeout,
		config.GlobalConfig.DB.MaxConnection,
		config.GlobalConfig.DB.MaxIdleConnection,
		config.GlobalConfig.DB.MaxLifetime,
	)
	return db.New(DBEngine, DBConfig)
}

func NewDBHandler(logger *common.Logger) *DBHandler {
	return &DBHandler{
		DB:     GoQuDB,
		Logger: logger,
	}
}
