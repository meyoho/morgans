package database

import (
	"time"

	"github.com/alauda/bergamot/diagnose"
)

// Diagnose start diagnose check
// http://confluence.alaudatech.com/pages/viewpage.action?pageId=14123161
func (h *DBHandler) Diagnose() diagnose.ComponentReport {
	report := diagnose.NewReport("database")
	start := time.Now()

	// this works
	tx, err := h.DB.Begin()
	if tx != nil {
		tx.Rollback()
	}
	report.Check(err, "Database ping failed", "Check environment variables or database health")
	report.AddLatency(start)
	return *report
}
