package elasticsearch

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	esConfig "github.com/olivere/elastic/config"
	elasticV6 "gopkg.in/olivere/elastic.v6"

	"morgans/common"
	"morgans/config"
)

var esClient *elasticV6.Client

type Client struct {
	*elasticV6.Client
	*common.Logger
}

func NewClient(logger *common.Logger) (*Client, error) {
	var err error
	var eSUsernameFile = "/etc/pass_es/username"
	var esUsername string
	if username, err := ioutil.ReadFile(eSUsernameFile); err == nil {
		esUsername = string(strings.TrimRight(string(username), "\n"))
	}

	var eSPasswordFile = "/etc/pass_es/password"
	var esPassword string
	if password, err := ioutil.ReadFile(eSPasswordFile); err == nil {
		esPassword = string(strings.TrimRight(string(password), "\n"))
	}

	sniffed := false
	cfg := &esConfig.Config{
		URL:      config.GlobalConfig.ElasticSearch.Url,
		Username: esUsername,
		Password: esPassword,
		Sniff:    &sniffed,
	}

	if esClient == nil {
		esClient, err = elasticV6.NewClientFromConfig(cfg)
	}

	return &Client{esClient, logger}, err
}

func (c *Client) GetIndexNames(docType string, startTime, endTime float64) (result []string) {
	if startTime >= endTime {
		return
	}

	c.Logger.Infof("startTime: %f, endTime: %f", startTime, endTime)

	start := time.Unix(int64(startTime), 0)
	end := time.Unix(int64(endTime), 0)
	for start.Before(end) || start.Equal(end) {
		indexName := fmt.Sprintf("%s-%4d%02d%02d", docType, start.Year(), int(start.Month()), start.Day())
		c.Logger.Infof("Tobe check index: %s", indexName)
		if c.Exists(indexName) {
			result = append(result, indexName)
		}
		start = start.AddDate(0, 0, 1)
		// for sake of spanning in two days
		start = time.Date(start.Year(), start.Month(), start.Day(), 0, 0, 0, 0, start.Location())
	}

	return
}

func (c *Client) Exists(name string) bool {
	exist, _ := c.IndexExists(name).Do(context.Background())
	c.Logger.Infof("Index %s exist: %t", name, exist)
	return exist
}

func (c *Client) Do(ctx context.Context, searchService *elasticV6.SearchService) (*elasticV6.SearchResult, error) {
	begin := time.Now()
	c.Logger.Infof("Request ElasticSearch: requesting")
	result, err := searchService.Do(ctx)
	c.Logger.Infof("Request ElasticSearch: Latency %.6fs", time.Since(begin).Seconds())
	return result, err
}

func (c *Client) Query(ctx context.Context, searchSource *elasticV6.SearchSource, indexes []string) (*elasticV6.SearchResult, error) {
	searchService := c.Search()
	if config.GlobalConfig.ElasticSearch.Verbose {
		source, err := searchSource.Source()
		if err != nil {
			return nil, err
		}
		data, _ := json.Marshal(source)
		c.Logger.Infof("Request ElasticSearch with body: %s", string(data))
	}
	return c.Do(ctx, searchService.SearchSource(searchSource).Index(indexes...).Pretty(true))
}

func (c *Client) RetrieveRecord(recordIndex, recordType, recordId string, retries int) (*elasticV6.SearchHit, error) {
	searchSource := elasticV6.NewSearchSource()

	timeSorter := elasticV6.NewFieldSort("time").Desc()
	docSorter := elasticV6.NewFieldSort("_doc").Asc()

	idsQuery := elasticV6.NewIdsQuery(recordType).Ids(recordId)
	constQuery := elasticV6.NewConstantScoreQuery(idsQuery)

	searchSource.Query(constQuery)
	searchSource.SortBy(timeSorter, docSorter)
	searchSource.FetchSource(true)
	searchSource.Size(1)

	begin := time.Now()
	c.Logger.Infof("Request ElasticSearch to retrieve %s/%s/%s", recordIndex, recordType, recordId)
	searchResult, err := c.Query(context.Background(), searchSource, []string{recordIndex})
	c.Logger.Infof("Request ElasticSearch to retrieve %s/%s/%s, Latency: %.6fs", recordIndex, recordType, recordId, time.Since(begin).Seconds())
	if err != nil {
		return nil, err
	}
	c.Logger.Infof("Request ElasticSearch to retrieve %s/%s/%s, result: %+v", recordIndex, recordType, recordId, searchResult)
	if searchResult.Hits.TotalHits == 0 {
		if retries < 0 {
			return nil, errors.New(fmt.Sprintf("No doc record find for specified: index(%s), type(%s), id(%s)", recordIndex, recordType, recordId))
		} else {
			return c.RetrieveRecord(recordIndex, recordType, recordId, retries-1)
		}
	} else if searchResult.Hits.TotalHits > 1 {
		if retries < 0 {
			return nil, errors.New(fmt.Sprintf("Multiple doc record find for specified: index(%s), type(%s), id(%s)", recordIndex, recordType, recordId))
		} else {
			return c.RetrieveRecord(recordIndex, recordType, recordId, retries-1)
		}
	}

	return searchResult.Hits.Hits[0], nil
}
