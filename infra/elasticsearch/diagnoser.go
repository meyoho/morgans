package elasticsearch

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/alauda/bergamot/diagnose"

	"morgans/config"
)

type Diagnoser struct {
	Endpoint string
}

func NewDiganoser() *Diagnoser {
	return &Diagnoser{
		Endpoint: config.GlobalConfig.ElasticSearch.Url,
	}
}

func (c *Diagnoser) Diagnose() diagnose.ComponentReport {
	reporter := diagnose.ComponentReport{
		Name:       "elasticsearch",
		Status:     "ok",
		Message:    "",
		Suggestion: "",
		Latency:    0,
	}
	url := fmt.Sprintf("%s/_cat/health", c.Endpoint)
	begin := time.Now()
	authedAccessElasticsearch(url, &reporter)
	reporter.Latency = time.Since(begin)
	return reporter
}

// check es cluster health
func authedAccessElasticsearch(url string, reporter *diagnose.ComponentReport) {
	var eSUsernameFile = "/etc/pass_es/username"
	var esUsername string
	if username, err := ioutil.ReadFile(eSUsernameFile); err == nil {
		esUsername = string(strings.TrimRight(string(username), "\n"))
	}

	var eSPasswordFile = "/etc/pass_es/password"
	var esPassword string
	if password, err := ioutil.ReadFile(eSPasswordFile); err == nil {
		esPassword = string(strings.TrimRight(string(password), "\n"))
	}
	client := &http.Client{
		Timeout: 10 * time.Second,
	}
	req, err := http.NewRequest("GET", url, nil)
	req.SetBasicAuth(esUsername, esPassword)
	resp, err := client.Do(req)
	if err != nil {
		reporter.Message = err.Error()
		reporter.Status = "error"
		return
	}
	defer resp.Body.Close()
	bodyText, _ := ioutil.ReadAll(resp.Body)
	message := string(bodyText)
	esHealth := strings.Split(message, " ")
	if len(esHealth) > 3 {
		reporter.Message = esHealth[3]
	} else {
		reporter.Message = "red"
	}
}
