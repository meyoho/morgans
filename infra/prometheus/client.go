package prometheus

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"morgans/common"
	"morgans/model"
	"morgans/pkg/prometheus/client-golang/api"
	"morgans/pkg/prometheus/client-golang/api/prometheus/v1"

	"github.com/levigross/grequests"
	pm "github.com/prometheus/common/model"
)

type Client struct {
	APIConfig api.Config
	Timeout   int
	Logger    *common.Logger
}

func NewClient(config model.PrometheusConfig, logger *common.Logger) *Client {
	return &Client{
		APIConfig: api.Config{Address: config.Endpoint, Username: config.Username, Password: config.Password},
		Timeout:   config.Timeout,
		Logger:    logger,
	}
}

func (c *Client) GetRange(query string, start int64, end int64, step int64) (pm.Value, error) {
	client, err := api.NewClient(c.APIConfig)
	if err != nil {
		return nil, err
	}

	handler := v1.NewAPI(client)
	ranger := v1.Range{
		Start: time.Unix(start, 0),
		End:   time.Unix(end, 0),
		Step:  time.Second * time.Duration(step),
	}
	u := client.URL("/api/v1/query_range", nil)
	q := u.Query()
	q.Set("query", query)
	q.Set("start", ranger.Start.Format(time.RFC3339Nano))
	q.Set("end", ranger.End.Format(time.RFC3339Nano))
	q.Set("step", strconv.FormatFloat(ranger.Step.Seconds(), 'f', 3, 64))
	u.RawQuery = q.Encode()

	begin := time.Now()
	c.Logger.Infof("Request prometheus: GET %s", u.String())
	value, err := handler.QueryRange(context.Background(), query, ranger)
	c.Logger.Infof("Request prometheus: Latency %.6fs", time.Since(begin).Seconds())
	if err != nil {
		c.Logger.Errorf("Request prometheus error, %s", err.Error())
		return nil, common.BuildServiceUnavailableError(err.Error())
	}
	return value, nil
}

func (c *Client) Query(expr string, ts int64) (pm.Value, error) {
	client, err := api.NewClient(c.APIConfig)
	if err != nil {
		return nil, err
	}

	handler := v1.NewAPI(client)
	u := client.URL("/api/v1/query", nil)
	q := u.Query()
	q.Set("query", expr)
	q.Set("time", time.Unix(ts, 0).Format(time.RFC3339Nano))
	u.RawQuery = q.Encode()

	begin := time.Now()
	c.Logger.Infof("Request prometheus: GET %s", u.String())
	result, err := handler.Query(context.Background(), expr, time.Unix(ts, 0))
	c.Logger.Infof("Request prometheus: Latency %.6fs", time.Since(begin).Seconds())
	if err != nil {
		c.Logger.Errorf("Request prometheus error, %s", err.Error())
		return result, common.BuildServiceUnavailableError(err.Error())
	}
	return result, nil
}

func (c *Client) Proxy(method string, url string, params map[string]string, data map[string]string) (string, int, *APIError) {
	begin := time.Now()
	url = c.APIConfig.Address + url
	c.Logger.Infof("Request prometheus: GET %s", url)
	var err error
	var response *grequests.Response
	requestOptions := &grequests.RequestOptions{
		Data:               data,
		Params:             params,
		Auth:               []string{c.APIConfig.Username, c.APIConfig.Password},
		RequestTimeout:     time.Duration(c.Timeout) * time.Second,
		InsecureSkipVerify: true,
	}
	switch method {
	case common.HttpGet:
		response, err = grequests.Get(url, requestOptions)
	case common.HttpPost:
		response, err = grequests.Post(url, requestOptions)
	case common.HttpPut:
		response, err = grequests.Put(url, requestOptions)
	case common.HttpDelete:
		response, err = grequests.Delete(url, requestOptions)
	default:
		return "", 500, BuildAPIError(0, fmt.Sprintf("oh no, method %s has not been implemented by morgans", method))
	}
	statusCode := -1
	if response != nil {
		statusCode = response.StatusCode
	}
	c.Logger.Infof("Request prometheus: Code %d, Latency %.6fs", statusCode, time.Since(begin).Seconds())
	if err != nil {
		c.Logger.Errorf("Request prometheus error, %s", err.Error())
		return "", statusCode, BuildAPIError(statusCode, err.Error())
	}
	if response.StatusCode >= 300 {
		c.Logger.Errorf("Code %d, Response %s", response.StatusCode, response.String())
		return "", response.StatusCode, BuildAPIError(response.StatusCode, response.String())
	}
	return response.String(), response.StatusCode, nil
}
