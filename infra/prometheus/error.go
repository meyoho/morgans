package prometheus

import (
	"encoding/json"
	"fmt"

	"morgans/common"
)

type APIError struct {
	Status       string `json:"status,omitempty"`
	ErrorType    string `json:"errorType,omitempty"`
	ErrorMessage string `json:"error,omitempty"`
}

func (e *APIError) Error() string {
	return e.ErrorMessage
}

func (e *APIError) IsBadDataError() bool {
	return e.Status == "error" && e.ErrorType == "bad_data"
}

func (e *APIError) Wrap() error {
	if e.IsBadDataError() {
		return common.BuildBadRequestError(e.Error())
	} else if e.Status == "" {
		return common.BuildServiceUnavailableError("Prometheus unavailable: " + e.Error())
	} else {
		return common.BuildUnknownError(e.Error())
	}
}

func BuildAPIError(code int, response string) *APIError {
	apiError := APIError{}
	if err := json.Unmarshal([]byte(response), &apiError); err != nil {
		apiError.ErrorMessage = fmt.Sprintf("got unexpected response code %d from prometheus, message %s", code, response)
	}
	if apiError.ErrorMessage == "" {
		apiError.ErrorMessage = fmt.Sprintf("got unexpected response code %d from prometheus, message %s", code, response)
	}
	return &apiError
}
