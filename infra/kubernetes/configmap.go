package kubernetes

import (
	"morgans/common"
	"morgans/config"
	"morgans/model"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GenerateCoreConfigmapResource(c *model.Cluster) *model.ConfigMapResource {
	namespace := config.GlobalConfig.Morgans.Namespace
	if c.PrometheusConfig != nil && c.PrometheusConfig.Namespace != "" {
		namespace = c.PrometheusConfig.Namespace
	}
	return &model.ConfigMapResource{
		ResourceMeta: model.ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: common.KubernetesAPIVersionV1,
				Kind:       model.KindConfigMap,
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name:      model.AlertConfigmapName,
				Namespace: namespace,
			},
		},
		Data: make(map[string]string, 0),
	}
}

func (c *Client) GetCoreConfigMap() (*model.ConfigMapResource, error) {
	resource := GenerateCoreConfigmapResource(c.Cluster)
	result, err := c.Request(common.HttpGet, resource, nil)
	if err != nil {
		return nil, err
	}
	return model.RestResultToConfigMap(result)
}

func (c *Client) CreateCoreConfigmap(resource *model.ConfigMapResource) error {
	_, err := c.Request(common.HttpPost, resource, nil)
	return err
}

func (c *Client) UpdateCoreConfigmap(resource *model.ConfigMapResource) error {
	_, err := c.Request(common.HttpPut, resource, nil)
	return err
}
