package davion

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"morgans/common"
	"morgans/config"
	"morgans/model"

	"github.com/alauda/bergamot/diagnose"
	"github.com/levigross/grequests"
)

type Client struct {
	Endpoint   string
	ApiVersion string
	Timeout    int
	Logger     *common.Logger
}

func (c *Client) Diagnose() diagnose.ComponentReport {
	reporter := diagnose.ComponentReport{
		Name:       "davion",
		Status:     "ok",
		Message:    "",
		Suggestion: "",
		Latency:    0,
	}
	url := fmt.Sprintf("%s/_ping", c.Endpoint)
	begin := time.Now()
	c.Logger.Infof("Request davion: GET %s", url)
	response, err := grequests.Get(url, &grequests.RequestOptions{RequestTimeout: time.Duration(c.Timeout) * time.Second})
	c.Logger.Infof("Request davion: Code %d, Latency %.6fs", response.StatusCode, time.Since(begin).Seconds())
	reporter.Latency = time.Since(begin)
	if err != nil {
		reporter.Message = err.Error()
		reporter.Status = "error"
		c.Logger.Errorf("Request davion error, %s", err.Error())
		return reporter
	}
	if response.StatusCode >= 300 {
		reporter.Message = fmt.Sprintf("davion response code (%d) is more than 300", response.StatusCode)
		reporter.Status = "error"
		c.Logger.Infof("Code %d, Response %s", response.StatusCode, response.String())
		return reporter
	}
	reporter.Message = response.String()
	return reporter
}
func (c *Client) ListIntegrations(uuids []string) (map[string]*model.Integration, error) {
	result := map[string]*model.Integration{}
	if len(uuids) == 0 {
		return result, nil
	}
	url := fmt.Sprintf("%s/%s/integrations", c.Endpoint, c.ApiVersion)
	begin := time.Now()
	c.Logger.Infof("Request davion: GET %s", url)
	response, err := grequests.Get(url, &grequests.RequestOptions{Params: map[string]string{"uuids": strings.Join(uuids, ",")}, RequestTimeout: time.Duration(c.Timeout) * time.Second})
	c.Logger.Infof("Request davion: Code %d, Latency %.6fs", response.StatusCode, time.Since(begin).Seconds())
	if err != nil {
		c.Logger.Errorf("Request davion error, %s", err.Error())
		return nil, err
	}
	if response.StatusCode >= 300 {
		c.Logger.Infof("Code %d, Response %s", response.StatusCode, response.String())
		return nil, common.NewError(common.CodeInternalError, fmt.Sprintf("Davion response code (%d) is more than 300", response.StatusCode))
	}

	integrations := make([]*model.Integration, 0)
	if err = json.Unmarshal([]byte(response.String()), &integrations); err != nil {
		return nil, err
	}

	for _, integration := range integrations {
		result[integration.ID] = integration
	}
	return result, nil
}

func (c *Client) ParseClusters(clusters map[string]*model.Cluster) error {
	if !config.GlobalConfig.Davion.Enabled {
		return nil
	}

	var uuids = make([]string, 0)
	for _, cluster := range clusters {
		if cluster.PrometheusConfig.IntegrationUUID != "" {
			uuids = append(uuids, cluster.PrometheusConfig.IntegrationUUID)
		}
	}
	integrations, err := c.ListIntegrations(uuids)
	if err != nil {
		return err
	}
	for _, cluster := range clusters {
		integration, ok := integrations[cluster.PrometheusConfig.IntegrationUUID]
		if !ok {
			cluster.HasMetricFeature = cluster.PrometheusConfig.Endpoint == ""
			continue
		}
		cluster.HasMetricFeature = true
		cluster.PrometheusConfig = &model.PrometheusConfig{
			Name:      integration.Fields.Name,
			Namespace: integration.Fields.Namespace,
			Endpoint:  integration.Fields.PrometheusURL,
			Timeout:   integration.Fields.PrometheusTimeout,
		}
		if cluster.PrometheusConfig.Timeout <= 0 {
			cluster.PrometheusConfig.Timeout = config.GlobalConfig.Prometheus.Timeout
		}
	}
	return nil
}

func NewClient(logger *common.Logger) *Client {
	return &Client{
		Endpoint:   config.GlobalConfig.Davion.Endpoint,
		ApiVersion: config.GlobalConfig.Davion.ApiVersion,
		Timeout:    config.GlobalConfig.Davion.Timeout,
		Logger:     logger,
	}
}
