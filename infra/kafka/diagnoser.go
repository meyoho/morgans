package kafka

import (
	"context"
	"github.com/segmentio/kafka-go/sasl/plain"
	"io/ioutil"
	"strings"
	"time"

	"github.com/alauda/bergamot/diagnose"
	"github.com/segmentio/kafka-go"

	"morgans/config"
)

type Diagnoser struct {
	Endpoint string
}

func NewDiganoser() *Diagnoser {
	return &Diagnoser{
		Endpoint: config.GlobalConfig.Kafka.Hosts,
	}
}

func (c *Diagnoser) Diagnose() diagnose.ComponentReport {
	var kafkaUsernameFile = "/etc/pass_secret/KAFKA_USER"
	var kafkaPasswordFile = "/etc/pass_secret/KAFKA_PASSWORD"
	var kafkaUsername string
	var kafkaPassword string
	if username, err := ioutil.ReadFile(kafkaUsernameFile); err == nil {
		kafkaUsername = string(strings.TrimRight(string(username), "\n"))
	}

	if password, err := ioutil.ReadFile(kafkaPasswordFile); err == nil {
		kafkaPassword = string(strings.TrimRight(string(password), "\n"))
	}
	reporter := diagnose.ComponentReport{
		Name:       "kafka",
		Status:     "ok",
		Message:    "Buffer me, me buffer",
		Suggestion: "",
		Latency:    0,
	}
	brokerList := strings.Split(c.Endpoint, ",")

	if len(brokerList) == 0 {
		reporter.Status = "error"
		reporter.Message = "KAFKA_HOSTS environment is required"
		return reporter
	}

	dialer := &kafka.Dialer{
		Timeout:   10 * time.Second,
		DualStack: true,
		SASLMechanism: plain.Mechanism{
			Username: kafkaUsername,
			Password: kafkaPassword,
		},
	}

	var totalLatency int64 = 0

	for _, broker := range brokerList {
		begin := time.Now()
		conn, err := dialer.DialContext(context.Background(), "tcp", broker)
		if err != nil {
			reporter.Message = err.Error()
			reporter.Status = "error"
			return reporter
		}
		defer conn.Close()
		conn.ApiVersions()
		totalLatency += int64(time.Since(begin))
	}

	reporter.Latency = time.Duration(totalLatency / int64(len(brokerList)))

	return reporter
}
