package kafka

import (
	"context"
	"github.com/segmentio/kafka-go/sasl/plain"
	"io/ioutil"
	"strings"

	"github.com/segmentio/kafka-go"

	"morgans/common"
	"morgans/config"
)

var writers map[string]*kafka.Writer = make(map[string]*kafka.Writer)

type Client struct {
	*kafka.Writer
	logger *common.Logger
}

func NewClient(logger *common.Logger, kafkaTopicName string) (*Client, error) {
	var kafkaUsernameFile = "/etc/pass_secret/KAFKA_USER"
	var kafkaPasswordFile = "/etc/pass_secret/KAFKA_PASSWORD"
	var kafkaUsername string
	var kafkaPassword string
	if username, err := ioutil.ReadFile(kafkaUsernameFile); err == nil {
		kafkaUsername = string(strings.TrimRight(string(username), "\n"))
	}

	if password, err := ioutil.ReadFile(kafkaPasswordFile); err == nil {
		kafkaPassword = string(strings.TrimRight(string(password), "\n"))
	}
	hosts := config.GlobalConfig.Kafka.Hosts
	brokerList := strings.Split(hosts, ",")
	writer, exist := writers[kafkaTopicName]
	if !exist || writer == nil {
		if config.GlobalConfig.Kafka.Auth {
			writer = kafka.NewWriter(kafka.WriterConfig{
				Dialer: &kafka.Dialer{
					SASLMechanism: plain.Mechanism{
						Username: kafkaUsername,
						Password: kafkaPassword,
					},
				},
				Brokers:    brokerList,
				Topic:      kafkaTopicName,
				BatchBytes: config.GlobalConfig.Kafka.MaxRequestSize,
			})
		} else {
			writer = kafka.NewWriter(kafka.WriterConfig{
				Brokers:    brokerList,
				Topic:      kafkaTopicName,
				BatchBytes: config.GlobalConfig.Kafka.MaxRequestSize,
			})
		}
		writers[kafkaTopicName] = writer
	}

	return &Client{writer, logger}, nil
}

func (c *Client) SendMsg(key, value []byte) error {
	err := c.Writer.WriteMessages(context.Background(),
		kafka.Message{
			Key:   key,
			Value: value,
		},
	)

	c.logger.Infof("Send key: %s with err: %+v", string(key), err)

	return err
}
