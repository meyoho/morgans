package utils

import (
	"encoding/json"
	"fmt"
	"regexp"

	"github.com/Jeffail/gabs"
)

func Search(items interface{}, path string, pattern string) (interface{}, error) {
	itemBytes, err := json.Marshal(items)
	if err != nil {
		return nil, err
	}

	itemList, err := gabs.ParseJSON(itemBytes)
	if err != nil {
		return nil, err
	}

	itemContainers, err := itemList.Children()
	if err != nil {
		return nil, err
	}

	results := make([]interface{}, 0)
	for _, c := range itemContainers {
		d := c.Path(path).Data()
		switch d.(type) {
		case string:
			matched, err := regexp.MatchString(pattern, d.(string))
			if err != nil {
				return nil, err
			}
			if matched {
				results = append(results, c.Data())
			}
		default:
			return nil, fmt.Errorf("search path %s is not a string", path)
		}
	}
	return results, nil
}
