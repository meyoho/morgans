package utils

import (
	"encoding/json"
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"

	"github.com/Jeffail/gabs"
)

type Page struct {
	Next     string      `json:"next"`
	Previous string      `json:"previous"`
	PageSize int         `json:"page_size"`
	NumPages int         `json:"num_pages"`
	Count    int         `json:"count"`
	Results  interface{} `json:"results"`
}

type ContainersSorter struct {
	Containers  []*gabs.Container
	OrderByPath string
	Reverse     bool
}

func (c ContainersSorter) Len() int {
	return len(c.Containers)
}

func (c ContainersSorter) Less(i, j int) bool {
	iContainer, jContainer := c.Containers[i], c.Containers[j]
	iData, jData := iContainer.Path(c.OrderByPath).Data(), jContainer.Path(c.OrderByPath).Data()
	less := false
	switch iData.(type) {
	case string:
		less = iData.(string) < jData.(string)
	case int:
		less = iData.(int) < jData.(int)
	case float64:
		less = iData.(float64) < jData.(float64)
	default:
		less = iContainer.Path("name").Data().(string) < jContainer.Path("name").Data().(string)
	}
	if c.Reverse {
		less = !less
	}
	return less
}

func (c ContainersSorter) Swap(i, j int) {
	c.Containers[i], c.Containers[j] = c.Containers[j], c.Containers[i]
}

func (c ContainersSorter) Sort() {
	sort.Sort(c)
}

func Pagination(items interface{}, orderBy string, pageSize int, pageNumber int) (*Page, error) {
	containers, err := sortItems(items, orderBy)
	if err != nil {
		return nil, err
	}

	totalPages := int(math.Max(math.Ceil(float64(len(containers))/float64(pageSize)), 1))
	if err := validate(containers, totalPages, pageSize, pageNumber); err != nil {
		return nil, err
	}

	next, previous := "", ""
	var hitContainers []*gabs.Container
	if pageNumber < totalPages {
		next = "?page=" + strconv.Itoa(pageNumber+1)
	}
	if pageNumber > 1 {
		previous = "?page=" + strconv.Itoa(pageNumber-1)
	}

	if pageNumber < totalPages {
		hitContainers = containers[(pageNumber-1)*pageSize : pageNumber*pageSize]
	} else {
		hitContainers = containers[(pageNumber-1)*pageSize:]
	}

	results := make([]interface{}, 0)
	for _, c := range hitContainers {
		results = append(results, c.Data())
	}

	page := &Page{
		Next:     next,
		Previous: previous,
		PageSize: pageSize,
		NumPages: totalPages,
		Count:    len(containers),
		Results:  results,
	}
	return page, nil
}

func validate(containers []*gabs.Container, totalPages int, pageSize int, pageNumber int) error {
	if pageSize < 0 {
		return fmt.Errorf("page size can not be less than 1 (now %d)", pageSize)
	}

	if pageNumber < 0 {
		return fmt.Errorf("page number can not be less than 1 (now %d)", pageNumber)
	}

	if pageNumber > totalPages {
		return fmt.Errorf("page number can not be more than %d (now %d)", totalPages, pageNumber)
	}
	return nil
}

func sortItems(items interface{}, orderBy string) ([]*gabs.Container, error) {
	itemBytes, err := json.Marshal(items)
	if err != nil {
		return nil, err
	}

	itemList, err := gabs.ParseJSON(itemBytes)
	if err != nil {
		return nil, err
	}

	itemContainers, err := itemList.Children()
	if err != nil {
		return nil, err
	}

	reverse := false
	if strings.HasPrefix(orderBy, "-") {
		reverse = true
		orderBy = orderBy[1:]
	}
	sorter := ContainersSorter{
		Containers:  itemContainers,
		OrderByPath: orderBy,
		Reverse:     reverse,
	}
	sorter.Sort()
	return sorter.Containers, nil
}

func GeneratePages(items interface{}, count, pageSize, pageNumber int) (*Page, error) {
	if count == 0 {
		return &Page{
			Next:     "",
			Previous: "",
			PageSize: pageSize,
			NumPages: count,
			Count:    count,
			Results:  []string{},
		}, nil
	}

	itemBytes, err := json.Marshal(items)
	if err != nil {
		return nil, err
	}

	itemList, err := gabs.ParseJSON(itemBytes)
	if err != nil {
		return nil, err
	}

	itemContainers, err := itemList.Children()
	if err != nil {
		return nil, err
	}

	totalPages := int(math.Max(math.Ceil(float64(count)/float64(pageSize)), 1))
	if err := validate(itemContainers, totalPages, pageSize, pageNumber); err != nil {
		return nil, err
	}

	next, previous := "", ""
	if pageNumber < totalPages {
		next = "?page=" + strconv.Itoa(pageNumber+1)
	}
	if pageNumber > 1 {
		previous = "?page=" + strconv.Itoa(pageNumber-1)
	}

	results := make([]interface{}, 0)
	for _, c := range itemContainers {
		results = append(results, c.Data())
	}

	page := &Page{
		Next:     next,
		Previous: previous,
		PageSize: pageSize,
		NumPages: totalPages,
		Count:    count,
		Results:  results,
	}
	return page, nil
}
