package handler

import (
	"net/http"

	"morgans/common"
	"morgans/infra/elasticsearch"
	"morgans/infra/kafka"
	"morgans/infra/meter"

	"github.com/alauda/bergamot/diagnose"
	"github.com/gin-gonic/gin"
)

func Ping(c *gin.Context) {
	c.String(http.StatusOK, "morgans:Let it go, the bug never bothered me anyway!")
}

func Diagnose(c *gin.Context) {
	reporter, _ := diagnose.New()
	logger := common.GetGinLogger(c)
	reporter.Add(elasticsearch.NewDiganoser())
	reporter.Add(kafka.NewDiganoser())
	reporter.Add(meter.NewClient(logger))
	c.JSON(http.StatusOK, reporter.Check())
}
