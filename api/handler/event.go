package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"

	"morgans/common"
	"morgans/config"
	"morgans/control/event"
	"morgans/model"
)

func ListEvents(c *gin.Context) {
	logger := common.GetGinLogger(c)

	valid, err := canI(c, "get", "events", "aiops.alauda.io", config.GlobalConfig.Morgans.Namespace)
	if err != nil {
		logger.Errorf("Failed to validate permission with err: %s", err.Error())
		HandleError(err, c, logger)
		return
	}
	if !valid {
		logger.Infof("User has no permission to get events")
		HandleError(apiErrors.NewUnauthorized("Invalid Token"), c, logger)
		return
	}

	controller, err := getEventQueryController(c, logger)
	if err != nil {
		logger.Errorf("Failed to get event controller, %s", err.Error())
		HandleError(err, c, logger)
		return
	}

	response, err := controller.ListEvents()

	if err != nil {
		logger.Errorf("Failed to get result from event controller, %s", err.Error())
		HandleError(err, c, logger)
		return
	}

	c.JSON(http.StatusOK, response)
}

func getEventQueryController(c *gin.Context, logger *common.Logger) (*event.Controller, error) {
	req, err := genEventRequest(c)
	if err != nil {
		return nil, err
	}

	s := &event.Controller{
		Request: req,
		Logger:  logger,
	}

	return s, nil
}

// genEventRequest construct an model.EventRequest from gin.Context
func genEventRequest(c *gin.Context) (*model.EventRequest, error) {
	req := &model.EventRequest{
		K8sNamespaces: c.Query("namespace"),
		Cluster:       c.Query("cluster"),
		K8sKinds:      c.Query("kind"),
		K8sNames:      c.Query("name"),
		Filters:       c.Query("filters"),
		Size:          model.DefaultEventPageSize,
		Page:          1,
		Source:        model.DefaultEventSource,
		Fields:        []string{"time", "namespace", "resource_type", "resource_id", "resource_name", "template_id", "log_level"},
		Fuzzy:         true,
		PartialFields: []string{"detail.*"},
	}

	if c.Query("size") != "" {
		size, err := strconv.ParseInt(c.Query("size"), 10, 0)
		if err != nil {
			return nil, err
		}
		req.Size = int(size)
	}
	if c.Query("pageno") != "" {
		page, err := strconv.ParseInt(c.Query("pageno"), 10, 0)
		if err != nil {
			return nil, err
		}
		req.Page = int(page)
	}
	if c.Query("start_time") != "" {
		startTime, err := strconv.ParseFloat(c.Query("start_time"), 64)
		if err != nil {
			return nil, err
		}
		req.StartTime = startTime
	}
	if c.Query("end_time") != "" {
		endTime, err := strconv.ParseFloat(c.Query("end_time"), 64)
		if err != nil {
			return nil, err
		}
		req.EndTime = endTime
	}
	if c.Query("fuzzy") != "" {
		fuzzy, err := strconv.ParseBool(c.Query("fuzzy"))
		if err != nil {
			return nil, err
		}
		req.Fuzzy = fuzzy
	}

	return req, nil
}
