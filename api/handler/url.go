package handler

import (
	"github.com/gin-gonic/gin"
)

func AddRoutes(r *gin.Engine) {
	r.GET("/", Ping)
	r.GET("/_ping", Ping)
	r.GET("/_diagnose", Diagnose)

	v1 := r.Group("/v1")
	{
		// Config api
		v1.GET("/config/alerts", GetConfig)

		// Prometheus apis
		v1.GET("/metrics/:cluster/prometheus/label/:labelName/values", PrometheusProxy)
		v1.GET("/metrics/:cluster/prometheus/query", PrometheusProxy)

		// Metric query and query range
		v1.GET("/metrics/:cluster/indicators", ListIndicator)
		v1.POST("/metrics/:cluster/query", Query)
		v1.POST("/metrics/:cluster/query_range", QueryRange)

		// Alerts router
		v1.POST("/alerts/:cluster/router", AlertRouter)

		// Alerts create, list and delete without namespace in url
		// TODO these api will be removed in future, please use api with namespace in url
		v1.GET("/alerts/:cluster", ListAlerts)
		v1.DELETE("/alerts/:cluster", DeleteAlert)
		// Alerts create, list, get, update and delete
		v1.POST("/alerts/:cluster", CreateAlert)
		v1.GET("/alerts/:cluster/:namespace", ListAlerts)
		v1.DELETE("/alerts/:cluster/:namespace", DeleteAlert)
		v1.PUT("/alerts/:cluster/:namespace/:resourceName/:groupName/:alertName", UpdateAlert)
		v1.GET("/alerts/:cluster/:namespace/:resourceName/:groupName/:alertName", GetAlert)
		v1.DELETE("/alerts/:cluster/:namespace/:resourceName/:groupName/:alertName", DeleteAlert)

		// Alert template
		v1.POST("/alert_templates/:organization", CreateAlertTemplate)
		v1.GET("/alert_templates/:organization", ListAlertTemplates)
		v1.GET("/alert_templates/:organization/:templateName", GetAlertTemplate)
		v1.PUT("/alert_templates/:organization/:templateName", UpdateAlertTemplate)
		v1.POST("/alert_templates/:organization/:templateName/apply", ApplyAlertTemplate)
		v1.DELETE("/alert_templates/:organization/:templateName", DeleteAlertTemplate)

		// Notification apis
		v1.POST("/notifications/:namespace", CreateNotification)
		v1.GET("/notifications/:namespace", ListNotifications)
		v1.GET("/notifications/:namespace/:notificationName", GetNotification)
		v1.PUT("/notifications/:namespace/:notificationName", UpdateNotification)
		v1.DELETE("/notifications/:namespace/:notificationName", DeleteNotification)
		v1.POST("/notifications/:namespace/:notificationName/send", SendNotification)

		// Meter apis
		v1.GET("/meter/topn", MeterTopN)
		v1.GET("/meter/total", MeterTotal)
		v1.GET("/meter/summary", MeterSummary)
		v1.GET("/meter/query", MeterQuery)
		v1.POST("/meter/reports", MeterReportUpload)
		v1.GET("/meter/reports/:reportName", MeterReportDownload)

		// Inner apis
		v1.GET("/inner/clusters", ListRegion)
		v1.GET("/inner/migration/:cluster/version", GetVersion)
		v1.PUT("/inner/migration/:cluster/version", UpdateVersion)
		v1.POST("/inner/kubernetes/:cluster/:resourceType", CreateResource)
		v1.GET("/inner/kubernetes/:cluster/:namespace/:resourceType/:resourceName", GetResource)
		v1.GET("/inner/kubernetes/:cluster/:namespace/:resourceType", ListResources)

		// Audit apis
		v1.GET("/kubernetes-audits/types", AggregateAudit)
		v1.GET("/kubernetes-audits", ListAudit)
	}

	v4 := r.Group("/v4")
	{
		// Log apis
		v4.GET("/logs/search", Search)
		v4.GET("/logs/aggregations", Aggregation)
		v4.GET("/logs/types", Types)
		v4.GET("/logs/context", Context)
		v4.POST("/callback/:dataSource", Callback)

		v4.GET("/events", ListEvents)
	}
}
