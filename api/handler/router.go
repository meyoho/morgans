package handler

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"morgans/common"
	"morgans/model"
	"morgans/store"
	"morgans/store/notification"
	"morgans/store/router"

	"github.com/gin-gonic/gin"
)

func AlertRouter(c *gin.Context) {
	// Get router helper
	r, err := getRouterHelper(c)
	if err != nil {
		r.Logger.Errorf("Get cluster config error, %s", err.Error())
		HandleError(err, c, r.Logger)
		return
	}

	// Output request body if needed
	if common.LogLevel > 3 {
		var bodyBytes []byte
		if c.Request.Body != nil {
			bodyBytes, _ = ioutil.ReadAll(c.Request.Body)
			r.Logger.Debugfl(3, "Request body, %s", string(bodyBytes))
		}
		c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	}

	// Bind http body to router request
	if err := c.Bind(r.Request); err != nil {
		r.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, r.Logger)
		return
	}

	// If no alerts, skip
	if len(r.Request.Alerts) < 1 {
		r.Logger.Errorf("Can not find alert")
		c.JSON(http.StatusNoContent, nil)
		return
	}

	// Trigger each alerts in body
	for _, item := range r.Request.Alerts {
		// Get associated notifications for this alert item
		notifications, ok := item.Annotations[model.AlertNotificationAnnotationKey]
		if !ok || notifications == "" || notifications == "[]" {
			r.Logger.Infof("Has no associated notifications, skip")
			continue
		}
		var notificationList []model.Notification
		if err := json.Unmarshal([]byte(notifications), &notificationList); err != nil {
			r.Logger.Errorf("Unmarshal notification list error, %s, skip", err.Error())
			continue
		}

		// Parse alert item to notification payload
		payload := map[string]interface{}{}
		body, err := json.Marshal(*item)
		if err != nil {
			r.Logger.Errorf("Marshal alert item error %s, continue", err.Error())
			continue
		}
		if err := json.Unmarshal(body, &payload); err != nil {
			r.Logger.Errorf("Unmarshal payload error %s, continue", err.Error())
			continue
		}

		// Send notification with notification store
		n := &notification.Store{
			BaseStore: store.BaseStore{
				Logger:  r.Logger,
				Cluster: r.Cluster,
			},
			Message: payload,
		}
		if err := n.SendNotification(notificationList); err != nil {
			r.Logger.Errorf("Send notification error %s, continue", err.Error())
			continue
		}
	}

	c.JSON(http.StatusNoContent, nil)
}

func getRouterHelper(c *gin.Context) (*router.Store, error) {
	r := &router.Store{
		BaseStore: store.BaseStore{
			Logger: common.GetGinLogger(c),
		},
		Request: &model.AlertRouter{},
	}

	cluster, err := GetGlobalClusterWithSystemToken()
	if err != nil {
		return r, err
	}
	r.Cluster = cluster
	r.Logger.Infof("Cluster kubernetes config: %s/%s %s", cluster.Name, cluster.UUID, cluster.KubernetesConfig.Endpoint)
	r.Logger.Infof("Cluster prometheus config: %s/%s %s", cluster.Name, cluster.UUID, cluster.PrometheusConfig.Endpoint)
	return r, nil
}
