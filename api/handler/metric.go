package handler

import (
	"net/http"
	"reflect"
	"strings"

	"morgans/common"
	"morgans/infra/prometheus"
	"morgans/model"
	"morgans/store"
	"morgans/store/metric"

	"github.com/gin-gonic/gin"
)

func PrometheusProxy(c *gin.Context) {
	// Get base store
	m, err := getBaseStore(c)
	if err != nil {
		m.Logger.Errorf("Get base store error, %s", err.Error())
		c.JSON(http.StatusOK, []string{})
		return
	}

	// Parse api params
	index := strings.Index(c.Request.URL.Path, "prometheus")
	url := "/api/v1" + c.Request.URL.Path[index+10:]
	params := map[string]string{}
	for k, v := range c.Request.URL.Query() {
		if len(v) > 0 {
			params[k] = v[0]
		}
	}

	// Forward api to prometheus
	client := prometheus.NewClient(*m.Cluster.PrometheusConfig, m.Logger)
	result, code, err := client.Proxy(c.Request.Method, url, params, nil)
	if !reflect.ValueOf(err).IsNil() {
		HandleError(err.(*prometheus.APIError).Wrap(), c, m.Logger)
		return
	}
	c.Data(code, "application/json; charset=utf-8", []byte(result))
}

func ListIndicator(c *gin.Context) {
	m, err := getBaseStore(c)
	if err != nil {
		m.Logger.Errorf("Get base store error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, m.Logger)
		return
	}
	var result []*model.IndicatorItem
	for _, v := range model.GetIndicatorMap(m.Cluster) {
		result = append(result, v)
	}
	c.JSON(http.StatusOK, result)
}

func Query(c *gin.Context) {
	// Get query store
	m, err := getQueryStore(c)
	if err != nil {
		m.Logger.Errorf("Get query store error, %s", err.Error())
		c.JSON(http.StatusOK, []string{})
		return
	}

	// Bind http body to query request
	if err := c.Bind(m.Request); err != nil {
		m.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, m.Logger)
		return
	}

	// Validate query request
	m.Logger.Infof("Validate body: %+v", *m.Request)
	if err := m.Validate(); err != nil {
		m.Logger.Errorf("Validate body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, m.Logger)
		return
	}

	// Execute query
	result, err := m.Query()
	if err != nil {
		m.Logger.Errorf("Query error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}
	m.Logger.Debugfl(2, "Query result: %+v", result)
	c.JSON(http.StatusOK, result)
}

func QueryRange(c *gin.Context) {
	// Get query store
	m, err := getQueryRangeStore(c)
	if err != nil {
		m.Logger.Errorf("Get query range store error, %s", err.Error())
		c.JSON(http.StatusOK, []string{})
		return
	}

	// Bind http body to query request
	if err := c.Bind(m.Request); err != nil {
		m.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, m.Logger)
		return
	}

	// Validate request body
	m.Logger.Infof("Validate body: %+v", *m.Request)
	if err := m.Validate(); err != nil {
		m.Logger.Errorf("Validate body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, m.Logger)
		return
	}

	// Execute query
	result, err := m.Query()
	if err != nil {
		m.Logger.Errorf("Query error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}
	m.Logger.Debugfl(2, "Query result: %v", result)
	c.JSON(http.StatusOK, result)
}

func getBaseStore(c *gin.Context) (*store.BaseStore, error) {
	m := &store.BaseStore{
		Logger: common.GetGinLogger(c),
	}
	cluster, err := GetClusterWithPrometheusInstance(c)
	if err != nil {
		m.Logger.Errorf("Get cluster config error, %s", err.Error())
		return m, err
	}
	m.Cluster = cluster
	m.Logger.Infof("Cluster config: %s/%s %s", cluster.Name, cluster.UUID, cluster.PrometheusConfig.Endpoint)
	return m, nil
}

func getQueryStore(c *gin.Context) (*metric.QueryStore, error) {
	m := &metric.QueryStore{
		BaseStore: store.BaseStore{
			Logger: common.GetGinLogger(c),
		},
		Request: &model.QueryRequest{},
	}
	cluster, err := GetClusterWithPrometheusInstance(c)
	if err != nil {
		m.Logger.Errorf("Get cluster config error, %s", err.Error())
		return m, err
	}
	m.Cluster = cluster
	m.Logger.Infof("Cluster config: %s/%s %s", cluster.Name, cluster.UUID, cluster.PrometheusConfig.Endpoint)
	return m, nil
}

func getQueryRangeStore(c *gin.Context) (*metric.QueryRangeStore, error) {
	m := &metric.QueryRangeStore{
		BaseStore: store.BaseStore{
			Logger: common.GetGinLogger(c),
		},
		Request: &model.QueryRangeRequest{},
	}
	cluster, err := GetClusterWithPrometheusInstance(c)
	if err != nil {
		m.Logger.Errorf("Get cluster config error, %s", err.Error())
		return m, err
	}
	m.Cluster = cluster
	m.Logger.Infof("Cluster config: %s/%s %s", cluster.Name, cluster.UUID, cluster.PrometheusConfig.Endpoint)
	return m, nil
}
