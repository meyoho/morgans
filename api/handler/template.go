package handler

import (
	"net/http"
	"strings"

	"morgans/common"
	"morgans/config"
	"morgans/model"
	"morgans/store"
	"morgans/store/alert"
	"morgans/store/template"

	"github.com/gin-gonic/gin"
)

func CreateAlertTemplate(c *gin.Context) {
	s := getAlertTemplateStore(c)

	// Bind http body to template request
	if err := c.Bind(s.Request); err != nil {
		s.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Translate template request body
	s.Request.Creator = ParseCreator(s.Request.Creator, c)
	s.TranslateForCreate()

	// Validate template request body
	if err := s.Validate(); err != nil {
		s.Logger.Errorf("Validate body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Create template
	if err := s.Create(); err != nil {
		s.Logger.Errorf("Create alert template error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("Create %s/%s succeeded, %+v", s.Request.Organization, s.Request.Name, s.Response)
	c.JSON(http.StatusOK, s.Response)
}

func GetAlertTemplate(c *gin.Context) {
	s := getAlertTemplateStore(c)

	// Retrieve template
	if err := s.Retrieve(); err != nil {
		s.Logger.Errorf("Get alert template error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("Get %s/%s succeeded, %+v", s.Request.Organization, s.Request.Name, s.Response)
	c.JSON(http.StatusOK, s.Response)
}

func ListAlertTemplates(c *gin.Context) {
	s := getAlertTemplateStore(c)

	// List template
	response, err := s.List()
	if err != nil {
		s.Logger.Errorf("List error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Transform templates list
	body, err := TransformItemArrayToResponseBody(response, c)
	if err != nil {
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("List %s templates succeeded, %+v", s.Request.Organization, response)
	c.JSON(http.StatusOK, body)
}

func UpdateAlertTemplate(c *gin.Context) {
	s := getAlertTemplateStore(c)

	// Bind http body to template request
	if err := c.Bind(s.Request); err != nil {
		s.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Translate template request body
	s.Request.Name = c.Param("templateName")
	s.Request.Creator = ParseCreator(s.Request.Creator, c)
	s.Request.Organization = c.Param("organization")
	s.TranslateForUpdate()

	// Validate request body
	if err := s.Validate(); err != nil {
		s.Logger.Errorf("Validate body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Update template
	if err := s.Update(); err != nil {
		s.Logger.Errorf("Update alert template error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("Update %s/%s succeeded, %+v", s.Response.Organization, s.Response.Name, s.Response)
	c.JSON(http.StatusOK, s.Response)
}

func DeleteAlertTemplate(c *gin.Context) {
	s := getAlertTemplateStore(c)

	// Delete template
	if err := s.Delete(); err != nil {
		s.Logger.Errorf("Delete alert template error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("Delete %s/%s succeeded", s.Response.Organization, s.Response.Name)
	c.JSON(http.StatusNoContent, "")
}

func ApplyAlertTemplate(c *gin.Context) {
	s := getAlertTemplateStore(c)

	// Get alert template parser
	p, err := s.GetParser()
	if err != nil {
		s.Logger.Errorf("Get alert template parser error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Bind http body to parser request and translate parser request
	if err := c.Bind(p.Request); err != nil {
		p.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}
	p.Request.Creator = ParseCreator(p.Request.Creator, c)

	// Get alert store for this parser and generate desired alert resource by parsing template
	ps, err := getAlertStoreForParser(c, p)
	if err != nil {
		s.Logger.Errorf("Get cluster config error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	var resource *model.AlertResource
	for _, request := range p.Parse() {
		ps.Request = request
		ps.TranslateForCreate()
		ps.Logger.Infof("Validate body: %+v", *s.Request)
		if err := ps.Validate(false); err != nil {
			ps.Logger.Errorf("Validate body error, %s", err.Error())
			HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
			return
		}
		if resource, err = ps.GenerateDesiredAlertResource(resource); err != nil {
			s.Logger.Errorf("Get desired alert resource error, %s", err.Error())
			HandleError(err, c, s.Logger)
			return
		}
	}

	// Sync alert resource to kubernetes and generate alert list
	resource, err = ps.ApplyAlertResource(resource)
	if err != nil {
		ps.Logger.Errorf("Apply alert resource error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	alerts, err := ps.AlertResourceToAlerts(resource, "")
	if err != nil {
		ps.Logger.Errorf("Alert resource to alerts error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("Apply %s/%s succeeded, %+v", s.Response.Organization, s.Response.Name, p.Request)
	c.JSON(http.StatusCreated, p.Filter(alerts))
}

func getAlertTemplateStore(c *gin.Context) *template.Store {
	s := &template.Store{
		Logger: common.GetGinLogger(c),
		Request: &model.AlertTemplateRequest{
			Name:         c.Param("templateName"),
			Organization: c.Param("organization"),
		},
		Response: &model.AlertTemplateResponse{},
	}

	isDBStorage := false
	switch strings.ToUpper(c.Query("backend")) {
	case "DB":
		isDBStorage = true
	case "CRD":
		isDBStorage = false
	default:
		if !config.GlobalConfig.Morgans.CRDEnabled {
			isDBStorage = true
		}
	}
	if isDBStorage {
		s.Storage = template.NewDBStorageManager(s.Logger)
		return s
	}

	cluster, err := GetGlobalClusterWithUserToken(c)
	if err != nil {
		s.Logger.Panicf("Get global cluster error, %s", err.Error())
	}
	s.Storage = template.NewETCDStorageManager(cluster, s.Logger)
	return s
}

func getAlertStoreForParser(c *gin.Context, p *template.Parser) (*alert.Store, error) {
	s := &alert.Store{
		BaseStore: store.BaseStore{Logger: p.Logger},
		Request:   &model.AlertRequest{},
		Response:  &model.AlertResponse{},
	}
	c.Params = append(c.Params, gin.Param{Key: "cluster", Value: p.Request.Cluster.Name})
	cluster, err := GetClusterWithUserToken(c)
	if err != nil {
		return s, err
	}
	s.Cluster = cluster
	s.Logger.Infof("Cluster kubernetes config: %s/%s %s", cluster.Name, cluster.UUID, cluster.KubernetesConfig.Endpoint)
	s.Logger.Infof("Cluster prometheus config: %s/%s %s", cluster.Name, cluster.UUID, cluster.PrometheusConfig.Endpoint)
	return s, nil
}
