package handler

import (
	"net/http"

	"morgans/common"
	"morgans/model"
	"morgans/store"
	"morgans/store/notification"

	"github.com/gin-gonic/gin"
)

func CreateNotification(c *gin.Context) {
	// Get notification store
	s, err := getNotificationStore(c)
	if err != nil {
		s.Logger.Errorf("Get notification store error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Bind http body to notification request
	if err := c.Bind(s.Request); err != nil {
		s.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Translate notification request
	s.Request.Project = c.Query("project_name")
	s.Request.Namespace = c.Param("namespace")
	s.TranslateForCreate()

	// Validate request body
	s.Logger.Infof("Validate body: %+v", *s.Request)
	if err := s.Validate(); err != nil {
		s.Logger.Errorf("Validate body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Create notification
	response, err := s.CreateNotification()
	if err != nil {
		s.Logger.Errorf("Create error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("Create response: %+v", response)
	c.JSON(http.StatusOK, response)
}

func UpdateNotification(c *gin.Context) {
	// Get notification store
	s, err := getNotificationStore(c)
	if err != nil {
		s.Logger.Errorf("Get notification store error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Bind http body to request
	if err := c.Bind(s.Request); err != nil {
		s.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Translate notification request
	s.Request.Name = c.Param("notificationName")
	s.Request.Namespace = c.Param("namespace")
	s.TranslateForUpdate()

	// Validate notification request
	s.Logger.Infof("Validate body: %+v", *s.Request)
	if err := s.Validate(); err != nil {
		s.Logger.Errorf("Validate body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Update notification
	response, err := s.UpdateNotification()
	if err != nil {
		s.Logger.Errorf("Update error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("Update response: %+v", response)
	c.JSON(http.StatusOK, response)
}

func GetNotification(c *gin.Context) {
	// Get notification store
	s, err := getNotificationStore(c)
	if err != nil {
		s.Logger.Errorf("Get notification store error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Translate notification request
	s.Request.Name = c.Param("notificationName")
	s.Request.Namespace = c.Param("namespace")
	s.Translate()

	// Get notification
	response, err := s.GetNotification(s.Request.Namespace, s.Request.Name)
	if err != nil {
		s.Logger.Errorf("Get notification error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Debugf("Get response: %+v", response)
	c.JSON(http.StatusOK, response)
}

func DeleteNotification(c *gin.Context) {
	// Get notification store
	s, err := getNotificationStore(c)
	if err != nil {
		s.Logger.Errorf("Get notification store error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Translate notification request
	s.Request.Name = c.Param("notificationName")
	s.Request.Namespace = c.Param("namespace")
	s.Translate()

	// Delete notification
	if err := s.DeleteNotification(s.Request.Namespace, s.Request.Name); err != nil {
		s.Logger.Errorf("Delete error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	c.JSON(http.StatusNoContent, nil)
}

func ListNotifications(c *gin.Context) {
	// Get notification store
	s, err := getNotificationStore(c)
	if err != nil {
		s.Logger.Errorf("Get notification store error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Translate notification request
	s.Request.Namespace = c.Param("namespace")
	s.Translate()

	// List notifications
	response, err := s.ListNotifications()
	if err != nil {
		s.Logger.Errorf("List resource error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Debugfl(2, "List response: %+v", response)
	c.JSON(http.StatusOK, response)
}

func SendNotification(c *gin.Context) {
	// Get notification store
	s, err := getNotificationStore(c)
	if err != nil {
		s.Logger.Errorf("Get notification store error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Bind http body to request
	if err := c.Bind(s.Message); err != nil {
		s.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Translate notification request
	s.Request.Name = c.Param("notificationName")
	s.Request.Namespace = c.Param("namespace")
	s.Translate()

	// Get notification
	response, err := s.GetNotification(s.Request.Namespace, s.Request.Name)
	if err != nil {
		s.Logger.Errorf("Get notification resource error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Send notification
	s.Logger.Infof("Send notification to %+v with body: %+v", response.Subscriptions, s.Message)
	notificationItem := model.Notification{Name: response.Name, Namespace: response.Namespace}
	if err = s.SendNotification([]model.Notification{notificationItem}); err != nil {
		s.Logger.Errorf("Send notification error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	c.JSON(http.StatusNoContent, nil)
}

func getNotificationStore(c *gin.Context) (*notification.Store, error) {
	s := &notification.Store{
		BaseStore: store.BaseStore{
			Logger: common.NewNoticationLogger(map[string]string{"request_id": c.GetHeader(common.RequestIDHeader)}),
		},
		Request:  &model.NotificationRequest{},
		Response: &model.NotificationResponse{},
		Message:  map[string]interface{}{},
	}

	cluster, err := GetGlobalClusterWithUserToken(c)
	if err != nil {
		s.Logger.Errorf("Get cluster config error: %s", err.Error())
		return s, err
	}

	s.Cluster = cluster
	s.Logger.Infof("Cluster kubernetes config: %s/%s %s", cluster.Name, cluster.UUID, cluster.KubernetesConfig.Endpoint)
	return s, nil
}
