package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"

	"morgans/common"
	"morgans/config"
	"morgans/control/log"
	"morgans/model"
)

func Search(c *gin.Context) {
	dispatchLog(c, "Search")
}

func Aggregation(c *gin.Context) {
	dispatchLog(c, "Aggregation")
}

func Types(c *gin.Context) {
	dispatchLog(c, "Types")
}

func Context(c *gin.Context) {
	dispatchLog(c, "Context")
}

func dispatchLog(c *gin.Context, methodName string) {
	logger := common.GetGinLogger(c)

	valid, err := canI(c, "get", "logs", "aiops.alauda.io", config.GlobalConfig.Morgans.Namespace)
	if err != nil {
		logger.Errorf("Failed to validate permission with err: %s", err.Error())
		HandleError(err, c, logger)
		return
	}
	if !valid {
		logger.Infof("User has no permission to get logs")
		HandleError(apiErrors.NewUnauthorized("Invalid Token"), c, logger)
		return
	}

	controller, err := getLogQueryController(c, logger)
	if err != nil {
		logger.Errorf("Failed to get log controller, %s", err.Error())
		HandleError(err, c, logger)
		return
	}

	var response interface{}

	switch methodName {
	case "Search":
		response, err = controller.Search()
	case "Aggregation":
		response, err = controller.Aggregation()
	case "Types":
		response, err = controller.Types()
	case "Context":
		response, err = controller.Context()
	}

	if err != nil {
		logger.Errorf("Failed to get result from log controller, %s", err.Error())
		HandleError(err, c, logger)
		return
	}

	c.JSON(http.StatusOK, response)
}

func getLogQueryController(c *gin.Context, logger *common.Logger) (*log.Controller, error) {
	req, err := genLogRequest(c)
	if err != nil {
		return nil, err
	}

	s := &log.Controller{
		Request: req,
		Logger:  logger,
	}

	return s, nil
}

// genLogRequest construct a model.LogRequest from gin.Context
func genLogRequest(c *gin.Context) (*model.LogRequest, error) {
	req := &model.LogRequest{
		RootAccount:   c.Param("namespace"),
		RegionNames:   c.Query("clusters"),
		ProjectNames:  c.Query("projects"),
		Nodes:         c.Query("nodes"),
		K8sNamespaces: c.Query("namespaces"),
		AppNames:      c.Query("applications"),
		PodNames:      c.Query("pods"),
		Paths:         c.Query("paths"),
		Sources:       c.Query("sources"),
		Components:    c.Query("components"),
		QueryString:   c.Query("query_string"),
		ContainerID8s: c.Query("instances"),
		Size:          model.DefaultLogPageSize,
		Page:          1,
		LogId:         c.Query("log_id"),
		LogIndex:      c.Query("log_index"),
		LogType:       c.Query("log_type"),
	}

	if c.Query("size") != "" {
		size, err := strconv.ParseInt(c.Query("size"), 10, 0)
		if err != nil {
			return nil, err
		}
		req.Size = int(size)
	}
	if c.Query("pageno") != "" {
		page, err := strconv.ParseInt(c.Query("pageno"), 10, 0)
		if err != nil {
			return nil, err
		}
		req.Page = int(page)
	}
	if c.Query("start_time") != "" {
		startTime, err := strconv.ParseFloat(c.Query("start_time"), 64)
		if err != nil {
			return nil, err
		}
		req.StartTime = startTime
	}
	if c.Query("end_time") != "" {
		endTime, err := strconv.ParseFloat(c.Query("end_time"), 64)
		if err != nil {
			return nil, err
		}
		req.EndTime = endTime
	}

	if c.Query("direction") != "" {
		if c.Query("direction") != "before" && c.Query("direction") != "later" {
			return nil, apiErrors.NewBadRequest("Invalid param direction specified, only support 'before' and 'later'")
		}
		req.Direction = c.Query("direction")
	}

	return req, nil
}
