package handler

import (
	"bytes"
	"compress/zlib"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"

	"morgans/common"
	"morgans/config"
	"morgans/control/callback"
)

func Callback(c *gin.Context) {
	logger := common.GetGinLogger(c)

	dataSource := c.Param("dataSource")
	switch dataSource {
	case "logs":
		dispatchCallback(c, config.GlobalConfig.Kafka.Log.Topic, true)
	case "events":
		dispatchCallback(c, config.GlobalConfig.Kafka.Event.Topic, config.GlobalConfig.Kafka.Event.Enable)
	case "audits":
		dispatchCallback(c, config.GlobalConfig.Kafka.Audit.Topic, true)
	default:
		err := apiErrors.NewBadRequest("Invalid callback types")
		logger.Errorf("Unsupported callback types, %s", dataSource)
		HandleError(err, c, logger)
		return
	}
}

func dispatchCallback(c *gin.Context, topic string, usingKafka bool) {
	logger := common.GetGinLogger(c)

	controller, err := getCallbackController(c, logger, topic)
	if err != nil {
		logger.Errorf("Failed to get controller, %s", err.Error())
		HandleError(err, c, logger)
		return
	}

	_, err = controller.SaveToStorage(usingKafka)
	if err != nil {
		logger.Errorf("Failed to get controller, %s", err.Error())
		HandleError(err, c, logger)
		return
	}

	c.Status(http.StatusNoContent)
}

func getCallbackController(c *gin.Context, logger *common.Logger, topic string) (*callback.Controller, error) {
	records, err := parseCallbackData(c, topic)
	if err != nil {
		return nil, err
	}

	s := &callback.Controller{
		Records: records,
		Topic:   topic,
		Logger:  logger,
	}

	return s, nil

}

func parseCallbackData(c *gin.Context, topic string) (records []map[string]interface{}, err error) {
	data, err := c.GetRawData()
	if err != nil || len(data) == 0 {
		return
	}

	if topic == config.GlobalConfig.Kafka.Log.Topic {
		// log
		token := getBearerToken(c)
		if token == "" {
			err = apiErrors.NewUnauthorized("Token required")
			return
		}

		// valid, err := canI(c, "create", "logs", "aiops.alauda.io/v1beta1")
		// if err != nil {
		// 	return records, err
		// }
		// if !valid {
		// 	err = apiErrors.NewUnauthorized("Invalid Token")
		// 	return records, err
		// }

		record := append(data, []byte(token)...)
		key := fmt.Sprintf("%.3f", float64(time.Now().UnixNano())/1000000000)
		records = append(records, map[string]interface{}{key: record})
	} else if topic == config.GlobalConfig.Kafka.Audit.Topic {
		r := bytes.NewBuffer(data)
		reader, err := zlib.NewReader(r)
		defer reader.Close()
		if err != nil {
			return records, err
		}

		var out bytes.Buffer
		io.Copy(&out, reader)
		audits := out.Bytes()

		key := fmt.Sprintf("%.3f", float64(time.Now().UnixNano())/1000000000)
		records = append(records, map[string]interface{}{key: audits})
	} else if topic == config.GlobalConfig.Kafka.Event.Topic {
		// events
		key := fmt.Sprintf("%.3f", float64(time.Now().UnixNano())/1000000000)
		records = append(records, map[string]interface{}{key: data})
	}

	return
}
