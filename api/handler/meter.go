package handler

import (
	"net/http"
	"os"
	"path/filepath"

	"morgans/common"
	"morgans/infra/elasticsearch"
	"morgans/model"
	"morgans/pkg/utils"
	"morgans/store"
	"morgans/store/meter"

	"github.com/gin-gonic/gin"
)

func MeterTopN(c *gin.Context) {
	// Get meter store with cluster resource and es client
	m, err := getMeterStore(c)
	if err != nil {
		m.Logger.Errorf("Get meter store error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Parse http query and translate to request body
	if err = m.Translate(c); err != nil {
		m.Logger.Errorf("Translate meter request error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Validate meter request and feature
	if err = m.Validate(false); err != nil {
		m.Logger.Errorf("Validate meter request error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Search elasticsearch and return topn response
	response, err := m.MeterTopN()
	if err != nil {
		m.Logger.Errorf("Get meter topn error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}
	m.Logger.Debugf("Get response of meter topn, %+v", response)

	c.JSON(http.StatusOK, response)
}

func MeterTotal(c *gin.Context) {
	m, err := getMeterStore(c)
	if err != nil {
		m.Logger.Errorf("Get meter store error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Parse http query and translate to request body
	if err = m.Translate(c); err != nil {
		m.Logger.Errorf("Translate meter request error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Validate meter request and feature
	if err = m.Validate(false); err != nil {
		m.Logger.Errorf("Validate meter request error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Search elasticsearch and return total response
	response, err := m.MeterTotal()
	if err != nil {
		m.Logger.Errorf("Get meter total error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}
	m.Logger.Debugf("Get response of meter total, %+v", response)

	c.JSON(http.StatusOK, response)
}

func MeterSummary(c *gin.Context) {
	m, err := getMeterStore(c)
	if err != nil {
		m.Logger.Errorf("Get meter store error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Parse http query and translate to request body
	if err = m.Translate(c); err != nil {
		m.Logger.Errorf("Translate meter request error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Validate meter request and feature
	if err = m.Validate(false); err != nil {
		m.Logger.Errorf("Validate meter request error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Search elasticsearch and return summary response
	response, count, err := m.MeterSummary()
	if err != nil {
		m.Logger.Errorf("Get meter summary error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}
	m.Logger.Debugf("Get response of meter summary, count %d, %+v", count, response)
	body, err := utils.GeneratePages(response, count, m.Request.PageSize, m.Request.Page)
	if err != nil {
		HandleError(err, c, m.Logger)
		return
	}

	c.JSON(http.StatusOK, body)
}

func MeterQuery(c *gin.Context) {
	m, err := getMeterStore(c)
	if err != nil {
		m.Logger.Errorf("Get meter store error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Parse http query and translate to request body
	if err = m.Translate(c); err != nil {
		m.Logger.Errorf("Translate meter request error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Validate meter request and feature
	if err = m.Validate(false); err != nil {
		m.Logger.Errorf("Validate meter request error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Search elasticsearch and return summary response
	response, count, err := m.MeterQuery()
	if err != nil {
		m.Logger.Errorf("Get meter query error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}
	m.Logger.Debugf("Get response of meter query, count %d, %+v", count, response)
	body, err := utils.GeneratePages(response, count, m.Request.PageSize, m.Request.Page)
	if err != nil {
		HandleError(err, c, m.Logger)
		return
	}

	c.JSON(http.StatusOK, body)
}

func MeterReportUpload(c *gin.Context) {
	m, err := getMeterStore(c)
	if err != nil {
		m.Logger.Errorf("Get meter store error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	if err = m.Validate(true); err != nil {
		m.Logger.Errorf("Validate meter request error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	if err = m.MeterReportUpload(c.Request); err != nil {
		m.Logger.Errorf("Upload meter report error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}
	c.JSON(http.StatusNoContent, nil)
}

func MeterReportDownload(c *gin.Context) {
	m, err := getMeterStore(c)
	if err != nil {
		m.Logger.Errorf("Get meter store error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	if err = m.Validate(true); err != nil {
		m.Logger.Errorf("Validate meter request error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	fileName := c.Param("reportName")
	m.Logger.Infof("Download meter report %s", fileName)
	filePath := filepath.Join(model.ReportPath, fileName)
	if _, err := os.Stat(filePath); err != nil {
		if os.IsNotExist(err) == true {
			HandleError(common.BuildNotFoundError(err.Error()), c, m.Logger)
		} else {
			HandleError(err, c, m.Logger)
		}
		return
	}
	m.Logger.Infof("Locate report file %s in %s", fileName, model.ReportPath)

	http.ServeFile(c.Writer, c.Request, filePath)
	m.Logger.Infof("Download report file %s successfully", fileName, filePath)
}

func getMeterStore(c *gin.Context) (*meter.MeterStore, error) {
	m := &meter.MeterStore{
		BaseStore: store.BaseStore{
			Logger: common.NewMeterLogger(map[string]string{"request_id": c.GetHeader(common.RequestIDHeader)}),
		},
		Request: &model.MeterRequest{},
		Client:  &elasticsearch.Client{},
	}

	cluster, err := GetGlobalClusterWithUserToken(c)
	if err != nil {
		m.Logger.Errorf("Get cluster config error, %s", err.Error())
		return m, err
	}
	m.Cluster = cluster
	m.Logger.Infof("Cluster config: %s/%s %s", cluster.Name, cluster.UUID, cluster.PrometheusConfig.Endpoint)

	client, err := elasticsearch.NewClient(m.Logger)
	if err != nil {
		m.Logger.Errorf("Get elasticsearch client error, %s", err.Error())
		return nil, err
	}
	m.Client = client

	return m, nil
}
