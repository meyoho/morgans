package handler

import (
	"net/http"

	"morgans/common"
	"morgans/model"
	"morgans/store"
	"morgans/store/alert"

	"github.com/gin-gonic/gin"
)

func GetConfig(c *gin.Context) {
	result := model.AlertConfig{
		SystemLabels: []string{
			model.LabelAlertName,
			model.LabelObjectName,
			model.LabelObjectKind,
			model.LabelNamespace,
			model.LabelCreator,
			model.LabelProject,
			model.LabelCluster,
			model.LabelIndicator,
			model.LabelCompare,
			model.LabelThreshold,
			model.LabelUnit,
			model.LabelSeverity,
			model.LabelApplication,
		},
		PrometheusFunctions: model.PrometheusFunctions,
		AlertResourceLabels: []string{
			model.AlertProjectLabelKey,
			model.AlertClusterLabelKey,
			model.AlertObjectKindLabelKey,
			model.AlertObjectNameLabelKey,
			model.AlertObjectNamespaceLabelKey,
			model.AlertObjectApplicationLabelKey,
		},
	}

	c.JSON(http.StatusOK, result)
}

func CreateAlert(c *gin.Context) {
	// Get alert store with cluster resource
	s, err := getAlertStore(c)
	if err != nil {
		s.Logger.Errorf("Get cluster config error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Bind http body to request body
	if err := c.Bind(s.Request); err != nil {
		s.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Translate request body
	s.Request.Creator = ParseCreator(s.Request.Creator, c)
	s.TranslateForCreate()

	// Validate request body
	s.Logger.Infof("Validate body: %+v", *s.Request)
	if err := s.Validate(common.ParseFlag(c.Query("force"))); err != nil {
		s.Logger.Errorf("Validate body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Create alert
	result, err := s.CreateAlert()
	if err != nil {
		s.Logger.Errorf("Create alert error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Translate alert response
	response, err := s.AlertResourceToAlert(result, s.Response.GroupName, s.Response.Name)
	if err != nil {
		s.Logger.Errorf("Alert resource to alert error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("Create response: %v", response)
	c.JSON(http.StatusOK, response)
}

func UpdateAlert(c *gin.Context) {
	// Get alert store with cluster resource
	s, err := getAlertStore(c)
	if err != nil {
		s.Logger.Errorf("Get cluster config error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Bind http body to request body
	if err := c.Bind(s.Request); err != nil {
		s.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Translate request body
	s.Request.Name = c.Param("alertName")
	s.Request.Namespace = c.Param("namespace")
	s.Request.ResourceName = c.Param("resourceName")
	s.Request.GroupName = c.Param("groupName")
	s.Request.Creator = ParseCreator(s.Request.Creator, c)
	s.TranslateForUpdate()

	// Validate request body
	s.Logger.Infof("Validate body: %+v", *s.Request)
	if err := s.Validate(common.ParseFlag(c.Query("force"))); err != nil {
		s.Logger.Errorf("Validate body error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Update alert
	result, err := s.UpdateAlert()
	if err != nil {
		s.Logger.Errorf("Update alert error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Translate alert response
	response, err := s.AlertResourceToAlert(result, s.Response.GroupName, s.Response.Name)
	if err != nil {
		s.Logger.Errorf("Alert resource to alert error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("Update response: %v", response)
	c.JSON(http.StatusOK, response)
}

func GetAlert(c *gin.Context) {
	// Get alert store with cluster resource
	s, err := getAlertStore(c)
	if err != nil {
		s.Logger.Errorf("Get cluster config error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Get alert
	resource, err := s.GetAlert(s.Request.ResourceName)
	if err != nil {
		s.Logger.Errorf("Get alert error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Translate alert response
	response, err := s.AlertResourceToAlert(resource, c.Param("groupName"), c.Param("alertName"))
	if err != nil {
		s.Logger.Errorf("Alert resource to alert error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Debugf("Get response: %v", response)
	c.JSON(http.StatusOK, response)
}

func DeleteAlert(c *gin.Context) {
	// Get alert store with cluster resource
	s, err := getAlertStore(c)
	if err != nil {
		s.Logger.Errorf("Get cluster config error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Validate request body
	if err := s.ValidateForDelete(c); err != nil {
		s.Logger.Errorf("Validate query error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// Delete alert
	if err := s.DeleteAlert(c.Param("resourceName"), c.Param("groupName"), c.Param("alertName"), c.Query("labelSelector")); err != nil {
		s.Logger.Errorf("Delete error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	c.JSON(http.StatusNoContent, nil)
}

func ListAlerts(c *gin.Context) {
	// Get alert store with cluster resource
	s, err := getAlertStore(c)
	if err != nil {
		s.Logger.Errorf("Get cluster config error, %s", err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, s.Logger)
		return
	}

	// List alerts
	result, err := s.ListAlerts(c.Query("labelSelector"))
	if err != nil {
		s.Logger.Errorf("List resource error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Translate alerts response
	response, err := s.AlertResourcesToAlerts(result)
	if err != nil {
		s.Logger.Errorf("Transition error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Debugfl(2, "List response: %v", response)
	body, err := TransformItemArrayToResponseBody(response, c)
	if err != nil {
		HandleError(err, c, s.Logger)
		return
	}
	c.JSON(http.StatusOK, body)
}

func getAlertStore(c *gin.Context) (*alert.Store, error) {
	s := &alert.Store{
		BaseStore: store.BaseStore{
			Logger: common.GetGinLogger(c),
		},
		Request: &model.AlertRequest{
			AlertTemplate: model.AlertTemplate{Name: c.Param("alertName")},
			Namespace:     c.Param("namespace"),
			ResourceName:  c.Param("resourceName"),
			GroupName:     c.Param("groupName"),
		},
		Response: &model.AlertResponse{},
	}

	cluster, err := GetClusterWithUserToken(c)
	if err != nil {
		return s, err
	}
	s.Cluster = cluster
	s.Logger.Infof("Cluster kubernetes config: %s/%s %s", cluster.Name, cluster.UUID, cluster.KubernetesConfig.Endpoint)
	s.Logger.Infof("Cluster prometheus config: %s/%s %s", cluster.Name, cluster.UUID, cluster.PrometheusConfig.Endpoint)
	return s, nil
}
