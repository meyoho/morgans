package handler

import (
	"morgans/common"

	"github.com/gin-gonic/gin"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

func HandleError(err error, c *gin.Context, l *common.Logger) {
	switch t := err.(type) {
	case *common.ErrorWrapper:
		l.Infof("Error code %s, message %s, status %d", t.Code, t.Message, t.Status)
		c.JSON(t.Status, t.ToK8SErrors())
	case apiErrors.APIStatus:
		var w *common.ErrorWrapper
		if apiErrors.IsNotFound(err) {
			w = common.BuildNotFoundError(err.Error()).(*common.ErrorWrapper)
		}
		if apiErrors.IsAlreadyExists(err) {
			w = common.BuildAlreadyExistsError(err.Error()).(*common.ErrorWrapper)
		}
		if apiErrors.IsUnauthorized(err) {
			w = common.BuildUnauthorizedError(err.Error()).(*common.ErrorWrapper)
		}
		if apiErrors.IsBadRequest(err) {
			w = common.BuildBadRequestError(err.Error()).(*common.ErrorWrapper)
		}
		if apiErrors.IsForbidden(err) {
			w = common.BuildForbiddenError(err.Error()).(*common.ErrorWrapper)
		}
		l.Infof("Error code %s, message %s, status %d", w.Code, w.Message, w.Status)
		c.JSON(w.Status, w.ToK8SErrors())
	default:
		w := common.BuildUnknownError(err.Error()).(*common.ErrorWrapper)
		l.Infof("Error code %s, message %s, status %d", w.Code, w.Message, w.Status)
		c.JSON(w.Status, w.ToK8SErrors())
	}
}
