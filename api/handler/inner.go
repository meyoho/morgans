package handler

import (
	"net/http"
	"strconv"

	"morgans/cache"
	"morgans/common"
	"morgans/model"
	"morgans/store"
	"morgans/store/inner"

	"github.com/gin-gonic/gin"
)

func ListRegion(c *gin.Context) {
	c.JSON(http.StatusOK, cache.ListClusters())
}

func GetVersion(c *gin.Context) {
	// Get migration store
	m, err := getMigrationStore(c)
	if err != nil {
		m.Logger.Errorf("Get migration store error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Get version
	version, err := m.GetVersion()
	if err != nil {
		m.Logger.Errorf("Get version error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}
	c.JSON(http.StatusOK, map[string]interface{}{"version": version})
}

func UpdateVersion(c *gin.Context) {
	// Get migration store
	m, err := getMigrationStore(c)
	if err != nil {
		m.Logger.Errorf("Get migration store error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}

	// Parse version
	v := c.Query("version")
	version, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		m.Logger.Error("Parse version error, %v, %s", v, err.Error())
		HandleError(common.BuildBadRequestError(err.Error()), c, m.Logger)
		return
	}

	// Update version
	if err = m.UpdateVersion(version); err != nil {
		m.Logger.Errorf("Update version error, %s", err.Error())
		HandleError(err, c, m.Logger)
		return
	}
	c.JSON(http.StatusNoContent, nil)
}

func CreateResource(c *gin.Context) {
	// Get kubernetes resource store
	s, err := getKubernetesResourceStore(c)
	if err != nil {
		s.Logger.Errorf("Get cluster config error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Bind http body to request
	if err := c.Bind(s.Request); err != nil {
		s.Logger.Errorf("Bind body error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Infof("Get body of request: %+v", s.Request)

	// Create resource
	result, err := s.CreateResource(common.ParseFlag(c.Query("apply")))
	if err != nil {
		s.Logger.Errorf("Create resource error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	c.JSON(http.StatusOK, result)
}

func GetResource(c *gin.Context) {
	// Get kubernetes resource store
	s, err := getKubernetesResourceStore(c)
	if err != nil {
		s.Logger.Errorf("Get cluster config error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// Get resource
	resource, err := s.GetResource(c.Param("namespace"), c.Param("resourceType"), c.Param("resourceName"))
	if err != nil {
		s.Logger.Errorf("Get resource error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}
	s.Logger.Debugf("Get response: %+v", resource)
	c.JSON(http.StatusOK, resource)
}

func ListResources(c *gin.Context) {
	// Get kubernetes resource store
	s, err := getKubernetesResourceStore(c)
	if err != nil {
		s.Logger.Errorf("Get region config error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	// List resources
	resource, err := s.GetResource(c.Param("namespace"), c.Param("resourceType"), c.Param("resourceName"))
	if err != nil {
		s.Logger.Errorf("List resource error, %s", err.Error())
		HandleError(err, c, s.Logger)
		return
	}

	s.Logger.Debugf("List response: %+v", resource)
	c.JSON(http.StatusOK, resource)
}

func getMigrationStore(c *gin.Context) (*inner.Store, error) {
	m := &inner.Store{
		BaseStore: store.BaseStore{
			Logger: common.GetGinLogger(c),
		},
	}
	cluster, err := GetClusterWithUserToken(c)
	if err != nil {
		m.Logger.Errorf("Get cluster config error, %s", err.Error())
		return m, err
	}
	m.Cluster = cluster
	m.Logger.Infof("Cluster config: %s/%s %s", cluster.Name, cluster.UUID, cluster.PrometheusConfig.Endpoint)
	return m, nil
}

func getKubernetesResourceStore(c *gin.Context) (*inner.KubernetesResourceStore, error) {
	s := &inner.KubernetesResourceStore{
		BaseStore: store.BaseStore{
			Logger: common.GetGinLogger(c),
		},
		Request: &model.KubernetesResource{},
	}

	cluster, err := GetClusterWithUserToken(c)
	if err != nil {
		s.Logger.Errorf("Get cluster config error, %s", err.Error())
		return s, err
	}
	s.Cluster = cluster
	s.Logger.Infof("Cluster config: %s/%s %s", cluster.Name, cluster.UUID, cluster.PrometheusConfig.Endpoint)
	return s, nil
}
