package handler

import (
	"fmt"
	"strconv"
	"strings"

	"morgans/cache"
	"morgans/common"
	"morgans/config"
	"morgans/model"
	"morgans/pkg/utils"

	"github.com/gin-gonic/gin"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

func getBearerToken(c *gin.Context) string {
	authorization := c.Request.Header.Get("Authorization")
	if authorization == "" {
		return ""
	}
	token := strings.SplitN(authorization, "Bearer ", 2)
	if len(token) != 2 {
		return ""
	}
	return string(token[1])
}

func ParseCreator(creator string, c *gin.Context) string {
	if !config.GlobalConfig.Morgans.CRDEnabled {
		return creator
	}
	if userInfo, err := model.ParseUser(getBearerToken(c)); err == nil {
		return userInfo.GetName()
	}
	return creator
}

func TransformItemArrayToResponseBody(response interface{}, c *gin.Context) (interface{}, error) {
	if c.Query("paginated") != "" && !common.ParseFlag(c.Query("paginated")) {
		return response, nil
	}
	if !config.GlobalConfig.Morgans.CRDEnabled {
		return response, nil
	}

	pageSize, err := strconv.Atoi(c.Query("page_size"))
	if err != nil {
		pageSize = 20
	}
	pageNumber, err := strconv.Atoi(c.Query("page"))
	if err != nil {
		pageNumber = 1
	}
	orderBy := c.Query("order_by")
	if orderBy == "" {
		orderBy = "name"
	}

	pattern := c.Query("name")
	if pattern != "" {
		pattern = ".*" + pattern + ".*"
		if response, err = utils.Search(response, "name", pattern); err != nil {
			return nil, common.BuildBadRequestError(err.Error())
		}
	}
	result, err := utils.Pagination(response, c.Query("order_by"), pageSize, pageNumber)
	if err != nil {
		return nil, common.BuildBadRequestError(err.Error())
	}
	return result, nil
}

// All apis which exposed to user should use user token in request header
func GetClusterWithUserToken(c *gin.Context) (*model.Cluster, error) {
	if !config.GlobalConfig.Morgans.CRDEnabled {
		return cache.GetClusterWithToken(c.Param("cluster"))
	}

	cluster, err := cache.GetClusterWithoutToken(c.Param("cluster"))
	if err != nil {
		return nil, err
	}
	cluster.KubernetesConfig.Token = getBearerToken(c)
	return cluster, nil
}

// All apis which exposed to user should use user token in request header
func GetGlobalClusterWithUserToken(c *gin.Context) (*model.Cluster, error) {
	if !config.GlobalConfig.Morgans.CRDEnabled {
		return cache.GetGlobalClusterWithToken()
	}

	cluster, err := cache.GetGlobalClusterWithoutToken()
	if err != nil {
		return nil, err
	}
	cluster.KubernetesConfig.Token = getBearerToken(c)
	return cluster, nil
}

// Some apis which exposed to our components can use system token, but this is not recommend
// Such as POST /v1/alerts/:cluster/router
func GetGlobalClusterWithSystemToken() (*model.Cluster, error) {
	return cache.GetGlobalClusterWithToken()
}

// All metrics apis which exposed to user will check if prometheus has been deployed
func GetClusterWithPrometheusInstance(c *gin.Context) (*model.Cluster, error) {
	if !config.GlobalConfig.Morgans.CRDEnabled {
		return cache.GetClusterWithToken(c.Param("cluster"))
	}

	cluster, err := cache.GetClusterWithoutToken(c.Param("cluster"))
	if err != nil {
		return nil, err
	}
	cluster.KubernetesConfig.Token = getBearerToken(c)

	if cluster.PrometheusConfig == nil || cluster.PrometheusConfig.Endpoint == "" {
		return nil, common.BuildNotFoundError(fmt.Sprintf("cluster %s has no prometheus instance", cluster.Name))
	}
	return cluster, nil
}

func canI(c *gin.Context, verb, resource, group, namespace string) (bool, error) {
	token := getBearerToken(c)
	if token == "" {
		err := apiErrors.NewUnauthorized("Token required")
		return false, err
	}

	return common.CanI(token, verb, resource, group, namespace)
}
