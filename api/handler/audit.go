package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"

	"morgans/common"
	"morgans/config"
	"morgans/control/audit"
	"morgans/model"
)

// AggregateAudit handle aggres handler
func AggregateAudit(c *gin.Context) {
	dispatchAudit(c, "AggregateAudit")
}

// ListAudit handle get request from gateway
func ListAudit(c *gin.Context) {
	dispatchAudit(c, "ListAudit")
}

func dispatchAudit(c *gin.Context, methodName string) {
	logger := common.GetGinLogger(c)

	if !config.GlobalConfig.Audit.SkipAuth {
		valid, err := canI(c, "get", "audits", "aiops.alauda.io", config.GlobalConfig.Morgans.Namespace)
		if err != nil {
			logger.Errorf("Failed to validate permission with err: %s", err.Error())
			HandleError(err, c, logger)
			return
		}
		if !valid {
			logger.Infof("User has no permission to get audits")
			HandleError(apiErrors.NewUnauthorized("Invalid Token"), c, logger)
			return
		}
	}

	controller, err := getAuditQueryController(c, logger, methodName == "AggregateAudit")
	if err != nil {
		logger.Errorf("Failed to get audit controller, %s", err.Error())
		HandleError(err, c, logger)
		return
	}

	var response interface{}

	switch methodName {
	case "ListAudit":
		response, err = controller.ListAudit()
	case "AggregateAudit":
		response, err = controller.AggregateAudit()
	}

	if err != nil {
		logger.Errorf("Failed to get result from audit controller, %s", err.Error())
		HandleError(err, c, logger)
		return
	}

	c.JSON(http.StatusOK, response)
}

func getAuditQueryController(c *gin.Context, logger *common.Logger, isAggs bool) (*audit.Controller, error) {
	req, err := genAuditRequest(c, isAggs)
	if err != nil {
		return nil, err
	}

	s := &audit.Controller{
		Request: req,
		Logger:  logger,
	}

	return s, nil
}

// genAuditRequest construct an model.AuditRequest from gin.Context
func genAuditRequest(c *gin.Context, isAggs bool) (*model.AuditRequest, error) {
	req := &model.AuditRequest{
		Field:         c.Query("field"),
		Username:      c.Query("user_name"),
		OperationType: c.Query("operation_type"),
		ResourceName:  c.Query("resource_name"),
		ResourceType:  c.Query("resource_type"),
		Size:          model.DefaultAuditPageSize,
		Page:          1,
	}

	if req.Field == "user_name" {
		req.Field = "User.Username"
	} else if req.Field == "resource_name" {
		req.Field = "ObjectRef.Resource"
	} else if isAggs && req.Field == "" {
		return nil, apiErrors.NewBadRequest("field is required")
	}

	if c.Query("size") != "" {
		size, err := strconv.ParseInt(c.Query("size"), 10, 0)
		if err != nil {
			return nil, err
		}
		req.Size = int(size)
	}
	if c.Query("pageno") != "" {
		page, err := strconv.ParseInt(c.Query("pageno"), 10, 0)
		if err != nil {
			return nil, err
		}
		req.Page = int(page)
	}

	if c.Query("start_time") == "" {
		return nil, apiErrors.NewBadRequest("start_time is required")
	}
	startTime, err := strconv.ParseFloat(c.Query("start_time"), 64)
	if err != nil {
		return nil, err
	}
	req.StartTime = startTime

	if c.Query("end_time") == "" {
		return nil, apiErrors.NewBadRequest("end_time is required")
	}
	endTime, err := strconv.ParseFloat(c.Query("end_time"), 64)
	if err != nil {
		return nil, err
	}
	req.EndTime = endTime

	return req, nil
}
