FROM golang:1.13 as builder

WORKDIR $GOPATH/src/morgans
COPY . .
RUN go build -ldflags "-w -s" -tags 'mysql' -v -o $GOPATH/src/morgans/bin/migrate-mysql ./vendor/github.com/golang-migrate/migrate/cli
RUN go build -ldflags "-w -s" -tags 'postgres' -v -o $GOPATH/src/morgans/bin/migrate-pgsql ./vendor/github.com/golang-migrate/migrate/cli
RUN CGO_ENABLED=0 go build -a -installsuffix cgo -o $GOPATH/src/morgans/bin/morgans .

FROM alpine:3.11

WORKDIR /morgans
RUN apk add --update ca-certificates py-pip supervisor curl gcc musl-dev postgresql-dev python-dev

COPY --from=builder /go/src/morgans/bin/morgans ./morgans
COPY --from=builder /go/src/morgans/migration ./migration
COPY --from=builder /go/src/morgans/bin/migrate-mysql /usr/local/bin/migrate-mysql
COPY --from=builder /go/src/morgans/bin/migrate-pgsql /usr/local/bin/migrate-pgsql
COPY --from=builder /go/src/morgans/run/supervisord/supervisord.conf /etc/supervisord.conf
COPY --from=builder /go/src/morgans/run/templates/* /etc/morgans/
COPY --from=builder /go/src/morgans/daemon/template/* /etc/morgans/
COPY --from=builder /go/src/morgans/notification/template/* /etc/morgans/notification/template/

RUN pip install --no-cache-dir -r ./migration/api/requirements.txt
RUN mkdir /lib64 && \
    ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2 && \
    mkdir -p /var/log/mathilde && \
    mkdir -p /var/log/supervisor && \
    chmod +x /morgans/morgans && \
    chmod +x /usr/local/bin/migrate-mysql && \
    chmod +x /usr/local/bin/migrate-pgsql && \
    chmod +x /morgans/migration/db/migrate.sh

EXPOSE 8080
CMD ["/usr/bin/supervisord", "--nodaemon", "--configuration", "/etc/supervisord.conf"]
