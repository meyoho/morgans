package namespace

import (
	"reflect"
	"sync"
	"time"

	cc "morgans/cache/cluster"
	"morgans/common"
	"morgans/config"
	"morgans/infra/kubernetes"
	"morgans/model"

	mapset "github.com/deckarep/golang-set"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var fetcher *Fetcher

type Fetcher struct {
	lock       *sync.RWMutex
	namespaces map[string][]*v1.Namespace
	Logger     *common.Logger
}

func (f *Fetcher) Keys(cluster string) []string {
	f.lock.RLock()
	defer f.lock.RUnlock()
	var keys []string
	for _, ns := range f.namespaces[cluster] {
		keys = append(keys, ns.Name)
	}
	return keys
}

func (f *Fetcher) Get(cluster, namespace string) *v1.Namespace {
	f.lock.RLock()
	defer f.lock.RUnlock()
	for _, ns := range f.namespaces[cluster] {
		if ns.Name == namespace {
			return ns.DeepCopy()
		}
	}
	return nil
}

func (f *Fetcher) Set(cluster string, v v1.Namespace) {
	f.lock.Lock()
	defer f.lock.Unlock()
	f.namespaces[cluster] = append(f.namespaces[cluster], &v)
}

func (f *Fetcher) Update(cluster string, v v1.Namespace) {
	f.lock.Lock()
	defer f.lock.Unlock()

	var nsList []*v1.Namespace
	for _, ns := range f.namespaces[cluster] {
		if ns.Name != v.Name {
			nsList = append(nsList, ns)
		}
	}
	nsList = append(nsList, &v)
	f.namespaces[cluster] = nsList
}

func (f *Fetcher) Delete(cluster, namespace string) {
	f.lock.Lock()
	defer f.lock.Unlock()
	if ns, ok := f.namespaces[cluster]; ok {
		var nsList []*v1.Namespace
		for _, n := range ns {
			if n.Name != namespace {
				nsList = append(nsList, n)
			}
		}
		f.namespaces[cluster] = nsList
	}
}

func (f *Fetcher) Watch() {
	for {
		clusters := cc.ListClusters()
		if len(clusters) < 1 {
			f.Logger.Errorf("No clusters found when listing clusters, sleep 15s")
			time.Sleep(time.Second * 15)
			continue
		}

		for _, c := range clusters {
			// List namespaces
			namespaces, err := f.List(c)
			if err != nil {
				f.Logger.Errorf("List namespace error, sleep 15s, %v", err.Error())
				time.Sleep(time.Second * 15)
				continue
			}

			// Update namespaces set
			f.updateFetcherNamespaces(c.Name, namespaces)
		}
		time.Sleep(time.Second * time.Duration(config.GlobalConfig.Furion.SyncPeriod))
	}
}

func (f *Fetcher) List(cluster *model.Cluster) ([]v1.Namespace, error) {
	resource := &model.ResourceMeta{
		TypeMeta: metaV1.TypeMeta{
			APIVersion: model.AVKubernetesV1,
			Kind:       model.RKNamespace,
		},
	}
	client := kubernetes.NewClient(cluster, resource, f.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		f.Logger.Errorf("Get namespaces resource error, %s", err.Error())
		return nil, err
	}
	return model.RestResultToNamespaces(result)
}

func (f *Fetcher) updateFetcherNamespaces(cluster string, namespaces []v1.Namespace) {
	current := mapset.NewSet()
	previous := mapset.NewSet()
	for _, ns := range namespaces {
		current.Add(ns.Name)
	}
	for _, key := range f.Keys(cluster) {
		previous.Add(key)
	}
	insertSet := current.Difference(previous)
	updateSet := current.Intersect(previous)
	deleteSet := previous.Difference(current)
	for _, ns := range namespaces {
		if insertSet.Contains(ns.Name) {
			f.Logger.Infof("Insert namespace %s in cluster %s", ns.Name, cluster)
			f.Set(cluster, ns)
		}
		if updateSet.Contains(ns.Name) {
			old := f.Get(cluster, ns.Name)
			if old != nil && reflect.DeepEqual(old, &ns) {
				continue
			}
			f.Logger.Infof("Update namespace %s in cluster %s", ns.Name, cluster)
			f.Update(cluster, ns)
		}
	}
	for _, key := range f.Keys(cluster) {
		if deleteSet.Contains(key) {
			n := f.Get(cluster, key)
			f.Logger.Infof("Delete namespace %s in cluster %s", n.Name, cluster)
			f.Delete(cluster, key)
		}
	}
}
