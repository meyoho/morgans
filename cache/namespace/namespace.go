package namespace

import (
	"sync"

	"morgans/common"
	"morgans/model"

	v1 "k8s.io/api/core/v1"
)

func RunNamespaceCache() {
	logger := common.NewCacheLogger(map[string]string{"r": "cache/namespace"})
	fetcher = &Fetcher{
		lock:       new(sync.RWMutex),
		namespaces: make(map[string][]*v1.Namespace),
		Logger:     logger,
	}
	go fetcher.Watch()
}

func (f *Fetcher) FindDisplayName(cluster *model.Cluster, namespace string) string {
	fetcher.lock.RLock()
	defer fetcher.lock.RUnlock()
	ns, ok := fetcher.namespaces[cluster.Name]
	if !ok {
		f.Logger.Errorf("Namespaces list for cluster %s not exists", cluster.Name)
		return ""
	}
	for _, n := range ns {
		if n.Name == namespace {
			return n.GetAnnotations()[model.AKDisplayName]
		}
	}
	f.Logger.Errorf("Namespace %s in cluster %s not found", namespace, cluster.Name)
	return ""
}

func GetDisplayName(cluster *model.Cluster, namespace string) string {
	return fetcher.FindDisplayName(cluster, namespace)
}
