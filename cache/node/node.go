package node

import (
	"fmt"
	"sync"

	"morgans/common"
	"morgans/infra/kubernetes"
	"morgans/model"

	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var fetcher *Fetcher

type Fetcher struct {
	lock   *sync.RWMutex
	nodes  map[string][]v1.Node
	Logger *common.Logger
}

func (f *Fetcher) Fetch(cluster *model.Cluster) error {
	resource := &model.NodeResource{
		ResourceMeta: model.ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: common.KubernetesAPIVersionV1,
				Kind:       string(model.NodeKind),
			},
		},
	}
	client := kubernetes.NewClient(cluster, resource, f.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		f.Logger.Errorf("Get nodes from kubernetes error: %s", err.Error())
		return err
	}
	nodes, err := model.RestResultToNodes(result)
	if err != nil {
		f.Logger.Errorf("Rest result to node list error: %s", err.Error())
		return err
	}

	fetcher.lock.Lock()
	defer fetcher.lock.Unlock()
	fetcher.nodes[cluster.UUID] = nodes
	return nil
}

func (f *Fetcher) FindHostname(cluster *model.Cluster, ip string) string {
	fetcher.lock.RLock()
	defer fetcher.lock.RUnlock()
	ns, ok := fetcher.nodes[cluster.UUID]
	if !ok {
		f.Logger.Errorf("Nodes list for cluster %s not exists", cluster.UUID)
		return ""
	}
	for _, n := range ns {
		for _, a := range n.Status.Addresses {
			if a.Address == ip {
				return n.Name
			}
		}
	}
	f.Logger.Errorf("NodeKind %s in cluster %s not found", ip, cluster.UUID)
	return ""
}

func (f *Fetcher) FindHostIP(cluster *model.Cluster, hostname string) string {
	fetcher.lock.RLock()
	defer fetcher.lock.RUnlock()
	ns, ok := fetcher.nodes[cluster.UUID]
	if !ok {
		f.Logger.Errorf("Nodes list for cluster %s not exists", cluster.UUID)
		return ""
	}
	for _, n := range ns {
		if n.Name != hostname {
			continue
		}
		for _, a := range n.Status.Addresses {
			if a.Type == v1.NodeInternalIP {
				return a.Address
			}
		}
	}
	f.Logger.Errorf("Node %s in cluster %s not found", hostname, cluster.UUID)
	return ""
}

func GetHostName(cluster *model.Cluster, ip string) (string, error) {
	result := fetcher.FindHostname(cluster, ip)
	if result != "" {
		return result, nil
	}
	err := fetcher.Fetch(cluster)
	if err != nil {
		return "", err
	}
	result = fetcher.FindHostname(cluster, ip)
	if result != "" {
		return result, nil
	} else {
		return "", fmt.Errorf("node %s in cluster %s not found", ip, cluster.UUID)
	}
}

func GetHostIP(cluster *model.Cluster, hostname string) (string, error) {
	result := fetcher.FindHostIP(cluster, hostname)
	if result != "" {
		return result, nil
	}
	err := fetcher.Fetch(cluster)
	if err != nil {
		return "", err
	}
	result = fetcher.FindHostIP(cluster, hostname)
	if result != "" {
		return result, nil
	} else {
		return "", fmt.Errorf("node %s in cluster %s not found", hostname, cluster.UUID)
	}
}

func RunNodeCache() {
	logger := common.NewCacheLogger(map[string]string{"r": "cn"})
	fetcher = &Fetcher{
		lock:   new(sync.RWMutex),
		nodes:  make(map[string][]v1.Node),
		Logger: logger,
	}
}
