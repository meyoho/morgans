package project

import (
	"fmt"
	"sync"

	"morgans/common"
	"morgans/model"
)

func RunProjectCache() {
	logger := common.NewCacheLogger(map[string]string{"r": "cache/project"})
	fetcher = &Fetcher{
		lock:     new(sync.RWMutex),
		projects: make(map[string]*model.ProjectResource),
		Logger:   logger,
	}
	go fetcher.Watch()
}

func GetProject(index string) (*model.ProjectResource, error) {
	project := fetcher.Get(index)
	if project != nil {
		return project, nil
	}
	keys := fetcher.Keys()
	for _, key := range keys {
		project := fetcher.Get(key)
		if project != nil && (project.Name == index) {
			return project, nil
		}
	}
	return nil, common.BuildNotFoundError(fmt.Sprintf("project %s not found", index))
}

func ListProject() []*model.ProjectResource {
	var result = make([]*model.ProjectResource, 0)
	if fetcher == nil {
		return result
	}
	keys := fetcher.Keys()
	for _, key := range keys {
		region := fetcher.Get(key)
		if region != nil {
			result = append(result, region)
		}
	}
	return result
}

func GetDisplayName(index string) string {
	project, err := GetProject(index)
	if err != nil {
		return ""
	}
	return project.GetAnnotations()[model.AKDisplayName]
}
