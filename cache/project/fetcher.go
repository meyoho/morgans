package project

import (
	"reflect"
	"sync"
	"time"

	"morgans/cache/cluster"
	"morgans/common"
	"morgans/config"
	"morgans/infra/kubernetes"
	"morgans/model"

	mapset "github.com/deckarep/golang-set"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var fetcher *Fetcher

type Fetcher struct {
	lock     *sync.RWMutex
	projects map[string]*model.ProjectResource
	Logger   *common.Logger
}

func (f *Fetcher) Keys() []string {
	f.lock.RLock()
	defer f.lock.RUnlock()
	var keys []string
	for key := range f.projects {
		keys = append(keys, key)
	}
	return keys
}

func (f *Fetcher) Get(k string) *model.ProjectResource {
	f.lock.RLock()
	defer f.lock.RUnlock()
	if val, ok := f.projects[k]; ok {
		return val.Clone()
	}
	return nil
}

func (f *Fetcher) Set(k string, v *model.ProjectResource) {
	f.lock.Lock()
	defer f.lock.Unlock()
	f.projects[k] = v
}

func (f *Fetcher) Delete(k string) {
	f.lock.Lock()
	defer f.lock.Unlock()
	delete(f.projects, k)
}

func (f *Fetcher) Watch() {
	for {
		// List projects
		projects, err := f.List()
		if err != nil {
			f.Logger.Errorf("List project error, sleep 15s, %v", err.Error())
			time.Sleep(time.Second * 15)
			continue
		}

		// Update projects set
		f.updateFetcherProjects(projects)
		time.Sleep(time.Second * time.Duration(config.GlobalConfig.Furion.SyncPeriod))
	}
}

func (f *Fetcher) List() (map[string]*model.ProjectResource, error) {
	globalCluster, err := cluster.GetGlobalCluster()
	if err != nil {
		f.Logger.Errorf("Can not find global cluster, %s", err.Error())
		return nil, err
	}

	resource := &model.NodeResource{
		ResourceMeta: model.ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: model.AVProjectV1,
				Kind:       model.RKProject,
			},
		},
	}
	client := kubernetes.NewClient(globalCluster, resource, f.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		f.Logger.Errorf("Get project resource error, %s", err.Error())
		return nil, err
	}
	projects, err := model.RestResultToProjects(result)
	if err != nil {
		f.Logger.Errorf("Rest result to projects list error: %s", err.Error())
		return nil, err
	}

	fetcher.lock.Lock()
	defer fetcher.lock.Unlock()

	response := map[string]*model.ProjectResource{}
	for _, project := range projects {
		response[project.Name] = project
	}
	return response, nil
}

func (f *Fetcher) updateFetcherProjects(projects map[string]*model.ProjectResource) {
	current := mapset.NewSet()
	previous := mapset.NewSet()
	for _, project := range projects {
		current.Add(project.Name)
	}
	for _, key := range f.Keys() {
		previous.Add(key)
	}
	insertSet := current.Difference(previous)
	updateSet := current.Intersect(previous)
	deleteSet := previous.Difference(current)
	for _, project := range projects {
		if insertSet.Contains(project.Name) {
			f.Logger.Infof("Insert project %s", project.Name)
			f.Set(project.Name, project)
		}
		if updateSet.Contains(project.Name) {
			old := f.Get(project.Name)
			if old != nil && reflect.DeepEqual(old, project) {
				continue
			}
			f.Logger.Infof("Update project %s", project.Name)
			f.Set(project.Name, project)
		}
	}
	for _, key := range f.Keys() {
		if deleteSet.Contains(key) {
			p := f.Get(key)
			f.Logger.Infof("Delete project %s", p.Name)
			f.Delete(key)
		}
	}
}
