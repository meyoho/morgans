package cache

import (
	"morgans/cache/cluster"
	"morgans/cache/namespace"
	"morgans/cache/node"
	"morgans/cache/project"
	"morgans/cache/prometheus"
	"morgans/model"
)

func Run() {
	// Start cluster cache
	cluster.RunClusterCache()
	// Start node cache
	node.RunNodeCache()
	// Start prometheus cache
	prometheus.RunPrometheusCache()
	// Start project cache
	project.RunProjectCache()
	// Start namespace cache
	namespace.RunNamespaceCache()
}

// Get global cluster, this is also the cluster which morgans has been deployed in
func GetGlobalClusterWithoutToken() (*model.Cluster, error) {
	c, err := cluster.GetGlobalCluster()
	if err != nil {
		return nil, err
	}
	c.KubernetesConfig.Token = ""
	return c, nil
}

// Get cluster by unique name(uuid or name)
func GetClusterWithoutToken(name string) (*model.Cluster, error) {
	c, err := cluster.GetCluster(name)
	if err != nil {
		return nil, err
	}
	c.KubernetesConfig.Token = ""
	return c, nil
}

// Get global cluster with token, this is also the cluster which morgans has been deployed in
// This should always be called by morgans itself as a system token has been used
func GetGlobalClusterWithToken() (*model.Cluster, error) {
	return cluster.GetGlobalCluster()
}

// Get cluster by unique name(uuid or name)
// This should always be called by morgans itself as a system token has been used
func GetClusterWithToken(name string) (*model.Cluster, error) {
	return cluster.GetCluster(name)
}

// List cluster will list all clusters
func ListClusters() []*model.Cluster {
	return cluster.ListClusters()
}

// Get ip by host name
func GetHostIP(cluster *model.Cluster, name string) (string, error) {
	return node.GetHostIP(cluster, name)
}

// Get host name by ip
func GetHostName(cluster *model.Cluster, ip string) (string, error) {
	return node.GetHostName(cluster, ip)
}

// Merge selector labels for alerts
func MergePrometheusRuleSelectorLabels(cluster *model.Cluster, labels map[string]string) map[string]string {
	return prometheus.UpdateLabelsWithPrometheusRuleSelector(cluster, labels)
}

// Merge servicemonitor selector labels for resources
func MergeServiceMonitorSelectorLabels(cluster *model.Cluster, labels map[string]string) map[string]string {
	return prometheus.UpdateLabelsWithServiceMonitorSelector(cluster, labels)
}
