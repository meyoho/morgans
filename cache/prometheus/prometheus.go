package prometheus

import (
	"sync"

	"morgans/common"
	"morgans/infra/kubernetes"
	"morgans/model"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var prometheusCache *PrometheusCache

type PrometheusCache struct {
	lock         *sync.RWMutex
	prometheuses map[string]*model.KubernetesResource
	Logger       *common.Logger
}

func RunPrometheusCache() {
	logger := common.NewCacheLogger(map[string]string{"r": "cp"})
	prometheusCache = &PrometheusCache{
		lock:         new(sync.RWMutex),
		prometheuses: make(map[string]*model.KubernetesResource),
		Logger:       logger,
	}
}

func (pc *PrometheusCache) Fetch(cluster *model.Cluster) error {
	resource := &model.ResourceMeta{
		TypeMeta: metaV1.TypeMeta{
			APIVersion: model.APIVersionMonitoringV1,
			Kind:       model.KindPrometheus,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      cluster.PrometheusConfig.Name,
			Namespace: cluster.PrometheusConfig.Namespace,
		},
	}
	client := kubernetes.NewClient(cluster, resource, pc.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		pc.Logger.Errorf("Get prometheus resource from kubernetes error: %s", err.Error())
		return err
	}
	prometheusResource, err := model.RestResultToResource(result)
	if err != nil {
		pc.Logger.Errorf("Rest result to prometheus resource error: %s", err.Error())
		return err
	}
	pc.Logger.Infof("Fetch prometheus resource for cluster %+v: %+v", cluster.Name, prometheusResource)

	pc.lock.Lock()
	defer pc.lock.Unlock()
	pc.prometheuses[cluster.UUID] = prometheusResource
	return nil
}

func (pc *PrometheusCache) FindSelector(cluster *model.Cluster, selector string) (matchLabels, inLabels, notInLabels, existsLabels, notExistLabels map[string]string) {
	_, ok := prometheusCache.prometheuses[cluster.UUID]
	if !ok {
		if err := pc.Fetch(cluster); err != nil {
			pc.Logger.Errorf("Fatch proemtheus cache for cluster %+v error: %s", cluster.Name, err.Error())
			return pc.parseSelector(nil, selector)
		}
	}
	pc.Logger.Infof("FindSelector for prometheus %+v in cluster %+v: %+v", selector, cluster.Name, prometheusCache.prometheuses[cluster.UUID])
	return pc.parseSelector(prometheusCache.prometheuses[cluster.UUID], selector)
}

func (pc *PrometheusCache) parseSelector(prometheus *model.KubernetesResource, selector string) (matchLabels, inLabels, notInLabels, existsLabels, notExistLabels map[string]string) {
	matchLabels = map[string]string{}
	inLabels = map[string]string{}
	notInLabels = map[string]string{}
	existsLabels = map[string]string{}
	notExistLabels = map[string]string{}

	if prometheus != nil {
		prometheusSpec := (*prometheus)["spec"].(map[string]interface{})
		prometheusRuleSelector := prometheusSpec[selector].(map[string]interface{})
		pc.Logger.Infof("Parse %+v of prometheus resource %+v: %+v", selector, prometheus.GetName(), prometheusRuleSelector)
		if labels, ok := prometheusRuleSelector["matchLabels"]; ok {
			for key, value := range labels.(map[string]interface{}) {
				matchLabels[key] = value.(string)
			}
		}
		if labels, ok := prometheusRuleSelector["matchExpressions"]; ok {
			for _, label := range labels.([]interface{}) {
				label := label.(map[string]interface{})
				if label["operator"] == "In" {
					labelValues := label["values"].([]interface{})
					for _, value := range labelValues {
						inLabels[label["key"].(string)] = value.(string)
					}
				} else if label["operator"] == "NotIn" {
					labelValues := label["values"].([]interface{})
					for _, value := range labelValues {
						notInLabels[label["key"].(string)] = value.(string)
					}
				} else if label["operator"] == "Exists" {
					existsLabels[label["key"].(string)] = ""
				} else if label["operator"] == "DoesNotExist" {
					notExistLabels[label["key"].(string)] = ""
				}
			}
		}
	}

	pc.Logger.Infof("Get matchLabels: %+v; inLabels: %+v; notInLabels: %+v; existsLabels: %+v; notExistLabels: %+v",
		matchLabels, inLabels, notInLabels, existsLabels, notExistLabels)
	return matchLabels, inLabels, notInLabels, existsLabels, notExistLabels
}

func MergeSelectorLabels(c *model.Cluster, matchLabels, inLables, notInLabels, existsLabels, notExistLabels, labels map[string]string) map[string]string {
	labels = common.MergeMaps(matchLabels, labels)
	labels = common.MergeMaps(inLables, labels)
	for key, val := range notInLabels {
		if currentVal, ok := labels[key]; ok {
			if val == currentVal {
				labels[key] = c.PrometheusConfig.Name + "-" + common.RandStringRunes(5)
			}
		}
	}

	for key, _ := range existsLabels {
		if _, ok := labels[key]; !ok {
			labels[key] = c.PrometheusConfig.Name + "-" + common.RandStringRunes(5)
		}
	}

	for key, _ := range notExistLabels {
		if _, ok := labels[key]; ok {
			delete(labels, key)
		}
	}
	return labels
}

func UpdateLabelsWithServiceMonitorSelector(c *model.Cluster, labels map[string]string) map[string]string {
	matchLabels, inLables, notInLabels, existsLabels, notExistLabels := prometheusCache.FindSelector(c, "serviceMonitorSelector")
	return MergeSelectorLabels(c, matchLabels, inLables, notInLabels, existsLabels, notExistLabels, labels)
}

func UpdateLabelsWithPrometheusRuleSelector(c *model.Cluster, labels map[string]string) map[string]string {
	matchLabels, inLables, notInLabels, existsLabels, notExistLabels := prometheusCache.FindSelector(c, "ruleSelector")
	return MergeSelectorLabels(c, matchLabels, inLables, notInLabels, existsLabels, notExistLabels, labels)
}
