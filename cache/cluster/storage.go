package cluster

import (
	"encoding/base64"
	"fmt"
	"strings"

	"morgans/common"
	"morgans/config"
	"morgans/infra/davion"
	"morgans/infra/furion"
	"morgans/infra/kubernetes"
	"morgans/model"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Storage interface {
	Lister
	Parser
}

type Lister interface {
	List() (map[string]*model.Cluster, error)
}

type Parser interface {
	Parse(clusters map[string]*model.Cluster) error
}

// DBStorageManager can get cluster info from furion and parse cluster features by davion.
// This is the default mode, it's rely on furion and davion certainly.
type DBStorageManager struct {
	furion *furion.Client
	davion *davion.Client
	Logger *common.Logger
}

func (m *DBStorageManager) List() (map[string]*model.Cluster, error) {
	return m.furion.ListClusters()
}

func (m *DBStorageManager) Parse(clusters map[string]*model.Cluster) error {
	if err := m.davion.ParseClusters(clusters); err != nil {
		return err
	}

	if globalClusterName == "" {
		for _, cluster := range clusters {
			resource := &model.ResourceMeta{
				TypeMeta: metaV1.TypeMeta{
					APIVersion: "v1",
					Kind:       "pod",
				},
				ObjectMeta: metaV1.ObjectMeta{
					Name:      config.GlobalConfig.Morgans.PodName,
					Namespace: config.GlobalConfig.Morgans.Namespace,
				},
			}
			client := kubernetes.NewClient(cluster, resource, m.Logger)
			if _, err := client.Request(common.HttpGet, resource, nil); err == nil {
				globalClusterName = cluster.Name
				break
			}
		}
	}
	for _, cluster := range clusters {
		cluster.IsGlobal = cluster.Name == globalClusterName
		cluster.HasMetricFeature = cluster.PrometheusConfig != nil && cluster.PrometheusConfig.Endpoint != ""
	}
	return nil
}

// ETCDStorageManager can get cluster crd resource from global and parse cluster features from private cluster.
// This is the custom mode, set MORGANS_CRD_ENABLED env as true if you want enable it.
type ETCDStorageManager struct {
	Global *model.Cluster
	Logger *common.Logger
}

func (m *ETCDStorageManager) List() (map[string]*model.Cluster, error) {
	clusters := make(map[string]*model.Cluster, 0)

	// Fetch cluster crd resources and token from global
	clusterResources, err := m.fetchClusterResources()
	if err != nil {
		m.Logger.Errorf("Fetch cluster resources from global error, %s", err.Error())
		return clusters, err
	}
	for _, resource := range clusterResources {
		token, err := m.getClusterToken(resource)
		if err != nil {
			m.Logger.Errorf("Get cluster token from global error, %s", err.Error())
			continue
		}
		c := &model.Cluster{
			Name:        resource.Name,
			DisplayName: resource.GetAnnotations()[model.AKDisplayName],
			IsOCP:       strings.ToLower(resource.GetLabels()[model.LKClusterType]) == "ocp",
			UUID:        string(resource.UID),
			KubernetesConfig: &model.KubernetesConfig{
				Endpoint: config.GlobalConfig.Erebus.Endpoint + "/kubernetes/" + resource.Name,
				Token:    string(token),
				Timeout:  config.GlobalConfig.Kubernetes.Timeout,
			},
			PrometheusConfig: &model.PrometheusConfig{},
		}
		if version, ok := resource.Status["version"]; ok {
			c.Version = version.(string)
		}
		clusters[resource.Name] = c
	}
	return clusters, nil
}

func (m *ETCDStorageManager) Parse(clusters map[string]*model.Cluster) error {
	addPrometheusConfig := func(cluster *model.Cluster) error {
		featureResource, err := m.fetchPrometheusFeatureResource(cluster)
		if err != nil {
			return common.AnnotateError(err, "fetch prometheus feature error")
		}
		endpoint, err := featureResource.GetStringByPath("spec.accessInfo.prometheusUrl")
		if err != nil {
			return common.AnnotateError(err, "get prometheus endpoint error")
		}
		name, err := featureResource.GetStringByPath("spec.accessInfo.name")
		if err != nil {
			return common.AnnotateError(err, "fetch prometheus name error")
		}
		namespace, err := featureResource.GetStringByPath("spec.accessInfo.namespace")
		if err != nil {
			return common.AnnotateError(err, "fetch prometheus namespace error")
		}
		username, _ := featureResource.GetStringByPath("spec.accessInfo.prometheusUser")
		password, _ := featureResource.GetStringByPath("spec.accessInfo.prometheusPassword")
		cluster.PrometheusConfig = &model.PrometheusConfig{
			Endpoint:  endpoint,
			Namespace: namespace,
			Name:      name,
			Username:  username,
			Password:  password,
		}
		return nil
	}

	// Add prometheus config for global and private clusters
	if err := addPrometheusConfig(m.Global); err != nil {
		m.Logger.Errorf("add prometheus config error, %s, continue", err.Error())
	}
	for _, cluster := range clusters {
		if err := addPrometheusConfig(cluster); err != nil {
			m.Logger.Errorf("add prometheus config error, %s, continue", err.Error())
			continue
		}
	}
	m.setClusterFlags(clusters)
	return nil
}

func (m *ETCDStorageManager) getClusterToken(cluster *model.ResourceItem) (string, error) {
	kind, err := cluster.GetStringByPath("spec.authInfo.controller.kind")
	if err != nil {
		return "", common.AnnotateError(err, "get controller kind error")
	}
	name, err := cluster.GetStringByPath("spec.authInfo.controller.name")
	if err != nil {
		return "", common.AnnotateError(err, "get controller name error")
	}
	namespace, err := cluster.GetStringByPath("spec.authInfo.controller.namespace")
	if err != nil {
		return "", common.AnnotateError(err, "get controller kind error")
	}

	if strings.ToLower(kind) != "secret" {
		return "", common.AnnotateError(err, kind+"controller has not been supported")
	}
	resource := &model.ResourceMeta{
		TypeMeta: metaV1.TypeMeta{
			APIVersion: model.AVKubernetesV1,
			Kind:       kind,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
	}
	client := kubernetes.NewClient(m.Global, resource, m.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		return "", common.AnnotateError(err, "get controller error")
	}
	controllerResource, err := model.RestToResourceItem(result)
	if err != nil {
		return "", common.AnnotateError(err, "get controller resource error")
	}
	encodedToken, err := controllerResource.GetStringByPath("data.token")
	if err != nil {
		return "", common.AnnotateError(err, "get encoded token error")
	}
	token, err := base64.StdEncoding.DecodeString(encodedToken)
	if err != nil {
		return "", common.AnnotateError(err, "decode token error")
	}
	return string(token), nil
}

func (m *ETCDStorageManager) fetchClusterResources() ([]*model.ResourceItem, error) {
	resource := &model.ResourceMeta{
		TypeMeta: metaV1.TypeMeta{
			APIVersion: model.AVClusterV1alpha1,
			Kind:       model.RKClusterRegistry,
		},
	}
	client := kubernetes.NewClient(m.Global, resource, m.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		m.Logger.Errorf("Get cluster resource error, %s", err.Error())
		return nil, err
	}
	return model.RestToResourceList(result)
}

func (m *ETCDStorageManager) fetchPrometheusFeatureResource(cluster *model.Cluster) (*model.ResourceItem, error) {
	resource := &model.ResourceMeta{
		TypeMeta: metaV1.TypeMeta{
			APIVersion: model.AVInfrastructureV1alpha1,
			Kind:       model.RKClusterFeature,
		},
	}
	client := kubernetes.NewClient(cluster, resource, m.Logger)
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		return nil, common.AnnotateError(err, "get feature resources error")
	}
	resources, err := model.RestToResourceList(result)
	if err != nil {
		return nil, common.AnnotateError(err, "rest to resource list error")
	}
	for _, resource := range resources {
		featureType, err := resource.GetStringByPath("spec.type")
		if err != nil {
			continue
		}
		instanceType, err := resource.GetStringByPath("spec.instanceType")
		if err != nil {
			continue
		}
		if featureType == "monitoring" && instanceType == "prometheus" {
			return resource, nil
		}
	}
	return nil, fmt.Errorf("can not find metric feature for cluster %s/%s", cluster.UUID, cluster.Name)
}

func (m *ETCDStorageManager) setClusterFlags(clusters map[string]*model.Cluster) {
	isGlobal := func(cluster *model.Cluster) bool {
		if cluster.Name == globalClusterName {
			return true
		}
		if cluster.PrometheusConfig.Endpoint != m.Global.PrometheusConfig.Endpoint {
			if cluster.KubernetesConfig.Token != m.Global.KubernetesConfig.Token {
				return false
			}
		}
		resource := &model.ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: "v1",
				Kind:       "pod",
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name:      config.GlobalConfig.Morgans.PodName,
				Namespace: config.GlobalConfig.Morgans.Namespace,
			},
		}
		client := kubernetes.NewClient(cluster, resource, m.Logger)
		_, err := client.Request(common.HttpGet, resource, nil)
		return err == nil
	}

	var global *model.Cluster
	for _, cluster := range clusters {
		if isGlobal(cluster) {
			global = cluster
			break
		}
	}
	if global != nil {
		globalClusterName = global.Name
	} else {
		clusters[m.Global.Name] = m.Global.Clone()
		globalClusterName = m.Global.Name
	}

	for _, cluster := range clusters {
		cluster.IsGlobal = cluster.Name == globalClusterName
		cluster.HasMetricFeature = cluster.PrometheusConfig.Endpoint != ""
		if cluster.IsGlobal {
			cluster.KubernetesConfig.Endpoint = m.Global.KubernetesConfig.Endpoint
		}
	}
}
