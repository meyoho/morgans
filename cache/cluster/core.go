package cluster

import (
	"fmt"
	"sync"

	"morgans/common"
	"morgans/config"
	"morgans/infra/davion"
	"morgans/infra/furion"
	"morgans/model"
)

func RunClusterCache() {
	logger := common.NewCacheLogger(map[string]string{"r": "cc"})
	var storage Storage
	if !config.GlobalConfig.Morgans.CRDEnabled {
		storage = &DBStorageManager{
			davion: davion.NewClient(logger),
			furion: furion.NewClient(logger),
			Logger: logger,
		}
	} else {
		storage = &ETCDStorageManager{
			Global: &model.Cluster{
				UUID:     VGClusterUUID,
				Name:     VGClusterName,
				IsGlobal: true,
				KubernetesConfig: &model.KubernetesConfig{
					Endpoint: config.GlobalConfig.Kubernetes.Endpoint,
					Token:    config.GlobalConfig.Kubernetes.Token,
					Timeout:  config.GlobalConfig.Kubernetes.Timeout,
				},
				PrometheusConfig: &model.PrometheusConfig{},
			},
			Logger: logger,
		}
	}
	fetcher = &Fetcher{
		lock:     new(sync.RWMutex),
		storage:  storage,
		clusters: make(map[string]*model.Cluster),
		Logger:   logger,
	}
	go fetcher.Watch()
}

func GetGlobalCluster() (*model.Cluster, error) {
	if fetcher == nil {
		return nil, fmt.Errorf("can not find global cluster")
	}
	keys := fetcher.Keys()
	for _, key := range keys {
		cluster := fetcher.Get(key)
		if cluster.IsGlobal {
			return cluster, nil
		}
	}
	return nil, common.BuildNotFoundError("can not find global cluster")
}

func GetCluster(index string) (*model.Cluster, error) {
	cluster := fetcher.Get(index)
	if cluster != nil {
		return cluster, nil
	}
	keys := fetcher.Keys()
	for _, key := range keys {
		cluster := fetcher.Get(key)
		if cluster != nil && (cluster.Name == index || cluster.UUID == index) {
			return cluster, nil
		}
	}
	return nil, common.BuildNotFoundError(fmt.Sprintf("cluster %s not found", index))
}

func ListClusters() []*model.Cluster {
	var result = make([]*model.Cluster, 0)
	if fetcher == nil {
		return result
	}
	keys := fetcher.Keys()
	for _, key := range keys {
		region := fetcher.Get(key)
		if region != nil {
			result = append(result, region)
		}
	}
	return result
}
