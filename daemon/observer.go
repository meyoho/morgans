package daemon

import (
	"time"

	"morgans/model"
)

type Observer struct {
	Cluster   *model.Cluster
	Retry     int
	Done      bool
	Next      time.Time
	UpdatedAt time.Time
}

func NewObserver(c *model.Cluster) *Observer {
	return &Observer{
		Cluster:   c,
		Retry:     0,
		Done:      false,
		Next:      time.Now().UTC(),
		UpdatedAt: time.Now().UTC(),
	}
}

func (o *Observer) SetDone(done bool) {
	o.Done = done
	o.UpdatedAt = time.Now().UTC()
	if !done {
		o.Retry += 1
		o.Next = time.Now().UTC().Add(time.Minute * time.Duration(o.Retry))
	}
}
