package daemon

import (
	"time"

	"morgans/cache"
	"morgans/config"
	"morgans/daemon/task"
	"morgans/model"
)

var registeredTasks []task.Task
var observers = make(map[string]*Observer, 0)

func init() {
	registeredTasks = []task.Task{
		&task.LogAlertAdapter{},
		&task.OperatorFineTuning{},
	}
}

func Run() {
	for {
		time.Sleep(time.Second * time.Duration(config.GlobalConfig.Daemon.SyncPeriod))

		// List clusters from cache and update observers status
		all := cache.ListClusters()
		updateObservers(all)

		for _, item := range all {
			logger := getDaemonLogger(item)
			if !item.HasMetricFeature {
				logger.Errorf("Cluster %s/%s has no metric feature. Skip", item.Name, item.UUID)
				continue
			}

			// If a daemon task failed, a next timestamp will be set to avoid frequent retry
			// If the current timestamp is before next, give up this chance and continue
			o := observers[item.UUID]
			if o.Done || time.Now().Before(o.Next) {
				continue
			}

			// Get task state from cluster, observer status will be set to false if an error occurred
			logger.Infof("Try to get tasks's state from cluster, observer status, %+v", o)
			oldTasks, err := GetTaskStateFromCluster(item)
			if err != nil {
				logger.Errorf("Get tasks's state error, %s", err.Error())
				o.SetDone(false)
				continue
			}

			// Check all registered tasks and continue if a task has been updated in 5 minutes
			logger.Infof("Try to check updated timestamp for registered tasks")
			ignore := false
			for _, rt := range registeredTasks {
				newTask := rt.Get(item)
				if oldTask, ok := oldTasks[newTask.Name]; ok {
					// Task will be ignored if it has been updated in 5 minutes
					// To avoid frequent refresh if morgans restarted frequently
					if newTask.UpdatedAt.Sub(oldTask.UpdatedAt) < time.Minute*5 {
						ignore = true
						break
					}
				}
			}
			if ignore {
				logger.Infof("Give up this chance due to a task has been updated in 5 minutes")
				continue
			}

			// Check all registered tasks and execute tasks if needed
			logger.Infof("Try to check status and version for registered tasks")
			newTasks := map[string]*model.DaemonTask{}
			for _, rt := range registeredTasks {
				newTask := rt.Get(item)
				logger.Infof("New task %+v status, %+v", newTask.Name, newTask)
				// Task will be ignored if all conditions met:
				// 1. The cluster observer retry is not zero
				// 2. The cluster task status is Done and task version is up to date
				if oldTask, ok := oldTasks[newTask.Name]; ok && o.Retry != 0 {
					logger.Infof("Old task %+v status, %+v", oldTask.Name, oldTask)
					if oldTask.HasDone() && newTask.Version <= oldTask.Version {
						logger.Infof("Task %s has done", oldTask.Name)
						continue
					}
				}
				logger.Errorf("Try to execute task %s", newTask.Name)
				rt.Exec(item, newTask)
				newTasks[newTask.Name] = newTask
			}

			// Update task state to clusters if needed
			logger.Infof("Try to update tasks's state")
			if err := UpdateTaskStateToCluster(item, newTasks); err != nil {
				logger.Errorf("Update tasks's state error %s, continue", err.Error())
				o.SetDone(false)
			} else {
				done := true
				for _, t := range newTasks {
					if !t.HasDone() {
						done = false
						break
					}
				}
				o.SetDone(done)
				logger.Infof("Update tasks's state successfully")
			}
			logger.Infof("Observer status after a loop, %+v", o)
		}
	}
}

func updateObservers(clusters []*model.Cluster) {
	// If a cluster changes, observer status should be set to false
	for _, item := range clusters {
		if _, ok := observers[item.UUID]; !ok {
			observers[item.UUID] = NewObserver(item)
		} else {
			if !item.Equal(observers[item.UUID].Cluster) {
				observers[item.UUID].Cluster = item
				observers[item.UUID].Done = false
				observers[item.UUID].UpdatedAt = time.Now().UTC()
			}
		}
	}

	// If the global cluster changes, all observers status should be set to false
	globalCluster := func([]*model.Cluster) *model.Cluster {
		for _, item := range clusters {
			if item.IsGlobal {
				return item
			}
		}
		return nil
	}
	if global := globalCluster(clusters); global != nil {
		if !observers[global.UUID].Done {
			for _, o := range observers {
				o.Done = false
				o.UpdatedAt = time.Now().UTC()
			}
		}
	}
}
