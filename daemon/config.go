package daemon

import (
	"encoding/json"
	"strings"

	"morgans/common"
	"morgans/infra/kubernetes"
	"morgans/model"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

const (
	TaskNamePrefix = "daemon_task_"
)

func getDaemonLogger(c *model.Cluster) *common.Logger {
	return common.NewDaemonLogger(map[string]string{"r": "core", "c": c.Name})
}

func GetTaskStateFromCluster(c *model.Cluster) (map[string]*model.DaemonTask, error) {
	logger := getDaemonLogger(c)
	client := kubernetes.NewClient(c, kubernetes.GenerateCoreConfigmapResource(c), logger)
	result := make(map[string]*model.DaemonTask, 0)
	resource, err := client.GetCoreConfigMap()
	if err != nil {
		if !apiErrors.IsNotFound(err) {
			return result, err
		}
		return result, nil
	}
	for key, value := range resource.Data {
		if !strings.HasPrefix(key, TaskNamePrefix) {
			continue
		}
		t := &model.DaemonTask{}
		if err := json.Unmarshal([]byte(value), t); err != nil {
			return result, err
		}
		result[t.Name] = t
	}
	return result, nil
}

func UpdateTaskStateToCluster(c *model.Cluster, ts map[string]*model.DaemonTask) error {
	translate := func(r *model.ConfigMapResource) *model.ConfigMapResource {
		if r.Data == nil {
			r.Data = make(map[string]string, 0)
		}
		for _, t := range ts {
			data, err := json.Marshal(*t)
			if err != nil {
				panic(err.Error())
			}
			r.Data[TaskNamePrefix+t.Name] = string(data)
		}
		return r
	}

	logger := getDaemonLogger(c)
	client := kubernetes.NewClient(c, kubernetes.GenerateCoreConfigmapResource(c), logger)
	resource, err := client.GetCoreConfigMap()
	if err != nil {
		if !apiErrors.IsNotFound(err) {
			return err
		}
		resource = kubernetes.GenerateCoreConfigmapResource(c)
		return client.CreateCoreConfigmap(translate(resource))
	}
	return client.UpdateCoreConfigmap(translate(resource))
}
