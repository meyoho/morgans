package task

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"time"

	"morgans/cache"
	"morgans/common"
	"morgans/config"
	"morgans/infra/kubernetes"
	"morgans/model"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

const (
	configDirectory              = "/etc/morgans/"
	generalServiceMonitorFile    = "service_monitor.json"
	federationServiceMonitorFile = "federation_service_monitor.json"
	federationServiceFile        = "federation_service.json"
	federationEndpointFile       = "federation_endpoint.json"
	federationSecretFile         = "federation_secret.json"
	federationSecretName         = "basic-auth-for-federation-with-global-prometheus"

	// Fake address
	fakeIP   = "111.111.111.111"
	fakePort = 9999
)

var (
	generalServiceMonitorResource    string
	federationServiceMonitorResource string
	federationServiceResource        string
	federationEndpointResource       string
	federationSecretResource         string
)

type LogAlertAdapter struct{}

func init() {
	generalServiceMonitorResource = common.ReadFileToString(configDirectory + generalServiceMonitorFile)
	federationServiceMonitorResource = common.ReadFileToString(configDirectory + federationServiceMonitorFile)
	federationServiceResource = common.ReadFileToString(configDirectory + federationServiceFile)
	federationEndpointResource = common.ReadFileToString(configDirectory + federationEndpointFile)
	federationSecretResource = common.ReadFileToString(configDirectory + federationSecretFile)
}

func getLogAlertAdapterLogger(c *model.Cluster) *common.Logger {
	return common.NewDaemonLogger(map[string]string{"r": "la_adapter", "c": c.Name})
}

func (l *LogAlertAdapter) Get(cluster *model.Cluster) *model.DaemonTask {
	return &model.DaemonTask{
		Name:      "log_alert_adapter",
		Status:    "pending",
		Message:   "",
		CreatedAt: time.Now().UTC(),
		UpdatedAt: time.Now().UTC(),
		Version:   1,
	}
}

func (l *LogAlertAdapter) Exec(cluster *model.Cluster, task *model.DaemonTask) {
	logger := getLogAlertAdapterLogger(cluster)
	var err error
	if cluster.IsGlobal {
		logger.Infof("Try to create adapter resource for global cluster")
		err = l.createAdapterResourceForGlobal(cluster)
	} else {
		logger.Infof("Try to create adapter resource for private cluster")
		err = l.createAdapterResourceForPrivate(cluster)
	}
	if err != nil {
		task.Status = model.FailedTaskState
		task.Message = err.Error()
		logger.Errorf("Create adapter resource error, %s", err.Error())
	} else {
		task.Status = model.SucceededTaskState
		task.Message = "Everything is ok"
	}
	logger.Infof("Change log alert adapter task state to %s with version %d", task.Status, task.Version)
}

func (l *LogAlertAdapter) createAdapterResourceForGlobal(cluster *model.Cluster) error {
	if !cluster.IsGlobal {
		return nil
	}
	generalServiceMonitorResource := fmt.Sprintf(
		generalServiceMonitorResource,
		model.APIVersionMonitoringV1,
		model.KindServiceMonitor,
		cluster.PrometheusConfig.Namespace,
		config.GlobalConfig.Morgans.Namespace,
	)
	return l.createKubernetesResource(cluster, generalServiceMonitorResource)
}

func (l *LogAlertAdapter) createAdapterResourceForPrivate(cluster *model.Cluster) error {
	if cluster.IsGlobal {
		return nil
	}
	globalCluster, err := cache.GetGlobalClusterWithToken()
	if err != nil {
		return common.AnnotateError(err, "get global cluster error")
	}

	// Create secret with basic auth to federate with global prometheus
	hasBasicAuth := globalCluster.PrometheusConfig.Username != "" || globalCluster.PrometheusConfig.Password != ""
	if hasBasicAuth {
		secretResource := fmt.Sprintf(
			federationSecretResource,
			base64.StdEncoding.EncodeToString([]byte(globalCluster.PrometheusConfig.Password)),
			base64.StdEncoding.EncodeToString([]byte(globalCluster.PrometheusConfig.Username)),
			federationSecretName,
			cluster.PrometheusConfig.Namespace,
		)
		if err := l.createKubernetesResource(cluster, secretResource); err != nil {
			return err
		}
	}

	scheme, host := common.GetSchemeHostFromUrl(globalCluster.PrometheusConfig.Endpoint)
	// Create endpoint for federation service
	endpointResource := fmt.Sprintf(
		federationEndpointResource,
		common.KubernetesAPIVersionV1,
		model.KindEndpoints,
		cluster.PrometheusConfig.Namespace,
		fakeIP,
		scheme+"-metrics",
		fakePort,
	)
	if err := l.createKubernetesResource(cluster, endpointResource); err != nil {
		return err
	}

	// Create service for service monitor
	serviceResource := fmt.Sprintf(
		federationServiceResource,
		common.KubernetesAPIVersionV1,
		model.KindService,
		cluster.PrometheusConfig.Namespace,
		scheme+"-metrics",
		fakePort,
	)
	if err := l.createKubernetesResource(cluster, serviceResource); err != nil {
		return err
	}

	// Create service monitor for prometheus operator
	serviceMonitorResource := fmt.Sprintf(
		federationServiceMonitorResource,
		model.APIVersionMonitoringV1,
		model.KindServiceMonitor,
		cluster.PrometheusConfig.Namespace,
		federationSecretName,
		federationSecretName,
		cluster.Name,
		cluster.Name,
		scheme+"-metrics",
		scheme,
		host,
		cluster.PrometheusConfig.Namespace,
	)
	var resource = &model.KubernetesResource{}
	if err := json.Unmarshal([]byte(serviceMonitorResource), resource); err != nil {
		return common.AnnotateError(err, "resource string unmarshal error")
	}
	endpoints, ok := (*resource)["spec"].(map[string]interface{})["endpoints"].([]interface{})
	if !ok {
		return fmt.Errorf("can not find enpoints in service monitor resource")
	}
	if !hasBasicAuth {
		delete(endpoints[0].(map[string]interface{}), "basicAuth")
		delete(endpoints[0].(map[string]interface{}), "tlsConfig")
	}
	serviceMonitorResourceBytes, err := json.Marshal(*resource)
	if err != nil {
		return common.AnnotateError(err, "service monitor resource string unmarshal error")
	}
	if err := l.createKubernetesResource(cluster, string(serviceMonitorResourceBytes)); err != nil {
		return err
	}
	return nil
}

func (l *LogAlertAdapter) createKubernetesResource(cluster *model.Cluster, resourceString string) error {
	var resource = &model.KubernetesResource{}
	if err := json.Unmarshal([]byte(resourceString), resource); err != nil {
		return common.AnnotateError(err, "resource string unmarshal error")
	}

	// Update resource labels
	metadata, ok := (*resource)["metadata"].(map[string]interface{})
	if !ok {
		return fmt.Errorf("can not find metadata in resource")
	}
	_, ok = metadata["labels"].(map[string]interface{})
	if !ok {
		metadata["labels"] = map[string]interface{}{}
	}
	for k, v := range cache.MergeServiceMonitorSelectorLabels(cluster, nil) {
		metadata["labels"].(map[string]interface{})[k] = v
	}

	prompt := fmt.Sprintf("%v %v in %v ", resource.GetType(), resource.GetName(), resource.GetNamespace())
	client := kubernetes.NewClient(cluster, resource, getLogAlertAdapterLogger(cluster))
	result, err := client.Request(common.HttpGet, resource, nil)
	if err != nil {
		if !apiErrors.IsNotFound(err) {
			return common.AnnotateError(err, prompt+"get error")
		}
		_, err = client.Request(common.HttpPost, resource, nil)
		return err
	}
	rs, err := model.RestResultToResource(result)
	if err != nil {
		return common.AnnotateError(err, "rest to resource error")
	}

	// Update the ResourceVersion that the resource has
	metadata["resourceVersion"] = rs.GetResourceVersion()

	// Update kubernetes resource
	_, err = client.Request(common.HttpPut, resource, nil)
	return err
}
