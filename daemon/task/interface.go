package task

import (
	"morgans/model"
)

type Task interface {
	Get(cluster *model.Cluster) *model.DaemonTask
	Exec(cluster *model.Cluster, task *model.DaemonTask)
}
