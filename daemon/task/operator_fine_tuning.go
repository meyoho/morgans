package task

import (
	"fmt"
	"time"

	"morgans/common"
	"morgans/infra/kubernetes"
	"morgans/model"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type OperatorFineTuning struct{}

func getOperatorFineTuningLogger(c *model.Cluster) *common.Logger {
	return common.NewDaemonLogger(map[string]string{"r": "pp_adapter", "c": c.Name})
}

func (l *OperatorFineTuning) Get(cluster *model.Cluster) *model.DaemonTask {
	return &model.DaemonTask{
		Name:      "operator_fine_tuning",
		Status:    "pending",
		Message:   "",
		CreatedAt: time.Now().UTC(),
		UpdatedAt: time.Now().UTC(),
	}
}

func (l *OperatorFineTuning) Exec(cluster *model.Cluster, task *model.DaemonTask) {
	logger := getOperatorFineTuningLogger(cluster)
	if !cluster.HasMetricFeature {
		logger.Errorf("Cluster has no metric feature")
		return
	}

	// Get prometheus resource if any
	meta := &model.ResourceMeta{
		TypeMeta: metaV1.TypeMeta{
			APIVersion: model.APIVersionMonitoringV1,
			Kind:       model.KindPrometheus,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      cluster.PrometheusConfig.Name,
			Namespace: cluster.PrometheusConfig.Namespace,
		},
	}
	prompt := fmt.Sprintf("%v %v in %v ", meta.GetType(), meta.GetName(), meta.GetNamespace())
	client := kubernetes.NewClient(cluster, meta, logger)
	result, err := client.Request(common.HttpGet, meta, nil)
	if err != nil {
		logger.Errorf("Get %s error, %s", prompt, err.Error())
		task.Status = model.FailedTaskState
		return
	}
	resource, err := model.RestResultToResource(result)
	if err != nil {
		logger.Errorf("Rest result to prometheus resource error, %s", err.Error())
		task.Status = model.FailedTaskState
		return
	}

	// Update prometheus resource
	spec, ok := (*resource)["spec"].(map[string]interface{})
	if !ok {
		task.Status = model.FailedTaskState
		logger.Errorf("Can not find spec in resource")
		return
	}
	spec["ruleNamespaceSelector"] = map[string]interface{}{"any": true}
	spec["serviceMonitorNamespaceSelector"] = map[string]interface{}{"any": true}
	if _, err := client.Request(common.HttpPut, resource, nil); err != nil {
		task.Status = model.FailedTaskState
		logger.Errorf("Update %s error, %s", prompt, err.Error())
		return
	}
	task.Status = model.SucceededTaskState
	logger.Infof("Change operator fine tuning task state to %s with version %d", task.Status, task.Version)
}
